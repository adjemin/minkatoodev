-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 04, 2020 at 08:20 PM
-- Server version: 10.3.23-MariaDB
-- PHP Version: 7.3.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qznotdwx_minkatoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dial_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'client',
  `otp` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp_verified` tinyint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `name`, `phone_number`, `phone`, `dial_code`, `country_code`, `email`, `photo_url`, `gender`, `bio`, `birthday`, `language`, `customer_type`, `otp`, `otp_verified`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mohamed', 'Soro', 'Mohamed Soro', '87015892', '22587015892', '225', 'CI', 'mohamedsoro@adjemin.com', NULL, 'HOMME', NULL, NULL, NULL, 'client', '123456', 1, '2020-02-13 18:31:27', '2020-02-17 15:39:25', NULL),
(2, 'Mohamed', 'Soro', 'Mohamed Soro', '77925078', '22577925078', '225', 'CI', 'mohamedsoro@adjemin.com', NULL, 'HOMME', NULL, NULL, NULL, 'client', '067221', 1, '2020-02-17 15:00:53', '2020-02-17 15:38:32', NULL),
(7, 'Abd', 'Simpson', 'Abd Simpson', '01020304', '22501020304', '225', 'CI', NULL, NULL, 'HOMME', NULL, NULL, NULL, 'client', '857019', 0, '2020-02-18 18:04:39', '2020-02-18 18:04:39', NULL),
(6, 'Frank', 'Brou', 'Frank Brou', '44908473', '22544908473', '225', 'CI', NULL, NULL, 'HOMME', NULL, NULL, NULL, 'client', NULL, 0, '2020-02-18 17:41:27', '2020-02-18 17:41:27', NULL),
(12, 'Ange', 'Bagui', 'Ange Bagui', '56888385', '22556888385', '225', 'CI', 'angebagui@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-07 00:20:24', '2020-03-07 00:20:24', NULL),
(13, 'Dikado', 'Ban', 'Dikado Ban', '08175784', '22508175784', '225', 'CI', 'dikadoban@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-07 21:31:48', '2020-03-07 21:31:48', NULL),
(14, 'Nicole', 'Yao', 'Nicole Yao', '77385080', '22577385080', '225', 'CI', 'nicole.adjoua@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-07 22:45:51', '2020-03-07 22:45:51', NULL),
(15, 'Casimir', 'Daple', 'Casimir Daple', '47694913', '22547694913', '225', 'CI', 'daplecasimir1970@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-08 17:15:15', '2020-03-08 17:15:15', NULL),
(16, 'Gba', 'Sinde franck', 'Gba Sinde franck', '59510446', '22559510446', '225', 'CI', 'francksinde@minkatoo.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-09 11:41:17', '2020-03-16 17:30:56', '2020-03-16 17:30:56'),
(17, 'Blandine', 'Bamba', 'Blandine Bamba', NULL, '22558125834', NULL, NULL, 'blandine.bamba74@gmail.com', NULL, 'FEMME', NULL, NULL, NULL, 'client', NULL, 0, '2020-03-13 19:10:48', '2020-03-16 15:10:48', NULL),
(18, 'Franck', 'Gba', 'Franck Gba', '44849587', '22544849587', '225', 'CI', 'francksinde@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-16 13:51:29', '2020-03-16 17:30:33', '2020-03-16 17:30:33'),
(19, 'Franck sinde', 'Gba', 'Franck sinde Gba', NULL, '59510446', NULL, NULL, 'francksinde@minkatoo.com', 'WIN_20170221_17_47_48_Pro.jpg', 'HOMME', NULL, NULL, 'Français', 'client', '383119', 0, '2020-03-16 17:48:52', '2020-03-16 18:21:40', '2020-03-16 18:21:40'),
(20, 'Franck sinde', 'Gba', 'Franck sinde Gba', NULL, '225 44849587', NULL, NULL, 'francksinde@minkatoo.com', 'WIN_20170221_17_47_48_Pro.jpg', 'HOMME', NULL, NULL, NULL, 'client', '511314', 0, '2020-03-17 11:32:02', '2020-05-25 18:17:11', NULL),
(21, 'Too', 'Taa', 'Too Taa', '84400312', '22584400312', '225', 'CI', 'angebagui@adjemin.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-05-29 22:11:33', '2020-05-29 22:11:33', NULL),
(22, 'Rokiatou', 'Ouattara', 'Rokiatou Ouattara', '04825231', '22504825231', '225', 'CI', 'rokiaouattara@adjemin.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-06-04 21:46:53', '2020-06-04 21:46:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_session`
--

CREATE TABLE `customer_session` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `location_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_latitude` double DEFAULT NULL,
  `location_longitude` double DEFAULT NULL,
  `battery` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `network` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isMobile` tinyint(1) NOT NULL DEFAULT 0,
  `isTesting` tinyint(1) NOT NULL DEFAULT 1,
  `deviceId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `devicesOSVersion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `devicesName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `w` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ms` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idapp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_session`
--

INSERT INTO `customer_session` (`id`, `token`, `customer_id`, `is_active`, `location_address`, `location_latitude`, `location_longitude`, `battery`, `version`, `device`, `ip_address`, `network`, `isMobile`, `isTesting`, `deviceId`, `devicesOSVersion`, `devicesName`, `w`, `h`, `ms`, `idapp`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-02-17 09:56:55', '2020-02-17 09:56:55', NULL),
(2, NULL, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-02-28 01:03:28', '2020-02-28 01:03:28', NULL),
(3, NULL, 9, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 10:20:25', '2020-03-04 10:20:25', NULL),
(4, NULL, 10, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 22:11:10', '2020-03-04 22:11:10', NULL),
(5, NULL, 11, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 22:49:31', '2020-03-04 22:49:31', NULL),
(6, NULL, 12, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-07 00:20:24', '2020-03-07 00:20:24', NULL),
(7, NULL, 13, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-07 21:31:48', '2020-03-07 21:31:48', NULL),
(8, NULL, 14, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-07 22:45:51', '2020-03-07 22:45:51', NULL),
(9, NULL, 15, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-08 17:15:15', '2020-03-08 17:15:15', NULL),
(10, NULL, 16, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-09 11:41:17', '2020-03-09 11:41:17', NULL),
(11, NULL, 17, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-13 19:10:48', '2020-03-13 19:10:48', NULL),
(12, NULL, 18, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-16 13:51:29', '2020-03-16 13:51:29', NULL),
(13, NULL, 21, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-29 22:11:33', '2020-05-29 22:11:33', NULL),
(14, NULL, 22, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-04 21:46:53', '2020-06-04 21:46:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `excursion_activities`
--

CREATE TABLE `excursion_activities` (
  `id` int(11) NOT NULL,
  `image` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `slug` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `excursion_activities`
--

INSERT INTO `excursion_activities` (`id`, `image`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'https://i.imgur.com/RbrqUS8.png', 'Excursions et visites', 'excursions_et_visites', '2020-03-04 08:25:11', '2020-03-04 13:23:41', NULL),
(2, 'https://i.imgur.com/DPHmpcn.png', 'Visites Guidées', 'visites_guidées', '2020-03-04 08:25:11', '2020-03-04 13:23:41', NULL),
(3, 'https://i.imgur.com/a7NZR6Q.png', 'Sites historiques', 'sites_historiques', '2020-03-04 08:26:01', '2020-03-04 13:25:15', NULL),
(4, 'https://i.imgur.com/ssKvbyq.png', 'Randonnées', 'randonnees', '2020-03-04 08:26:01', '2020-03-04 13:25:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favoris`
--

CREATE TABLE `favoris` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `offer_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `offer` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `food_specialties`
--

CREATE TABLE `food_specialties` (
  `id` int(11) NOT NULL,
  `image` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `slug` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `food_specialties`
--

INSERT INTO `food_specialties` (`id`, `image`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'https://i.imgur.com/mJY3XGH.png', 'Cuisine Africaine', 'cusisine_africaine', '2020-03-04 08:20:35', '2020-03-04 13:19:22', NULL),
(2, 'https://i.imgur.com/FvE5oqJ.png', 'Cuisine Asiatique', 'cuisine_asiatique', '2020-03-04 08:20:35', '2020-03-04 13:19:22', NULL),
(3, 'https://i.imgur.com/ocki9Mb.png', 'Cuisine Francaise', 'cuisine-francaise', '2020-03-04 10:46:23', '2020-03-04 15:44:50', NULL),
(4, 'https://i.imgur.com/YJrzF3E.png', 'Cuisine Orientale', 'cuisine-orientale', '2020-03-04 10:46:23', '2020-03-04 15:44:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE `hotels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_map` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_specification`
--

CREATE TABLE `hotel_specification` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hotel_id` bigint(20) UNSIGNED NOT NULL,
  `specification_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtotal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fees_delivery` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_paid_by_customer` tinyint(1) NOT NULL DEFAULT 0,
  `is_paid_by_delivery_service` tinyint(1) NOT NULL DEFAULT 0,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `order_id`, `customer_id`, `reference`, `link`, `subtotal`, `tax`, `fees_delivery`, `total`, `status`, `is_paid_by_customer`, `is_paid_by_delivery_service`, `currency_code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 1, 'BOOKING-1-6-1-1583597215', '#', '100', '18', NULL, '118', 'unpaid', 0, 0, 'XOF', '2020-03-07 21:06:55', '2020-03-07 21:06:55', NULL),
(2, 7, 21, 'BOOKING-2-7-21-1591291648', '#', '100', '18', NULL, '118', 'unpaid', 0, 0, 'XOF', '2020-06-04 21:27:28', '2020-06-04 21:27:28', NULL),
(3, 8, 22, 'BOOKING-3-8-22-1591293319', '#', '30000', '5400', NULL, '35400', 'unpaid', 0, 0, 'XOF', '2020-06-04 21:55:19', '2020-06-04 21:55:19', NULL),
(4, 9, 22, 'BOOKING-4-9-22-1591293585', '#', '100', '18', NULL, '118', 'unpaid', 0, 0, 'XOF', '2020-06-04 21:59:45', '2020-06-04 21:59:45', NULL),
(5, 10, 22, 'BOOKING-5-10-22-1591293851', '#', '100', '18', NULL, '118', 'unpaid', 0, 0, 'XOF', '2020-06-04 22:04:11', '2020-06-04 22:04:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_payments`
--

CREATE TABLE `invoice_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) DEFAULT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cp',
  `payment_reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'XOF',
  `creator_id` bigint(20) DEFAULT NULL,
  `creator_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creator` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'waiting',
  `is_waiting` tinyint(1) NOT NULL DEFAULT 1,
  `is_completed` tinyint(1) NOT NULL DEFAULT 0,
  `payment_gateway_trans_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_custom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_currency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_payid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_payment_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_payment_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_error_message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_payment_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_phone_prefixe` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_cel_phone_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_ipn_ack` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_created_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_updated_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_cpm_result` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_trans_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_designation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_buyer_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_signature` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_payments`
--

INSERT INTO `invoice_payments` (`id`, `invoice_id`, `payment_method`, `payment_reference`, `amount`, `currency_code`, `creator_id`, `creator_name`, `creator`, `status`, `is_waiting`, `is_completed`, `payment_gateway_trans_id`, `payment_gateway_custom`, `payment_gateway_currency`, `payment_gateway_amount`, `payment_gateway_payid`, `payment_gateway_payment_date`, `payment_gateway_payment_time`, `payment_gateway_error_message`, `payment_gateway_payment_method`, `payment_gateway_phone_prefixe`, `payment_gateway_cel_phone_num`, `payment_gateway_ipn_ack`, `payment_gateway_created_at`, `payment_gateway_updated_at`, `payment_gateway_cpm_result`, `payment_gateway_trans_status`, `payment_gateway_designation`, `payment_gateway_buyer_name`, `payment_gateway_signature`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'cp', '580156448-1-1-6', '118', 'XOF', 1, 'Mohamed Soro', 'customer', 'waiting', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-07 21:06:55', '2020-03-07 21:06:55', NULL),
(2, 2, 'cp', '628481824-21-2-7', '118', 'XOF', 21, 'Too Taa', 'customer', 'waiting', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-04 21:27:28', '2020-06-04 21:27:28', NULL),
(3, 3, 'cp', '6451906810-22-3-8', '35400', 'XOF', 22, 'Rokiatou Ouattara', 'customer', 'waiting', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-04 21:55:19', '2020-06-04 21:55:19', NULL),
(4, 4, 'cp', '6478563602-22-4-9', '118', 'XOF', 22, 'Rokiatou Ouattara', 'customer', 'waiting', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-04 21:59:45', '2020-06-04 21:59:45', NULL),
(5, 5, 'cp', '6505136331-22-5-10', '118', 'XOF', 22, 'Rokiatou Ouattara', 'customer', 'waiting', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-04 22:04:11', '2020-06-04 22:04:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_map` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `location_specification`
--

CREATE TABLE `location_specification` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `specification_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_id` bigint(20) UNSIGNED DEFAULT NULL,
  `location_id` bigint(20) UNSIGNED DEFAULT NULL,
  `restaurant_id` bigint(20) UNSIGNED DEFAULT NULL,
  `excursion_id` bigint(20) UNSIGNED DEFAULT NULL,
  `meuseum_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `meta_data_service`
--

CREATE TABLE `meta_data_service` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_02_06_094440_create_customers_table', 1),
(5, '2020_02_13_111742_create_offers_table', 2),
(6, '2020_02_13_112037_create_orders_table', 2),
(7, '2020_02_13_112144_create_invoices_table', 2),
(8, '2020_02_13_112222_create_invoice_payments_table', 2),
(9, '2020_02_13_112255_create_order_items_table', 2),
(10, '2020_02_13_112324_create_receipts_table', 2),
(11, '2020_02_13_115645_create_owner_sources_table', 2),
(12, '2020_02_13_115710_create_meta_data_service_table', 2),
(13, '2020_02_13_190647_create_customer_session_table', 3),
(14, '2020_02_17_102314_create_favoris_table', 4),
(15, '2020_06_02_095644_create_hotels_table', 5),
(16, '2020_06_02_095844_create_specifications_table', 5),
(17, '2020_06_02_104650_create_hotel_specification', 5),
(18, '2020_06_02_111423_create_media_table', 5),
(19, '2020_06_02_121805_create_specification_locations_table', 5),
(20, '2020_06_02_121820_create_locations_table', 5),
(21, '2020_06_02_122009_create_location_specification', 5),
(22, '2020_06_02_150102_create_restaurants_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_source_id` bigint(20) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  `publisher_id` bigint(20) DEFAULT NULL,
  `publisher_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_children` tinyint(1) NOT NULL DEFAULT 0,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_price` tinyint(1) NOT NULL DEFAULT 0,
  `rating` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `etablissement` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specification` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_lng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_gps` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slide_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slide_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_escort` tinyint(1) DEFAULT 0,
  `escort_fees` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `parent_source_id`, `title`, `meta_data`, `type`, `owner_id`, `publisher_id`, `publisher_name`, `has_children`, `description`, `original_price`, `price`, `currency_code`, `has_price`, `rating`, `location_name`, `picture1`, `picture2`, `picture3`, `room`, `etablissement`, `specification`, `location_lat`, `location_lng`, `location_gps`, `slide_url`, `slide_title`, `is_escort`, `escort_fees`, `created_at`, `updated_at`, `deleted_at`) VALUES
(16, 0, 'Musée national du costume de Grand-Bassam', '{\r\n  \"specifications\": [\r\n    {\r\n      \"label_name\": \"Visiter le Site Web\",\r\n      \"label_value\": \"https://adjemincloud.com\"\r\n    },\r\n    {\r\n      \"label_name\": \"Téléphone\",\r\n      \"label_value\": \"+22546888385\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://blogs.roux-f.com.fr/blog-mcs3-47/wp-content/uploads/sites/136/2020/01/2014-03-29.jpg\",\r\n    \"https://www.fratmat.info/media/k2/items/cache/954834345dc80d3be639ada52ee49261_XL.jpg\",\r\n    \"https://ivoirhotel.com/storage/images/800x540/63507761de855d6f4e788898d4a69faaf71a2a80.jpg\",\r\n    \"https://media-cdn.tripadvisor.com/media/photo-s/0e/b8/6e/49/src-47025235-largejpg.jpg\"\r\n  ]\r\n}', 'musee', 0, 1, NULL, 0, 'Le musée national du costume de Grand-Bassam', '100', '100', 'XOF', 1, '3', 'Comoé,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.41350405', '-3.51696377', NULL, 'https://i.imgur.com/iLxeE4n.jpg', 'Muséede Grand-Bassam', 0, NULL, '2020-03-04 19:07:24', '2020-03-04 19:07:24', NULL),
(22, 0, 'Chez murphy', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Visiter le Site Web\",\r\n      \"label_value\": \"https://adjemincloud.com\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Téléphone\",\r\n      \"label_value\": \"+22546888385\"\r\n    }\r\n  ],\r\n  \"specialties\": [\r\n    {\r\n      \"image\": \"https://i2.wp.com/negronews.fr/wp-content/uploads/2019/12/africanfood.jpg?fit=900%2C600\",\r\n      \"name\": \"Cusisine Africaine\",\r\n      \"slug\": \"cusisine_africaine\",\r\n      \"id\": 1\r\n    },\r\n    {\r\n      \"image\": \"https://i.f1g.fr/media/madame/1900x1900/sites/default/files/img/2016/08/pays-photo-28.jpg\",\r\n      \"name\": \"Cusisine Asiatique\",\r\n      \"slug\": \"cusisine_asiatique\",\r\n      \"id\": 2\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Francaise\",\r\n      \"slug\": \"cusisine_francaise\",\r\n      \"id\": 3\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Orientale\",\r\n      \"slug\": \"cusisine_orientale\",\r\n      \"id\": 4\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/51150921_1816304761812546_8234552071462846464_o.jpg?_nc_cat=101&_nc_sid=110474&_nc_ohc=f9sJDgLNzUUAX9u4jV_&_nc_ht=scontent.fabj3-1.fna&oh=a45ee74da33d7646bb4744e6a0a0fc95&oe=5E8219D5\",\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/51060103_1812131532229869_6901307138027552768_o.jpg?_nc_cat=104&_nc_sid=110474&_nc_ohc=iIHDMO-ll80AX_QWQbJ&_nc_ht=scontent.fabj3-1.fna&oh=7e4a2632077055707326adbb5b389d18&oe=5E98ECA0\"\r\n  ]\r\n}', 'restaurant', 0, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, '4', 'Cocody,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.39731620', '-3.97778040', NULL, 'https://i.imgur.com/cXV6nFn.jpg', 'murphy banniere', 0, NULL, '2020-03-04 19:51:16', '2020-03-04 19:51:17', NULL),
(21, 0, 'la case blue', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Visiter le Site Web\",\r\n      \"label_value\": \"https://adjemincloud.com\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Téléphone\",\r\n      \"label_value\": \"+22546888385\"\r\n    }\r\n  ],\r\n  \"specialties\": [\r\n    {\r\n      \"image\": \"https://i2.wp.com/negronews.fr/wp-content/uploads/2019/12/africanfood.jpg?fit=900%2C600\",\r\n      \"name\": \"Cusisine Africaine\",\r\n      \"slug\": \"cusisine_africaine\",\r\n      \"id\": 1\r\n    },\r\n    {\r\n      \"image\": \"https://i.f1g.fr/media/madame/1900x1900/sites/default/files/img/2016/08/pays-photo-28.jpg\",\r\n      \"name\": \"Cusisine Asiatique\",\r\n      \"slug\": \"cusisine_asiatique\",\r\n      \"id\": 2\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Francaise\",\r\n      \"slug\": \"cusisine_francaise\",\r\n      \"id\": 3\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Orientale\",\r\n      \"slug\": \"cusisine_orientale\",\r\n      \"id\": 4\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/60082553_1989291577842984_7372145126874808320_o.jpg?_nc_cat=107&_nc_sid=8024bb&_nc_ohc=9ld6EImk27QAX8_Six_&_nc_ht=scontent.fabj3-1.fna&oh=c27a3fe85ff8be131a99362d1bc3fa3c&oe=5E803052\",\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/60174714_1989290701176405_7166748168598585344_n.jpg?_nc_cat=106&_nc_sid=8024bb&_nc_ohc=i7i3-nzr_mcAX9Wlikv&_nc_ht=scontent.fabj3-1.fna&oh=f02af75037573e4d201325e96adf8c61&oe=5E815FA1\",\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/59543735_1977728928999249_2935943092680785920_n.jpg?_nc_cat=111&_nc_sid=8024bb&_nc_ohc=0xEt7Xp1EZQAX_KDa49&_nc_ht=scontent.fabj3-1.fna&oh=418a9a43135100315e31e114d3408f4a&oe=5E8037A3\",\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/56424394_1927778663994276_2294723480618270720_o.jpg?_nc_cat=109&_nc_sid=8024bb&_nc_ohc=Y2P1Y3OtFe4AX-LEzEB&_nc_ht=scontent.fabj3-1.fna&oh=9147d504b80d2be0f99ce00909134338&oe=5E7FF19F\"\r\n  ]\r\n}', 'restaurant', 0, 1, NULL, 0, 'case blue restaurant situé a bassam', NULL, NULL, NULL, 0, '4', 'undefined,Comoé,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.41350405', '-3.51696377', NULL, 'https://i.imgur.com/lbOa1PA.jpg', 'case blue banniere', 0, NULL, '2020-03-04 19:42:20', '2020-03-04 19:42:21', NULL),
(20, 0, 'KKT Resi', '{\r\n  \"room_equipments\": [\r\n    {\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://resize-elle.ladmedia.fr/rcrop/638,,forcex/img/var/plain_site/storage/images/deco/reportages/city-guide/airbnb-biarritz-les-appartements-lofts-et-maisons-de-reve-a-biarritz/83299433-3-fre-FR/Airbnb-Biarritz-20-appartements-lofts-et-maisons-de-reve-a-Biarritz.jpg\",\r\n    \"https://www.biarritzsothebysrealty.com/theme/_siteSIR1812.sirbiarri/datas/articles/images/212/212_00-2019-11-07-1719.jpg\",\r\n    \"https://oliviergerber.fr/wp-content/uploads/photographeimmobilier-oliviergerber-easycles-elena-appartement-t3-location-vacances-biarritzcotebasque-7756.jpg\",\r\n    \"https://4.bp.blogspot.com/-2pWCcjfQgCQ/Vd8FS0NKssI/AAAAAAAAjSk/Im-_K2tVm5o/s1600/appart-location%2Bairbnb%2BBiarritz.jpg\",\r\n    \"https://static.trip101.com/paragraph_media/pictures/001/135/097/large/30e14bf7-582a-438f-b455-7839e6bd84ac.jpg?1527498277\",\r\n    \"https://www.toutsurmesfinances.com/impots/wp-content/uploads/sites/6/2019/04/meuble-location-airbnb.jpg\"\r\n  ]\r\n}', 'residence', 0, 1, NULL, 0, 'Residence top etoile', '100', '100', 'XOF', 1, '4', 'Le Plateau,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.32781449', '-4.02116776', NULL, 'https://i.imgur.com/wqALkjV.jpg', 'Residence KK', 0, NULL, '2020-03-04 19:42:07', '2020-03-04 19:42:09', NULL),
(19, 0, 'la terrasse', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Visiter le Site Web\",\r\n      \"label_value\": \"https://adjemincloud.com\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Téléphone\",\r\n      \"label_value\": \"+22546888385\"\r\n    }\r\n  ],\r\n  \"specialties\": [\r\n  {\r\n      \"image\": \"https://i2.wp.com/negronews.fr/wp-content/uploads/2019/12/africanfood.jpg?fit=900%2C600\",\r\n      \"name\": \"Cusisine Africaine\",\r\n      \"slug\": \"cusisine_africaine\",\r\n      \"id\": 1\r\n    },\r\n    {\r\n      \"image\": \"https://i.f1g.fr/media/madame/1900x1900/sites/default/files/img/2016/08/pays-photo-28.jpg\",\r\n      \"name\": \"Cusisine Asiatique\",\r\n      \"slug\": \"cusisine_asiatique\",\r\n      \"id\": 2\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Francaise\",\r\n      \"slug\": \"cusisine_francaise\",\r\n      \"id\": 3\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Orientale\",\r\n      \"slug\": \"cusisine_orientale\",\r\n      \"id\": 4\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://www.abidjanresto.net/assets/images/la-terrasse4-700x389.jpg\",\r\n    \"https://www.abidjanresto.net/assets/images/la-terrasse2-960x365.jpg\",\r\n    \"https://www.abidjanresto.net/assets/images/la-terrasse3-699x381.jpg\"\r\n  ]\r\n}', 'restaurant', 0, 1, NULL, 0, 'la terrasse zone 4', NULL, NULL, NULL, 0, '4', 'Marcory,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.28819560', '-3.97865900', NULL, 'https://i.imgur.com/ZwmvWSG.jpg', 'banniere la terrasse', 0, NULL, '2020-03-04 19:33:20', '2020-03-04 19:33:21', NULL),
(17, 0, 'signature\'D', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Visiter le Site Web\",\r\n      \"label_value\": \"https://adjemincloud.com\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Téléphone\",\r\n      \"label_value\": \"+22546888385\"\r\n    }\r\n  ],\r\n  \"specialties\": [\r\n    {\r\n      \"image\": \"https://i2.wp.com/negronews.fr/wp-content/uploads/2019/12/africanfood.jpg?fit=900%2C600\",\r\n      \"name\": \"Cusisine Africaine\",\r\n      \"slug\": \"cusisine_africaine\",\r\n      \"id\": 1\r\n    },\r\n    {\r\n      \"image\": \"https://i.f1g.fr/media/madame/1900x1900/sites/default/files/img/2016/08/pays-photo-28.jpg\",\r\n      \"name\": \"Cusisine Asiatique\",\r\n      \"slug\": \"cusisine_asiatique\",\r\n      \"id\": 2\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Francaise\",\r\n      \"slug\": \"cusisine_francaise\",\r\n      \"id\": 3\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Orientale\",\r\n      \"slug\": \"cusisine_orientale\",\r\n      \"id\": 4\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/82210773_1031084040563595_2306747391887278080_n.jpg?_nc_cat=103&_nc_sid=8024bb&_nc_ohc=3ym1iXVcv-wAX-ekNGx&_nc_ht=scontent.fabj3-1.fna&oh=378a090213b7fe99543ade9d475b4577&oe=5E80BB3C\",\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/82342842_1031084037230262_8761312553138651136_n.jpg?_nc_cat=106&_nc_sid=8024bb&_nc_ohc=Q0b7uogooNAAX-LaHXo&_nc_ht=scontent.fabj3-1.fna&oh=d76bc10b48326e0d0e8011f4bed6e8d1&oe=5E991015\",\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/82102246_1027621274243205_3734474282915332096_n.jpg?_nc_cat=106&_nc_sid=8024bb&_nc_ohc=VA-x_VPuyUEAX-AWxAF&_nc_ht=scontent.fabj3-1.fna&oh=b5ad0e0f2c5a8d55e18dcd6bb2935add&oe=5E9739E5\"\r\n  ]\r\n}', 'restaurant', 0, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, '0', 'Cocody,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.40378330', '-3.98488620', NULL, 'https://i.imgur.com/2jyRU0l.jpg', 'signature\'d banniere', 0, NULL, '2020-03-04 19:28:03', '2020-03-04 19:28:04', NULL),
(11, 0, 'porsche panamera', '{\r\n  \"model\": \"panamera\",\r\n  \"model_label\": \"Modèle\",\r\n  \"brand\": \"porsche\",\r\n  \"brand_label\": \"Marque\",\r\n  \"year\": \"2010\",\r\n  \"year_label\": \"Année\",\r\n  \"body_type\": \"cylindrique\",\r\n  \"body_type_label\": \"Type de carroserie\",\r\n  \"color\": \"white\",\r\n  \"color_label\": \"Couleur\",\r\n  \"doors\": 5,\r\n  \"doors_label\": \"Nombre de portières\",\r\n  \"motors\": \"6 Cylindres\",\r\n  \"motors_label\": \"Moteur\",\r\n  \"gearbox\": \"Manual\",\r\n  \"gearbox_label\": \"Boîte de vitesse\",\r\n  \"pictures\": [\r\n    \"https://presskit.porsche.de/models/daten/assets/models-porsche-panamera-turbo-s-e-hybrid/images/topic-slider/Slider_SEHybr_04_drivesystem.jpg\",\r\n    \"https://hips.hearstapps.com/roa.h-cdn.co/assets/cm/14/47/5469ae83bb127_-_2012-porsche-panamera-01-lg.jpg?crop=1xw:0.8184143222506394xh;center,top&resize=480:*\",\r\n    \"https://presskit.porsche.de/models/daten/assets/models-porsche-panamera-turbo-s-e-hybrid/images/topic-slider/Slider_SEHybr_04_drivesystem.jpg\"\r\n  ]\r\n}', 'car', 0, 1, NULL, 0, 'elle traverse le temps', '100', '100', 'XOF', 1, '2', 'Marcory,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.30361200', '-3.98539500', NULL, 'https://i.imgur.com/qiK91Kd.jpg', 'porsche panamera banniere', 0, NULL, '2020-03-04 17:48:22', '2020-03-04 17:48:23', NULL),
(18, 0, 'Reisdence joe', '{\r\n  \"room_equipments\": [\r\n    {\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://dqevkwcr7bdtq.cloudfront.net/filer_public_thumbnails/filer_public/09/32/0932af18-fffd-4a81-9831-8a15b7916480/residence-secondaire-quartier-latin-t.jpg__1170x0_q85_subsampling-2_upscale.jpg\",\r\n    \"https://cdn-travel.jumia.com/web_hotel_detail_gallery/residence-niable-4478-5740febd5499d7af687da201b78083c1e0b1cea0.jpg\",\r\n    \"https://st.hzcdn.com/simgs/7891ef610d1bfa97_4-6002/home-design.jpg\",\r\n    \"https://a0.muscache.com/im/pictures/bd544e3f-31f3-4d17-a339-373620b4037a.jpg?aki_policy=large\",\r\n    \"https://luckey.fr/cached_img/asset/bWFpbi9ibG9nLWFuZC10aXBzL3Jlc2lkZW5jZS1zZWNvbmRhaXJlLTE1NDI5NzExNDIuanBn?w=600&h=600&fit=crop&s=6accd77158aada93965583f6f2728c7f\",\r\n    \"https://static.lexpress.fr/medias_11185/w_2048,h_1146,c_crop,x_0,y_215/w_1000,h_563,c_fill,g_north/v1476691156/indoor-view-of-a-modern-individual-bistro_5726991.jpg\"\r\n  ]\r\n}', 'residence', 0, 1, NULL, 0, 'Residence Djoe Frank brou', '100', '100', 'XOF', 1, '2', 'Le Plateau,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.33124140', '-4.02388370', NULL, 'https://i.imgur.com/1qBsnip.jpg', NULL, 0, NULL, '2020-03-04 19:29:53', '2020-03-04 19:29:53', NULL),
(13, 0, 'chevrolet corvette', '{\r\n  \"model\": \"corvette\",\r\n  \"model_label\": \"Modèle\",\r\n  \"brand\": \"Chevrolet\",\r\n  \"brand_label\": \"Marque\",\r\n  \"year\": \"2010\",\r\n  \"year_label\": \"Année\",\r\n  \"body_type\": \"cylindre\",\r\n  \"body_type_label\": \"Type de carroserie\",\r\n  \"color\": \"yellow\",\r\n  \"color_label\": \"Couleur\",\r\n  \"doors\": 3,\r\n  \"doors_label\": \"Nombre de portières\",\r\n  \"motors\": \"6 Cylindres\",\r\n  \"motors_label\": \"Moteur\",\r\n  \"gearbox\": \"Automatique\",\r\n  \"gearbox_label\": \"Boîte de vitesse\",\r\n  \"pictures\": [\r\n    \"https://cdn.motor1.com/images/mgl/OWjvo/s1/2020-chevy-corvette-stingray-feature.jpg\",\r\n    \"https://www.motortrend.com/uploads/sites/5/2019/12/2020-Chevrolet-Corvette-C8-VIR-10.jpg?fit=around%7C875:492\"\r\n  ]\r\n}', 'car', 0, 1, NULL, 0, 'belle chevrolet', '250', '250', 'XOF', 1, '5', 'Abobo,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.43548700', '-4.02900700', NULL, 'https://i.imgur.com/m3bsvqu.jpg', 'chevrolet corvette banniere', 0, NULL, '2020-03-04 17:59:02', '2020-03-04 17:59:03', NULL),
(14, 0, 'toyota supra', '{\r\n  \"model\": \"Supra\",\r\n  \"model_label\": \"Modèle\",\r\n  \"brand\": \"Toyota\",\r\n  \"brand_label\": \"Marque\",\r\n  \"year\": \"2018\",\r\n  \"year_label\": \"Année\",\r\n  \"body_type\": \"cylindre\",\r\n  \"body_type_label\": \"Type de carroserie\",\r\n  \"color\": \"black\",\r\n  \"color_label\": \"Couleur\",\r\n  \"doors\": 4,\r\n  \"doors_label\": \"Nombre de portières\",\r\n  \"motors\": \"6 Cylindres\",\r\n  \"motors_label\": \"Moteur\",\r\n  \"gearbox\": \"Automatique\",\r\n  \"gearbox_label\": \"Boîte de vitesse\",\r\n  \"pictures\": [\r\n    \"https://st.motortrend.com/uploads/sites/10/2019/01/2020-Toyota-Supra-3.jpg?interpolation=lanczos-none&fit=around|660:372\",\r\n    \"https://www.cstatic-images.com/car-pictures/maxWidth503/cad00toc361a021001.png\",\r\n    \"https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Toyota_Supra_GR_Genf_2019_1Y7A5650.jpg/1200px-Toyota_Supra_GR_Genf_2019_1Y7A5650.jpg\"\r\n  ]\r\n}', 'car', 0, 1, NULL, 0, 'toyota supra voiture puissante', '250', '250', 'XOF', 1, '4', 'Cocody,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.35852970', '-3.99400850', NULL, 'https://i.imgur.com/lNtyMB0.jpg', 'toyota supra banniere', 0, NULL, '2020-03-04 18:06:19', '2020-03-04 18:06:20', NULL),
(15, 0, 'Le musée des civilisations de Côte d’Ivoire', '{\r\n  \"specifications\": [\r\n    {\r\n      \"label_name\": \"Visiter le Site Web\",\r\n      \"label_value\": \"https://adjemincloud.com\"\r\n    },\r\n    {\r\n      \"label_name\": \"Téléphone\",\r\n      \"label_value\": \"+22546888385\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://images.ladepeche.fr/api/v1/images/view/5c3721ad3e454666a7491495/large/image.jpg\",\r\n    \"https://lh3.googleusercontent.com/p/AF1QipPOPkbR14ou6NTtqKHW7CzSGiRK2k9BVacjoGXL=w660-h440-c\",\r\n    \"https://www.financialafrik.com/wp-content/uploads/2018/12/musee_0.jpg\",\r\n    \"https://lh3.googleusercontent.com/p/AF1QipN4_nAOa9uydGhqDywof0UCWQHe1fYOEVt9tncj=s1600-w1600\",\r\n    \"https://media-cdn.tripadvisor.com/media/daodao/photo-s/14/59/b5/57/caption.jpg\",\r\n    \"https://media-cdn.tripadvisor.com/media/photo-s/0a/10/d2/0f/musee-des-civilisations.jpg\"\r\n  ]\r\n}', 'musee', 0, 1, NULL, 0, 'Le musée des civilisations de Côte d’Ivoire refait surface, pour le bonheur du public et des amoureux de l’art. Avec « Renaissance », il décide de nous ouvrir ses portes pour un voyage dans l’histoire culturelle ivoirienne.', '250', '250', 'XOF', 1, '3', 'Le Plateau,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.33381802', '-4.02442396', NULL, 'https://i.imgur.com/f0H3EqI.jpg', 'Musée des civilisations de Côte d’Ivoire', 0, NULL, '2020-03-04 18:56:22', '2020-03-04 18:56:22', NULL),
(24, 0, 'ivoire golf club', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://i.imgur.com/oXqcq9x.jpg\"\r\n  ]\r\n}', 'hotel', 0, 1, NULL, 0, NULL, '100', '100', 'XOF', 0, '5', 'Cocody,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.33529380', '-3.96463290', NULL, 'https://i.imgur.com/S8qeozZ.png', 'igc banniere', 0, NULL, '2020-03-04 19:57:30', '2020-03-04 19:57:31', NULL),
(25, 0, 'Residence adjemin', '{\r\n  \"room_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://a0.muscache.com/im/pictures/3b563991-b779-4de5-bbbd-d233dc9e322c.jpg?aki_policy=large\",\r\n    \"https://www.bungalowsvanillepacane.com/wp-content/uploads/2019/12/location-bungalow-trois-ilets-martinique_3.jpeg\",\r\n    \"https://a0.muscache.com/im/pictures/e0a8ddff-9e7b-4ef3-85c0-76091240cbaf.jpg?aki_policy=large\",\r\n    \"https://a0.muscache.com/im/pictures/b677f46d-a9f6-4297-ae4d-56d64351388b.jpg?aki_policy=large\",\r\n    \"https://media-cdn.tripadvisor.com/media/photo-s/1a/8c/c9/f0/apartamentos-turisticos.jpg\",\r\n    \"https://i1.wp.com/rimabelrhazi.com/wp-content/uploads/2016/12/airbnb.png?resize=900%2C592&ssl=1\"\r\n  ]\r\n}', 'residence', 0, 1, NULL, 0, 'Residence adjemin top etoile', '100', '100', 'XOF', 1, '5', 'Cocody,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.40543320', '-3.98362960', NULL, 'https://i.imgur.com/j3sW7aq.png', 'Residence Adjemin', 0, NULL, '2020-03-04 20:00:24', '2020-03-04 20:00:25', NULL),
(36, 0, 'Bienvenue au Domaine Bini côté lagune', '{\r\n    \"specifications\":[\r\n        {\r\n            \"icon\":\"https://image.adjemin.com/eoeooe.png\",\r\n            \"label_name\": \"Visiter le Site Web\",\r\n            \"label_value\": \"https://adjemincloud.com\"\r\n        },\r\n        {\r\n            \"icon\":\"https://image.adjemin.com/eoeooe.png\",\r\n            \"label_name\": \"Téléphone\",\r\n            \"label_value\": \"+22508334773\"\r\n        }\r\n    ],\r\n    \"specialties\": [\r\n\r\n        {\r\n            \"image\":\"https://i1.wp.com/ivonomad.com/wp-content/uploads/2019/04/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-8.jpg?resize=1024%2C768\",\r\n            \"name\": \"Bienvenue au Domaine Bini côté lagune\",\r\n            \"slug\": \"bienvenue_au_domaine_bini_cote_lagune\",\r\n            \"id\":1\r\n        }\r\n\r\n    ],\r\n    \"pictures\":[\r\n        \"https://i1.wp.com/ivonomad.com/wp-content/uploads/2019/04/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-30.jpg?resize=1024%2C768\",\r\n        \"https://i1.wp.com/ivonomad.com/wp-content/uploads/2019/04/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-30.jpg?resize=1024%2C768\",\r\n        \"https://i1.wp.com/ivonomad.com/wp-content/uploads/2019/04/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-32.jpg?w=886\",\r\n        \"https://i0.wp.com/ivonomad.com/wp-content/uploads/2019/04/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-5.jpg?w=886\",\r\n        \"https://i1.wp.com/ivonomad.com/wp-content/uploads/2019/04/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-8.jpg?resize=1024%2C768\",\r\n        \"https://i2.wp.com/ivonomad.com/wp-content/uploads/2019/04/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-4-1.jpg?w=886\"\r\n        \r\n    ]\r\n}', 'excursion', 0, 1, NULL, 0, 'Le Domaine Bini Lagune est un site d’éco-tourisme, situé au cœur d’Abidjan.', '100', '100', 'CFA', 1, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5.4091179', '-4.0422099', NULL, NULL, 'Residence Bini', 0, NULL, '2020-03-04 20:37:40', '2020-03-04 20:37:40', NULL),
(26, 0, 'Ivotel Abidjan', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://q-cf.bstatic.com/images/hotel/max1024x768/234/234605839.jpg\",\r\n    \"https://images.squarespace-cdn.com/content/v1/5c3213b5372b96b6bfcf6dc7/1559244484871-JXT61KMBIV7WXYUNFSN5/ke17ZwdGBToddI8pDm48kLkXF2pIyv_F2eUT9F60jBl7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0iyqMbMesKd95J-X4EagrgU9L3Sa3U8cogeb0tjXbfawd0urKshkc5MgdBeJmALQKw/9E2A4328.jpg?format=2500w\"\r\n  ]\r\n}', 'hotel', 0, 1, NULL, 0, 'hotel ivotel situé a abidjan plateau', NULL, NULL, NULL, 0, '0', 'Le Plateau,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.32409340', '-4.01876320', NULL, NULL, 'ivotel banniere', 0, NULL, '2020-03-04 20:03:12', '2020-03-04 20:10:04', '2020-03-04 20:10:04'),
(27, 0, 'Ivotel Abidjan', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://q-cf.bstatic.com/images/hotel/max1024x768/234/234605839.jpg\",\r\n    \"https://images.squarespace-cdn.com/content/v1/5c3213b5372b96b6bfcf6dc7/1559244484871-JXT61KMBIV7WXYUNFSN5/ke17ZwdGBToddI8pDm48kLkXF2pIyv_F2eUT9F60jBl7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0iyqMbMesKd95J-X4EagrgU9L3Sa3U8cogeb0tjXbfawd0urKshkc5MgdBeJmALQKw/9E2A4328.jpg?format=2500w\"\r\n  ]\r\n}', 'hotel', 0, 1, NULL, 0, 'hotel ivotel situé a abidjan plateau', NULL, NULL, NULL, 0, '0', 'Le Plateau,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.32409340', '-4.01876320', NULL, NULL, 'ivotel banniere', 0, NULL, '2020-03-04 20:05:18', '2020-03-04 20:09:41', '2020-03-04 20:09:41'),
(29, 0, 'Ivotel Abidjan', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://q-cf.bstatic.com/images/hotel/max1024x768/234/234605839.jpg\",\r\n    \"https://images.squarespace-cdn.com/content/v1/5c3213b5372b96b6bfcf6dc7/1559244484871-JXT61KMBIV7WXYUNFSN5/ke17ZwdGBToddI8pDm48kLkXF2pIyv_F2eUT9F60jBl7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0iyqMbMesKd95J-X4EagrgU9L3Sa3U8cogeb0tjXbfawd0urKshkc5MgdBeJmALQKw/9E2A4328.jpg?format=2500w\"\r\n  ]\r\n}', 'hotel', 0, 1, NULL, 0, 'hotel ivotel situé a abidjan plateau', NULL, NULL, NULL, 0, '0', 'Le Plateau,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.32409340', '-4.01876320', NULL, NULL, 'ivotel banniere', 0, NULL, '2020-03-04 20:06:21', '2020-03-04 20:09:13', '2020-03-04 20:09:13'),
(30, 0, 'Ivotel Abidjan', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://q-cf.bstatic.com/images/hotel/max1024x768/234/234605839.jpg\",\r\n    \"https://images.squarespace-cdn.com/content/v1/5c3213b5372b96b6bfcf6dc7/1559244484871-JXT61KMBIV7WXYUNFSN5/ke17ZwdGBToddI8pDm48kLkXF2pIyv_F2eUT9F60jBl7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0iyqMbMesKd95J-X4EagrgU9L3Sa3U8cogeb0tjXbfawd0urKshkc5MgdBeJmALQKw/9E2A4328.jpg?format=2500w\"\r\n  ]\r\n}', 'hotel', 0, 1, NULL, 0, 'hotel ivotel situé a abidjan plateau', NULL, NULL, NULL, 0, '0', 'Le Plateau,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.32409340', '-4.01876320', NULL, 'https://i.imgur.com/l7lnCdE.jpg', 'ivotel banniere', 0, NULL, '2020-03-04 20:07:38', '2020-03-04 20:07:39', NULL),
(31, 0, 'Residence kouame', '{\r\n  \"room_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://q-xx.bstatic.com/xdata/images/hotel/840x460/126866991.jpg?k=7ba69a9e5a8e8b426f3080954e192dc4369fba71d34a839a8a7500a0926e3999&o=\",\r\n    \"https://q-xx.bstatic.com/xdata/images/hotel/840x460/130232645.jpg?k=4f80842fa0090f38dde32194a63de4e8199a083c6195b70b34f6465e11534940&o=\",\r\n    \"https://q-cf.bstatic.com/images/hotel/max1024x768/159/159810114.jpg\",\r\n    \"https://www.cotedivoiretourisme.ci/images/2017/04/13/aho_large.jpg\",\r\n    \"https://q-ec.bstatic.com/images/hotel/max1024x768/130/130572761.jpg\",\r\n    \"https://cdn-travel.jumia.com/web_hotel_detail_gallery/residence-fondy-9775-39f7030b2fa73e4d46aeeaec90a9e427df3ee1b3.jpeg\"\r\n  ]\r\n}', 'residence', 0, 1, NULL, 0, 'Residence kouame bab', '100', '100', 'XOF', 1, '3', 'Yamoussoukro,Yamoussoukro,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '6.86914500', '-5.28232770', NULL, 'https://i.imgur.com/ciN1DXN.jpg', 'residence kouame b', 0, NULL, '2020-03-04 20:08:24', '2020-03-04 20:08:25', NULL),
(33, 0, 'residence panam', '{\r\n  \"room_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://q-cf.bstatic.com/images/hotel/max1024x768/361/36122830.jpg\",\r\n    \"https://media-cdn.tripadvisor.com/media/photo-s/06/40/1d/ad/residence-panama.jpg\",\r\n    \"https://content.r9cdn.net/rimg/himg/07/d5/b8/hotelsdotcom-559944032-12259704_w-899619.jpg?height=500&crop=true&caller=HotelDetailsPhoto\",\r\n    \"https://thumbnails.trvl-media.com/mxFAxNBYRIrFTLFGdeXnZnzePoU=/582x388/smart/filters:quality(60)/images.trvl-media.com/hotels/18000000/17470000/17467100/17467001/b8672a4d_y.jpg\",\r\n    \"https://q-xx.bstatic.com/xdata/images/hotel/840x460/148135059.jpg?k=3359182d36654e771603b4e7e001d548f3c914eb7d77be8e9df893bf9ddc4855&o=\",\r\n    \"https://d1nabgopwop1kh.cloudfront.net/hotel-asset/10000003007048726_wh\"\r\n  ]\r\n}', 'residence', 0, 1, NULL, 0, 'residence panam BK', '110', '125', 'XOF', 1, '2', 'Bouaké,Vallée du Bandama,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '7.69060580', '-5.02984080', NULL, 'https://i.imgur.com/Rt6WVJ6.jpg', 'Residence panam de bouake', 0, NULL, '2020-03-04 20:15:08', '2020-03-04 20:15:09', NULL),
(39, 37, 'chambre 1', '{\r\n  \"establishment_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"room_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"http://www.hotel.tiama.ci/images/rooms/slider/classic1.jpg\",\r\n    \"http://www.hotel.tiama.ci/images/rooms/slider/classic2.jpg\",\r\n    \"http://www.hotel.tiama.ci/images/rooms/slider/classic1.jpg\"\r\n  ]\r\n}', 'chambre', 0, 1, NULL, 1, 'chambre classique hotel tiama', '100', '100', 'XOF', 1, '0', 'Le Plateau,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.32626250', '-4.01998207', NULL, 'https://i.imgur.com/X2caW6q.jpg', 'chambre 1 classic hotel tiama', 0, NULL, '2020-03-04 20:50:15', '2020-03-16 14:57:03', '2020-03-16 14:57:03'),
(34, 30, 'Chambre 1', '{\r\n  \"establishment_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"room_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://q-cf.bstatic.com/images/hotel/max1024x768/234/234605839.jpg\",\r\n    \"https://q-cf.bstatic.com/images/hotel/max1024x768/234/234605813.jpg\",\r\n    \"https://r-cf.bstatic.com/images/hotel/max1024x768/234/234589137.jpg\",\r\n    \"https://q-cf.bstatic.com/images/hotel/max1024x768/182/182874861.jpg\"\r\n  ]\r\n}', 'chambre', 0, 1, NULL, 1, 'l\'une des nombreuses chambre de l\'hotel ivotel plateau', '100', '100', 'XOF', 1, '2', 'Le Plateau,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.32409340', '-4.01876320', NULL, 'https://i.imgur.com/Bf2yjSt.jpg', 'chambre 1 ivotel banniere', 0, NULL, '2020-03-04 20:18:45', '2020-03-04 20:18:46', NULL),
(35, 30, 'chambre 2', '{\r\n  \"establishment_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"room_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://r-cf.bstatic.com/images/hotel/max1024x768/234/234587266.jpg\",\r\n    \"https://q-cf.bstatic.com/images/hotel/max1024x768/234/234587223.jpg\",\r\n    \"https://q-cf.bstatic.com/images/hotel/max1024x768/234/234215923.jpg\",\r\n    \"https://r-cf.bstatic.com/images/hotel/max1024x768/234/234589316.jpg\"\r\n  ]\r\n}', 'chambre', 0, 1, NULL, 1, 'Chambre pour 2, élégante et luxieuse', '100', '100', 'XOF', 1, '3', 'Le Plateau,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.32409340', '-4.01876320', NULL, 'https://i.imgur.com/ChoztKn.jpg', 'chambre 2 banniere', 0, NULL, '2020-03-04 20:32:05', '2020-03-04 20:32:06', NULL),
(37, 0, 'Hotel tiama', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://cdn.ostrovok.ru/t/640x400/content/23/f1/23f1d5cf396236330204950c53eef3b32d3235e5.jpeg\",\r\n    \"https://lh3.googleusercontent.com/proxy/-4dN15kEbbKlDMnJwuQHXddLKdBo6G0_a83wceeP85Xp9ayY32QD0SE_xKXv_Oi-5p9hyQasSvgrd12ZzwESpCDpc_pvUJ0u\",\r\n    \"https://r-cf.bstatic.com/images/hotel/max1280x900/178/178821407.jpg\",\r\n    \"https://lh3.googleusercontent.com/proxy/FfPXIV99Wl7qScVFOrBxmCUMUdEGYQVR6vkZLrQQbuNVBCget9sDn2suT86UFZrqopuDrH2-z236Qw4hMwn0tbCv3cg0kJH3E7cYRWTf\"\r\n  ]\r\n}', 'hotel', 0, 1, NULL, 0, 'hotel tiama plateau', NULL, NULL, NULL, 0, '4', 'Le Plateau,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.32626250', '-4.01998207', NULL, 'https://i.imgur.com/etOyPIP.jpg', 'hotel tiama banniere', 0, NULL, '2020-03-04 20:42:01', '2020-03-04 20:42:02', NULL),
(38, 0, 'Musée Régional Gbon Coulibaly', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Visiter le Site Web\",\r\n      \"label_value\": \"https://adjemincloud.com\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Téléphone\",\r\n      \"label_value\": \"+22546888385\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://upload.wikimedia.org/wikipedia/commons/3/3e/Le_Mus%C3%A9e_r%C3%A9gional_P%C3%A9l%C3%A8foro_Gbon_Coulibaly_de_Korhogo.jpg\",\r\n    \"https://i.ytimg.com/vi/cR1ow7fcrmA/hqdefault.jpg\",\r\n    \"https://upload.wikimedia.org/wikipedia/commons/e/ea/Int%C3%A9rieur_du_Mus%C3%A9e_r%C3%A9gional_de_Korhogo.jpg\",\r\n    \"https://www.jamaafunding.com/uploads/users/daliaruth@hotmail.de/NewFolder/IMG_3157.JPG\",\r\n    \"https://media-cdn.tripadvisor.com/media/photo-s/1a/09/d2/54/photo3jpg.jpg\",\r\n    \"https://assets.catawiki.nl/assets/2016/11/16/a/c/0/ac0b9206-ac1b-11e6-98cd-1ee8cee9b20e.jpg\"\r\n  ]\r\n}', 'musee', 0, 1, NULL, 0, 'Le Musée régional Pélèforo Gbon Coulibaly de Korhogo', '3010', '5010', 'XOF', 1, '3', 'Korhogo,Savanes,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '9.45747190', '-5.63424710', NULL, 'https://i.imgur.com/OnEpSsV.jpg', 'Musée Régional Gbon Coulibaly a korhogo', 0, NULL, '2020-03-04 20:46:40', '2020-03-04 20:46:43', NULL),
(40, 37, 'chambre supérieur', '{\r\n  \"establishment_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"room_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"http://www.hotel.tiama.ci/images/rooms/slider/superieur1.jpg\",\r\n    \"http://www.hotel.tiama.ci/images/rooms/slider/superieur5.jpg\",\r\n    \"http://www.hotel.tiama.ci/images/rooms/slider/superieur2.jpg\",\r\n    \"http://www.hotel.tiama.ci/images/rooms/slider/superieur3.jpg\"\r\n  ]\r\n}', 'chambre', 0, 1, NULL, 1, 'chambre superieur hotel tiama', '95000', '100000', 'XOF', 1, '0', 'Le Plateau,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.32626250', '-4.01998207', NULL, 'https://i.imgur.com/0UGiZ3q.jpg', 'chambre superieur tiama banniere', 0, NULL, '2020-03-04 21:01:05', '2020-03-04 21:01:05', NULL),
(41, 0, 'Hotel Azalai', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://www.blueprintafrica.com/wp-content/uploads/2017/05/1.jpg\",\r\n    \"https://www.cotedivoiretourisme.ci/images/speasyimagegallery/albums/16/images/1.jpg\",\r\n    \"https://media-cdn.tripadvisor.com/media/photo-s/12/10/73/6d/photo0jpg.jpg\",\r\n    \"https://lh5.googleusercontent.com/proxy/orxNq5PVRMyR2C6AczhfSbYGTSe40A_Ox0HiHl9pNt3yiq0gsevNdf_XpN92mUqr4GTn263-lETbSGqs3CPOwzVp_w838Q7ZWWe_SibDF9LYozvG_VU7smBJZ8zrkHCv\",\r\n    \"https://r-cf.bstatic.com/images/hotel/max500/189/189176296.jpg\"\r\n  ]\r\n}', 'hotel', 0, 1, NULL, 0, 'azalai hotel côte d\'ivoire', NULL, NULL, NULL, 0, '5', 'Marcory,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.30361200', '-3.98539500', NULL, 'https://i.imgur.com/n09jDNo.jpg', 'Azalai hotel', 0, NULL, '2020-03-04 21:07:18', '2020-03-04 21:07:18', NULL),
(42, 41, 'Chambre 1', '{\r\n  \"establishment_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"room_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://cdn-travel.jumia.com/web_hotel_detail_gallery/azalai-hotel-abidjan-28282-89027c8cc5eaba678c0788d1cdd92d87b4169346.jpeg\",\r\n    \"https://static.readytotrip.com/upload/information_system_24/1/2/7/item_1273139/information_items_property_10938846.jpg\",\r\n    \"https://www.cotedivoiretourisme.ci/images/2017/02/14/azalai_large.jpg\",\r\n    \"https://d2xf5gjipzd8cd.cloudfront.net/available/349107046/349107046_WxH.jpg\",\r\n    \"https://i2.wp.com/www.azalai.com/azalaihotelabidjan/wp-content/uploads/2019/02/ftr.jpg?fit=1600%2C1200&ssl=1\"\r\n  ]\r\n}', 'chambre', 0, 1, NULL, 1, 'Chambre top 4', '300000', '320000', 'XOF', 1, '4', 'Marcory,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.30361200', '-3.98539500', NULL, 'https://i.imgur.com/meMyLX7.jpg', 'Hotel Azalai chambre 1', 0, NULL, '2020-03-04 21:19:53', '2020-03-04 21:19:56', NULL),
(43, 0, 'Range rover velar', '{\r\n  \"model\": \"Velar\",\r\n  \"model_label\": \"Modèle\",\r\n  \"brand\": \"Range\",\r\n  \"brand_label\": \"Marque\",\r\n  \"year\": \"2019\",\r\n  \"year_label\": \"Année\",\r\n  \"body_type\": \"4X4\",\r\n  \"body_type_label\": \"Type de carroserie\",\r\n  \"color\": \"black\",\r\n  \"color_label\": \"Couleur\",\r\n  \"doors\": 5,\r\n  \"doors_label\": \"Nombre de portières\",\r\n  \"motors\": \"6 Cylindres\",\r\n  \"motors_label\": \"Moteur\",\r\n  \"gearbox\": \"Manuel\",\r\n  \"gearbox_label\": \"Boîte de vitesse\",\r\n  \"pictures\": [\r\n    \"https://www.challenges.fr/assets/img/2017/02/27/images_list-r4x3w1000-58b4693683943-L560_18MY_032_GLHD_PR.jpg\",\r\n    \"https://photos.tf1.fr/940/531/land-rover-ranger-rover-velar-2017-1-576489-0@1x.jpg\",\r\n    \"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR7T4KIttINxTe-4-ZpD6PEIxrRGijS4GsYyxGmPHBjqVAXYklf\"\r\n  ]\r\n}', 'car', 0, 1, NULL, 1, 'range rover velar, LA VOITURE', '75000000', '80000000', 'XOF', 1, '5', 'Cocody,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.33648060', '-4.00380180', NULL, 'https://i.imgur.com/Z8FPcuE.jpg', 'range rover velar banniere', 0, NULL, '2020-03-04 21:30:09', '2020-03-04 21:30:10', NULL),
(44, 41, 'Chambre 2', '{\r\n  \"establishment_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"room_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://cdn-travel.jumia.com/web_hotel_detail_gallery/seen-hotel-abidjan-plateau-28549-b9aebb017163f71d7f3fca9920e6b03f0ea7d665.jpeg\",\r\n    \"https://thumbnails.trvl-media.com/RVYUAKM3vP0-sMCySVJHAKAeyWg=/582x388/smart/filters:quality(60)/images.trvl-media.com/hotels/18000000/17480000/17476900/17476854/ddbde746_z.jpg\",\r\n    \"https://www.cotedivoiretourisme.ci/images/2017/05/12/seen_large.jpg\",\r\n    \"https://i2.wp.com/serialfoodieleblog.com/wp-content/uploads/2018/03/28576255_10211261120620655_1632563875763191808_o.jpg?fit=1400%2C1050&ssl=1\",\r\n    \"https://cdn.galaxy.tf/unit-media/tc-default/uploads/images/hotel_photo/001/550/574/seen-suite-room-standard.jpg\",\r\n    \"https://q-xx.bstatic.com/xdata/images/hotel/max500/96362887.jpg?k=ce4f7ebee90569eece255900e9787fe98f5a8b63bb82b1281dd39219d5d47963&o=\"\r\n  ]\r\n}', 'chambre', 0, 1, NULL, 0, 'azalai hotel côte d\'ivoire chambre 2', '250000', '30000', 'XOF', 0, '4', 'Marcory,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.30361200', '-3.98539500', NULL, 'https://i.imgur.com/Nu9RTQQ.jpg', 'Azalai hotel chambre 2', 0, NULL, '2020-03-04 21:31:15', '2020-03-04 21:31:16', NULL),
(45, 24, 'Chambre 44', '{\r\n  \"establishment_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"room_equipments\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://r-cf.bstatic.com/images/hotel/max1024x768/559/55965966.jpg\",\r\n    \"https://r-cf.bstatic.com/images/hotel/max1024x768/559/55965234.jpg\",\r\n    \"https://q-cf.bstatic.com/images/hotel/max1024x768/559/55965964.jpg\",\r\n    \"https://q-cf.bstatic.com/images/hotel/max1024x768/519/51909361.jpg\"\r\n  ]\r\n}', 'chambre', 0, 1, NULL, 1, 'chambre 44 de l\'eden golf ivoire', '63000', '68000', 'XOF', 1, '4', 'Cocody,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.33529380', '-3.96463290', NULL, 'https://i.imgur.com/Z5u5sdJ.jpg', 'chambre 44 ivoire golf club banniere', 0, NULL, '2020-03-04 21:45:02', '2020-03-04 21:45:03', NULL);
INSERT INTO `offers` (`id`, `parent_source_id`, `title`, `meta_data`, `type`, `owner_id`, `publisher_id`, `publisher_name`, `has_children`, `description`, `original_price`, `price`, `currency_code`, `has_price`, `rating`, `location_name`, `picture1`, `picture2`, `picture3`, `room`, `etablissement`, `specification`, `location_lat`, `location_lng`, `location_gps`, `slide_url`, `slide_title`, `is_escort`, `escort_fees`, `created_at`, `updated_at`, `deleted_at`) VALUES
(46, 21, 'menu', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Visiter le Site Web\",\r\n      \"label_value\": \"https://adjemincloud.com\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Téléphone\",\r\n      \"label_value\": \"+22546888385\"\r\n    }\r\n  ],\r\n  \"specialties\": [\r\n  {\r\n      \"image\": \"https://i2.wp.com/negronews.fr/wp-content/uploads/2019/12/africanfood.jpg?fit=900%2C600\",\r\n      \"name\": \"Cusisine Africaine\",\r\n      \"slug\": \"cusisine_africaine\",\r\n      \"id\": 1\r\n    },\r\n    {\r\n      \"image\": \"https://i.f1g.fr/media/madame/1900x1900/sites/default/files/img/2016/08/pays-photo-28.jpg\",\r\n      \"name\": \"Cusisine Asiatique\",\r\n      \"slug\": \"cusisine_asiatique\",\r\n      \"id\": 2\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Francaise\",\r\n      \"slug\": \"cusisine_francaise\",\r\n      \"id\": 3\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Orientale\",\r\n      \"slug\": \"cusisine_orientale\",\r\n      \"id\": 4\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://cdn.pratico-pratiques.com/app/uploads/sites/3/2018/10/04142143/boeuf-africain.jpg\",\r\n    \"https://cdn.pratico-pratiques.com/app/uploads/sites/2/2018/08/29100929/saute-de-boeuf-aux-mandarines-et-noix-de-cajou-600x600.jpeg\",\r\n    \"https://cdn.pratico-pratiques.com/app/uploads/sites/2/2019/06/04082945/saute-de-boeuf-mongolien.jpg\",\r\n    \"https://static.cuisineaz.com/400x320/i120905-poulet-a-l-africaine.jpeg\",\r\n    \"https://cdn.pratico-pratiques.com/app/uploads/sites/2/2018/08/29094757/saute-de-boeuf-malaysien.jpeg\",\r\n    \"https://cdn.pratico-pratiques.com/app/uploads/sites/2/2018/08/29085938/saute-de-boeuf-a-lorange.jpeg\"\r\n  ]\r\n}', 'menu', 0, 1, NULL, 0, 'plat de riz', '3000', '3500', 'XOF', 0, '3', 'undefined,Baghdad Governorate,Iraq', NULL, NULL, NULL, NULL, NULL, NULL, '33.42646300', '44.16303700', NULL, 'https://i.imgur.com/Ejw4TZP.jpg', 'du riz a l\'africaine', 0, NULL, '2020-03-04 21:46:25', '2020-03-04 21:46:26', NULL),
(47, 0, 'Hotel président Yamoussoukro', '{\"specifications\":[{\"icon\":\"\",\"name\":\"piscine\"},{\"icon\":\"\",\"name\":\"golf\"},{\"icon\":\"\",\"name\":\"sport\"}],\"pictures\":[\"https://i.imgur.com/Cwkl3IZ.jpg\",\"https://i.imgur.com/FhCMOBW.jpg\",\"https://i.imgur.com/uHAyCW2.jpg\"]}', 'hotel', NULL, 1, 'Mohamed Soro', 0, 'hotel situé à yamoussoukro', NULL, NULL, NULL, 0, '5', 'Yamoussoukro,Yamoussoukro,Côte d\'Ivoire', 'https://i.imgur.com/Cwkl3IZ.jpg', 'https://i.imgur.com/FhCMOBW.jpg', 'https://i.imgur.com/uHAyCW2.jpg', '{\"equipement de la chambre\":\",,\"}', '{\"equipement etablissement\":\"piscine,golf,sport\"}', '{\"site web\":null,\"Telephone\":null}', '6.81902630', '-5.27463480', '6.81902630,-5.27463480', 'https://i.imgur.com/SzWg4TZ.jpg', 'hotel président banniere', 0, NULL, '2020-03-13 14:59:21', '2020-03-13 14:59:22', NULL),
(48, 0, 'Hotel lorenzo', '{\"specifications\":[{\"icon\":\"\",\"name\":\"piscine\"},{\"icon\":\"\",\"name\":\"salle de sport\"},{\"icon\":\"\",\"name\":\"Jaccuzi\"}],\"pictures\":[\"https://i.imgur.com/PRzhT8h.jpg\",\"https://i.imgur.com/Tf6NOQ5.jpg\",\"https://i.imgur.com/YmAp5oE.jpg\"]}', 'hotel', NULL, 1, 'Mohamed Soro', 0, 'Situé a la palmeraie', NULL, NULL, NULL, 0, '3', 'Cocody,Abidjan,Côte d\'Ivoire', 'https://i.imgur.com/PRzhT8h.jpg', 'https://i.imgur.com/Tf6NOQ5.jpg', 'https://i.imgur.com/YmAp5oE.jpg', ',,', 'piscine,salle de sport,Jaccuzi', ' / ', '5.36303898', '-3.95642996', '5.36303898,-3.95642996', 'https://i.imgur.com/oY9Il66.jpg', 'lorenzo banniere', 0, NULL, '2020-03-13 20:15:11', '2020-03-13 22:05:07', '2020-03-13 22:05:07'),
(49, 48, 'Chambre', '{\"establishment_equipments\":[{\"icon\":\"\",\"name\":\"Baignoire\"},{\"icon\":\"\",\"name\":\"\"},{\"icon\":\"\",\"name\":\"\"}],\"room_equipments\":[{\"icon\":\"\",\"name\":\"Climatiseur\"},{\"icon\":\"\",\"name\":\"Chauffe eau\"},{\"icon\":\"\",\"name\":\"Jacuzzi\"}],\"pictures\":[\"https://i.imgur.com/oI2JoVz.jpg\",\"https://i.imgur.com/ZuvaRU1.jpg\",\"https://i.imgur.com/PcWuO0j.jpg\"]}', 'chambre', NULL, 5, 'Sinde Franck', 1, 'Chambre double avec vue sur mer', '75000', '90000', 'XOF', 1, '3', 'Cocody,Abidjan,Côte d\'Ivoire', 'https://i.imgur.com/oI2JoVz.jpg', 'https://i.imgur.com/ZuvaRU1.jpg', 'https://i.imgur.com/PcWuO0j.jpg', 'Climatiseur,Chauffe eau,Jacuzzi', 'Baignoire,,', ' / ', '5.36288060', '-3.95777310', '5.36288060,-3.95777310', NULL, 'Chambre 52', 0, NULL, '2020-03-13 20:34:40', '2020-03-16 23:23:54', '2020-03-16 23:23:54'),
(50, 0, 'CHAMBRE', '{\"specifications\":[{\"icon\":\"\",\"name\":\"piscine,balcon de 7m\\u00b2\"},{\"icon\":\"\",\"name\":\"chambre climatis\\u00e9e, double lit,dressing,jacuzzi, t\\u00e9l\\u00e9 ecran plasma 42\',un bureau\"},{\"icon\":\"\",\"name\":\"\"}],\"pictures\":[\"https://i.imgur.com/j91eqDH.jpg\",\"https://i.imgur.com/PVsuqJx.jpg\",\"https://i.imgur.com/rVKMwvd.jpg\"]}', 'hotel', NULL, 5, 'Sinde Franck', 0, 'Tel:+225 21 76 19 22/ Cel: 59 51 04 46 /WEB: www.minkatoo.com/email: relationclient@minkatoo.com', '60000', '100000', 'XOF', 0, '3', 'Cocody,Abidjan,Côte d\'Ivoire', 'https://i.imgur.com/j91eqDH.jpg', 'https://i.imgur.com/PVsuqJx.jpg', 'https://i.imgur.com/rVKMwvd.jpg', ',,', 'piscine,balcon de 7m²,chambre climatisée, double lit,dressing,jacuzzi, télé ecran plasma 42\',un bureau,', ' / ', '5.36288060', '-3.95777310', '5.36288060,-3.95777310', 'https://i.imgur.com/POICx74.jpg', 'LORENZO RESSORT', 0, NULL, '2020-03-13 22:53:12', '2020-03-16 14:59:00', '2020-03-16 14:59:00'),
(51, 0, 'CHAMBRE', '{\"specifications\":[{\"icon\":\"\",\"name\":\"piscine,balcon de 7m\\u00b2\"},{\"icon\":\"\",\"name\":\"chambre climatis\\u00e9e, double lit,dressing,jacuzzi, t\\u00e9l\\u00e9 ecran plasma 42\',un bureau\"},{\"icon\":\"\",\"name\":\"\"}],\"pictures\":[\"https://i.imgur.com/ynzAkjG.jpg\",\"https://i.imgur.com/EaWvr8P.jpg\",\"https://i.imgur.com/ELqCZ5z.jpg\"]}', 'hotel', NULL, 5, 'Sinde Franck', 0, 'Tel:+225 21 76 19 22/ Cel: 59 51 04 46 /WEB: www.minkatoo.com/email: relationclient@minkatoo.com', '60000', '100000', 'XOF', 0, '3', 'Cocody,Abidjan,Côte d\'Ivoire', 'https://i.imgur.com/ynzAkjG.jpg', 'https://i.imgur.com/EaWvr8P.jpg', 'https://i.imgur.com/ELqCZ5z.jpg', ',,', 'piscine,balcon de 7m²,chambre climatisée, double lit,dressing,jacuzzi, télé ecran plasma 42\',un bureau,', ' / ', '5.36288060', '-3.95777310', '5.36288060,-3.95777310', 'https://i.imgur.com/n9knxbk.jpg', 'LORENZO RESSORT', 0, NULL, '2020-03-13 22:53:15', '2020-03-16 14:57:44', '2020-03-16 14:57:44'),
(52, 37, 'CHAMBRE', '{\"specifications\":[{\"icon\":\"\",\"name\":\"Piscine,balcon de 7m\\u00b2\"},{\"icon\":\"\",\"name\":\"jacuzzi,dressing,bureau\"},{\"icon\":\"\",\"name\":\"T\\u00e9l\\u00e9 \\u00e9cran  plasma 42\'\"}],\"pictures\":[\"https://i.imgur.com/fbYaifI.jpg\",\"https://i.imgur.com/3PPrWDG.jpg\",\"https://i.imgur.com/Z2iDdM6.jpg\"]}', 'hotel', NULL, 5, 'Sinde Franck', 0, 'Tel: 21 76 19 22/ Cel: 59 51 04 46/ /WEB:www.minkatoo.com/ Email: relationclient@minkatoo.com', '60000', '100000', 'XOF', 0, '3', 'Cocody,Abidjan,Côte d\'Ivoire', 'https://i.imgur.com/fbYaifI.jpg', 'https://i.imgur.com/3PPrWDG.jpg', 'https://i.imgur.com/Z2iDdM6.jpg', ',,', 'Piscine,balcon de 7m²,jacuzzi,dressing,bureau,Télé écran  plasma 42\'', ' / ', '5.36288060', '-3.95777310', '5.36288060,-3.95777310', 'https://i.imgur.com/RrVivky.jpg', 'LORENZO RESSORT', 0, NULL, '2020-03-13 23:02:15', '2020-03-16 14:56:23', '2020-03-16 14:56:23'),
(53, 0, 'Hotel Lorenzo', '{\"specifications\":[{\"icon\":\"\",\"name\":\"Tele ecran plasma,bureau, jacuzzi, wifi gratuit\"},{\"icon\":\"\",\"name\":\"\"},{\"icon\":\"\",\"name\":\"\"}],\"pictures\":[\"https://i.imgur.com/HlGKLOk.jpg\",\"https://i.imgur.com/cpOP1UM.jpg\",\"https://i.imgur.com/5AihqnT.jpg\"]}', 'hotel', NULL, 5, 'Sinde Franck', 0, 'LORENZO HOTEL est situé dans un cadre paisible,dispose d\'un restaurant, une connexion wifi gratuit, un service de navette assuré gratuitement', '60 000', '100 000', 'XOF', 0, '2', 'Cocody,Abidjan,Côte d\'Ivoire', 'https://i.imgur.com/HlGKLOk.jpg', 'https://i.imgur.com/cpOP1UM.jpg', 'https://i.imgur.com/5AihqnT.jpg', ',,', 'Tele ecran plasma,bureau, jacuzzi, wifi gratuit,,', ' / ', '5.36288060', '-3.95777310', '5.36288060,-3.95777310', 'https://i.imgur.com/uSkMWT6.jpg', 'LORENZO RESSORT', 0, NULL, '2020-03-16 16:08:25', '2020-03-16 16:08:26', NULL),
(54, 53, 'chambre standart', '{\"establishment_equipments\":[{\"icon\":\"\",\"name\":\"bacon de 7m\\u00b2\"},{\"icon\":\"\",\"name\":\"service de navette gratuit\"},{\"icon\":\"\",\"name\":\"\"}],\"room_equipments\":[{\"icon\":\"\",\"name\":\"chambre climatis\\u00e9e, wifi gratuit,bureau,\"},{\"icon\":\"\",\"name\":\"jacuzzi,dressing,\"},{\"icon\":\"\",\"name\":\"tele ecran plasma\"}],\"pictures\":[\"https://i.imgur.com/fe1moSc.jpg\",\"https://i.imgur.com/uMXpxiH.jpg\",\"https://i.imgur.com/hDUIc7G.jpg\"]}', 'chambre', NULL, 5, 'Sinde Franck', 1, 'chambre standart', '60 000', '100 000', 'XOF', 0, '3', 'Cocody,Abidjan,Côte d\'Ivoire', 'https://i.imgur.com/fe1moSc.jpg', 'https://i.imgur.com/uMXpxiH.jpg', 'https://i.imgur.com/hDUIc7G.jpg', 'chambre climatisée, wifi gratuit,bureau,,jacuzzi,dressing,,tele ecran plasma', 'bacon de 7m²,service de navette gratuit,', ' / ', '5.36288060', '-3.95777310', '5.36288060,-3.95777310', 'https://i.imgur.com/CUSgY9K.jpg', NULL, 0, NULL, '2020-03-16 16:24:54', '2020-03-16 16:24:55', NULL),
(55, 53, 'suite royale', '{\"establishment_equipments\":[{\"icon\":\"\",\"name\":\"offre une belle vue sur la mer\"},{\"icon\":\"\",\"name\":\"dressing,petit dejeuner en chambre\"},{\"icon\":\"\",\"name\":\"\"}],\"room_equipments\":[{\"icon\":\"\",\"name\":\"chambre climatis\\u00e9e, wifi gratuit,bureau,jacuzzi\"},{\"icon\":\"\",\"name\":\"balcon de 10m\\u00b2, un salon equip\\u00e9\"},{\"icon\":\"\",\"name\":\"tele ecran plasma dans la chambre et le salon,mini bar, un packing gratuit\"}],\"pictures\":[\"https://i.imgur.com/7kviGv8.jpg\",\"https://i.imgur.com/ZtakEX9.jpg\",\"https://i.imgur.com/N8EHWAY.jpg\"]}', 'chambre', NULL, 5, 'Sinde Franck', 1, 'Doté d\'un bar,d\'un restaurant,d\'une terrase et d\'un parking gratuit, le Lorenzo Hotel abidjan propose des hebergements.', '80 000', '100 000', 'XOF', 1, '3', 'Cocody,Abidjan,Côte d\'Ivoire', 'https://i.imgur.com/7kviGv8.jpg', 'https://i.imgur.com/ZtakEX9.jpg', 'https://i.imgur.com/N8EHWAY.jpg', 'chambre climatisée, wifi gratuit,bureau,jacuzzi,balcon de 10m², un salon equipé,tele ecran plasma dans la chambre et le salon,mini bar, un packing gratuit', 'offre une belle vue sur la mer,dressing,petit dejeuner en chambre,', ' / ', '5.36288060', '-3.95777310', '5.36288060,-3.95777310', 'https://i.imgur.com/0N32txG.jpg', 'LA SUITE DU ROI', 0, NULL, '2020-03-16 16:51:30', '2020-03-16 16:51:31', NULL),
(56, 0, 'SOFITELHOTEL IVOIRE', '{\"specifications\":[{\"icon\":\"\",\"name\":\"\"},{\"icon\":\"\",\"name\":\"\"},{\"icon\":\"\",\"name\":\"\"}],\"pictures\":[\"https://i.imgur.com/jkQz9K9.jpg\",\"https://i.imgur.com/8JPeeHC.jpg\",\"https://i.imgur.com/XzdZPzB.jpg\"]}', 'hotel', NULL, 5, 'Sinde Franck', 0, NULL, NULL, NULL, NULL, 0, '2', 'Cocody,Abidjan,Côte d\'Ivoire', 'https://i.imgur.com/jkQz9K9.jpg', 'https://i.imgur.com/8JPeeHC.jpg', 'https://i.imgur.com/XzdZPzB.jpg', ',,', ',,', ' / ', '5.32773290', '-4.00325650', '5.32773290,-4.00325650', 'https://i.imgur.com/1554CYS.jpg', NULL, 0, NULL, '2020-03-16 19:05:14', '2020-03-16 19:07:27', '2020-03-16 19:07:27'),
(57, 0, 'SAB PARK AUTO', '{\"model\":\"MERCEDES MAYBACH\",\"model_label\":\"Mod\\u00e8le\",\"brand\":\"MERCEDES\",\"brand_label\":\"Marque\",\"year\":\"2018\",\"year_label\":\"Ann\\u00e9e\",\"body_type\":\"ALLU\",\"body_type_label\":\"Type de carroserie\",\"color\":\"NOIR\",\"color_label\":\"Couleur\",\"doors\":\"5\",\"doors_label\":\"Nombre de porti\\u00e8res\",\"motors\":\"V6\",\"motors_label\":\"Moteur\",\"gearbox\":\"AUTOMATIQUE\",\"gearbox_label\":\"Bo\\u00eete de vitesse\",\"pictures\":[\"https://i.imgur.com/w0JcVAI.jpg\",\"https://i.imgur.com/3GRkB5s.jpg\",\"https://i.imgur.com/Cnr0jeZ.jpg\"]}', 'car', NULL, 5, 'Sinde Franck', 0, 'MERCEDES,4000KM', '60 000', '100 000', 'XOF', 0, '1', 'Cocody,Abidjan,Côte d\'Ivoire', 'https://i.imgur.com/w0JcVAI.jpg', 'https://i.imgur.com/3GRkB5s.jpg', 'https://i.imgur.com/Cnr0jeZ.jpg', ',,', ',,', ' / ', '5.36967238', '-3.95896733', '5.36967238,-3.95896733', 'https://i.imgur.com/Nv8M3Np.jpg', NULL, 0, NULL, '2020-03-16 19:37:02', '2020-03-16 19:37:04', NULL),
(58, 0, 'Solarino', '{\"specifications\":[{\"icon\":\"\",\"label_name\":\"Visiter le Site Web\",\"label_value\":\"\"},{\"icon\":\"\",\"label_name\":\"Telephone\",\"label_value\":\"+225677782842\"}],\"specialties\":[{\"image\":\"https://i.imgur.com/mJY3XGH.png\",\"name\":\"Cuisine Africaine\",\"slug\":\"cusisine_africaine\",\"id\":1},{\"image\":\"https://i.imgur.com/FvE5oqJ.png\",\"name\":\"Cuisine Asiatique\",\"slug\":\"cuisine_asiatique\",\"id\":2},{\"image\":\"https://i.imgur.com/ocki9Mb.png\",\"name\":\"Cuisine Francaise\",\"slug\":\"cuisine-francaise\",\"id\":3},{\"image\":\"https://i.imgur.com/YJrzF3E.png\",\"name\":\"Cuisine Orientale\",\"slug\":\"cuisine-orientale\",\"id\":4}],\"pictures\":[\"https://i.imgur.com/z5ksceB.jpg\",\"https://i.imgur.com/RXpCVgC.jpg\",\"https://i.imgur.com/lVXgj0G.jpg\"]}', 'restaurant', 0, 1, 'Mohamed Soro', 0, 'Situé à la 7eme tranche', NULL, NULL, NULL, 0, '4', 'Cocody,Abidjan,Côte d\'Ivoire', 'https://i.imgur.com/z5ksceB.jpg', 'https://i.imgur.com/RXpCVgC.jpg', 'https://i.imgur.com/lVXgj0G.jpg', ',,', ',,', ' / +225677782842', '5.39311370', '-3.98663710', '5.39311370,-3.98663710', 'https://i.imgur.com/GOn2ct9.jpg', 'solarino banniere', 0, NULL, '2020-03-17 17:58:05', '2020-03-17 17:58:06', NULL),
(59, 58, 'menu 1', '{\"specifications\":[{\"icon\":\"\",\"label_name\":\"Visiter le Site Web\",\"label_value\":\"\"},{\"icon\":\"\",\"label_name\":\"Telephone\",\"label_value\":\"\"}],\"specialties\":[{\"image\":\"https://i.imgur.com/mJY3XGH.png\",\"name\":\"Cuisine Africaine\",\"slug\":\"cusisine_africaine\",\"id\":1},{\"image\":\"https://i.imgur.com/FvE5oqJ.png\",\"name\":\"Cuisine Asiatique\",\"slug\":\"cuisine_asiatique\",\"id\":2},{\"image\":\"https://i.imgur.com/ocki9Mb.png\",\"name\":\"Cuisine Francaise\",\"slug\":\"cuisine-francaise\",\"id\":3},{\"image\":\"https://i.imgur.com/YJrzF3E.png\",\"name\":\"Cuisine Orientale\",\"slug\":\"cuisine-orientale\",\"id\":4}],\"pictures\":[\"https://i.imgur.com/3BPpkCf.jpg\",\"https://i.imgur.com/6kUnj5E.jpg\",\"https://i.imgur.com/lP99i2P.jpg\"]}', 'menu', 0, 1, 'Mohamed Soro', 1, 'menu salade d\'avocat', '5000', '6000', 'XOF', 1, '2', 'Cocody,Abidjan,Côte d\'Ivoire', 'https://i.imgur.com/3BPpkCf.jpg', 'https://i.imgur.com/6kUnj5E.jpg', 'https://i.imgur.com/lP99i2P.jpg', ',,', ',,', ' / ', '5.39311370', '-3.98663710', '5.39311370,-3.98663710', 'https://i.imgur.com/M7wFw4Z.jpg', 'menu 1 banniere', 0, NULL, '2020-03-17 20:44:08', '2020-03-17 20:44:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `is_waiting` tinyint(1) NOT NULL DEFAULT 1,
  `current_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cash',
  `delivery_fees` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_delivered` tinyint(1) NOT NULL DEFAULT 0,
  `delivery_date` date DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `is_waiting`, `current_status`, `payment_method_slug`, `delivery_fees`, `is_delivered`, `delivery_date`, `amount`, `currency_code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'waiting', 'cash', NULL, 0, NULL, NULL, 'XOF', '2020-03-07 20:59:16', '2020-03-07 20:59:16', NULL),
(2, 1, 1, 'waiting', 'cash', NULL, 0, NULL, NULL, 'XOF', '2020-03-07 21:01:17', '2020-03-07 21:01:17', NULL),
(3, 1, 1, 'waiting', 'cash', NULL, 0, NULL, NULL, 'XOF', '2020-03-07 21:01:19', '2020-03-07 21:01:19', NULL),
(4, 1, 1, 'waiting', 'cash', NULL, 0, NULL, NULL, 'XOF', '2020-03-07 21:01:22', '2020-03-07 21:01:22', NULL),
(5, 1, 1, 'waiting', 'cash', NULL, 0, NULL, NULL, 'XOF', '2020-03-07 21:05:48', '2020-03-07 21:05:48', NULL),
(6, 1, 1, 'waiting', 'cp', NULL, 0, NULL, '118', 'XOF', '2020-03-07 21:06:55', '2020-03-07 21:06:55', NULL),
(7, 21, 1, 'waiting', 'cp', NULL, 0, NULL, '118', 'XOF', '2020-06-04 21:27:28', '2020-06-04 21:27:28', NULL),
(8, 22, 1, 'waiting', 'cp', NULL, 0, NULL, '35400', 'XOF', '2020-06-04 21:55:19', '2020-06-04 21:55:19', NULL),
(9, 22, 1, 'waiting', 'cp', NULL, 0, NULL, '118', 'XOF', '2020-06-04 21:59:45', '2020-06-04 21:59:45', NULL),
(10, 22, 1, 'waiting', 'cp', NULL, 0, NULL, '118', 'XOF', '2020-06-04 22:04:11', '2020-06-04 22:04:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `meta_data_id` bigint(20) DEFAULT NULL,
  `meta_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `quantity_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `meta_data_id`, `meta_data`, `quantity`, `quantity_unit`, `unit_price`, `currency_code`, `total_amount`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 16, '{\"id\":16,\"parent_source_id\":0,\"title\":\"Mus\\u00e9e national du costume de Grand-Bassam\",\"meta_data\":\"{\\r\\n  \\\"specifications\\\": [\\r\\n    {\\r\\n      \\\"label_name\\\": \\\"Visiter le Site Web\\\",\\r\\n      \\\"label_value\\\": \\\"https:\\/\\/adjemincloud.com\\\"\\r\\n    },\\r\\n    {\\r\\n      \\\"label_name\\\": \\\"T\\u00e9l\\u00e9phone\\\",\\r\\n      \\\"label_value\\\": \\\"+22546888385\\\"\\r\\n    }\\r\\n  ],\\r\\n  \\\"pictures\\\": [\\r\\n    \\\"https:\\/\\/blogs.roux-f.com.fr\\/blog-mcs3-47\\/wp-content\\/uploads\\/sites\\/136\\/2020\\/01\\/2014-03-29.jpg\\\",\\r\\n    \\\"https:\\/\\/www.fratmat.info\\/media\\/k2\\/items\\/cache\\/954834345dc80d3be639ada52ee49261_XL.jpg\\\",\\r\\n    \\\"https:\\/\\/ivoirhotel.com\\/storage\\/images\\/800x540\\/63507761de855d6f4e788898d4a69faaf71a2a80.jpg\\\",\\r\\n    \\\"https:\\/\\/media-cdn.tripadvisor.com\\/media\\/photo-s\\/0e\\/b8\\/6e\\/49\\/src-47025235-largejpg.jpg\\\"\\r\\n  ]\\r\\n}\",\"type\":\"musee\",\"owner_id\":0,\"publisher_id\":1,\"has_children\":false,\"description\":\"Le mus\\u00e9e national du costume de Grand-Bassam\",\"original_price\":\"100\",\"price\":\"100\",\"currency_code\":\"XOF\",\"has_price\":true,\"rating\":\"3\",\"location_name\":\"Como\\u00e9,C\\u00f4te d\'Ivoire\",\"location_lat\":\"5.41350405\",\"location_lng\":\"-3.51696377\",\"location_gps\":null,\"slide_url\":\"https:\\/\\/i.imgur.com\\/iLxeE4n.jpg\",\"slide_title\":\"Mus\\u00e9ede Grand-Bassam\",\"is_escort\":false,\"escort_fees\":null,\"created_at\":\"2020-03-04 14:07:24\",\"updated_at\":\"2020-03-04 14:07:24\",\"deleted_at\":null}', 1, NULL, '100', 'XOF', NULL, '2020-03-07 21:06:55', '2020-03-07 21:06:55', NULL),
(2, 7, 36, '{\"id\":36,\"parent_source_id\":0,\"title\":\"Bienvenue au Domaine Bini c\\u00f4t\\u00e9 lagune\",\"meta_data\":\"{\\r\\n    \\\"specifications\\\":[\\r\\n        {\\r\\n            \\\"icon\\\":\\\"https:\\/\\/image.adjemin.com\\/eoeooe.png\\\",\\r\\n            \\\"label_name\\\": \\\"Visiter le Site Web\\\",\\r\\n            \\\"label_value\\\": \\\"https:\\/\\/adjemincloud.com\\\"\\r\\n        },\\r\\n        {\\r\\n            \\\"icon\\\":\\\"https:\\/\\/image.adjemin.com\\/eoeooe.png\\\",\\r\\n            \\\"label_name\\\": \\\"T\\u00e9l\\u00e9phone\\\",\\r\\n            \\\"label_value\\\": \\\"+22508334773\\\"\\r\\n        }\\r\\n    ],\\r\\n    \\\"specialties\\\": [\\r\\n\\r\\n        {\\r\\n            \\\"image\\\":\\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-8.jpg?resize=1024%2C768\\\",\\r\\n            \\\"name\\\": \\\"Bienvenue au Domaine Bini c\\u00f4t\\u00e9 lagune\\\",\\r\\n            \\\"slug\\\": \\\"bienvenue_au_domaine_bini_cote_lagune\\\",\\r\\n            \\\"id\\\":1\\r\\n        }\\r\\n\\r\\n    ],\\r\\n    \\\"pictures\\\":[\\r\\n        \\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-30.jpg?resize=1024%2C768\\\",\\r\\n        \\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-30.jpg?resize=1024%2C768\\\",\\r\\n        \\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-32.jpg?w=886\\\",\\r\\n        \\\"https:\\/\\/i0.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-5.jpg?w=886\\\",\\r\\n        \\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-8.jpg?resize=1024%2C768\\\",\\r\\n        \\\"https:\\/\\/i2.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-4-1.jpg?w=886\\\"\\r\\n        \\r\\n    ]\\r\\n}\",\"type\":\"excursion\",\"owner_id\":0,\"publisher_id\":1,\"publisher_name\":null,\"has_children\":false,\"description\":\"Le Domaine Bini Lagune est un site d\\u2019\\u00e9co-tourisme, situ\\u00e9 au c\\u0153ur d\\u2019Abidjan.\",\"original_price\":\"100\",\"price\":\"100\",\"currency_code\":\"CFA\",\"has_price\":true,\"rating\":\"0\",\"location_name\":null,\"picture1\":null,\"picture2\":null,\"picture3\":null,\"room\":null,\"etablissement\":null,\"specification\":null,\"location_lat\":\"5.4091179\",\"location_lng\":\"-4.0422099\",\"location_gps\":null,\"slide_url\":null,\"slide_title\":\"Residence Bini\",\"is_escort\":false,\"escort_fees\":null,\"created_at\":\"2020-03-04 15:37:40\",\"updated_at\":\"2020-03-04 15:37:40\",\"deleted_at\":null}', 1, NULL, '100', 'XOF', NULL, '2020-06-04 21:27:28', '2020-06-04 21:27:28', NULL),
(3, 8, 44, '{\"id\":44,\"parent_source_id\":41,\"title\":\"Chambre 2\",\"meta_data\":\"{\\r\\n  \\\"establishment_equipments\\\": [\\r\\n    {\\r\\n      \\\"icon\\\": \\\"\\\",\\r\\n      \\\"name\\\": \\\"Piscine\\\"\\r\\n    },\\r\\n    {\\r\\n      \\\"icon\\\": \\\"\\\",\\r\\n      \\\"name\\\": \\\"Piscine\\\"\\r\\n    },\\r\\n    {\\r\\n      \\\"icon\\\": \\\"\\\",\\r\\n      \\\"name\\\": \\\"Piscine\\\"\\r\\n    },\\r\\n    {\\r\\n      \\\"icon\\\": \\\"\\\",\\r\\n      \\\"name\\\": \\\"Piscine\\\"\\r\\n    },\\r\\n    {\\r\\n      \\\"icon\\\": \\\"\\\",\\r\\n      \\\"name\\\": \\\"Piscine\\\"\\r\\n    },\\r\\n    {\\r\\n      \\\"icon\\\": \\\"\\\",\\r\\n      \\\"name\\\": \\\"Piscine\\\"\\r\\n    }\\r\\n  ],\\r\\n  \\\"room_equipments\\\": [\\r\\n    {\\r\\n      \\\"icon\\\": \\\"\\\",\\r\\n      \\\"name\\\": \\\"Piscine\\\"\\r\\n    },\\r\\n    {\\r\\n      \\\"icon\\\": \\\"\\\",\\r\\n      \\\"name\\\": \\\"Piscine\\\"\\r\\n    },\\r\\n    {\\r\\n      \\\"icon\\\": \\\"\\\",\\r\\n      \\\"name\\\": \\\"Piscine\\\"\\r\\n    },\\r\\n    {\\r\\n      \\\"icon\\\": \\\"\\\",\\r\\n      \\\"name\\\": \\\"Piscine\\\"\\r\\n    },\\r\\n    {\\r\\n      \\\"icon\\\": \\\"\\\",\\r\\n      \\\"name\\\": \\\"Piscine\\\"\\r\\n    }\\r\\n  ],\\r\\n  \\\"pictures\\\": [\\r\\n    \\\"https:\\/\\/cdn-travel.jumia.com\\/web_hotel_detail_gallery\\/seen-hotel-abidjan-plateau-28549-b9aebb017163f71d7f3fca9920e6b03f0ea7d665.jpeg\\\",\\r\\n    \\\"https:\\/\\/thumbnails.trvl-media.com\\/RVYUAKM3vP0-sMCySVJHAKAeyWg=\\/582x388\\/smart\\/filters:quality(60)\\/images.trvl-media.com\\/hotels\\/18000000\\/17480000\\/17476900\\/17476854\\/ddbde746_z.jpg\\\",\\r\\n    \\\"https:\\/\\/www.cotedivoiretourisme.ci\\/images\\/2017\\/05\\/12\\/seen_large.jpg\\\",\\r\\n    \\\"https:\\/\\/i2.wp.com\\/serialfoodieleblog.com\\/wp-content\\/uploads\\/2018\\/03\\/28576255_10211261120620655_1632563875763191808_o.jpg?fit=1400%2C1050&ssl=1\\\",\\r\\n    \\\"https:\\/\\/cdn.galaxy.tf\\/unit-media\\/tc-default\\/uploads\\/images\\/hotel_photo\\/001\\/550\\/574\\/seen-suite-room-standard.jpg\\\",\\r\\n    \\\"https:\\/\\/q-xx.bstatic.com\\/xdata\\/images\\/hotel\\/max500\\/96362887.jpg?k=ce4f7ebee90569eece255900e9787fe98f5a8b63bb82b1281dd39219d5d47963&o=\\\"\\r\\n  ]\\r\\n}\",\"type\":\"chambre\",\"owner_id\":0,\"publisher_id\":1,\"publisher_name\":null,\"has_children\":false,\"description\":\"azalai hotel c\\u00f4te d\'ivoire chambre 2\",\"original_price\":\"250000\",\"price\":\"30000\",\"currency_code\":\"XOF\",\"has_price\":false,\"rating\":\"4\",\"location_name\":\"Marcory,Abidjan,C\\u00f4te d\'Ivoire\",\"picture1\":null,\"picture2\":null,\"picture3\":null,\"room\":null,\"etablissement\":null,\"specification\":null,\"location_lat\":\"5.30361200\",\"location_lng\":\"-3.98539500\",\"location_gps\":null,\"slide_url\":\"https:\\/\\/i.imgur.com\\/Nu9RTQQ.jpg\",\"slide_title\":\"Azalai hotel chambre 2\",\"is_escort\":false,\"escort_fees\":null,\"created_at\":\"2020-03-04 16:31:15\",\"updated_at\":\"2020-03-04 16:31:16\",\"deleted_at\":null}', 1, NULL, '30000', 'XOF', NULL, '2020-06-04 21:55:19', '2020-06-04 21:55:19', NULL),
(4, 9, 36, '{\"id\":36,\"parent_source_id\":0,\"title\":\"Bienvenue au Domaine Bini c\\u00f4t\\u00e9 lagune\",\"meta_data\":\"{\\r\\n    \\\"specifications\\\":[\\r\\n        {\\r\\n            \\\"icon\\\":\\\"https:\\/\\/image.adjemin.com\\/eoeooe.png\\\",\\r\\n            \\\"label_name\\\": \\\"Visiter le Site Web\\\",\\r\\n            \\\"label_value\\\": \\\"https:\\/\\/adjemincloud.com\\\"\\r\\n        },\\r\\n        {\\r\\n            \\\"icon\\\":\\\"https:\\/\\/image.adjemin.com\\/eoeooe.png\\\",\\r\\n            \\\"label_name\\\": \\\"T\\u00e9l\\u00e9phone\\\",\\r\\n            \\\"label_value\\\": \\\"+22508334773\\\"\\r\\n        }\\r\\n    ],\\r\\n    \\\"specialties\\\": [\\r\\n\\r\\n        {\\r\\n            \\\"image\\\":\\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-8.jpg?resize=1024%2C768\\\",\\r\\n            \\\"name\\\": \\\"Bienvenue au Domaine Bini c\\u00f4t\\u00e9 lagune\\\",\\r\\n            \\\"slug\\\": \\\"bienvenue_au_domaine_bini_cote_lagune\\\",\\r\\n            \\\"id\\\":1\\r\\n        }\\r\\n\\r\\n    ],\\r\\n    \\\"pictures\\\":[\\r\\n        \\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-30.jpg?resize=1024%2C768\\\",\\r\\n        \\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-30.jpg?resize=1024%2C768\\\",\\r\\n        \\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-32.jpg?w=886\\\",\\r\\n        \\\"https:\\/\\/i0.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-5.jpg?w=886\\\",\\r\\n        \\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-8.jpg?resize=1024%2C768\\\",\\r\\n        \\\"https:\\/\\/i2.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-4-1.jpg?w=886\\\"\\r\\n        \\r\\n    ]\\r\\n}\",\"type\":\"excursion\",\"owner_id\":0,\"publisher_id\":1,\"publisher_name\":null,\"has_children\":false,\"description\":\"Le Domaine Bini Lagune est un site d\\u2019\\u00e9co-tourisme, situ\\u00e9 au c\\u0153ur d\\u2019Abidjan.\",\"original_price\":\"100\",\"price\":\"100\",\"currency_code\":\"CFA\",\"has_price\":true,\"rating\":\"0\",\"location_name\":null,\"picture1\":null,\"picture2\":null,\"picture3\":null,\"room\":null,\"etablissement\":null,\"specification\":null,\"location_lat\":\"5.4091179\",\"location_lng\":\"-4.0422099\",\"location_gps\":null,\"slide_url\":null,\"slide_title\":\"Residence Bini\",\"is_escort\":false,\"escort_fees\":null,\"created_at\":\"2020-03-04 15:37:40\",\"updated_at\":\"2020-03-04 15:37:40\",\"deleted_at\":null}', 1, NULL, '100', 'XOF', NULL, '2020-06-04 21:59:45', '2020-06-04 21:59:45', NULL),
(5, 10, 36, '{\"id\":36,\"parent_source_id\":0,\"title\":\"Bienvenue au Domaine Bini c\\u00f4t\\u00e9 lagune\",\"meta_data\":\"{\\r\\n    \\\"specifications\\\":[\\r\\n        {\\r\\n            \\\"icon\\\":\\\"https:\\/\\/image.adjemin.com\\/eoeooe.png\\\",\\r\\n            \\\"label_name\\\": \\\"Visiter le Site Web\\\",\\r\\n            \\\"label_value\\\": \\\"https:\\/\\/adjemincloud.com\\\"\\r\\n        },\\r\\n        {\\r\\n            \\\"icon\\\":\\\"https:\\/\\/image.adjemin.com\\/eoeooe.png\\\",\\r\\n            \\\"label_name\\\": \\\"T\\u00e9l\\u00e9phone\\\",\\r\\n            \\\"label_value\\\": \\\"+22508334773\\\"\\r\\n        }\\r\\n    ],\\r\\n    \\\"specialties\\\": [\\r\\n\\r\\n        {\\r\\n            \\\"image\\\":\\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-8.jpg?resize=1024%2C768\\\",\\r\\n            \\\"name\\\": \\\"Bienvenue au Domaine Bini c\\u00f4t\\u00e9 lagune\\\",\\r\\n            \\\"slug\\\": \\\"bienvenue_au_domaine_bini_cote_lagune\\\",\\r\\n            \\\"id\\\":1\\r\\n        }\\r\\n\\r\\n    ],\\r\\n    \\\"pictures\\\":[\\r\\n        \\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-30.jpg?resize=1024%2C768\\\",\\r\\n        \\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-30.jpg?resize=1024%2C768\\\",\\r\\n        \\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-32.jpg?w=886\\\",\\r\\n        \\\"https:\\/\\/i0.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-5.jpg?w=886\\\",\\r\\n        \\\"https:\\/\\/i1.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-8.jpg?resize=1024%2C768\\\",\\r\\n        \\\"https:\\/\\/i2.wp.com\\/ivonomad.com\\/wp-content\\/uploads\\/2019\\/04\\/Domaine-Bini-Lagune-Tourisme-Abidjan-Ivonomad-4-1.jpg?w=886\\\"\\r\\n        \\r\\n    ]\\r\\n}\",\"type\":\"excursion\",\"owner_id\":0,\"publisher_id\":1,\"publisher_name\":null,\"has_children\":false,\"description\":\"Le Domaine Bini Lagune est un site d\\u2019\\u00e9co-tourisme, situ\\u00e9 au c\\u0153ur d\\u2019Abidjan.\",\"original_price\":\"100\",\"price\":\"100\",\"currency_code\":\"CFA\",\"has_price\":true,\"rating\":\"0\",\"location_name\":null,\"picture1\":null,\"picture2\":null,\"picture3\":null,\"room\":null,\"etablissement\":null,\"specification\":null,\"location_lat\":\"5.4091179\",\"location_lng\":\"-4.0422099\",\"location_gps\":null,\"slide_url\":null,\"slide_title\":\"Residence Bini\",\"is_escort\":false,\"escort_fees\":null,\"created_at\":\"2020-03-04 15:37:40\",\"updated_at\":\"2020-03-04 15:37:40\",\"deleted_at\":null}', 1, NULL, '100', 'XOF', NULL, '2020-06-04 22:04:11', '2020-06-04 22:04:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `owner_sources`
--

CREATE TABLE `owner_sources` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('dikadoban@minkatoo.net', '$2y$10$vq7eGFvUe749Yyd8wt04L.SxxC/8.cyytpyCJbAMHrwzRRroEGOEy', '2020-05-25 18:15:34');

-- --------------------------------------------------------

--
-- Table structure for table `receipts`
--

CREATE TABLE `receipts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `restaurants`
--

CREATE TABLE `restaurants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_map` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `specification_hotels`
--

CREATE TABLE `specification_hotels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `specification_locations`
--

CREATE TABLE `specification_locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mohamed Soro', 'mohamedsoro@adjemin.com', NULL, '$2y$10$btnB/q8Jv3YKWKPl5q0vDuXOiRVwkAHkQhk0d1lYLqIjqB8T2N70a', NULL, '2020-02-13 14:00:36', '2020-02-13 14:00:36', NULL),
(3, 'Ange Bagui', 'angebagui@adjemin.com', NULL, '$2y$10$BLG2zCHPetgnxCkTrbSRwef9ZYh0DaGv8jHas3hpcUPuyvnd4JRSS', NULL, '2020-02-28 11:50:43', '2020-02-28 11:50:43', NULL),
(4, 'Thierry Kouame', 'thierry.kouame10@gmail.com', NULL, '123456', NULL, '2020-03-12 21:09:42', '2020-03-12 21:09:48', '2020-03-12 21:09:48'),
(5, 'Sinde Franck', 'francksinde@minkatoo.com', NULL, '$2y$10$GGUV/uhZC7Fp44d4vzhxtOAYh8xQUEdfme4F3gw5sOlW/rQa3VvJS', NULL, '2020-03-13 19:34:28', '2020-03-13 19:34:28', NULL),
(6, 'Bamba Blandine', 'ya.blandine@minkatoo.com', NULL, '$2y$10$If5ZIVloz8iIhseCohan1erIHqEfk3KWDUC0AmJf5MBCyZgqRVNyu', NULL, '2020-03-13 19:37:59', '2020-03-13 19:47:01', '2020-03-13 19:47:01'),
(7, 'Dikado Ban', 'dikadoban@minkatoo.net', NULL, '$2y$10$nzp0f6H6xXYW0n05cUbHQexNQaEm8b741TQaDhMvin68dE2RU5CGy', NULL, '2020-03-13 19:40:54', '2020-03-13 19:40:54', NULL),
(8, 'Bindé Ben-Imad KOUAKAN', 'benkouakan@adjemin.com', NULL, '$2y$10$vxHjUGltcaX4KcnJw1M7A.OZv/PjZT8CLz3iI9/P8qR7bmJoiUzo2', NULL, '2020-05-26 17:08:43', '2020-05-26 17:08:43', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_phone_unique` (`phone`);

--
-- Indexes for table `customer_session`
--
ALTER TABLE `customer_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `excursion_activities`
--
ALTER TABLE `excursion_activities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favoris`
--
ALTER TABLE `favoris`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `food_specialties`
--
ALTER TABLE `food_specialties`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel_specification`
--
ALTER TABLE `hotel_specification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoices_reference_unique` (`reference`);

--
-- Indexes for table `invoice_payments`
--
ALTER TABLE `invoice_payments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_payments_payment_reference_unique` (`payment_reference`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location_specification`
--
ALTER TABLE `location_specification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_hotel_id_index` (`hotel_id`),
  ADD KEY `media_location_id_index` (`location_id`),
  ADD KEY `media_restaurant_id_index` (`restaurant_id`),
  ADD KEY `media_excursion_id_index` (`excursion_id`),
  ADD KEY `media_meuseum_id_index` (`meuseum_id`);

--
-- Indexes for table `meta_data_service`
--
ALTER TABLE `meta_data_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner_sources`
--
ALTER TABLE `owner_sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restaurants`
--
ALTER TABLE `restaurants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specification_hotels`
--
ALTER TABLE `specification_hotels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specification_locations`
--
ALTER TABLE `specification_locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `customer_session`
--
ALTER TABLE `customer_session`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `excursion_activities`
--
ALTER TABLE `excursion_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favoris`
--
ALTER TABLE `favoris`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `food_specialties`
--
ALTER TABLE `food_specialties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotel_specification`
--
ALTER TABLE `hotel_specification`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `invoice_payments`
--
ALTER TABLE `invoice_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `location_specification`
--
ALTER TABLE `location_specification`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `meta_data_service`
--
ALTER TABLE `meta_data_service`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `owner_sources`
--
ALTER TABLE `owner_sources`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `restaurants`
--
ALTER TABLE `restaurants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `specification_hotels`
--
ALTER TABLE `specification_hotels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `specification_locations`
--
ALTER TABLE `specification_locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
