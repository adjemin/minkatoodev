<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
            body {
                width: 100%;

            }
        }

    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <!--sweetalert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    {{-- <script src="sweetalert2.all.min.js"></script> --}}
    <!-- Optional: include a polyfill for ES6 Promises for IE11 -->
    {{-- <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script> --}}
    
    {{-- <link rel="stylesheet" href="sweetalert2.min.css"> --}}
    <!-- endsweetalert -->

    <script charset="utf-8" src="https://www.cinetpay.com/cdn/seamless_sdk/latest/cinetpay.prod.min.js"
        type="text/javascript"></script>
    <script type="text/javascript">
        function onSuccess() {

            Swal.fire({
                title: 'Félicitation',
                text: 'Votre commande à été éffectuée !',
                icon: 'success'
            }).then(function() {
                window.location.href = "{{ url('customer/customer.requests') }}";
                
                /* ---------------- */
                /* ---------------- */



            });
        }

        function onError() {

            Swal.fire({
                title: 'Erreur',
                text: 'Commande non effectuée ! ',
                icon: 'error'
            }).then(function() {
                window.location.href = "{{ url('cancel', ['transactionId' => $transactionId]) }}";
                window.location.href = "{{ url('customer/customer.requests') }}";

            });
        }


        function init() {



            CinetPay.setConfig({
                apikey: document.getElementById('apikey').value,
                site_id: parseInt(document.getElementById('siteId').value),
                notify_url: "https://minkato-dev.adjemincloud.com/api/notify",
            });
            //-------------Gestion des evenements
            //error
            CinetPay.on('error', function(e) {
                console.log('error found ' + e);
            });
            //ajax
            CinetPay.on('ajaxStart', function() {
                console.log('ajaxStart');
            });
            CinetPay.on('ajaxStop', function() {
                console.log('ajaxStop');
            });
            //Lorsque la signature est généré
            CinetPay.on('signatureCreated', function(token) {
                console.log('Tocken généré: ' + token);

            });
            CinetPay.on('paymentPending', function(e) {


            });
            CinetPay.on('paymentSuccessfull', function(paymentInfo) {
                if (paymentInfo.cpm_result == '00') {
                    onSuccess();
                } else {
                    onError();
                }

            });

            //Application des méthodes

            CinetPay.setSignatureData({
                amount: parseInt(document.getElementById('amount').value),
                trans_id: document.getElementById('trans_id').value,
                currency: document.getElementById('currency').value,
                designation: document.getElementById('designation').value,
                customer: document.getElementById('cpm_custom').value,

            });
            CinetPay.getSignature();



        }

    </script>

    <script>
        //window.onload = function() {

            // document.getElementById('signature').submit();
            /*$.ajax({
                url: 'https://api.cinetpay.com/v1/?method=getSignatureByPost',
                type: 'post',
                dataType: 'json',
                data: {
                    'cpm_site_id': '622120',
                    'apikey': '14047243215ebd680ed0d0c0.07903569',
                    'cpm_amount': '110',
                    'cpm_trans_id': '{{ $transactionId }}',
                    'cpm_currency': 'CFA',
                    'cpm_designation': '{{ $transaction_designation }}',
                    'cpm_custom': '{{ $payment_method }}',
                    'cpm_payment_config': 'SINGLE',
                    'cpm_page_action': 'PAYMENT',
                    'cpm_version': 'V1',
                    'cpm_trans_date': '{{ now() }}',
                    'cpm_language': 'fr'

                },
                success: function(data) {
                    console.log(data);
                },
                error: function() {
                    alert('error');
                }
            });*/

            // alert(document.getElementById('signaturenv').value);
            // console.log(signature);
            // var deco = JSON.parse(signature);
            // console.log(deco);
            // var oo = document.getElementById('signaturenv').value = ;

        //}

    </script>

</head>

@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

<body>
    <div>

        <form method="post" id="signature" name="signature"
            action="#">

            <input type="hidden" name="cpm_site_id" id="siteId" value="622120">
            <input type="hidden" name="apikey" id="apikey" value="14047243215ebd680ed0d0c0.07903569">
            <input type="hidden" name="cpm_amount" id="amount" value="{{ $amount }}">
            <input type="hidden" name="cpm_trans_id" id="trans_id" value="{{ $transactionId }}">
            <input type="hidden" name="cpm_currency" id="currency" value="CFA">
            <input type="hidden" name="cpm_designation" id="designation" value="{{ $transaction_designation }}">
            <input type="hidden" name="cpm_custom" id="cpm_custom" value="{{ $payment_method }}">
            <input type="hidden" name="cpm_payment_config" id="cpm_payment_config" value="SINGLE">
            <input type="hidden" name="cpm_page_action" id="cpm_page_action" value="PAYMENT">
            <input type="hidden" name="cpm_version" id="cpm_version" value="V1">
            <input type="hidden" name="cpm_trans_date" value="{{ now() }}">
            <input type="hidden" name="cpm_language" value="fr">
        </form>



        <!-- <form id="info_paiement" action="https://secure.cinetpay.com">

            <input type="hidden" name="apikey" id="apikey" value="14047243215ebd680ed0d0c0.07903569">
            <input type="hidden" name="cpm_site_id" id="siteId" value="622120">
            <input type="hidden" name="notify_url" id="notify_url" value="{{ url('notify') }}">
            <input type="hidden" name="return_url" id="return_url" value="{{ url('success') }}">

            <input type="hidden" name="cpm_amount" id="amount" value="{{ $amount }}">

            <input type="hidden" name="cpm_trans_date" value="{{ now() }}">

            <input type="hidden" name="cpm_payment_config" id="cpm_payment_config" value="SINGLE">

            <input type="hidden" name="cpm_page_action" id="cpm_page_action" value="PAYMENT">

            <input type="hidden" name="cpm_version" id="cpm_version" value="V1">

            <input type="hidden" name="cpm_currency" id="currency" value="CFA">

            <input type="hidden" name="cpm_trans_id" id="trans_id" value="{{ $transactionId }}">

            <input type="hidden" name="cpm_custom" id="cpm_custom" value="{{ $payment_method }}">

            <input type="hidden" name="cpm_language" value="fr">

            <input type="hidden" id="cpm_return_mode">
            <input type="hidden" name="signaturenv" id="signaturenv" value="">

            <input type="hidden" name="cpm_designation" id="designation" value="{{ $transaction_designation }}">

        </form> -->


    </div>




    <script>
        $(document).ready(init())
    </script>
</body>

</html>
