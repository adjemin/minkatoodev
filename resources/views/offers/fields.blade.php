<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"/>
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
{{--  --}}
<script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<script src="https://code.jquery.composer dumpautoloadcom/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

{{--  --}}
<!-- Parent Source Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('parent_source_id', 'Parent:') !!}
    <select id="chargement" onchange="changer()" name="parent_source_id" class="form-control">
        <option value="0">Aucun</option>
        <optgroup label="Hôtels">
            @foreach($hotel as $hotels)
                <option value="{{$hotels->id}}">{{ $hotels->title }}</option>
            @endforeach
        </optgroup>
        <optgroup label="Résidences">
            @foreach($residence as $residences)
                <option value="{{$residences->id}}">{{ $residences->title }}</option>
            @endforeach
        </optgroup>
        <optgroup label="Restaurants">
            @foreach($restaurant as $restaurants)
                <option value="{{$restaurants->id}}">{{$restaurants->title}}</option>
            @endforeach
        </optgroup>
        <optgroup label="Musees">
            @foreach($musee as $musees)
                <option value="{{$musees->id}}">{{$musees->title}}</option>
            @endforeach
        </optgroup>
        <optgroup label="Voitures">
            @foreach($voiture as $voitures)
                <option value="{{$voitures->id}}">{{$voitures->title}}</option>
            @endforeach
        </optgroup>
        <optgroup label="Excursions">
            @foreach($excursion as $excursions)
                <option value="{{$excursions->id}}">{{$excursions->title}}</option>
            @endforeach
        </optgroup>
    </select>
</div>

<div class="row">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-header">
                <h5>Type d'offre</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <!-- Type Field -->
                    <div class="form-group col-sm-10">
                        {!! Form::label('type', 'Type d\'offre : ') !!}
                        <select id="offerTypeSelect" onchange="checking();" name="type" class="form-control">
                            {{-- @if($offer != null){
                                    <option value="{{$offer->type}}">{{$offer->type}}</option>
                                }
                            @endif --}}
                            <option value="aucun"> Aucun</option>
                            <option id="hotel" value="hotel">Hôtel</option>
                            <option id="restaurant" value="restaurant">Restaurant</option>
                            <option id="musee" value="musee">Musée</option>
                            <option id="car" value="car">Voiture</option>
                            <option id="excursion" value="excursion">Excursion</option>
                            <option id="residence" value="residence">Résidence</option>
                            <option id="chambre" value="chambre">Chambre</option>
                            <option id="menu" value="menu">Menu</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>


        <div class="card" id="tarificationCard">
            <div class="card-header">
                <h5>Tarification</h5>
            </div>
            <div class="card-body">
                <div class="col-sm-12 p-2">
                    <div class="row">
                        <!-- Original Price Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('original_price', 'Prix Original:') !!}
                            {!! Form::text('original_price', $offer->original_price ?? null, ['class' => 'form-control']) !!}
                            {{-- <input type="text" class="form-control" name="original_price" value="{{$offer->price ?? ''}}"> --}}
                        </div>

                        <!-- Price Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('price', 'Prix:') !!}
                            {!! Form::text('price', $offer->price ?? null, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="row">
                        <!-- Currency Code Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::label('currency_code', 'Code Devise:') !!}
                            {{--    {!! Form::text('currency_code', null, ['class' => 'form-control']) !!}--}}
                            <select id="currency_code" name="currency_code" class='form-control'>
                                {{-- @if ($offer != null)
                                    <option required value="XOF">Francs CFA</option>

                                @endif --}}
                                <option required value="XOF">Francs CFA</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-8">
        <div class="card">
            <div class="card-header">
                <h5>Détails</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <!-- Title Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::label('title', 'Titre:') !!}
                        {{-- {!! Form::text('title', null, ['class' => 'form-control']) !!} --}}
                    <input type="text" class="form-control" name="title"  required value="@if($offer != null){{$offer->title}} @endif">
                    </div>
                </div>
                <div class="row">
                    <!-- Description Field -->
                    <div class="form-group col-sm-12">
                        {{-- {!! Form::label('description', 'Description:') !!} --}}
                        {{-- {!! Form::textarea('description', null, ['class' => 'form-control']) !!} --}}
                        <label for="inputDescription">Description:</label>
                        <textarea name="description" id="inputDescription" cols="30" rows="10" class="form-control" required>@if($offer != null){!!$offer->description!!}@endif</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                <h5>Spécifications <span id="OfferTypeTitle"></span></h5>
            </div>
            <div class="card-body">
                <div class="col-sm-12">
                    <!-- specifications hotel -->
                    <div id="Hotelspecification" style="display: none;" class="form-group">
                        {{-- {!! Form::label('Hotelspecification', 'Spécifications:') !!} --}}
                        <div class="row justify-content-between">
                            <label for="Hotelspecification">Spécifications</label>
                            {{-- // Add a new specification modal --}}
                            {{-- <button class="btn btn-info" type="button" data-toggle="modal" data-target="#newSpecModal">
                                Ajouter nouvelle spécification
                            </button>
                            <div id="newSpecModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="newSpecModal-title" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="newSpecModal-title">Nouvelle Spécification</h5>
                                            <button class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="/" method="POST">
                                                {{ csrf_token() }}
                                                <input type="text">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <select id="Hotelspecification" multiple name="Hotelspecification[]" class='form-control'>
                            <option value="piscine">Piscine</option>
                            <option value="wifi-gratuit">Wifi gratuit</option>
                            <option value="spa">Spa</option>
                            <option value="petit déjeuner inclus">Petit déjeuner inclus</option>
                            <option value="navette_aeroport">Navette aéroport</option>
                            <option value="bar">Bar/Lounge</option>
                            {{-- <option value="parking">Parking gratuit</option>    --}}
                            {{-- <option value="restaurant">Restaurant</option> --}}
                            {{-- <option value="activite">Activité pour enfant</option> --}}
                            {{-- <option value="service-blanchisserie">Service de blanchisserie</option> --}}
                        </select>
                        {{--<input type="text" name="hotel1" placeholder="ex: piscine" class="form-control"><br>
                        <input type="text" name="hotel2" placeholder="ex: piscine" class="form-control"><br>
                        <input type="text" name="hotel3" placeholder="ex: piscine" class="form-control">--}}
                    </div>
                    {{-- Autres specifications --}}
                    <!-- establishment_equipments -->
                    <div id="equipement" style="display: none;" class="form-group col-sm-6">
                        {!! Form::label('equipement', 'Equipement établissement:') !!}
                        <input type="text" name="equip1" class="form-control"><br>
                        <input type="text" name="equip2" class="form-control"><br>
                        <input type="text" name="equip3" class="form-control"><br>
                        <input type="text" name="equip4" class="form-control"><br>
                        <input type="text" name="equip5" class="form-control"><br>
                        <input type="text" name="equip6" class="form-control">
                    </div>

                    <!-- room_equipments chambre residence -->
                    <div id="room" style="display: none;" class="form-group col-sm-6">
                        {!! Form::label('room', 'Equipement:') !!}
                        <input type="text" name="room1" placeholder="ex: piscine" class="form-control"><br>
                        <input type="text" name="room2" placeholder="ex: piscine" class="form-control"><br>
                        <input type="text" name="room3" placeholder="ex: piscine" class="form-control">
                    </div>

                    <div id="voiture" style="display: none;" class="form-group col-sm-6">
                        <input type="text" name="model" placeholder="Modèle" class="form-control"><br>
                        <input type="text" name="brand" placeholder="Marque" class="form-control"><br>
                        <input type="text" name="year" placeholder="Année" class="form-control"><br>
                        <input type="text" name="body_type" placeholder="Type de carroserie" class="form-control"><br>
                        <input type="text" name="color" placeholder="Couleur" class="form-control"><br>
                        <input type="text" name="doors" placeholder="Nombre de portières" class="form-control"><br>
                        <input type="text" name="motors" placeholder="Moteur" class="form-control"><br>
                        <input type="text" name="gearbox" placeholder="Boîte de vitesse" class="form-control">
                    </div>

                    <!-- specifications excursion musee restaurant -->
                    <div id="specification" style="display: none;" class="form-group col-sm-6">
                        {!! Form::label('specification', 'Spécifications:') !!}
                        <input type="text" name="website" placeholder="Site Web" class="form-control" value="@if($offer != null){{$offer->website}} @endif"><br>
                        <input type="text" name="phone_number" placeholder="Téléphone" class="form-control" value="@if($offer != null){{$offer->phone_number}} @endif">
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                <h5>Images<span id="OfferTypeTitle"></span></h5>
            </div>
            <div class="card-body">
                <div class="col-sm-12">
                    <!-- picture specification -->
                    <div id="picture" style="display: none;" class="form-group col-sm-6">
                        {!! Form::label('picture', 'photo:') !!}
                        <input type="file" accept="image/*" onchange="loadFile1(event)" @if($offer == null) required @endif name="pictures[]" multiple class="form-control">
                        <img style="width: 40%; height: 40%;" id="output1"/><br>
                        {{--<input type="file" accept="image/*" onchange="loadFile2(event)" required name="picture2" class="form-control">
                        <img style="width: 40%; height: 40%;" id="output2"/><br>
                        <input type="file" accept="image/*" onchange="loadFile3(event)" required name="picture3" class="form-control">
                        <img style="width: 40%; height: 40%;" id="output3"/>
                        <script>
                            var loadFile1 = function (event1) {
                                var reader = new FileReader();
                                reader.onload = function () {
                                    var output = document.getElementById('output1');
                                    output.src = reader.result;
                                };
                                reader.readAsDataURL(event.target.files[0]);
                            };
                        </script>
                        <script>
                            var loadFile2 = function (event2) {
                                var reader = new FileReader();
                                reader.onload = function () {
                                    var output = document.getElementById('output2');
                                    output.src = reader.result;
                                };
                                reader.readAsDataURL(event.target.files[0]);
                            };
                        </script>
                        <script>
                            var loadFile3 = function (event3) {
                                var reader = new FileReader();
                                reader.onload = function () {
                                    var output = document.getElementById('output3');
                                    output.src = reader.result;
                                };
                                reader.readAsDataURL(event.target.files[0]);
                            };
                        </script>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">

</div>
<!-- Owner Id Field -->
    <div class="row">
        <div class="form-group col-sm-6">
            <label for="owner_id">Propriétaire:</label>
            <div class="input-group">
                <select name="owner_id" class="form-control">
                    @if ($offer != null)
                        <option value="{{$offer->owner_id ?? "0"}}">{{$offer->customer->name ?? "Aucun"}}</option>
                        @foreach($customer as $customers)
                            <option value="{{$customers->id}}">{{$customers->name}}</option>
                        @endforeach
                    @else
                        <option value="0">Aucun</option>
                        @foreach($customer as $customers)
                            <option value="{{$customers->id}}">{{$customers->name}}</option>
                        @endforeach
                    @endif
                </select>

                <div class="input-group-append">
                    <a href="{{route('customers.create')}}" class="btn btn-info">Ajouter Un Propriétaire</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h5>Spécifications Culinaire ou Excursion<span id="OfferTypeTitle"></span></h5>
                </div>
                <div class="card-body">
                    <div class="col-sm-12">
                        <!-- specifications hotel -->
                        <div id="food_specialties" style="display: none;" class="form-group">
                            {{-- {!! Form::label('specification_hotel', 'Spécifications:') !!} --}}
                            <div class="row justify-content-between">
                                <label for="food_specialties">Spécifications Culinaires</label>
                            </div>
                            <select id="food_specialties" multiple name="food_specialties[]" class='form-control'>
                                <option value="cusisine_africaine">Cuisine Africaine</option>
                                <option value="cuisine-francaise">Cuisine Européenne</option>
                                <option value="cuisine_asiatique">Cuisine Asiatique</option>
                                <option value="cuisine-orientale">Cuisine Rafinée</option>
                            </select>
                        </div>
                    </div>


                    <div id="specification_excursion" style="display: none;" class="form-group">
                        {{-- {!! Form::label('specification_hotel', 'Spécifications:') !!} --}}
                        <div class="row justify-content-between">
                            <label for="specification_excursion">Spécifications Excursions</label>
                        </div>
                        <select multiple name="excursion_activities[]" class='form-control'>
                            <option value="excursions_et_visites">Excursion et visite</option>
                            <option value="visites_guidées">Visite guidée</option>
                            <option value="sites_historiques">Visite Historique</option>
                            <option value="randonnees">Randonnée</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Publisher Id Field -->
<div class="form-group col-sm-6">
    {{-- <label for="publisher_id">Créateur:</label> --}}
    <select name="publisher_id" class="form-control" hidden>
        <option value="{{Auth::user()->id}}">Admin</option>
    <!-- <optgroup label="Client">
                @foreach($customer as $customers)
        <option value="{{$customers->id}}">{{$customers->name}}</option>
                @endforeach
        </optgroup> -->
    </select>
</div>

<!-- Has Children Field -->
<div class="form-group col-sm-6">
    {{-- {!! Form::label('has_children', 'A un parent:') !!} --}}
    <label class="checkbox-inline">
        {!! Form::hidden('has_children', 0) !!}
        {{-- {!! Form::checkbox('has_children', '1', null) !!} --}}
    </label>
</div>




<!-- Has Price Field -->
<div class="form-group col-sm-6">
    {{-- {!! Form::label('has_price', 'A un prix:') !!} --}}
    <label class="checkbox-inline">
        {!! Form::hidden('has_price', 0) !!}
        {{-- {!! Form::checkbox('has_price', '1', null) !!} --}}
    </label>
</div>


<!-- Rating Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('rating', 'Note:') !!}
    <select name="rating" class="form-control">
        <option value="0">0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
    </select>
</div> --}}

<!-- Location Name Field -->
<div id="search" class="form-group col-sm-6">
    <div class="input-group">
        {!! Form::label('location_name', 'Situation géograpique:') !!}
        <input type="text" id="addr" name="addr" class="form-control"

        {{-- @if($offer == null) required @endif  --}}
        placeholder="Localisation"
               data-toggle="modal" data-target="#myModal">{{--<input type="text" id="addr" name="addr" class="form-control" required placeholder="Localisation"
               data-toggle="modal" data-target="#myModal">--}}
        <div class="input-group-append">
            <button type="button" onclick="addr_search();" class="btn btn-info">Rechercher</button>
        </div>
    </div>
</div>
<div class="col-md-6">

    <div id="results" class="form-group col-sm-6"></div>
    <input type="text" id="locat" name="location_name" class="form-control col-sm-6" placeholder="Adresse" readonly>
    <br>
    <input type="hidden" id="lat" name="location_lat">
    <input type="hidden" id="lon" name="location_lng">
    <div style="height:400px ;width: 100%" id="map"></div>
</div>

<br>

<!-- Slide Url Field -->
<div class="form-group col-sm-6">
    <label for="slide_url">Slide:</label>
    <input type="file" name="slide_url" accept="image/*" onchange="loadFile(event)" class="form-control" @if($offer == null) required @endif>
    <img style="width: 40%; height: 130px;" id="output"/>
</div>
<script>
    var loadFile = function (event) {
        var reader = new FileReader();
        reader.onload = function () {
            var output = document.getElementById('output');
            output.src = reader.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };
</script>
{{--
<!-- Slide Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slide_title', 'Titre du slide:') !!}
    {!! Form::text('slide_title', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Escort Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_escort', 'Accompagné ?:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_escort', 0) !!}
        {!! Form::checkbox('is_escort', '1', null) !!}
    </label>
</div>


<!-- Escort Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('escort_fees', 'Frais d\'accompagnement:') !!}
    {!! Form::text('escort_fees', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('offers.index') }}" class="btn btn-secondary">Annuler</a>
</div>

{{--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Situer l'offre sur une carte</h4>
            </div>
            <div class="modal-body">
                <div id="search" class="form-group col-sm-6">
                    {!! Form::label('location_name', 'Rechercher un lieu') !!}
                    <input type="text" id="addr" name="addr" class="form-control" required placeholder="Chercher le lieu"
                           data-toggle="modal" data-target="#myModal">
                    <button type="button" onclick="addr_search();" class="btn btn-primary">Rechercher</button>
                </div>
                <div id="results" class="form-group col-sm-6"></div>
                <input type="text" id="locat" name="location_name" class="form-control col-sm-6" placeholder="Adresse"
                       readonly>
                <br>
                <input type="hidden" id="lat" name="location_lat">
                <input type="hidden" id="lon" name="location_lng">
                <div style="height:400px ;width: 400px" id="map"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>--}}

<script>
    // CKEDITOR init
    window.onload = function () {
        document.getElementById('hotel').style.display = 'block';
        document.getElementById('restaurant').style.display = 'block';
        document.getElementById('car').style.display = 'block';
        document.getElementById('musee').style.display = 'block';
        document.getElementById('excursion').style.display = 'block';
        document.getElementById('residence').style.display = 'block';
        document.getElementById('chambre').style.display = 'none';
        document.getElementById('menu').style.display = 'none';

        var inputDescription = document.getElementById("inputDescription");
        CKEDITOR.replace(inputDescription,{
            language:'en-gb'
        });
        CKEDITOR.config.allowedContent = true;
        // alert("loaded");
        // CKEDITOR.replace( 'description' );
    }

    function changer() {
        if (document.getElementById('chargement').value != 0) {
            document.getElementById('hotel').style.display = 'none';
            document.getElementById('restaurant').style.display = 'none';
            document.getElementById('car').style.display = 'none';
            document.getElementById('musee').style.display = 'none';
            document.getElementById('excursion').style.display = 'none';
            document.getElementById('residence').style.display = 'none';
            document.getElementById('search').style.display = 'none';
            document.getElementById('results').style.display = 'none';
            document.getElementById('locat').style.display = 'none';
            document.getElementById('map').style.display = 'none';
            document.getElementById('chambre').style.display = 'block';
            document.getElementById('menu').style.display = 'block';
        } else {
            document.getElementById('hotel').style.display = 'block';
            document.getElementById('restaurant').style.display = 'block';
            document.getElementById('car').style.display = 'block';
            document.getElementById('musee').style.display = 'block';
            document.getElementById('excursion').style.display = 'block';
            document.getElementById('residence').style.display = 'block';
            document.getElementById('search').style.display = 'block';
            document.getElementById('results').style.display = 'block';
            document.getElementById('locat').style.display = 'block';
            document.getElementById('map').style.display = 'block';
            document.getElementById('chambre').style.display = 'none';
            document.getElementById('menu').style.display = 'none';
        }
    }
</script>

<!-- script specifications -->
<script>
    function checking() {
        //
        var offerType = document.getElementById('offerTypeSelect').value;

        if (offerType == 'hotel') {
            document.getElementById('Hotelspecification').style.display = 'block';
            document.getElementById('tarificationCard').children[1].children[0].style.display = 'none';
        }
        // if (offerType == 'hotel') {
        //     document.getElementById('specification_hotel').style.display = 'block';
        //     document.getElementById('tarificationCard').children[1].children[0].style.display = 'none';
        // }

        if (offerType == 'hotel' || offerType == 'chambre') {
            document.getElementById('Hotelspecification').style.display = 'block';
            document.getElementById('picture').style.display = 'block';
        } else {
            document.getElementById('Hotelspecification').style.display = 'none';
        }
        if (offerType == 'restaurant' ||  offerType == 'musee' || offerType == 'menu') {
            //alert('ok');
            document.getElementById('specification').style.display = 'block';
            document.getElementById('picture').style.display = 'block';
            document.getElementById('food_specialties').style.display = 'block';
        } else {
            document.getElementById('specification').style.display = 'none';
            document.getElementById('food_specialties').style.display = 'none';
        }if(offerType == 'excursion'){
            document.getElementById('specification_excursion').style.display = 'block';
            document.getElementById('picture').style.display = 'block';
        }


        if (offerType == 'car') {
            document.getElementById('voiture').style.display = 'block';
            document.getElementById('picture').style.display = 'block';
            document.getElementById('tarificationCard').children[1].children[0].style.display = 'block';
        } else {
            document.getElementById('voiture').style.display = 'none';
        }
        if (offerType == 'residence' || offerType == 'chambre') {
            document.getElementById('room').style.display = 'block';
            document.getElementById('picture').style.display = 'block';
        } else {
            document.getElementById('room').style.display = 'none';
        }
        /*if(offerType == 'chambre')
            {
                document.getElementById('room').style.display = 'block';
                document.getElementById('specification').style.display = 'block';
                document.getElementById('picture').style.display = 'block';
            }else{
                document.getElementById('room').style.display = 'none';
                document.getElementById('specification').style.display = 'none';
            }
        if (offerType == 'menu') {
            //document.getElementById('room').style.display = 'block';
            //document.getElementById('specification').style.display = 'block';
            document.getElementById('picture').style.display = 'block';
        }/*else{
                    document.getElementById('room').style.display = 'none';
                    document.getElementById('specification').style.display = 'none';
                }*/
        if (offerType == 'aucun') {
            document.getElementById('picture').style.display = 'none';
        }

        // document.getElementById('OfferTypeTitle').innerHTML = offerType;

    }
</script>

<!-- leaft -->
<script>

    // Abidjan
    var startlat = 5.40911790;
    var startlon = -4.04220990;


    var options = {
        center: [startlat, startlon],
        zoom: 9
    }

    document.getElementById('lat').value = startlat;
    document.getElementById('lon').value = startlon;

    var map = L.map('map', options);
    var nzoom = 12;

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OSM'}).addTo(map);

    var myMarker = L.marker([startlat, startlon], {
        title: "Coordinates",
        alt: "Coordinates",
        draggable: true
    }).addTo(map).on('dragend', function () {
        var lat = myMarker.getLatLng().lat.toFixed(8);
        var lon = myMarker.getLatLng().lng.toFixed(8);
        var czoom = map.getZoom();

        if (czoom < 18) {
            nzoom = czoom + 2;
        }
        if (nzoom > 18) {
            nzoom = 18;
        }
        if (czoom != 18) {
            map.setView([lat, lon], nzoom);
        } else {
            map.setView([lat, lon]);
        }
        document.getElementById('lat').value = lat;
        document.getElementById('lon').value = lon;
        myMarker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();
    });

    function chooseAddr(lat1, lng1) {
        myMarker.closePopup();
        map.setView([lat1, lng1], 18);
        myMarker.setLatLng([lat1, lng1]);
        lat = lat1.toFixed(8);
        lon = lng1.toFixed(8);
        document.getElementById('lat').value = lat;
        document.getElementById('lon').value = lon;
        //document.getElementById('locat').value = addrn;
        //alert(addrn);
        myMarker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();

        $.get('https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=' + lat + '&lon=' + lon, function (data) {
            //console.log(data);
            // document.getElementById('locat').value = data.address.city + ',' + data.address.state + ',' + data.address.country;
            document.getElementById('locat').value = data.display_name;
        });
        //alert(i);
    //document.getElementById('location').submit();
    }

    function lolp(ii) {
        alert(ii);
    }

    function loca(i) {
        alert(i);
    }

    function myFunction(arr) {
        var out = "<br />";
        var i;

        if (arr.length > 0) {
            for (i = 0; i < arr.length; i++) {
                out += "<div class='address' title='Show Location and Coordinates' style='cursor:pointer' value='" + arr[i].display_name + "' onclick='chooseAddr(" + arr[i].lat + ", " + arr[i].lon + ");return false;'>" + arr[i].display_name + "</div>";
            }
            document.getElementById('results').innerHTML = out;
        } else {
            document.getElementById('results').innerHTML = "Sorry, no results...";
        }

    }

    function addr_search() {
        var inp = document.getElementById("addr");
        var xmlhttp = new XMLHttpRequest();
        var url = "https://nominatim.openstreetmap.org/search?format=json&limit=3&q=" + inp.value;
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);
                myFunction(myArr);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }

    //onclick='"+loca(arr[i].display_name);"'
    //"+arr[i].display_name+",
    //  onclick='loca(\""+arr[i].display_name+"\")'
</script>
<script type="text/javascript">
    $(document).ready(function() {
      $(".btn-success").click(function(){
          var html = $(".clone").html();
          $(".pictures").after(html);
      });
      $("body").on("click",".btn-danger",function(){
          $(this).parents(".form-group").remove();
      });
    });
</script>
