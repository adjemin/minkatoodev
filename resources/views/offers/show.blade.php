@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('offers.index') }}">Offres</a>
        </li>
        <li class="breadcrumb-item active">Detail</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Details Offre #{{ $offer->id ." : ".$offer->type."  ".$offer->title }}</strong>
                            <div>
                                {{-- <i class="fa fa-trash"></i>', --}}
                                {{-- Delete button --}}
                                {!! Form::button('Supprimer'
                                ,['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Voulez-vous supprimer cette offre?')"]) !!}
                                {{-- Edit button --}}
                                <a href="{{ route('offers.edit', [$offer->id]) }}" class="btn btn-warning">Modifier</a>
                                {{-- Go back button --}}
                                <a href="{{ route('offers.index') }}" class="btn btn-light">Retour</a>
                            </div>
                        </div>
                        <div class="card-body">
                            @include('offers.show_fields')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
