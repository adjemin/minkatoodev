@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('offers.index') !!}">Offres</a>
          </li>
          <li class="breadcrumb-item active">Modifier</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Modifier une offre</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($offer, ['route' => ['offers.update', $offer->id], 'method' => 'patch']) !!}

                              @include('offers.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection