{{-- <link rel="stylesheet" href="{{asset('css/main.css')}}"> --}}
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"/>
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>


<style>
    /* offer detail page */
    .showcase{
        position: relative !important;
        max-height: 450px;
        overflow: hidden;
    }
    .showcase-single{
        position: relative !important;
        max-height: 200px !important;
        overflow: hidden !important;
    }
    /* TODO showcase black overlay */
    .showcase img{
        /* max-height: 400px; */
        /* height: 100%;
        width: auto; */
        position: relative;
    }
    .showcase-item{
        position: relative;
        width: 100% !important;
        display: block !important;
        height: auto !important;
    }
    .img-fluid{
        display: block;
        width: 100%;
        height: auto;
    }
    /* .slider-container .showcase-item{
            width: 25%;
        } */
</style>

{{-- <div class="row">
    <div class="col-md-6">
        <h2>

            {{$offer->type." ".$offer->title}}
        </h2>
    </div>
    <div class="col-md-6">
        <h2>
            Proprietaire :
            {{$offer->owner_name ?? "Non_défini"}}
        </h2>
    </div>
</div> --}}
<header class="">
    <div class="showcase">
        <img src="{{$offer->slide_url}}" class="img-fluid" alt="">
    </div>
    {{-- Slider Elements Section --}}
    {{-- // TODO add javascript --}}
    <div class="container-fluid col-md-10 slider-container mx-auto my-2">
        <div class="row d-md-flex px-md-4 justify-content-center">
            <div class="col-md-4 showcase-single">
                <img src="{{$offer->picture1}}" class="img-fluid showcase-item " alt="">
            </div>
            <div class="col-md-4 showcase-single">
                <img src="{{$offer->picture2}}" class="img-fluid showcase-item " alt="">
            </div>
            <div class="col-md-4 showcase-single">
                <img src="{{$offer->picture3}}" class="img-fluid showcase-item " alt="">
            </div>
        </div>
    </div>
</header>
    <div class="container-fluid">
        <div class="row">
            {{-- // TODO make dis container full height to match its price container conterpart --}}
            {{-- Title and summary container --}}
            <div class="col-md-8 p-0 pr-md-3 h-100 description-container">
                <div class="jumbotron jumbotron-fluid mb-2 mb-md-0 p-3 pb-md-3 h-100">
                    <div class="d-md-flex flex-column justify-content-between">
                        <div>
                            <h2>
                                {{$offer->title}}
                                <br>
                                <small>
                                    {{$offer->location_name}}
                                </small>
                            </h2>
                            <hr>
                        </div>
                        {{-- Summary --}}
                        <div>
                            Description:
                        </div>
                        <div class="h-100">
                            {!!$offer->description!!}
                        </div>
                        {{--  --}}
                        {{-- <div class="my-lg-2"></div> --}}
                    </div>
                </div>
            </div>
            {{-- Price container --}}
            <div class="col-md-4 p-0 h-100">
                <div class="">
                    @if ($offer->price)
                        <h4>Prix :
                            <small>
                            {{$offer->price}}
                            </small>
                        </h4>
                        <h4>Prix original:
                            <small>
                            {{$offer->original_price}}
                            </small>
                        </h4>
                    @endif
                    @if ($offer->owner_name)
                        <h4>Propriétaire :
                            <small>
                            {{$offer->owner_name ?? "non_défini"}}
                            </small>
                        </h4>
                    @endif
                    <hr>
                    <p>
                        <span>
                            Publié le {{$offer->created_at . " par "}}
                        </span>
                        <span>
                            {{$offer->publisher_name ?? "admin"}}
                        </span>
                    </p>
                    <p>
                        Dernière modification le {{
                            $offer->updated_at
                        }}
                    </p>
                </div>
            </div>
        </div>
    </div>
    {{-- A propos --}}
<hr/>
<div class="container-fluid">
    <div class="row">
        <div class="container-fluid">
            @if ($offer->type == "car")
                <h3>Caractéristiques</h3>
                @if ($offer->meta_data != null)
                    {{$offer->meta_data}}
                @else
                @endif
            @else
                <h3>Charactéristiques</h3>
                <div class="row">
                    {{-- @if ($hotel->specification != null)
                        @foreach ($hotel->specification as $spec)
                        <div class="col-6 col-md-4 my-1">
                            <div>
                                <img src="{{asset('img/icon/'.$spec->icon)}}" class="" width="22px" alt="">
                                {{$spec->name}}
                            </div>
                        </div>
                        @endforeach
                    @else
                    @endif --}}
                </div>
            @endif
        </div>
    </div>
</div>
<hr>
    <div class="container-fluid">
        <div class="row">
            <div class="container-fluid">
                <h3>Chambres</h3>
                <div class="row">
                    {{-- @if ($hotel->specification != null)
                        @foreach ($hotel->specification as $spec)
                        <div class="col-6 col-md-4 my-1">
                            <div>
                                <img src="{{asset('img/icon/'.$spec->icon)}}" class="" width="22px" alt="">
                                {{$spec->name}}
                            </div>
                        </div>
                        @endforeach
                    @else

                    @endif --}}
                </div>
            </div>
        </div>
    </div>
<hr>
{{--  --}}
{{-- Section Carte --}}
<section id="maps-container container">
    <div style="height:400px ;width: 100%" id="map"></div>
</section>

{{-- <!-- Parent Source Id Field -->
<div class="form-group">
    {!! Form::label('parent_source_id', 'Parent Source Id:') !!}
    <p>{{ $offer->parent_source_id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $offer->title }}</p>
</div>

<!-- Meta Data Field -->
<div class="form-group">
    {!! Form::label('meta_data', 'Meta Data:') !!}
    <p>{{ $offer->meta_data }}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $offer->type }}</p>
</div>

<!-- Owner Id Field -->
<div class="form-group">
    {!! Form::label('owner_id', 'Owner Id:') !!}
    <p>{{ $offer->owner_id }}</p>
</div>

<!-- Publisher Id Field -->
<div class="form-group">
    {!! Form::label('publisher_id', 'Publisher Id:') !!}
    <p>{{ $offer->publisher_id }}</p>
</div>

<!-- Has Children Field -->
<div class="form-group">
    {!! Form::label('has_children', 'Has Children:') !!}
    <p>{{ $offer->has_children }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $offer->description }}</p>
</div>

<!-- Original Price Field -->
<div class="form-group">
    {!! Form::label('original_price', 'Original Price:') !!}
    <p>{{ $offer->original_price }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $offer->price }}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{{ $offer->currency_code }}</p>
</div>

<!-- Has Price Field -->
<div class="form-group">
    {!! Form::label('has_price', 'Has Price:') !!}
    <p>{{ $offer->has_price }}</p>
</div>

<!-- Rating Field -->
<div class="form-group">
    {!! Form::label('rating', 'Rating:') !!}
    <p>{{ $offer->rating }}</p>
</div>

<!-- Location Name Field -->
<div class="form-group">
    {!! Form::label('location_name', 'Location Name:') !!}
    <p>{{ $offer->location_name }}</p>
</div>


<!-- Slide Url Field -->
<div class="form-group">
    {!! Form::label('slide_url', 'Slide Url:') !!}
    <p>{{ $offer->slide_url }}</p>
</div>

<!-- Slide Title Field -->
<div class="form-group">
    {!! Form::label('slide_title', 'Slide Title:') !!}
    <p>{{ $offer->slide_title }}</p>
</div>

<!-- Is Escort Field -->
<div class="form-group">
    {!! Form::label('is_escort', 'Is Escort:') !!}
    <p>{{ $offer->is_escort }}</p>
</div>

<!-- Escort Fees Field -->
<div class="form-group">
    {!! Form::label('escort_fees', 'Escort Fees:') !!}
    <p>{{ $offer->escort_fees }}</p>
</div>
--}}


<input type="hidden" id="lat" name="location_lat" value="{{$offer->location_lat}}">
<input type="hidden" id="lon" name="location_lng" value="{{$offer->location_lng}}">


<!-- Location Lat Field -->
{{-- <div class="form-group">
    {!! Form::label('location_lat', 'Location Lat:') !!}
    <p>{{ $offer->location_lat }}</p>
</div>

<!-- Location Lng Field -->
<div class="form-group">
    {!! Form::label('location_lng', 'Location Lng:') !!}
    <p>{{ $offer->location_lng }}</p>
</div>

<!-- Location Gps Field -->
<div class="form-group">
    {!! Form::label('location_gps', 'Location Gps:') !!}
    <p>{{ $offer->location_gps }}</p>
</div> --}}


<script>

    // Abidjan
    // var startlat = 5.40911790;
    // var startlon = -4.04220990;
    var startlat = document.getElementById('lat').value;
    var startlon = document.getElementById('lon').value;

    var options = {
        center: [startlat, startlon],
        zoom: 9
    }

    document.getElementById('lat').value = startlat;
    document.getElementById('lon').value = startlon;

    var map = L.map('map', options);
    var nzoom = 12;

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OSM'}).addTo(map);

    var myMarker = L.marker([startlat, startlon], {
        title: "Coordinates",
        alt: "Coordinates",
        draggable: true
    }).addTo(map).on('dragend', function () {
        var lat = myMarker.getLatLng().lat.toFixed(8);
        var lon = myMarker.getLatLng().lng.toFixed(8);
        var czoom = map.getZoom();
        if (czoom < 18) {
            nzoom = czoom + 2;
        }
        if (nzoom > 18) {
            nzoom = 18;
        }
        if (czoom != 18) {
            map.setView([lat, lon], nzoom);
        } else {
            map.setView([lat, lon]);
        }
        document.getElementById('lat').value = lat;
        document.getElementById('lon').value = lon;
        myMarker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();
    });

    function chooseAddr(lat1, lng1) {
        myMarker.closePopup();
        map.setView([lat1, lng1], 18);
        myMarker.setLatLng([lat1, lng1]);
        lat = lat1.toFixed(8);
        lon = lng1.toFixed(8);
        document.getElementById('lat').value = lat;
        document.getElementById('lon').value = lon;
        //document.getElementById('locat').value = addrn;
        //alert(addrn);
        myMarker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();

        $.get('https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=' + lat + '&lon=' + lon, function (data) {
            //console.log(data);
            document.getElementById('locat').value = data.address.city + ',' + data.address.state + ',' + data.address.country;
        });
        //alert(i);
    //document.getElementById('location').submit();
    }

    function lolp(ii) {
        alert(ii);
    }

    function loca(i) {
        alert(i);
    }

    function myFunction(arr) {
        var out = "<br />";
        var i;

        if (arr.length > 0) {
            for (i = 0; i < arr.length; i++) {
                out += "<div class='address' title='Show Location and Coordinates' style='cursor:pointer' value='" + arr[i].display_name + "' onclick='chooseAddr(" + arr[i].lat + ", " + arr[i].lon + ");return false;'>" + arr[i].display_name + "</div>";
            }
            document.getElementById('results').innerHTML = out;
        } else {
            document.getElementById('results').innerHTML = "Sorry, no results...";
        }

    }

    function addr_search() {
        var inp = document.getElementById("addr");
        var xmlhttp = new XMLHttpRequest();
        var url = "https://nominatim.openstreetmap.org/search?format=json&limit=3&q=" + inp.value;
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);
                myFunction(myArr);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }

    //onclick='"+loca(arr[i].display_name);"'
    //"+arr[i].display_name+",
    //  onclick='loca(\""+arr[i].display_name+"\")'
</script>
