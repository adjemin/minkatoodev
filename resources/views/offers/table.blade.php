<div class="box">

    <!-- /.box-header -->
    <div class="box-body" style="overflow-x: scroll;">
        <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive-sm">
            <thead>
                <tr>
                <th>Action</th>
                <th>Parent</th>
                <th>Titre</th>
                <th>Photo 1</th>
                <th>Photo 2</th>
                <th>Photo 3</th>
                <th>Photo 4</th>
                {{-- <th>Spécifications web/Telephone</th> --}}
                {{-- <th>Chambre/Résidence</th> --}}
                {{-- <th>Equipement établissement</th> --}}
                <th>Type</th>
                <th>Propriétaire</th>
                {{-- <th>Identifiant du créateur</th> --}}
                {{-- <th>Appartient a un type d'offre</th> --}}
                <th>Description</th>
                {{-- <th>Prix original</th> --}}
                {{-- <th>Prix</th> --}}
                {{-- <th>Code de la devise</th> --}}
                {{-- <th>A un prix</th> --}}
                {{-- <th>Note</th> --}}
                <th>Situation géograpique</th>
                {{-- <th>Latitude</th> --}}
                {{-- <th>Longitude</th> --}}
                {{-- <th>Gps</th> --}}
                <th>Slide</th>
                <th>Site Web</th>
                <th>Contact Téléphonique</th>
                {{-- <th>Titre du slide</th> --}}
                {{-- <th>Accompagné ?</th> --}}
                {{-- <th>Frais d'accompagnement</th> --}}
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @forelse($offers->reverse() as $offer)
                <tr>
                    <td>
                        {!! Form::open(['route' => ['offers.destroy', $offer->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('offers.show', [$offer->id]) }}" class='btn btn-success'><i
                                    class="fa fa-eye"></i></a>
                            <a href="{{ route('offers.edit', [$offer->id]) }}" class='btn btn-info'><i
                                    class="fa fa-edit"></i></a>
                            {!!
                                Form::button('<i class="fa fa-trash"></i>',
                                ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"])
                            !!}

                        </div>
                        {!! Form::close() !!}
                    </td>
                    <td>
                        @if($offer->parent_source_id != 0)

                                <?php
                                echo (App\Models\Offer::where('id', $offer->parent_source_id)->get());
                                ?>
                                {{-- {{ ( json_decode($offer->getItemsAttribute()[0]->meta_data)->title ) }} --}}
                        @endif
                    </td>
                    <td>{{ $offer->title }}</td>
                    <td>
                        @if($offer->picture1 != null)
                            <img src="{{ $offer->picture1 }}" height="90" width="90">
                        @endif
                    </td>
                    <td>
                        @if($offer->picture2 != null)
                            <img src="{{ $offer->picture2 }}" height="90" width="90">
                        @endif
                    </td>
                    <td>
                        @if($offer->picture3 != null)
                            <img src="{{ $offer->picture3 }}" height="90" width="90">
                        @endif
                    </td>
                    <td>
                        @if($offer->picture3 != null)
                            <img src="{{ $offer->picture4 }}" height="90" width="90">
                        @endif
                    </td>
                    {{-- <td>@if($offer->specification != null)
                            {{$offer->specification}}
                        @endif
                    </td> --}}
                    {{-- <td>@if($offer->room != null)
                            {{$offer->room}}
                        @endif
                    </td> --}}
                    {{-- <td>@if($offer->etalissement != null)
                            {{$offer->etalissement}}
                        @endif
                    </td> --}}
                    <td>{{ $offer->type }}</td>
                    <td>
                        {{-- @if (!empty($offer->customer)) --}}
                            {{-- {{ $offer->customer->name }} --}}
                            @php
                            $owner = App\Models\Customer::find($offer->owner_id);
                                if(!empty($owner)){
                                    echo
                                    $owner->name;
                                } else {
                                    echo "Non_defini";
                                }
                            @endphp
                        {{-- @endif --}}
                        {{-- @if (!empty($offer->customer))
                            {{ $offer->customer->name }}
                        @endif --}}
                    </td>
                    {{-- <td>{{ $offer->publisher_id }}</td> --}}
                    {{-- <td>{{ $offer->publisher_name }}</td> --}}
                    {{-- <td>
                        @if($offer->has_children == 0)
                            Non
                        @else
                            Oui
                        @endif
                    </td> --}}
                    <td>{!! $offer->description !!}</td>
                    {{-- <td>{{ $offer->original_price }}</td>
                    <td>{{ $offer->price }}</td> --}}
                    {{-- <td>{{ $offer->currency_code }}</td> --}}
                    {{-- <td>
                        @if($offer->has_price == 0)
                            Non
                        @else
                            Oui
                        @endif
                    </td> --}}
                    {{-- <td>{{ $offer->rating }}</td> --}}
                    <td>{{ $offer->location_name }}</td>
                    {{--
                    <td>{{ $offer->location_lat }}</td>
                    <td>{{ $offer->location_lng }}</td>
                    <td>{{ $offer->location_gps }}</td>
                    --}}
                    <td>
                        <img src="{{ $offer->slide_url  }}" height="90" width="90">
                    </td>
                    <td>{{ $offer->website }}</td>
                    <td>{{ $offer->phone_number }}</td>
                    {{-- <td>{{ $offer->slide_title }}</td> --}}
                    {{-- <td>
                        @if($offer->is_escort == 0)
                            Non
                        @else
                            Oui
                        @endif
                    </td> --}}
                    {{-- <td>{{ $offer->escort_fees }}</td> --}}
                    <td>

                        {!! Form::open(['route' => ['offers.destroy', $offer->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('offers.show', [$offer->id]) }}" class='btn btn-success'><i
                                    class="fa fa-eye"></i></a>
                            <a href="{{ route('offers.edit', [$offer->id]) }}" class='btn btn-info'><i
                                    class="fa fa-edit"></i></a>
                            {!!
                                Form::button('<i class="fa fa-trash"></i>',
                                ['type' => 'submit', 'class' => 'btn btn-danger', 'onclick' => "return confirm('Are you sure?')"])
                            !!}

                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @empty
                <div class="d-flex justify-content-center">
                    Aucune commande éffectuée.
                </div>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
