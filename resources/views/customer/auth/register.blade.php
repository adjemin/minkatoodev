{{-- @extends('customer.layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('customer.register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
@extends('layouts.master')

@section('title')
    Inscription
@endsection

@section('content')
    <div class="mb30">
    <section class="our-log-reg bgc-fa">
    		<div class="container">
    			<div class="row">
    				<div class="col-sm-12 col-lg-6 offset-lg-3">
    					<div class="sign_up_form inner_page">
    						<div class="heading">
    							<h3 class="text-center">Inscrivez-vous pour commencer</h3>
    							<p class="text-center">Vous avez déjà un compte? <a class="text-thm" href="{{ route('customer.login') }}">Se connecter</a></p>
    						</div>
    						<div class="details">
                                <form action="{{ route('customer.register.submit')}}" method="POST">
                                    @csrf
    								<div class="form-group">
    							    	<input type="text" class="form-control input required {{ $errors->has('last_name')?'is-invalid':'' }}" name="last_name" value="{{ old('last_name') }}" placeholder="Votre Nom de famille">
                                    </div>

                                    <div class="form-group">
    							    	<input type="text" class="form-control input required {{ $errors->has('first_name')?'is-invalid':'' }}" name="first_name" value="{{ old('first_name') }}" placeholder="Votre Prénom">
    								</div>

    								 <div class="form-group">
    							    	<input type="email" class="form-control input required {{ $errors->has('email')?'is-invalid':'' }}" name="email" value="{{ old('email') }}" placeholder="Adresse Email">
                                    </div>

                                    <div class="form-group">
                                        {{-- <div class="iti iti--allow-dropdown iti--separate-dial-code">
                                            <div class="iti__flag-container">
                                               <div class="iti__selected-flag" role="combobox" aria-owns="iti-0__country-listbox" aria-expanded="false"
                                                   tabindex="0" title="Côte d’Ivoire: +225" aria-activedescendant="iti-0__item-ci">
                                                   <div class="iti__flag iti__ci"></div>
                                                   <div class="iti__selected-dial-code">+225</div>
                                                   <div class="iti__arrow"></div>
                                               </div>
                                           </div>
                                           <input type="tel" name="phone_number" id="phone_number" class="form-control input required {{ $errors->has('phone_number')?'is-invalid':'' }}"
                                           value="{{ old('phone_number') }}" required placeholder="Votre Contact téléphonique" autocomplete="off"
                                           data-intl-tel-input-id="0"  style="padding-left: 228px;">
                                       </div> --}}
                                        <input type="tel" name="phone_number" id="phone_number" class="form-control input required {{ $errors->has('phone_number')?'is-invalid':'' }}"
                                         value="{{ old('phone_number') }}"  required  placeholder="Votre contact téléphonique" style="padding-left: 96px; height: 53px; width: 393px;">

                                        <span>
                                            @error('phone_number')
                                                {{ $message }}
                                            @enderror
                                        </span>

                                        <input type="hidden" name="dial_code" id="dial_code">
                                        <input type="hidden" name="phone_numberCustomer" id="phone_numberCustomer">
                                        <input type="hidden" name="country_code" id="country_code" value=""/>
                                        {{-- <span id="valid-msgPhoneShop" class="hide"></span>
                                        <span id="error-msgPhoneShop"  class="hide" style="color:red;"></span> --}}

                                   </div>
                                    {{-- <div class="form-group">
                                        <input type="tel" name="phone_number" id="phone_number" class="form-control input required {{ $errors->has('phone_number')?'is-invalid':'' }}"
                                        value="{{ old('phone_number') }}" required placeholder="Votre Contact téléphonique" >
                                        <input type="hidden" name="dial_code" id="dial_code">
                                        <input type="hidden" name="phone_numberCustomer" id="phone_numberCustomer">
                                        <input type="hidden" name="country_code" id="country_code" value=""/>
                                        <span id="valid-msgPhoneShop" class="hide"></span>
                                        <span id="error-msgPhoneShop"  class="hide" style="color:red;"></span>
                                    </div> --}}
    								<button type="submit" class="btn btn-log btn-block btn-thm2">Suivant</button>
    								{{-- <div class="divide">
    									<span class="lf_divider">Ou</span>
    									<hr>
    								</div>
    								<div class="row mt40">
    									<div class="col-lg">
    										<button type="submit" class="btn btn-block color-white bgc-fb mb0"><i class="fa fa-facebook float-left mt5"></i> Facebook</button>
    									</div>
    									<div class="col-lg">
    										<button type="submit" class="btn btn2 btn-block color-white bgc-gogle mb0"><i class="fa fa-google float-left mt5"></i> Google</button>
    									</div>
    								</div> --}}
    							</form>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</section>
    </div>
    {{-- @include('layouts.scriptntelput') --}}

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src="{{asset('asset/js/intlTelInput.js')}}"></script>
<script src="{{asset('asset/js/utils.js')}}"></script>
<script>
    var CSRFToken = "{{ csrf_token() }}";


    var inputPhoneCustomer = document.querySelector("#phone_number");

        var   errorMsg = document.querySelector("#error-msgPhoneShop"),
            validMsg = document.querySelector("#valid-msgPhoneShop");

        // here, the index maps to the error code returned from getValidationError - see readme
        var errorMap = [ "Numéro Invalide !", "Code de pays invalide !", "Numéro de téléphone Trop Court", "Numéro de téléphone Trop Long", "Numéro Invalide"];
        var iti = window.intlTelInput(inputPhoneCustomer, {
            // allowDropdown: false,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            dropdownContainer: document.body,
            // excludeCountries: ["us"],
            // formatOnDisplay: false,
            initialCountry: "auto",
            separateDialCode: true,
            geoIpLookup: function(success, failure) {
                $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "CI";
                    // var countryCode = resp.country;
                    success(countryCode);
                });
            },
            // hiddenInput: "full_number",
            // localizedCountries: { 'de': 'Deutschland' },
            //nationalMode: true,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            placeholderNumberType: "MOBILE",
            // preferredCountries: ['cn', 'jp'],
            utilsScript: "{{asset('asset/js/utils.js?1562189064761')}}",
            // utilsScript: "{{asset('customer/js/utils.js')}}",
        });
        var reset = function() {
            inputPhoneCustomer.classList.remove("error");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };

        // on blur: validate
        inputPhoneCustomer.addEventListener('blur', function() {
            reset();
            if (inputPhoneCustomer.value.trim()) {
                if (iti.isValidNumber()) {
                    $("#dial_code").val(iti.getSelectedCountryData().dialCode);
                   // $("#phone_number").val(iti.);
                    $("#country_code").val(iti.getSelectedCountryData().iso2.toUpperCase());
                    //console.log(iti.s.phone);
                    validMsg.classList.remove("hide");
                } else {
                    inputPhoneCustomer.classList.add("error");
                    var errorCode = iti.getValidationError();
                    errorMsg.innerHTML = errorMap[errorCode];
                    errorMsg.classList.remove("hide");
                }
            }
        });

        // on keyup / change flag: reset
        inputPhoneCustomer.addEventListener('change', reset);
        inputPhoneCustomer.addEventListener('keyup', reset);
</script>

@endsection


