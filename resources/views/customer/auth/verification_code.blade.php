{{-- @extends('customer.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('customer.verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
@extends('layouts.master')

@section('title')
    Code de Vérification
@endsection


@section('content')
<div class="mb30">
    <section class="our-log bgc-fa">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-lg-6 offset-lg-3">
					<div class="login_form inner_page">
                        <div class="heading">
                            <h3 class="text-center">Renseigner le code reçu par SMS pour valider vôtre inscription</h3>
                        </div>
                        <div class="details">
                            <form action="{{ route('verify_otp_by_phone') }}" method="POST" id="verification_code_form">
                                @csrf
                                <br>
                                <p class="text-center"></p>
                                <!-- Nom, prénoms -->
                                <div class="container my-3">
                                    <div class="form-group">
                                        <input class="form-control {{ $errors->has('verification_code')?'is-invalid':'' }}" type="text" value="{{ old('verification_code') }}" required name="verification_code" placeholder="Entrer le code de vérification">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-log btn-block btn-thm2">
                                    Vérifier
                                </button>
                                <div class="divide">
                                    <span class="lf_divider">Ou</span>
                                    <hr>
                                </div>
                                <p class="text-center">Vous avez déjà un compte? <a class="text-thm" href="{{ route('customer.login') }}">Se connecter</a></p>
                            </form>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

