@extends('layouts.customer_master')

@section('title')
    Minkatoo - Détails Commandes
@endsection

@section('css')
    <style>
        a {
            text-decoration: none !important;
        }

        .content {
            min-height: 100px !important;
            padding: 20px;
            min-width: 100%;
            margin-right: 0 !important;
            margin-left: 0 !important;
        }

        .badge-secondary {
            background-color: #6c757d6b !important;
        }
</style>
@endsection

@section('content')
<div class="d-flex" id="wrapper">

    <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading">
            {{-- <img src="{{ asset(!is_null(
            Auth::guard('customer')->user()->photo_url) ? Auth::guard('customer')->user()->photo_url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQEyza1-CKEJK6Hyq50yE8oOpyba5HbJaCqMioudHzaxVh-a9Is')}}"
            width="50"> --}}
            @if(!empty($customer->photo_url))
        <img src="{{ asset('storage/'.Auth::guard('customer')->user()->photo_url)}}" width="50" height="50" alt='Image de profil'/>
        @else
        <img src={{ asset("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQEyza1-CKEJK6Hyq50yE8oOpyba5HbJaCqMioudHzaxVh-a9Is") }} width="50" height="50" alt="">
        @endif
            Mon Espace 
        </div>
        <div class="list-group list-group-flush">
          <a href="{{ route('customer.requests') }}" class=" list-group-item list-group-item-action bg-light">Mes Commandes</a>
          <a href="{{ route('customer.profile', [ Auth::guard('customer')->user()->id ]) }}" class="list-group-item list-group-item-action bg-light">Mon profil</a>
        </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <button class="btn btn-primary" id="menu-toggle"><i class="fa fa-bars"></i></button>
        
            {{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button> --}}
        
            {{-- <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                  <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                  </div>
                </li>
              </ul>
            </div> --}}
        
        </nav>

      <div class="container-fluid">
        {{-- <h1 class="mt-4">Simple Sidebar</h1>
        <p>The starting state of the menu will appear collapsed on smaller screens, and will appear non-collapsed on larger screens. When toggled using the button below, the menu will change.</p>
        <p>Make sure to keep all page content within the <code>#page-content-wrapper</code>. The top navbar is optional, and just for demonstration. Just create an element with the <code>#menu-toggle</code> ID which will toggle the menu when clicked.</p> --}}

        <h3 class="mt-4 mb-4">Détails</h3>
        <div>
            <div class="" id="app">
                    <div class="col-md-8">
                        <div class="">
                            <div class="header">
                                <p class="category">
                                    Détails de la Commande.
                                </p>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-4"><strong>Service<span class="pull-right">:</span></strong></div>
                                    <div class="col-md-8">
                                        @if($customerRequest->service_slug == "acte-deces")
                                            Hotels
                                        @elseif($customerRequest->service_slug == "acte-naissance")
                                            Location voiture 
                                        {{-- @elseif($customerRequest->service_slug == "celebration-mariage")
                                            Célébration de mariage
                                        @elseif($customerRequest->service_slug == "declaration-commerce")
                                            Déclaration de commerce
                                        @elseif($customerRequest->service_slug == "legalisation-document")
                                            Légalisation de documents
                                        @elseif($customerRequest->service_slug == "acte-mariage")
                                            Acte de mariage
                                        @elseif($customerRequest->service_slug == "acte-deces")
                                            Acte de commerce --}}
                                        @else
                                            Service inconnu
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4"><strong>Date<span class="pull-right">:</span></strong></div>
                                    <div class="col-md-8">{{ $customerRequest->created_at }}</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4"><strong>Mode de paiement<span class="pull-right">:</span></strong></div>
                                    <div class="col-md-8">
                                        {{-- {{ !is_null($customerRequest->payment_method_slug) ? $customerRequest->payment_method_slug : '---' }} --}}
                                        @if (!empty($customerRequest))
                                            @if($customerRequest->payment_method_slug == "cash")
                                            A la livraison
                                            @else
                                            En ligne
                                            @endif
                                            @else
                                            ---
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4"><strong>Status actuel<span class="pull-right">:</span></strong></div>
                                    <div class="col-md-8">

                                        @if($customerRequest->current_status == "waiting")
                                            <span class="badge badge-secondary text-warning">Commande en attente</span>
                                        @elseif($customerRequest->current_status == "failed")
                                            <span class="badge badge-secondary text-danger">Echec de la commande</span>
                                        @elseif($customerRequest->current_status == "cancel")
                                            <span class="badge badge-secondary">Commande Annulée</span>
                                        @elseif($customerRequest->current_status == "successful")
                                            <span class="badge badge-secondary text-success">Commande réussie</span>
                                        @elseif($customerRequest->current_status == "started")
                                            <span class="badge badge-secondary text-success">Commande démarrée</span>
                                        @elseif($customerRequest->current_status == "managed")
                                            <span class="badge badge-secondary text-success">Commande gérée</span>
                                        @elseif($customerRequest->current_status == "edited")
                                            <span class="badge badge-secondary text-success">Commande éditée</span>
                                        @elseif($customerRequest->current_status == "ready-for-delivery")
                                            <span class="badge badge-secondary text-info">Prête à la livraison</span>
                                        @elseif($customerRequest->current_status == "waiting-customer-delivery")
                                            <span class="badge badge-secondary text-info">En attente de livraison au client</span>
                                        {{-- @elseif($customerRequest->current_status == "waiting-customer-pickup")
                                            <span class="badge badge-secondary text-info">En attente de ramassage au client</span> --}}
                                        @else
                                            <span class="badge badge-secondary text-warning">Commande en attente</span>
                                        @endif
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4"><strong>Adresse de livraison<span class="pull-right">:</span></strong></div>
                                    <div class="col-md-8">
                                        @if($customerRequest->delivery_address != null)
                                            {{$customerRequest->delivery_address}}
                                        @else
                                            Aucune adresse
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

@endsection

@section('scripts')
    <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('customer_wides/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('customer_wides/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
@endsection
