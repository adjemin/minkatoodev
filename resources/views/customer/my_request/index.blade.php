@extends('layouts.customer_master')

@section('title')
    Minkatoo - Mes Commandes
@endsection

@section('css')
    <style>
        a {
            text-decoration: none !important;
        }
    </style>
@endsection

@section('content')
<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
<div class="bg-light border-right" id="sidebar-wrapper">
    <div class="sidebar-heading">
        <img src="{{ asset(!is_null(
        Auth::guard('customer')->user()->photo_url) ? asset('storage/' .Auth::guard('customer')->user()->photo_url): 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQEyza1-CKEJK6Hyq50yE8oOpyba5HbJaCqMioudHzaxVh-a9Is')}}"
        width="50">
        Mon Espace 
    </div>
    <div class="list-group list-group-flush">
      <a href="{{ route('customer.requests') }}" class=" list-group-item list-group-item-action bg-light">Mes Commandes</a>
      <a href="{{ route('customer.profile', [ Auth::guard('customer')->user()->id ]) }}" class="list-group-item list-group-item-action bg-light">Mon profil</a>
    </div>
  </div>
  <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <button class="btn btn-primary" id="menu-toggle"><i class="fa fa-bars"></i></button>
        
        {{--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button> --}}
        
            {{-- <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                  <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                  </div>
                </li>
              </ul>
            </div> --}}
        
        </nav>

      <div class="container-fluid">
        {{-- <h1 class="mt-4">Simple Sidebar</h1>
        <p>The starting state of the menu will appear collapsed on smaller screens, and will appear non-collapsed on larger screens. When toggled using the button below, the menu will change.</p>
        <p>Make sure to keep all page content within the <code>#page-content-wrapper</code>. The top navbar is optional, and just for demonstration. Just create an element with the <code>#menu-toggle</code> ID which will toggle the menu when clicked.</p> --}}

        <h3 class="mt-4 mb-4">Mes Commandes</h3>
        {{-- <a href="{{route('request_form')}}" rel="tooltip" title="Détails">
          <button class="btn btn-accent pull-right mb-4" id="menu-toggle"><i class="fa fa-add"></i>Faire une Commande</button>
        </a> --}}
        <div>
            <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Service</th>
                    <th scope="col">Status</th>
                    <th scope="col">Montant</th>
                    <th scope="col">Paiement</th>
                    <th scope="col" colspan="3">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($customerRequests->reverse() as $customerRequest)
                    <tr>
                        <th scope="row">{{ $customerRequest->id }}</th>
                        <td>
                            @if($customerRequest->service_slug == "acte-deces")
                                Hotels
                            @elseif($customerRequest->service_slug == "acte-naissance")
                                Location voiture 
                            {{-- @elseif($customerRequest->service_slug == "celebration-mariage")
                                Célébration de mariage
                            @elseif($customerRequest->service_slug == "declaration-commerce")
                                Déclaration de commerce
                            @elseif($customerRequest->service_slug == "legalisation-document")
                                Légalisation de documents
                            @elseif($customerRequest->service_slug == "acte-mariage")
                                Acte de mariage
                            @elseif($customerRequest->service_slug == "acte-deces")
                                Acte de commerce --}}
                            @else
                                Service inconnu
                            @endif
                        </td>
                        <td>
                            @if($customerRequest->current_status == "waiting")
                                <span class="badge badge-secondary text-warning">Commande en attente</span>
                            @elseif($customerRequest->current_status == "failed")
                                <span class="badge badge-secondary text-danger">Echec de la commande</span>
                            @elseif($customerRequest->current_status == "cancel")
                                <span class="badge badge-secondary">Commande Annulée</span>
                            @elseif($customerRequest->current_status == "successful")
                                <span class="badge badge-secondary text-success">Commande réussie</span>
                            @elseif($customerRequest->current_status == "started")
                                <span class="badge badge-secondary text-success">Commande démarrée</span>
                            @elseif($customerRequest->current_status == "managed")
                                <span class="badge badge-secondary text-success">Commande gérée</span>
                            @elseif($customerRequest->current_status == "edited")
                                <span class="badge badge-secondary text-success">Commande éditée</span>
                            @elseif($customerRequest->current_status == "ready-for-delivery")
                                <span class="badge badge-secondary text-info">Prête à la livraison</span>
                            @elseif($customerRequest->current_status == "waiting-customer-delivery")
                                <span class="badge badge-secondary text-info">En attente de livraison au client</span>
                            {{-- @elseif($customerRequest->current_status == "waiting-customer-pickup")
                                <span class="badge badge-secondary text-info">En attente de ramassage au client</span> --}}
                            @else
                                <span class="badge badge-secondary text-warning">Commande en attente</span>
                            @endif
                        </td>
                        <td>{{ $customerRequest->amount }}  {{ $customerRequest->currency_code }}</td>
                        <td>
                            @if (!is_null($customerRequest) || !empty($customerRequest))
                                @if($customerRequest->payment_method_slug == "cash")
                                A la livraison
                                @else
                                En ligne
                                @endif
                                @else
                                ---
                            @endif
                        </td>
                        <td class="">
                            <a href="{{route('customer.request.show', [$customerRequest->id])}}" rel="tooltip" title="Détails">
                                <button type="button" class="btn btn-secondary btn-simple btn-xs">
                                    {{-- <i class="fa fa-eye"></i> --}}Voir
                                </button>
                            </a>
                            {{-- <a href="{{ route('agents.edit', $customerRequest->id) }}" rel="tooltip" title="Modifier">
                                <button type="button" rel="tooltip" class="btn btn-secondary btn-simple btn-xs">
                                    </i>Modifier
                                </button>
                            </a> --}}
                            {{-- <a href="#!" onclick="if(confirm('Voulez-vous vraiment éffectuer cette action?')){
                                document.getElementById('delete_event_{{$loop->iteration}}').submit();
                                }" rel="tooltip" title="Supprimer">
                                <button type="button" rel="tooltip" class="btn btn-danger btn-simple btn-xs">
                                    Supprimer
                                </button>
                            </a> --}}
                            {{-- <form id="delete_event_{{$loop->iteration}}" style="display: none;" action="{{route('agents.destroy', $customerRequest->id)}}" method="POST">
                                <input type="hidden" name="_method" value="delete"/>
                                {{ csrf_field() }}
                            </form> --}}
                        </td>
                      </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- {{ $customerRequest->links() }} --}}
        </div>
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

@endsection

@section('scripts')
    <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('customer_wides/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('customer_wides/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>
@endsection
