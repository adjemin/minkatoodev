@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('hotelSpecifications.index') !!}">Spécifications hotel</a>
      </li>
      <li class="breadcrumb-item active">Create</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                    <strong>Create Spécifications hotel</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'hotelSpecifications.store', 'files'=>true])  !!}

                                   @include('hotel_specifications.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
