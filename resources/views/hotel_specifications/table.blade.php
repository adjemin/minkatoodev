<div class="table-responsive-sm">
    <table class="table table-striped" id="foodSpeialties-table">
        <thead>
        <th>Icon</th>
        <th>Nom</th>
        <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($hotelSpecifications as $hotelSpecification)
            <tr>
                <td>
                    <img src="{{ $hotelSpecification->icon }}" height="70" width="70">
                </td>
                <td>{{ $hotelSpecification->name }}</td>
                <td>
                    {!! Form::open(['route' => ['hotelSpecifications.destroy', $hotelSpecification->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('hotelSpecifications.show', [$hotelSpecification->id]) }}"
                           class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('hotelSpecifications.edit', [$hotelSpecification->id]) }}"
                           class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
