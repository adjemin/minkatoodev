@extends('layouts.main')

@section('header')

<header>
    {{-- // Included connexion state from layouts.main --}}
    <!-- Main Header Image -->
    <div>
        <img src="img/sea-dawn-nature-sky-127160.jpg" alt="" class="img-fluid">
    </div>
    <div id="navBarSearch" class="col-12 col-sm-10 col-md-8 col-lg-6">
        <!-- <form action=""> -->
            <span class="">
                <!-- Place Holder Icon -->
                <span>
                    <img src="img/placeholder-for-map@1X.png" alt="" width="15px">
                </span>
                <input type="text" placeholder="Abidjan, Côte d'ivoire">
            </span>
            <button type="submit">
                <!-- Search icon -->
                <img src="img/search (1)@1X.png" alt="" width="15px">
            </button>
            <!-- </form> -->
        </div>
    </header>
@endsection
@section('main')
    <main>
        <!-- Navigation catégories -->
        <nav id="iconNavbar" class="container">
            <ul class="">
                {{-- // TODO turn into TabView --}}
                <li class="iconItem">
                    <a href="/hotels" class="">
                        <p>Hotels</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/hotel@1X.png" alt="Hotels">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="/restaurants">

                        <p>Restaurants</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/plate-fork-and-knife@1X.png" alt="restaurants">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="excursion">

                        <p>Excursion</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/sunset@1X.png" alt="Excursion">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="/locations">

                        <p>Location Voiture</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/car@1X.png" alt="location Voiture">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="musees">

                        <p>Musée</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/museum@1X.png" alt="musée">

                        </div>
                    </a>
                </li>
                <!--  -->
            </ul>
        </nav>
        <!--  -->
        <!-- Section hotels à proximité -->
        <section class="contentWrapper container">
            <div class="sectionHead">
                <h2>Hotels à proximité</h2>
                <a href="" class="flatLink color">Tout afficher</a>
            </div>
            <div class="row">
                @for ($i = 0; $i < 4; $i++)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                    <div class="card">
                        <div class="card-img">
                            <img src="img/bedroom.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="card-content">
                            <h4>Hotel Golf Club</h4>
                            <div class="rating">
                                <!-- Stars -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                <span>(15)</span>
                            </div>
                            <div class="card-footer-text">
                                <img src="img/placeholder-for-map@1X.png" alt=""> <!-- mappin icon -->
                                <span>
                                    76 km
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                @endfor
                <!-- Card Item -->
            </div>
        </section>
        <!-- Section restaurants à proximité -->
        <section class="contentWrapper container">
            <div class="sectionHead">
                {{-- // TODO set responsive font for title --}}
                <h3>Restaurants à proximité</h3>
                <a href="" class="flatLink color">Tout afficher</a>
            </div>
            <div class="row">
                @for ($i = 0; $i < 4; $i++)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                        <div class="card">
                            <div class="card-img">
                                <img src="img/dining-table.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="card-content">
                                <h4>Ivoire Golf Club</h4>
                                <div class="rating">
                                    <!-- Stars -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                    <span>(15)</span>
                                </div>
                                <div class="card-footer-text">
                                    <img src="img/placeholder-for-map@1X.png" alt=""> <!-- mappin icon -->
                                    <span>
                                        76 km
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endfor
                    <!-- Card Item -->
                </div>
            </section>
            <!-- Banner Ssection excursions -->
            <section id="excursions" class="banner">
                <div class="banner-head container">

                    <h1>Nos Suggestions</h1>
                    <h3>Excursions en ce moments</h3>
                </div>
                <div class="banner-img">
                    <div class="banner-img-overlay">
                        <div class="banner-img-text container">
                            <h1 class="title">Excursion au domaine bini</h1>
                            <div>Abidjan, Côte d'Ivoire</div>
                            <div>Trip in Africa</div>
                        </div>
                    </div>


                </div>
            </section>

            <!-- Section Meilleures activités à abidjan -->
            <section class="contentWrapper container">
                <div class="sectionHead">
                    <h1>Meilleures activités à abidjan</h1>
                    <!-- <a href="" class="flatLink color">Tout afficher</a> -->
                    <span class="spacer-10">

                    </span>
                </div>

                <div class="row">
                    @for ($i = 0; $i < 4; $i++)
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                        <div class="card">
                            <div class="card-img">
                                <img src="img/bedroom.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="card-content">
                                <h4>Hotel Golf Club</h4>
                                <div class="rating">
                                    <!-- Stars -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                    <span>(15)</span>
                                </div>
                                <div class="card-footer-text">
                                    <img src="img/placeholder-for-map@1X.png" alt=""> <!-- mappin icon -->
                                    <span>
                                        76 km
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endfor
                    <!-- Card Item -->
                </div>
            </section>

            <!--  -->
        </main>
        <footer>
            <!--  -->
        </footer>
    </body>

    </html>
@endsection
