
<div class="box">

    <!-- /.box-header -->
    <div class="box-body" style="overflow-x: scroll;">
      <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive-sm">
        <thead>
            <tr>
            <th>Identifiant Facture</th>
        <th>Méthode Paiement</th>
        <th>Réference Paiement</th>
        <th>Montant</th>
        <th>Devise Code</th>
        <th>Identifiant Créateur</th>
        <th>Nom Créateur</th>
        <th>Créateur</th>
        <th>Status</th>
        <th>En attente</th>
        <th>Terminé</th>
        <th>Identifiant Trans Passerelle paiement</th>
        <th>Passerelle paiement Modifié</th>
        <th>Passerelle paiement Devise</th>
        <th>Passerelle paiement Montant</th>
        <th>Passerelle paiement Payid</th>
        <th>Date paiement Passerelle paiement</th>
        <th>eure paiement Passerelle paiement</th>
        <th>Passerelle paiement message d'erreur</th>
        <th>Passerelle paiement méthode paiement</th>
        <th>Passerelle paiement Indicatif Num</th>
        <th>Passerelle paiement Numero</th>
        <th>Passerelle paiement Ipn Ack</th>
        <th>Passerelle paiement créé à</th>
        <th>Passerelle paiement modifiéé à</th>
        <th>Passerelle paiement Cpm résultat</th>
        <th>Passerelle paiement Trans status</th>
        <th>Passerelle paiement désignation</th>
        <th>Passerelle paiement acheteur</th>
        <th>Passerelle paiement signature</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($invoicePayments as $invoicePayment)
            <tr>
                <td>{{ $invoicePayment->invoice_id }}</td>
            <td>{{ $invoicePayment->payment_method }}</td>
            <td>{{ $invoicePayment->payment_reference }}</td>
            <td>{{ $invoicePayment->amount }}</td>
            <td>{{ $invoicePayment->currency_code }}</td>
            <td>{{ $invoicePayment->creator_id }}</td>
            <td>{{ $invoicePayment->creator_name }}</td>
            <td>{{ $invoicePayment->creator }}</td>
            <td>{{ $invoicePayment->status }}</td>
            <td>
                @if($invoicePayment->is_waiting == 0)
                Non
                @else
                Oui
                @endif
            </td>
            <td>
                 @if($invoicePayment->is_completed == 0)
                 Non
                 @else
                 Oui
                 @endif
            </td>
            <td>{{ $invoicePayment->payment_gateway_trans_id }}</td>
            <td>{{ $invoicePayment->payment_gateway_custom }}</td>
            <td>{{ $invoicePayment->payment_gateway_currency }}</td>
            <td>{{ $invoicePayment->payment_gateway_amount }}</td>
            <td>{{ $invoicePayment->payment_gateway_payid }}</td>
            <td>{{ $invoicePayment->payment_gateway_payment_date }}</td>
            <td>{{ $invoicePayment->payment_gateway_payment_time }}</td>
            <td>{{ $invoicePayment->payment_gateway_error_message }}</td>
            <td>{{ $invoicePayment->payment_gateway_payment_method }}</td>
            <td>{{ $invoicePayment->payment_gateway_phone_prefixe }}</td>
            <td>{{ $invoicePayment->payment_gateway_cel_phone_num }}</td>
            <td>{{ $invoicePayment->payment_gateway_ipn_ack }}</td>
            <td>{{ $invoicePayment->payment_gateway_created_at }}</td>
            <td>{{ $invoicePayment->payment_gateway_updated_at }}</td>
            <td>{{ $invoicePayment->payment_gateway_cpm_result }}</td>
            <td>{{ $invoicePayment->payment_gateway_trans_status }}</td>
            <td>{{ $invoicePayment->payment_gateway_designation }}</td>
            <td>{{ $invoicePayment->payment_gateway_buyer_name }}</td>
            <td>{{ $invoicePayment->payment_gateway_signature }}</td>
                <td>
                    {!! Form::open(['route' => ['invoicePayments.destroy', $invoicePayment->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                       <!--  <a href="{{ route('invoicePayments.show', [$invoicePayment->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a> -->
                        <!-- <a href="{{ route('invoicePayments.edit', [$invoicePayment->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a> -->
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Êtes vous sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>