  
	<!-- jQuery 3 -->
	<script src="{{asset('admin/assets/vendor_components/jquery/dist/jquery.js')}}"></script>
	
	<!-- jQuery UI 1.11.4 -->
	<script src="{{asset('admin/assets/vendor_components/jquery-ui/jquery-ui.js')}}"></script>
	
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	
	<!-- popper -->
	<script src="{{asset('admin/assets/vendor_components/popper/dist/popper.min.js')}}"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="{{asset('admin/assets/vendor_components/bootstrap/dist/js/bootstrap.js')}}"></script>	
	
	<!-- ChartJS -->
	<script src="{{asset('admin/assets/vendor_components/chart-js/chart.js')}}"></script>
	
	<!-- Sparkline -->
	<script src="{{asset('admin/assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.js')}}"></script>
	
	<!-- jvectormap -->
	<script src="{{asset('admin/assets/vendor_plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>	
	<script src="{{asset('admin/assets/vendor_plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
	
	<!-- jQuery Knob Chart -->
	<script src="{{asset('admin/assets/vendor_components/jquery-knob/js/jquery.knob.js')}}"></script>
	
	<!-- daterangepicker -->
	<script src="{{asset('admin/assets/vendor_components/moment/min/moment.min.js')}}"></script>
	<script src="{{asset('admin/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
	
	<!-- datepicker -->
	<script src="{{asset('admin/assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js')}}"></script>
	
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{{asset('admin/assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js')}}"></script>
	
	<!-- Slimscroll -->
	<script src="{{asset('admin/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
	
	<!-- FastClick -->
	<script src="{{asset('admin/assets/vendor_components/fastclick/lib/fastclick.js')}}"></script>
	
	<!-- minimal_admin App -->
	<script src="{{asset('admin/js/template.js')}}"></script>
	
	<!-- minimal_admin dashboard demo (This is only for demo purposes) -->
	<script src="{{asset('admin/js/pages/dashboard.js')}}"></script>
	
	<!-- minimal_admin for demo purposes -->
	<script src="{{asset('admin/js/demo.js')}}"></script>
	
	<!-- weather for demo purposes -->
	<script src="{{asset('admin/assets/vendor_plugins/weather-icons/WeatherIcon.js')}}"></script>

    	<!-- This is data table -->
        <script src="{{asset('admin/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js')}}"></script>
    
        <!-- start - This is for export functionality only -->
        <script src="{{asset('admin/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('admin/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js')}}"></script>
        <script src="{{asset('admin/assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js')}}"></script>
        <script src="{{asset('admin/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js')}}"></script>
        <script src="{{asset('admin/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js')}}"></script>
        <script src="{{asset('admin/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
        <script src="{{asset('admin/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js')}}"></script>
        <!-- end - This is for export functionality only -->
        
        <!-- minimal_admin for Data Table -->
        <script src="{{asset('admin/js/pages/data-table.js')}}"></script>
     