<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>{{ config('app.name') }} &#8211; @yield('title')</title>

    {{-- <link rel="icon" type="image/png" href="{{asset('front/images/LOGO_MINKATOO.png')}}"> --}}

    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,800;0,900;1,500&display=swap" rel="stylesheet">


    <!-- <link rel="stylesheet" href="css/responsive-grid.css"> -->

    {{-- Bootstrap CDN --}}

    <link rel="stylesheet" href="{{asset('css/bootstrap-grid.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    {{-- W3.CSS CDN --}}
    {{-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> --}}

    {{-- // Main Page Style CSS --}}

    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/media-queries.css">
    {{--  --}}
    <script src="{{asset('js/app.js')}}"></script>
    {{--  --}}
    @yield('styles')

    @yield('css')


    <style>
    body{
        font-size: 16px;
    }
    </style>
</head>

<body>
    <!-- Logo plus connexion state -->
    @include('front.inc.page_header')
    @yield('header')
    {{-- <main> --}}
        @yield('main')
    {{-- </main> --}}
    <!-- Our Footer -->
    <div class="wrapper">
        <section class="footer_one">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 pr0 pl0">
                        <div class="footer_about_widget">
                            <h4 style="color: #e67e2a;
                                    font-weight: bold;
                                    margin-bottom: 30px;
                                    font-size: 18px;">A propos de nous</h4>
                            <p >Nous désirons vous faciliter la vie en vous permettant de decouvrir sur notre plateforme toutes les meilleurs offres du moment en Côte d'ivoire en seul lieu.</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                        <div class="footer_qlink_widget">
                            <h4 style="color: #e67e2a;
                                        font-weight: bold;
                                        margin-bottom: 30px;
                                        font-size: 18px;">Liens Utiles</h4>
                            <ul class="list-unstyled">
                                <li><a href="/hotels" '>Hotels</a></li>
                                <li><a href="/restaurants" >Restaurants</a></li>
                                <li><a href="/excursion" >Excursion</a></li>
                                <li><a href="/location" >Location voiture</a></li>
                                <li><a href="/musees" >Musée</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                        <div class="footer_contact_widget">
                            <h4 style="color: #e67e2a;
                                            font-weight: bold;
                                            margin-bottom: 30px;
                                            font-size: 18px;">Nous Contacter</h4>
                            <ul class="list-unstyled">
                                <li><a href="#"><span class="__cf_email__" data-cfemail="4f262129200f2926212b27203a3c2a612c2022">dikadoban@minkatoo.net</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                        <div class="footer_social_widget">
                            <h4 style="color: #e67e2a;
                                        font-weight: bold;
                                        margin-bottom: 30px;
                                        font-size: 18px;">Nous Suivre</h4>
                            <ul class="mb30">
                                <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                            {{-- <h4>S'abonner à la newsletter</h4>
                            <form class="footer_mailchimp_form">
                                <div class="form-row align-items-center">
                                    <div class="col-auto">
                                        <input type="email" class="form-control mb-2" id="inlineFormInput" placeholder="Entrez votre adresse email">
                                    </div>
                                    <div class="col-auto">
                                        <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-angle-right"></i></button>
                                    </div>
                                </div>
                            </form> --}}


                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Our Footer Bottom Area -->
        <section class="footer_middle_area pt30 pb30" style="margin: -32px  0px !important;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-6">
                        <!--div class="footer_menu_widget">
                            <ul>
                                <li class="list-inline-item"><a href="#">Home</a></li>
                                <li class="list-inline-item"><a href="#">Listing</a></li>
                                <li class="list-inline-item"><a href="#">Property</a></li>
                                <li class="list-inline-item"><a href="#">Pages</a></li>
                                <li class="list-inline-item"><a href="#">Blog</a></li>
                                <li class="list-inline-item"><a href="#">Contact</a></li>
                            </ul>
                        </div-->
                    </div>
                    <div class="col-lg-6 col-xl-6">
                        <div class="copyright-widget text-right">
                            <p>© 2020 Minkatoo. Tous droits réservés <a href="https://adjemin.com/Products_Services" target="_blank" style="color: chocolate"> Adjemin</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <a class="scrollToHome" href="#" style="display: inline;"><i class="flaticon-arrows" style="box-sizing: border-box; font-style: italic;"></i></a>
    </div>




    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script type="text/javascript">
        /* ----- Scroll To top ----- */
        function scrollToTop() {
            $(window).scroll(function(){
                if ($(this).scrollTop() > 600) {
                    $('.scrollToHome').fadeIn();
                } else {
                    $('.scrollToHome').fadeOut();
                }
            });

            //Click event to scroll to top
            $('.scrollToHome').on('click',function(){
                $('html, body').animate({scrollTop : 0},800);
                return false;
            });
        }
    </script>

    <style>
        .footer_about_widget p{
            font-size: 14px;
            font-family: 'Montserrat';
            color: #8a99b3;
            line-height: 2.143;
            margin-bottom: 0;
        }
        .footer_about_widget.home5 p,
        .footer_qlink_widget.home5 li a,
        .footer_contact_widget.home5 li a,
        .footer_social_widget.home5 li a{
        color: #98a2aa;
        }
        /* .footer_about_widget.home6 p,
        .footer_qlink_widget.home6 li a,
        .footer_contact_widget.home6 li a{
        color: #777777;
        } */
        .footer_social_widget.home6 li a{
        color: #484848;
        }
        /* .footer_about_widget.home3 p{
        color: #777777;
        } */
        .footer_contact_widget li{
        padding-bottom: 0;
        }
        .footer_qlink_widget{
        padding-left: 70px;
        }
        .footer_about_widget h4,
        .footer_qlink_widget h4,
        .footer_contact_widget h4,
        .footer_social_widget h4,
        .footer_apps_widget h4{
            color: #e67e2a;
            font-weight: bold;
            margin-bottom: 25px;
            font-size: 18px;
        /* font-family: 'Montserrat', sans-serif; */

        }
        .footer_about_widget.home3 h4,
        .footer_qlink_widget.home3 h4,
        .footer_contact_widget.home3 h4,
        .footer_social_widget.home3 h4,
        .footer_apps_widget.home3 h4{
                color: #484848;
                font-weight: bold;
                margin-bottom: 25px;
        }
        .footer_about_widget.home3 li a,
        .footer_qlink_widget.home3 li a,
        .footer_contact_widget.home3 li a{
        color: #777777;
        }
        .footer_about_widget.home6 h4,
        .footer_qlink_widget.home6 h4,
        .footer_contact_widget.home6 h4,
        .footer_social_widget.home6 h4,
        .footer_apps_widget.home6 h4{
        /* : "Nunito"; */
        color: #484848;
        font-weight: bold;
        line-height: 1.333;
        }
        .footer_qlink_widget.home6 li:hover a{
        color: #484848;
        }
        .footer_about_widget li a,
        .footer_qlink_widget li a,
        .footer_contact_widget li a{
        font-size: 14px;
        /* : "Nunito"; */
        color: #8a99b3;
        line-height: 2.429;
        -webkit-transition: all 0.3s ease 0s;
        -moz-transition: all 0.3s ease 0s;
        -o-transition: all 0.3s ease 0s;
        transition: all 0.3s ease 0s;
        }
        .footer_about_widget li:hover a,
        .footer_qlink_widget li:hover a,
        .footer_contact_widget li:hover a {
        color: #ffffff;
        padding-left: 8px;
        }
        .footer_apps_widget .app_grid .apple_btn,
        .footer_apps_widget .app_grid .play_store_btn{
        background-color: #484848;
        border: none;
        border-radius: 4px;
        height: 75px;
        margin-bottom: 15px;
        margin-right: 0;
        width: 220px;
        }
        .footer_apps_widget .app_grid .apple_btn:hover,
        .footer_apps_widget .app_grid .play_store_btn:hover{
        background-color: #eb7206;
        }
        .footer_apps_widget .app_grid .play_store_btn{
        margin-bottom: 0;
        }
        .footer_apps_widget .app_grid .apple_btn span,
        .footer_apps_widget .app_grid .play_store_btn span{
        margin-right: 5px;
        }
        .footer_apps_widget .app_grid .apple_btn span.title,
        .footer_apps_widget .app_grid .play_store_btn span.title{
        color: #ffffff;
        }
        .footer_apps_widget .app_grid .apple_btn span,
        .footer_apps_widget .app_grid .apple_btn span.subtitle,
        .footer_apps_widget .app_grid .play_store_btn span,
        .footer_apps_widget .app_grid .play_store_btn span.subtitle{
        color: #969696;
        }
        .footer_apps_widget .app_grid .apple_btn:hover span,
        .footer_apps_widget .app_grid .apple_btn:hover span.subtitle,
        .footer_apps_widget .app_grid .play_store_btn:hover span,
        .footer_apps_widget .app_grid .play_store_btn:hover span.subtitle{
        color: #ffffff;
        }
        .footer_apps_widget.home3 .app_grid .apple_btn:hover,
        .footer_apps_widget.home3 .app_grid .play_store_btn:hover{
        background-color: #369fdb;
        }
        .footer_apps_widget .app_grid .apple_btn:focus,
        .footer_apps_widget.home3 .app_grid .apple_btn:active,
        .footer_apps_widget.home3 .app_grid .apple_btn:focus,
        .footer_apps_widget.home3 .app_grid .play_store_btn:active,
        .footer_apps_widget.home3 .app_grid .play_store_btn:focus{
        box-shadow: none;
        outline: none;
        }

        .copyright-widget p {
          font-size: 14px;
          /* font-family: "Nunito"; */
          color: #8a99b3;
          line-height: 2.143;
          margin-bottom: 0;
        }
        .copyright-widget.home3 p,
        .copyright-widget.home6 p{
          color: #777777;
        }

        </style>
</body>

</html>
