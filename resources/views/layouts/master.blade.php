<!DOCTYPE html>
<html dir="ltr" lang="fr">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="Minkatoo">
{{-- <meta name="csrf-token" content="{{ csrf_token() }}"/> --}}
@stack('token')

<!-- css file -->
<title>{{ config('app.name') }}- @yield('title') </title>
<link rel="stylesheet" href="{{asset('asset/css/intlTelInput.css')}}">
<link rel="icon" type="image/png" sizes="96x96" href="{{asset('front/images/LOGO_MINKATOO.png')}}">
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"/>
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="{{asset('asset/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('asset/css/style.css')}}">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,800;0,900;1,500&display=swap" rel="stylesheet">
<!-- Responsive stylesheet -->
<link rel="stylesheet" href="{{asset('asset/css/responsive.css')}}">
{{-- <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/Minkatoo/flag.png') }}"> --}}

{{-- //AdjeminPay --}}
<script src="https://cdn.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

{{-- Style --}}
@yield('css')


<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
<style>
    body {
        font-family: 'Montserrat';
    }
</style>
<!-- Favicon --
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
<link href="images/favicon.ico" sizes="128x128" rel="shortcut icon" />
-->
</head>
<body>
	<style media="screen">
	@media only screen and (max-width: 991px) {
			.display-mob {
				display: block;
			}
	}
	@media only screen and (min-width: 991px) {
			.display-mob {
				display: none;
			}
	}

	</style>
<div class="wrapper">
	<!-- Main Header Nav -->
	<header class="header-nav menu_style_home_one style2 navbar-scrolltofixed stricky main-menu">
		<div class="container">
		    <!-- Ace Responsive Menu -->
		    <nav>
		        <!-- Menu Toggle btn-->
		        <div class="menu-toggle">
		            <img class="nav_logo_img img-fluid" width="85" height="85" src="{{asset('front/images/LOGO_MINKATOO.png')}}" alt="header-logo.png">
		            <button type="button" id="menu-btn">
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		            </button>
		        </div>
		        <a href="/" class="navbar_brand float-left dn-smd">
		            <img class="logo1 img-fluid" width="95" height="95" src="{{asset('front/images/LOGO_MINKATOO.png')}}" alt="header-logo.png">
		            <img class="logo2 img-fluid" width="95" height="95" src="{{asset('front/images/LOGO_MINKATOO.png')}}" alt="header-logo2.png">
		            {{-- <span>Minkatoo</span> --}}
		        </a>
		        <!-- Responsive Menu Structure-->
		        <!--Note: declare the Menu style in the data-menu-style="horizontal" (options: horizontal, vertical, accordion) -->
                @if (Auth::guard('customer')->user())
                    <nav class="navbar navbar-static-top">
      			<!-- Sidebar toggle button-->
      			<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        		<span class="sr-only">Toggle navigation</span>
      			</a>

      			<div class="navbar-custom-menu">
        			<ul class="ace-responsive-menu text-right" data-menu-style="horizontal" >
		 				<!-- User Account: style can be found in dropdown.less -->
         				<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" style="color:black" href="#" id="navbarDropdownMenuLink" role="button"
							data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                				{{ strtoupper(Auth::guard('customer')->user()->last_name) }} {{ Auth::guard('customer')->user()->first_name }}
            				</a>
							<!-- Menu Body -->
							{{-- <ul class="dropdown-menu scale-up"> --}}
                				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 79px, 0px);">
                  					<div class="col-12 text-left">
										<a class="dropdown-item" href="{{ url('/')}}">Accueil</a>

										<a class="dropdown-item" href="{{ route('customerProfile', [ Auth::guard('customer')->user()->id ]) }}">Mon Profil</a>

                			<a class="dropdown-item" href="{{ route('customerRequest', [Auth::guard('customer')->user()->id]) }}">Mes Commandes</a>
									  </div>

                  					<div class="col-12 text-left">
                    					<a class="dropdown-item" href="{!! url('/logout') !!}" class="btn btn-default btn-flat"
                   							onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    						Déconnexion
                						</a>
                						<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                   					 			{{ csrf_field() }}
                						</form>
									</div>

                				</div>
							{{-- </ul>		<!-- /.row --> --}}
          				</li>
        			</ul>
      			</div>

                @else
                    <ul id="respMenu" class="ace-responsive-menu text-right" data-menu-style="horizontal">
	                	<li class="list-inline-item list_s"><a href="{{ route('customer.register') }}" > <span class="dn-lg">S'inscrire</span></a></li>
	                	<li class="list-inline-item add_listing home2"><a href="{{ route('customer.login') }}">Se connecter</a></li>
		        	</ul>
                @endif
		    </nav>
		</div>
	</header>

	<!-- Main Header Nav For Mobile -->
	<di id="page" class="stylehome1 h0 display-mob">
		<div class="mobile-menu">
			<div class="header stylehome1">
				<div class="main_logo_home2 text-center">
						 <img class="nav_logo_img img-fluid mt20" width="40" height="40" src="{{asset('images/minkatoologo.png')}}" alt="header-logo2.png">
						 <span class="mt20">Minkatoo</span>
				</div>
				<ul class="menu_bar_home2">
	        <li class="list-inline-item list_s">  </li>
					<li class="list-inline-item"><a href="#menu"><span></span></a></li>
				</ul>
			</div>
		</div><!-- /.mobile-menu -->
		<nav id="menu" class="stylehome1">
			<ul>
				<li><a href="{{ url('/')}}"><span class="flaticon-home"></span> Accueil</a></li>
				<li><a href="{{ route('customer.login') }}"><span class="flaticon-user"></span> Se connecter</a></li>
				<li><a href="{{ route('customer.register') }}"><span class="flaticon-edit"></span> S'inscrire</a></li>
			</ul>
		</nav>
	</di v>
	@yield('content')
	<!-- Our Footer -->
	<section class="footer_one">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 pr0 pl0">
					<div class="footer_about_widget">
						<h4>A propos de nous</h4>
						<p>Nous désirons vous faciliter la vie en vous permettant de decouvrir sur notre plateforme toutes les meilleurs offres du moment en Côte d'ivoire en seul lieu.</p>
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<div class="footer_qlink_widget">
						<h4>Liens Utiles</h4>
						<ul class="list-unstyled">
							<li><a href="/view/hotels">Hotels</a></li>
							<li><a href="/view/restaurants">Restaurants</a></li>
							<li><a href="/excursion">Excursion</a></li>
							<li><a href="/location">Location voiture</a></li>
							<li><a href="/musees">Musée</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<div class="footer_contact_widget">
						<h4>Contacts</h4>
						<ul class="list-unstyled">
							<li><a href="#"><span class="__cf_email__" data-cfemail="4f262129200f2926212b27203a3c2a612c2022">
                                dikadoban@minkatoo.com
                            </span></a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<div class="footer_social_widget">
						<h4>Nous Suivre</h4>
						<ul class="mb30">
							<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
						</ul>
						{{-- <h4>S'abonner à la newsletter</h4>
						<form class="footer_mailchimp_form">
						 	<div class="form-row align-items-center">
							    <div class="col-auto">
							    	<input type="email" class="form-control mb-2" id="inlineFormInput" placeholder="Entrez votre adresse email">
							    </div>
							    <div class="col-auto">
							    	<button type="submit" class="btn btn-primary mb-2"><i class="fa fa-angle-right"></i></button>
							    </div>
						  	</div>
						</form> --}}
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Our Footer Bottom Area -->
	<section class="footer_middle_area pt30 pb30">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-xl-6">
					<!--div class="footer_menu_widget">
						<ul>
							<li class="list-inline-item"><a href="#">Home</a></li>
							<li class="list-inline-item"><a href="#">Listing</a></li>
							<li class="list-inline-item"><a href="#">Property</a></li>
							<li class="list-inline-item"><a href="#">Pages</a></li>
							<li class="list-inline-item"><a href="#">Blog</a></li>
							<li class="list-inline-item"><a href="#">Contact</a></li>
						</ul>
					</div-->
				</div>
				<div class="col-lg-6 col-xl-6">
					<div class="copyright-widget text-right">
						<p>© 2020 Minkatoo. Tous droits réservés .<a href="https://adjemin.com/Products_Services" target="_blank" style="color: chocolate"> Développer par Adjemin</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>
<a class="scrollToHome" href="#"><i class="flaticon-arrows"></i></a>
</div>
<!-- Wrapper End -->
{{-- <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script type="text/javascript" src="{{ asset('asset/js/intlTelInput.js') }}"> </script> --}}

{{-- <script type="text/javascript" src="{{ asset('/public/asset/js/intlTelInput.js') }}"> </script> --}}

<script type="text/javascript" src="{{asset('asset/js/jquery-3.3.1.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/jquery-migrate-3.0.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/jquery.mmenu.all.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/ace-responsive-menu.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/isotop.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/snackbar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/simplebar.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/parallax.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/scrollto.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/jquery-scrolltofixed-min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/jquery.counterup.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/wow.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/slider.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/timepicker.js')}}"></script>
{{-- <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js'></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
crossorigin="anonymous"></script> --}}
<!-- Custom script for all pages -->
{{-- <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'> </script> --}}
{{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script type="text/javascript" src="{{asset('asset/js/script.js')}}"> </script>
<script src='https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js'> </script>
@stack('firebase')

</body>
</html>
