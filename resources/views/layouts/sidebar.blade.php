<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">

        <div class="info float-left">
            <p>  {{ Auth::user()->name }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
        <br><br><br> <br><br>
        <!-- /.search form -->
    </div>

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">

        <li class="{{ Request::is('home*') ? 'active' : '' }}">
            <a href="{{url('/home')}}">
                <i class="fa fa-home"></i> <span>Dashboard</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('customers*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('customers.index') }}">
                <i class="fa fa-users"></i>
                <span>Clients</span>
            </a>
        </li>
    <!-- <li class="nav-item {{ Request::is('customerSessions*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('customerSessions.index') }}">
        <i class="fa fa-user"></i>
        <span>Sessions Client</span>
    </a>
    </li> -->
        <li class="nav-item {{ Request::is('offers*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('offers.index') }}">
                <i class="fa fa-list"></i>
                <span>Offres</span>
            </a>
        </li>
    <!-- <li class="nav-item {{ Request::is('favoris*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('favoris.index') }}">
        <i class="fa fa-heart"></i>
        <span>Favoris</span>
    </a>
    </li> -->
        <li class="nav-item {{ Request::is('orders*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('orders.index') }}">
                <i class="fa fa-list"></i>
                <span>Commandes</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('orderItems*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('orderItems.index') }}">
                <i class="fa fa-list"></i>
                <span>Offres Commandées</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('foodSpeialties*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('foodSpeialties.index') }}">
                <i class="fa fa-apple"></i>
                <span>Spécialités culinaires</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('hotelSpecifications*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('hotelSpecifications.index') }}">
                <i class="fa fa-apple"></i>
                <span>Spécification hôtel</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('excursionActivities*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('excursionActivities.index') }}">
                <i class="fa fa-bus"></i>
                <span>Excursion Activities</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('invoices*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('invoices.index') }}">
                <i class="fa fa-list"></i>
                <span>Reçus</span>
            </a>
        </li>
        <li class="nav-item {{ Request::is('invoicePayments*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('invoicePayments.index') }}">
                <i class="fa fa-list"></i>
                <span>Reçus de paiement</span>
            </a>
        </li>
    <!-- <li class="nav-item {{ Request::is('metaDataServices*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('metaDataServices.index') }}">
        <i class="fa fa-circle"></i>
        <span>Meta Data Services</span>
    </a>
    </li> -->
    <!-- <li class="nav-item {{ Request::is('ownerSources*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('ownerSources.index') }}">
        <i class="fa fa-users"></i>
        <span>Propriétaires</span>
    </a>
    </li> -->
    <!-- <li class="nav-item {{ Request::is('receipts*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('receipts.index') }}">
        <i class="fa fa-list"></i>
        <span>Reçus imprimable</span>
    </a>
    </li> -->
        <li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('users.index') }}">
                <i class="fa fa-user"></i>
                <span>Administrateurs</span>
            </a>
        </li>

    </ul>
</section>
<!-- /.sidebar footer -->
<div class="sidebar-footer">
    <!-- item-->
    <!-- <a href="#" class="link" data-toggle="tooltip" title="" data-original-title="Settings"><i class="fa fa-cog fa-spin"></i></a> -->
    <!-- item-->
    <!-- <a href="#" class="link" data-toggle="tooltip" title="" data-original-title="Email"><i class="fa fa-envelope"></i></a> -->
    <!-- item-->
    <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form2').submit();" class="link"
       data-toggle="tooltip" title="" data-original-title="Logout"><i class="fa fa-power-off"></i></a>
    <form id="logout-form2" action="{{ url('/logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</div>
