<li class="nav-item {{ Request::is('customers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('customers.index') }}">
        <i class="nav-icon icon-people"></i>
        <span>Clients</span>
    </a>
</li>
<li class="nav-item {{ Request::is('customerSessions*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('customerSessions.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Sessions Client</span>
    </a>
</li>
<li class="nav-item {{ Request::is('offers*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('offers.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Offres</span>
    </a>
</li>
<li class="nav-item {{ Request::is('favoris*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('favoris.index') }}">
        <i class="nav-icon icon-like"></i>
        <span>Favoris</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orders*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orders.index') }}">
        <i class="nav-icon icon-basket"></i>
        <span>Commandes</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orderItems*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orderItems.index') }}">
        <i class="nav-icon icon-basket"></i>
        <span>Offres Commandées</span>
    </a>
</li>
<li class="nav-item {{ Request::is('invoices*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('invoices.index') }}">
        <i class="nav-icon icon-list"></i>
        <span>Reçus</span>
    </a>
</li>
<li class="nav-item {{ Request::is('invoicePayments*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('invoicePayments.index') }}">
        <i class="nav-icon icon-list"></i>
        <span>Reçus de paiement</span>
    </a>
</li>
<li class="nav-item {{ Request::is('metaDataServices*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('metaDataServices.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Meta Data Services</span>
    </a>
</li>
<li class="nav-item {{ Request::is('ownerSources*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('ownerSources.index') }}">
        <i class="nav-icon icon-people"></i>
        <span>Propriétaires</span>
    </a>
</li>
<li class="nav-item {{ Request::is('receipts*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('receipts.index') }}">
        <i class="nav-icon icon-list"></i>
        <span>Reçus imprimable</span>
    </a>
</li>

<li class="nav-item {{ Request::is('foodSpeialties*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('foodSpeialties.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Spécialités culinaires</span>
    </a>
</li>
<li class="nav-item {{ Request::is('excursionActivities*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('excursionActivities.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Excursion Activities</span>
    </a>
</li>
<li class="nav-item {{ Request::is('customerDevices*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('customerDevices.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Customer Devices</span>
    </a>
</li>
<li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('users.index') }}">
        <i class="nav-icon icon-user"></i>
        <span>Administrateurs</span>
    </a>
</li>
<li class="nav-item {{ Request::is('customerRequests*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('customerRequests.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Customer Requests</span>
    </a>
</li>
<li class="nav-item {{ Request::is('transcations*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('transcations.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Transcations</span>
    </a>
</li>
