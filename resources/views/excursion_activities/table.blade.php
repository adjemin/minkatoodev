<div class="table-responsive-sm">
    <table class="table table-striped" id="excursionActivities-table">
        <thead>
            <th>Image</th>
        <th>Name</th>
        <th>Slug</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($excursionActivities as $excursionActivity)
            <tr>
                <td>
                    <img src="{{ $excursionActivity->image }}" width="70" height="70"></td>
            <td>{{ $excursionActivity->name }}</td>
            <td>{{ $excursionActivity->slug }}</td>
                <td>
                    {!! Form::open(['route' => ['excursionActivities.destroy', $excursionActivity->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('excursionActivities.show', [$excursionActivity->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('excursionActivities.edit', [$excursionActivity->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>