<?php
        if (isset($_POST['cpm_trans_id'])) {
            // SDK PHP de CinetPay 
             require_once __DIR__ . '/../src/cinetpay.php';
        
            try {
                // Initialisation de CinetPay et Identification du paiement
                $id_transaction = $_POST['cpm_trans_id'];
                //Veuillez entrer votre apiKey et site ID
                $apiKey = "21585943f75164bbc2.38014639";
                $site_id = "296911";
                $plateform = "PROD";
                $version = "V1";
                $CinetPay = new CinetPay($site_id, $apiKey, $plateform, $version);
                $CinetPay->setTransId($id_transaction)->getPayStatus();
                $cpm_site_id = $CinetPay->_cpm_site_id;
                $signature = $CinetPay->_signature;
                $cpm_amount = $CinetPay->_cpm_amount;
                $cpm_trans_id = $CinetPay->_cpm_trans_id;
                $cpm_custom = $CinetPay->_cpm_custom;
                $cpm_currency = $CinetPay->_cpm_currency;
                $cpm_payid = $CinetPay->_cpm_payid;
                $cpm_payment_date = $CinetPay->_cpm_payment_date;
                $cpm_payment_time = $CinetPay->_cpm_payment_time;
                $cpm_error_message = $CinetPay->_cpm_error_message;
                $payment_method = $CinetPay->_payment_method;
                $cpm_phone_prefixe = $CinetPay->_cpm_phone_prefixe;
                $cel_phone_num = $CinetPay->_cel_phone_num;
                $cpm_ipn_ack = $CinetPay->_cpm_ipn_ack;
                $created_at = $CinetPay->_created_at;
                $updated_at = $CinetPay->_updated_at;
                $cpm_result = $CinetPay->_cpm_result;
                $cpm_trans_status = $CinetPay->_cpm_trans_status;
                $cpm_designation = $CinetPay->_cpm_designation;
                $buyer_name = $CinetPay->_buyer_name;
                // Aucun enregistrement dans la base de donnée ici
                if($cpm_result == '00'){
                    // une page HTML de paiement bon
                     echo 'Felicitation, votre paiement a été effectué avec succès';
                     die();
                }else{
                    // une page HTML de paiement echoué
                      echo 'Echec, votre paiement a échoué';
                      die();
                }
            } catch (Exception $e) {
                echo "Erreur :" . $e->getMessage();
                // Une erreur s'est produite
            }
        } else {
           // redirection vers la page d'accueil
           header('Location: /');
           die();
        }
        ?>
