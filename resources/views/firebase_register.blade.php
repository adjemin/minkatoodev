@extends('layouts.master')

@section('title')
    Inscription
@endsection

@section('content')

    <div id="log_section">
        <div class="mb30">
            <section class="our-log bgc-fa">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-lg-6 offset-lg-3">
                            <div class="login_form inner_page">
                                {{-- <form
                                > --}}
                                    {{-- @csrf --}}
                                    <div class="heading">
                                        <h3 class="text-center">Connectez-vous à votre compte</h3>
                                        <p class="text-center">Vous avez déja un compte? <a class="text-thm" href="{{ route('customer.login') }}">Se connecter!</a></p>
                                    </div>
                                    <div class="form-group">
                                        {{-- <input type="tel" name="phone_number" id="phone_number" class="form-control input required {{ $errors->has('phone_number')?'is-invalid':'' }}"
                                            value="{{ old('phone_number') }}"  required  placeholder="Votre contact téléphonique" style="padding-left: 96px; height: 53px; width: 393px;">
                                        <input type="hidden" name="dial_code" id="dial_code"> --}}

                                        {{-- firebase --}}

                                        <form
                                            id="register_form"
                                        >

                                        <input type="tel" name="phone_number" id="phone_number" class="form-control input required {{ $errors->has('phone_number')?'is-invalid':'' }}"
                                            value="{{ old('phone_number') }}"  required  placeholder="Votre contact téléphonique" style="padding-left: 96px; height: 53px; width: 393px;">
                                        <input type="hidden" name="dial_code" id="dial_code">
                                        <span class="alert alert-danger" id="phone_number_error"></span>
                                        <div class="mt-3">

                                        </div>

                                            {{-- <div class="form-group">
                                                <input type="text" name="phone" id="number" class="form-control" placeholder="Votre contact téléphonique">
                                            </div> --}}

                                            <div class="form-group">
                                                <input type="text" class="form-control input required {{ $errors->has('last_name')?'is-invalid':'' }} mb-3" name="last_name" value="{{ old('last_name') }}" placeholder="Votre Nom de famille">
                                            </div>
                                            <span class="alert alert-danger" id="last_name_error"></span>



                                            <div class="form-group">
                                                <input type="text" class="form-control input required {{ $errors->has('first_name')?'is-invalid':'' }}" name="first_name" value="{{ old('first_name') }}" placeholder="Votre Prénom">
                                            </div>
                                            <span class="alert alert-danger" id="first_name_error"></span>


                                            <div class="form-group">
                                                <input type="email" class="form-control input required {{ $errors->has('email')?'is-invalid':'' }}" name="email" value="{{ old('email') }}" placeholder="Adresse Email">
                                            </div>
                                            <span class="alert alert-danger" id="email_error"></span>


                                            <span>
                                                @error('phone_number')
                                                    {{ $message }}
                                                @enderror
                                            </span>

                                            <input type="hidden" name="dial_code" id="dial_code">
                                            <input type="hidden" name="phone_numberCustomer" id="phone_numberCustomer">
                                            <input type="hidden" name="country_code" id="country_code" value=""/>

                                            <div class="d-flex justify-content-center mb-3">
                                                <div id="recaptcha-container"></div>
                                            </div>

                                            {{-- <button type="button" class="btn btn-primary mt-3" onclick="sendOTP();">Send OTP</button> --}}

                                            <button
                                            type="button"
                                            onclick="sendOTP();"
                                            {{-- type="submit"  --}}
                                            class="btn btn-log btn-block btn-thm2">Envoyer Code</button>
                                        </form>


                                        <input type="hidden" name="phone_numberCustomer" id="phone_numberCustomer">
                                        <input type="hidden" name="country_code" id="country_code" value=""/>
                                    </div>

                                {{-- </form> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div id="verification_section" style="display:none;">
        <div class="mb30">
            <section class="our-log bgc-fa">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-lg-6 offset-lg-3">
                            <div class="login_form inner_page">
                                <div class="heading">
                                    <h3 class="text-center">Renseigner le code reçu par SMS pour valider vôtre inscription</h3>
                                </div>
                                <div class="details">

                                        <br>
                                        <p c lass="text-center"></p>

                                        <div class="alert alert-success" id="successOtpAuth" style="display: none;"></div>
                                        <!-- Nom, prénoms -->
                                        <div class="container my-3">
                                            <div class="form-group">
                                                {{-- <input class="form-control {{ $errors->has('verification_code')?'is-invalid':'' }}" type="text" value="{{ old('verification_code') }}" required name="verification_code" placeholder="Entrer le code de vérification"> --}}

                                                <form>
                                                    <input type="text" id="verification" class="form-control" placeholder="Entrer le code de vérification">

                                                    <button type="button" class="btn btn-log btn-block btn-thm2"
                                                    onclick="verify()"
                                                    >
                                                        Vérifier
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="divide">
                                            <span class="lf_divider">Ou</span>
                                            <hr>
                                        </div>
                                        <p class="text-center">Vous avez déjà un compte? <a class="text-thm" href="{{ route('customer.login') }}">Se connecter</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
    <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/6.0.2/firebase.js"></script>


    <script src="{{asset('asset/js/intlTelInput.js')}}"></script>
    <script src="{{asset('asset/js/utils.js')}}"></script>
    <script>
        var CSRFToken = "{{ csrf_token() }}";

        var inputPhoneCustomer = document.querySelector("#phone_number");

            // var  errorMsg = document.querySelector("#error-msgPhoneShop"),
            //     validMsg = document.querySelector("#valid-msgPhoneShop");

            // here, the index maps to the error code returned from getValidationError - see readme
            var errorMap = [ "Numéro Invalide !", "Code de pays invalide !", "Numéro de téléphone Trop Court", "Numéro de téléphone Trop Long", "Numéro Invalide"];
            var iti = window.intlTelInput(inputPhoneCustomer, {
                // allowDropdown: false,
                // autoHideDialCode: false,
                // autoPlaceholder: "off",
                dropdownContainer: document.body,
                // excludeCountries: ["us"],
                // formatOnDisplay: false,
                initialCountry: "auto",
                separateDialCode: true,
                geoIpLookup: function(success, failure) {
                    $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "CI";
                        // var countryCode = resp.country;
                        success(countryCode);
                    });
                },
                // hiddenInput: "full_number",
                // localizedCountries: { 'de': 'Deutschland' },
                //nationalMode: true,
                // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                placeholderNumberType: "MOBILE",
                // preferredCountries: ['cn', 'jp'],
                utilsScript: "{{asset('asset/js/utils.js?1562189064761')}}",
                // utilsScript: "{{asset('customer/js/utils.js')}}",
            });
            // var reset = function() {
            //     inputPhoneCustomer.classList.remove("error");
            //     errorMsg.innerHTML = "";
            //     errorMsg.classList.add("hide");
            //     validMsg.classList.add("hide");
            // };

            // on blur: validate
            inputPhoneCustomer.addEventListener('blur', function() {
                // reset();
                if (inputPhoneCustomer.value.trim()) {
                    if (iti.isValidNumber()) {
                        $("#dial_code").val(iti.getSelectedCountryData().dialCode);
                    // $("#phone_number").val(iti.);
                        $("#country_code").val(iti.getSelectedCountryData().iso2.toUpperCase());
                        //console.log(iti.s.phone);
                        validMsg.classList.remove("hide");
                    } else {
                        inputPhoneCustomer.classList.add("error");
                        var errorCode = iti.getValidationError();
                        errorMsg.innerHTML = errorMap[errorCode];
                        errorMsg.classList.remove("hide");
                    }
                }
            });

            // on keyup / change flag: reset
            inputPhoneCustomer.addEventListener('change', reset);
            inputPhoneCustomer.addEventListener('keyup', reset);
    </script>

    <script>
        var firebaseConfig = {
            apiKey: "AIzaSyBpyQr5qKgfPOmui4XK5W9nVSYZT7UZ3zg",
            authDomain: "minkatoo-8a316.firebaseapp.com",
            projectId: "minkatoo-8a316",
            storageBucket: "minkatoo-8a316.appspot.com",
            messagingSenderId: "40149485168",
            appId: "1:40149485168:web:b76f35bd868de8ca2c96b5",
            measurementId: "G-6X8Q7SD0NF"
        };
        firebase.initializeApp(firebaseConfig);
    </script>

    <script type="text/javascript">
        $('#phone_number_error').hide();
        $('#last_name_error').hide();
        $('#first_name_error').hide();
        $('#email_error').hide();
        var formData = {};
        const phone_number = document.querySelector('#phone_number');
        const dial_code = document.querySelector('#dial_code');
        var number;
        var userInfo;

        window.onload = function () {
            render();
        };

        function render() {
            window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
            recaptchaVerifier.render();
        }

        function sendOTP() {
            phone_num = phone_number.value;
            dial = dial_code.value  == "" ? "+225" : dial_code.value;

            formData = $('#register_form').serialize();
            formData.phone_number = phone_num;
            formData.dial_code = dial;

            number = dial + phone_num;

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN':  '{{ csrf_token() }}'
                },
                url : "{{ route('customer.register') }}", // Url of backend (can be python, php, etc..)
                type: "POST", // data type (can be get, post, put, delete)
                data : formData, // data in json format
                async : false, // enable or disable async (optional, but suggested as false if you need to   populate data afterwards)
                success: function(response, textStatus, jqXHR) {
                    console.log(response);

                     if(response.status == 200){

                        userInfo = response.customer
                        // alert('GOOD !')
                        // TODO display verification form
                        const verif_sec = document.querySelector('#verification_section');
                        verif_sec.style.display="block";


                        // TODO hide login form
                        const log_section = document.querySelector('#log_section');
                        log_section.style.display="none";


                        firebase.auth().signInWithPhoneNumber(number,
                            window.recaptchaVerifier
                        ).then(function (confirmationResult) {

                            // var number = document.getElementById('number')

                            window.confirmationResult = confirmationResult;
                            coderesult = confirmationResult;
                            console.log(coderesult);
                            $("#successAuth").text("Message sent");
                            $("#successAuth").show();
                        }).catch(function (error) {
                            $("#error").text(error.message);
                            $("#error").show();
                        });


                        // return false;
                    }else if(response.status == 422){
                         alert('error')
                    }
                    // return false;

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);

                    console.log(jqXHR.responseJSON.errors);

                    Object.entries(jqXHR.responseJSON.errors).forEach(error => {
                        // if(error[0] == ""){
                        //     error[1]
                        // }

                        if(error[0]=='phone_number'){
                            // console.log(error[1]);
                            $('#phone_number_error').show().text(error[1]);
                        }


                        if(error[0]=='email'){
                            // console.log(error[1]);
                            $('#email_error').show().text(error[1]);
                        }

                        if(error[0]=='first_name'){
                            // console.log(error[1]);
                            $('#first_name_error').show().text(error[1]);
                        }

                        if(error[0]=='last_name'){
                            // console.log(error[1]);
                            $('#last_name_error').show().text(error[1]);
                        }

                        console.log(error)
                        // value: student[1]
                        // console.log(`Student: ${student[0]} is ${student[1].age} years old`);
                    });

                    console.log(textStatus);
                    console.log(errorThrown);
                    return false;
                }
            });

        }

        function verify() {
            var code = $("#verification").val();
            coderesult.confirm(code).then(function (result) {
                console.log(result)
                var user = result.user;
                console.log(user);

                // alert('Zoooo')

                $.ajax({
                    url : "{{ route('loginUsingId') }}", // Url of backend (can be python, php, etc..)
                    type: "GET", // data type (can be get, post, put, delete)
                    data : userInfo, // data in json format
                    async : false, // enable or disable async (optional, but suggested as false if you need to   populate data afterwards)
                    success: function(response, textStatus, jqXHR) {
                        console.log(response);
                        // return false;
                        // console.log(['response >>>>>>'+response, 'response status >>>>>>'+ response.status]);
                        if (response.status == 200) {
                            setTimeout(() => {
                                    window.location.href = "{{ url('/') }}";
                            }, 3000)
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });

                // TODO
                // Add swal

                // $("#successOtpAuth").text("Auth is successful");
                // $("#successOtpAuth").show();
            }).catch(function (error) {
                $("#error").text(error.message);
                $("#error").show();
            });
        }




    </script>

@endsection
