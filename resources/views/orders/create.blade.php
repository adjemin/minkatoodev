@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('orders.index') !!}">Commandes</a>
      </li>
      <li class="breadcrumb-item active">Créer</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Créer Commande</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'orders.store']) !!}

                                   @include('orders.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
