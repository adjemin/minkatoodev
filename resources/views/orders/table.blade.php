
<div class="box">

    <!-- /.box-header -->
    <div class="box-body" style="overflow-x: scroll;">
      <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive-sm">
        <thead>
            <tr>
            <th>Client</th>
        <th>En attente</th>
        <th>Status</th>
        <th>Méthode de paiement</th>
        <th>Frais de livraison</th>
        <th>Livré</th>
{{--        <th>Date de livraison</th>--}}
        <th>Montant</th>
        <th>Devise</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @forelse($orders as $order)
            <tr>
                <td>
                    {{ $order->getCustomer() != null ? $order->getCustomer()->name  : "non défini"}}</td>
                <td>

                @if($order->is_waiting == 0)
                    Non
                @else
                    Oui
                @endif
            </td>
            <td>{{ $order->current_status }}</td>
            <td>
                @if($order->payment_method_slug != null)
                    @if($order->payment_method_slug == 'cash')
                        Paiement en espèce
                    @else
                        Paiement en ligne
                    @endif
                @else
                    Status inconnu
                @endif</td>
            <td>{{ $order->delivery_fees }}</td>
            <td>
                @if($order->is_delivered == 0)
                    Non
                @else
                    Oui
                @endif
            </td>
            {{--<td>{{ $order->delivery_date }}</td>--}}
            <td>{{ $order->amount }}</td>
            <td>{{ $order->currency_code }}</td>
                <td>
                    {!! Form::open(['route' => ['orders.destroy', $order->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('orders.show', [$order->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('orders.edit', [$order->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Êtes vous sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @empty
            <div class="d-flex justify-content-center">
                Aucune données
            </div>
        @endforelse
        </tbody>
    </table>
</div>
</div>
