@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('orders.index') !!}">Commandes</a>
          </li>
          <li class="breadcrumb-item active">Modifier</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Modifier Commande</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($order, ['route' => ['orders.update', $order->id], 'method' => 'patch']) !!}

                              @include('orders.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection