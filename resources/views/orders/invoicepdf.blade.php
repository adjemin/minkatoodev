<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
        <title></title>
</head>
<body>
<style>
    .content {
        min-height: 100px !important;
        padding: 20px;
        min-width: 100%;
        margin-right: 0 !important;
        margin-left: 0 !important;
    }

    .badge-secondary {
        background-color: #6c757d6b !important;
</style>
<div class="row">
    <div class="col-md-6">
        <div class="">
            <div class="content">
                <div class="row">
                    <div class="col-md-4"><strong>Commande en attente<span
                                class="pull-right">?</span></strong></div>
                    <div class="col-md-8">
                        @if($order->is_waiting == 1)
                            OUI
                        @else
                            NON
                        @endif
                    </div>
                </div>
                {{--<div class="row">
                    <div class="col-md-8">
                        @if($order->is_waiting != null)
                            @if($order->is_waiting == 1)
                                OUI
                            @else
                                NON
                            @endif
                        @else
                            Status inconnu
                        @endif
                    </div>
                </div>--}}
                <div class="row">
                    <div class="col-md-4"><strong>Mode de paiement<span
                                class="pull-right">:</span></strong></div>
                    <div class="col-md-8">
                        {{--                        {{ !is_null($order->payment_method_slug) ? $order->payment_method_slug : '---' }}--}}
                        @if($order->payment_method_slug != null)
                            @if($order->payment_method_slug == 'cash')
                                Paiement en espèce
                            @else
                                Paiement en ligne
                            @endif
                        @else
                            Status inconnu
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4"><strong>Frais de livraison<span
                                class="pull-right">:</span></strong></div>
                    <div class="col-md-8">
                        {{ !is_null($order->delivery_fees) ? $order->delivery_fees : '---' }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4"><strong>Montant<span
                                class="pull-right">:</span></strong></div>
                    <div class="col-md-8">
                        {{ !is_null($order->amount) ? $order->amount : '---'}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4"><strong>Date commande<span
                                class="pull-right">:</span></strong></div>
                    <div class="col-md-8">
                        {{ !is_null($order->created_at) ? $order->created_at : '---' }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4"><strong>Date livraison<span
                                class="pull-right">:</span></strong></div>
                    <div class="col-md-8">
                        {{ !is_null($order->created_at) ? $order->created_at : '---' }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Status actuel
                            <span
                                class="pull-right">:</span>
                        </strong>
                    </div>
                    <div class="col-md-8">
                        @if($order->current_status == "waiting")
                            <span class="badge badge-secondary text-warning">La commande est attente</span>
                        @elseif($order->current_status == "failed")
                            <span class="badge badge-secondary text-danger">La commnde a échouée</span>
                        @elseif($order->current_status == "cancel")
                            <span class="badge badge-secondary text-danger">La commande est annulée</span>
                        @elseif($order->current_status == "successful")
                            <span class="badge badge-secondary text-success">La commande est réussie</span>
                        @elseif($order->current_status == "started")
                            <span class="badge badge-secondary text-success">La commande est lancée</span>
                        @elseif($order->current_status == "managed")
                            <span class="badge badge-secondary text-success">La commande est gérée</span>
                        @elseif($order->current_status == "edited")
                            <span class="badge badge-secondary text-success">La commande est modifiée</span>
                        @elseif($order->current_status == "ready-for-delivery")
                            <span class="badge badge-secondary text-success">Prête pour la livraison</span>
                        @else
                            <span class="badge badge-secondary text-info">La commande est supprimée</span>
                        @endif
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="content">
                        <div class="card">
                            <div class="card-header">
                                Commande
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nom</th>
                                        <th scope="col">Quantité</th>
                                        <th scope="col">Prix</th>
                                        <th scope="col">Montant</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($order->getItemsAttribute() as $orderItem)
                                        <tr>
                                            <td>
                                                <div>{{$orderItem->id}}</div>
                                            </td>
                                            <td>
                                                <div>{{$orderItem->servicePricing()->getTitle()}}</div>

                                            </td>
                                            <td>
                                                <div>{{$orderItem->quantity}}</div>
                                            </td>
                                            <td>
                                                <div>{{$orderItem->unit_price}}</div>
                                            </td>
                                            <td>
                                                <div>{{$orderItem->servicePricing()->getPrice()}}</div>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5">Aucun élément associé à cette commande.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="card">
            <div class="header">
                <h4 class="title">Ramassage (Client)</h4>
                <p class="category">
                    Ramassage chez le client et Livraison chez l'agence.
                </p>
            </div>
            <div class="content">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#clientpickup">Client</a>
                    </li>
                    <li><a data-toggle="tab" href="#agencypickup">Agence</a></li>
                </ul>
                <div class="tab-content">
                    <div id="clientpickup" class="tab-pane fade in active">
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <th class="text-center">#ID</th>
                                <th class="text-center">Type</th>
                                <th class="text-center">Client</th>
                                <th class="text-center">Address</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Signatures</th>
                                <th class="text-center">Image</th>
                                <th class="text-center">Agent</th>
                                </thead>
                                <tbody>
                                @if($order->getTaskCustomerToAgency() != null)
                                    @foreach($order->getTaskCustomerToAgency()->getPickupsAttribute() as $job)
                                        <tr>
                                            <td class="text-center">#{{$job->id}}</td>
                                            <td class="text-center">
                                                @if($job->has_pickup)
                                                    Pickup
                                                @else
                                                    Delivery
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($job->has_pickup)
                                                    Customer
                                                @else
                                                    {{$job->delivery_customer_username}}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($job->has_pickup)
                                                    {{$job->pickup_address}}
                                                @else
                                                    {{$job->delivery_customer_address}}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($job->current_status == "successful")
                                                    <span
                                                        class="badge badge-success"> {{$job->current_status}}</span>
                                                @elseif($job->current_status == "failed")
                                                    <span
                                                        class="badge badge-danger"> {{$job->current_status}}</span>
                                                @elseif($job->current_status == "arrived")
                                                    <span
                                                        class="badge badge-info"> {{$job->current_status}}</span>
                                                @elseif($job->current_status == "started")
                                                    <span
                                                        class="badge badge-info"> {{$job->current_status}}</span>
                                                @else
                                                    <span
                                                        class="badge badge-warning"> {{$job->current_status}}</span>
                                                @endif

                                            </td>
                                            <td class="text-center">
                                                @if($job->signatures != null)
                                                    <a href=" {{$job->signatures}}">See
                                                        Signature</a>
                                                @else
                                                    None
                                                @endif
                                            </td>
                                            <td class="text-center"></td>
                                            <td class="text-center">
                                                @if($order->getTaskCustomerToAgency()->getAgent() != null)
                                                    {{$order->getTaskCustomerToAgency()->getAgent()->name}}
                                                @else
                                                    None
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    @foreach($order->getTaskCustomerToAgency()->getDeliveriesAttribute() as $job)
                                        <tr>
                                            <td class="text-center">#{{$job->id}}</td>
                                            <td class="text-center">
                                                @if($job->has_pickup)
                                                    Pickup
                                                @else
                                                    Delivery
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($job->has_pickup)
                                                    Customer
                                                @else
                                                    {{$job->delivery_customer_username}}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($job->has_pickup)
                                                    {{$job->pickup_address}}
                                                @else
                                                    {{$job->delivery_customer_address}}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($job->current_status == "successful")
                                                    <span
                                                        class="badge badge-success"> {{$job->current_status}}</span>
                                                @elseif($job->current_status == "failed")
                                                    <span
                                                        class="badge badge-danger"> {{$job->current_status}}</span>
                                                @elseif($job->current_status == "arrived")
                                                    <span
                                                        class="badge badge-info"> {{$job->current_status}}</span>
                                                @elseif($job->current_status == "started")
                                                    <span
                                                        class="badge badge-info"> {{$job->current_status}}</span>
                                                @else
                                                    <span
                                                        class="badge badge-warning"> {{$job->current_status}}</span>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($job->signatures != null)
                                                    <a href=" {{$job->signatures}}">See
                                                        Signature</a>
                                                @else
                                                    None
                                                @endif

                                            </td>
                                            <td class="text-center"></td>
                                            <td class="text-center">
                                                @if($order->getTaskCustomerToAgency()->getAgent() != null)
                                                    {{$order->getTaskCustomerToAgency()->getAgent()->name}}
                                                @else
                                                    None
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="agencypickup" class="tab-pane fade in">
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <th class="text-center">#ID</th>
                                <th class="text-center">Type</th>
                                <th class="text-center">Client</th>
                                <th class="text-center">Address</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Signatures</th>
                                <th class="text-center">Image</th>
                                <th class="text-center">Agent</th>
                                </thead>
                                <tbody>
                                @if($order->getTaskAgencyToCustomer() != null)
                                    @foreach($order->getTaskAgencyToCustomer()->getPickupsAttribute() as $job)
                                        <tr>
                                            <td class="text-center">#{{$job->id}}</td>
                                            <td class="text-center">
                                                @if($job->has_pickup)
                                                    Pickup
                                                @else
                                                    Delivery
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($job->has_pickup)
                                                    Customer
                                                @else
                                                    {{$job->delivery_customer_username}}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($job->has_pickup)
                                                    {{$job->pickup_address}}
                                                @else
                                                    {{$job->delivery_customer_address}}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($job->current_status == "successful")
                                                    <span
                                                        class="badge badge-success"> {{$job->current_status}}</span>
                                                @elseif($job->current_status == "failed")
                                                    <span
                                                        class="badge badge-danger"> {{$job->current_status}}</span>
                                                @elseif($job->current_status == "arrived")
                                                    <span
                                                        class="badge badge-info"> {{$job->current_status}}</span>
                                                @elseif($job->current_status == "started")
                                                    <span
                                                        class="badge badge-info"> {{$job->current_status}}</span>
                                                @else
                                                    <span
                                                        class="badge badge-warning"> {{$job->current_status}}</span>
                                                @endif
                                            </td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center">
                                                @if($order->getTaskAgencyToCustomer()->getAgent() != null)
                                                    {{$order->getTaskAgencyToCustomer()->getAgent()->name}}
                                                @else
                                                    None
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    @foreach($order->getTaskAgencyToCustomer()->getDeliveriesAttribute() as $job)
                                        <tr>
                                            <td class="text-center">#{{$job->id}}</td>
                                            <td class="text-center">
                                                @if($job->has_pickup)
                                                    Pickup
                                                @else
                                                    Delivery
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($job->has_pickup)
                                                    Customer
                                                @else
                                                    {{$job->delivery_customer_username}}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($job->has_pickup)
                                                    {{$job->pickup_address}}
                                                @else
                                                    {{$job->delivery_customer_address}}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($job->current_status == "successful")
                                                    <span
                                                        class="badge badge-success"> {{$job->current_status}}</span>
                                                @elseif($job->current_status == "failed")
                                                    <span
                                                        class="badge badge-danger"> {{$job->current_status}}</span>
                                                @elseif($job->current_status == "arrived")
                                                    <span
                                                        class="badge badge-info"> {{$job->current_status}}</span>
                                                @elseif($job->current_status == "started")
                                                    <span
                                                        class="badge badge-info"> {{$job->current_status}}</span>
                                                @else
                                                    <span
                                                        class="badge badge-warning"> {{$job->current_status}}</span>
                                                @endif
                                            </td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center">
                                                @if($order->getTaskAgencyToCustomer()->getAgent() != null)
                                                    {{$order->getTaskAgencyToCustomer()->getAgent()->name}}
                                                @else
                                                    None
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach

                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>--}}
    </div>
    <div class="col-md-6">
        @php
            $customer = $order->getCustomer();
        @endphp
        @if(!is_null($customer))
            <div class="">
                <div class="content">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img
                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQEyza1-CKEJK6Hyq50yE8oOpyba5HbJaCqMioudHzaxVh-a9Is"
                                width="100">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center"><span
                                class="badge badge-success text-{{ $customer->otp_verified == 1 ? 'success' : 'danger' }}">{{ $customer->otp_verified == 1 ? 'Client actif' : 'Client inactif' }}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div
                            class="col-md-12 text-center">{{ !is_null($customer->name) ? $customer->name.' | ' : '' }} {{ !is_null($customer->language) ? $customer->language : 'Fr' }}</div>
                    </div>
                </div>
            </div>
        @endif
        <div class="">
            <div class="header">
                {{--                <h4 class="title">Client</h4>--}}
                <br>
                <p class="category">
                    Détails du client
                </p>
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-5">
                                <strong>Nom<span class="pull-right">:</span></strong>
                            </div>
                            <div class="col-md-7">
                                @if($order->getCustomer() != null && $order->getCustomer()->name != null)
                                    {!! $order->getCustomer()->name !!}<br>
                                    <div class="small text-muted">
                                        Enregistré le: {{$order->getCustomer()->created_at}}
                                    </div>
                                @else
                                    Inconnu
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <strong>Téléphone<span class="pull-right">:</span></strong>
                            </div>
                            <div class="col-md-7">
                                @if($order->getCustomer() != null && $order->getCustomer()->phone != null)
                                    + {!! $order->getCustomer()->dial_code !!} {!! $order->getCustomer()->phone_number !!}
                                @else
                                    Inconnu
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <strong>Email<span class="pull-right">:</span></strong>
                            </div>
                            <div class="col-md-7">
                                @if($order->getCustomer() != null && $order->getCustomer()->email != null)
                                    {!! $order->getCustomer()->email !!}
                                @else
                                    Inconnu
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
