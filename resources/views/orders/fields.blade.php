<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Identifiant client:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Waiting Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_waiting', 'En attente:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_waiting', 0) !!}
        {!! Form::checkbox('is_waiting', '1', null) !!}
    </label>
</div>


<!-- Current Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('current_status', 'Status:') !!}
    {!! Form::text('current_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Method Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_method_slug', 'Méthode de paiement:') !!}
    {!! Form::text('payment_method_slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Delivery Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_fees', 'Frais de livraison:') !!}
    {!! Form::text('delivery_fees', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Delivered Field -->eb
<div class="form-group col-sm-6">
    {!! Form::label('is_delivered', 'Délivré ?:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_delivered', 0) !!}
        {!! Form::checkbox('is_delivered', '1', null) !!}
    </label>
</div>


<!-- Delivery Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('delivery_date', 'Date de livraison:') !!}
    {!! Form::text('delivery_date', null, ['class' => 'form-control','id'=>'delivery_date']) !!}
</div>

@push('scripts')
   <script type="text/javascript">
           $('#delivery_date').datetimepicker({
               format: 'YYYY-MM-DD HH:mm:ss',
               useCurrent: true,
               icons: {
                   up: "icon-arrow-up-circle icons font-2xl",
                   down: "icon-arrow-down-circle icons font-2xl"
               },
               sideBySide: true
           })
       </script>
@endpush


<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Montant:') !!}
    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_code', 'Code de la devise:') !!}
    {!! Form::text('currency_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('orders.index') }}" class="btn btn-secondary">Annuler</a>
</div>
