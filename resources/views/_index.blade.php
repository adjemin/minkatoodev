<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/main.css')}}">
    <!-- <link rel="stylesheet" href="css/responsive-grid.css"> -->
    <link rel="stylesheet" href="{{ asset('css/media-queries.css')}}">
    {{-- W3CSS --}}
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body>
    <header>
        <!--  -->
        <div id="header">
            <div id="brand">
                <img src="{{asset('img/LOGO-MINKATOO-1@1X.png')}}" alt="" class="img-fluid">
            </div>
            <div>
                <a href="" class="flatLink">S'inscrire</a>
                <a href="" class="btn btn-link">Se connecter</a>
            </div>
        </div>
        <!-- Main Header Image -->
        <div>
            <img src="{{asset('img/sea-dawn-nature-sky-127160.jpg')}}" alt="" class="img-fluid">
        </div>
        <div id="navBarSearch">
            <!-- <form action=""> -->
            <span class="">
                <!-- Place Holder Icon -->
                <span>
                    <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt="" width="15px">
                </span>
                <input type="text" placeholder="Abidjan, Côte d'ivoire">
            </span>
            <button type="submit">
                <!-- Search icon -->
                <img src="{{asset('img/search (1)@1X.png')}}" alt="" width="15px">
            </button>
            <!-- </form> -->
        </div>
    </header>
    <main>
        <nav id="iconNavbar">
            <ul>
                <li class="iconItem ">
                    <a href="" class="">

                        <p>Hotels</p>
                        <div>
                            <!-- Item image -->
                            <img src="{{asset('img/hotel@1X.png')}}" alt="Hotels">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem ">
                    <a href="">

                        <p>Restaurants</p>
                        <div>
                            <!-- Item image -->
                            <img src="{{asset('img/plate-fork-and-knife@1X.png')}}" alt="restaurants">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem ">
                    <a href="">

                        <p>Excursion</p>
                        <div>
                            <!-- Item image -->
                            <img src="{{asset('img/sunset@1X.png')}}" alt="Excursion">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem ">
                    <a href="">

                        <p>Location Voiture</p>
                        <div>
                            <!-- Item image -->
                            <img src="{{asset('img/car@1X.png')}}" alt="location Voiture">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem ">
                    <a href="">

                        <p>Musée</p>
                        <div>
                            <!-- Item image -->
                            <img src="{{asset('img/museum@1X.png')}}" alt="musée">

                        </div>
                    </a>
                </li>
                <!--  -->
            </ul>
        </nav>
        <!--  -->
        <!-- Section hotels à proximité -->
        <section class="contentWrapper">
            <div class="sectionHead">
                <h1>Hotels à proximité</h1>
                <a href="" class="flatLink color">Tout afficher</a>
            </div>
            <div class="row">
                <!-- Card Item -->
                <div class="card">
                    <div class="card-img">
                        <img src="{{asset('img/bedroom.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="card-content">
                        <h4>Hotel Golf Club</h4>
                        <div class="rating">
                            <!-- Stars -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star.png')}}" alt=""> <!-- halfstar icon -->
                            <span>(15)</span>
                        </div>
                        <div class="card-footer">
                            <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt=""> <!-- mappin icon -->
                            <span>
                                76 km
                            </span>
                        </div>
                    </div>
                </div>
                <!-- Card Item -->
                <div class="card col">
                    <div class="card-img">
                        <img src="{{asset('img/bedroom.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="card-content">
                        <h4>Hotel Golf Club</h4>
                        <div class="rating">
                            <!-- Stars -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star.png')}}" alt=""> <!-- halfstar icon -->
                            <span>(15)</span>
                        </div>
                        <div class="card-footer">
                            <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt=""> <!-- mappin icon -->
                            <span>
                                76 km
                            </span>
                        </div>
                    </div>
                </div>
                <!-- Card Item -->
                <div class="card col">
                    <div class="card-img">
                        <img src="{{asset('img/bedroom.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="card-content">
                        <h4>Hotel Golf Club</h4>
                        <div class="rating">
                            <!-- Stars -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star.png')}}" alt=""> <!-- halfstar icon -->
                            <span>(15)</span>
                        </div>
                        <div class="card-footer">
                            <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt=""> <!-- mappin icon -->
                            <span>
                                76 km
                            </span>
                        </div>
                    </div>
                </div>
                <!-- Card Item -->
                <div class="card col">
                    <div class="card-img">
                        <img src="{{asset('img/bedroom.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="card-content">
                        <h4>Hotel Golf Club</h4>
                        <div class="rating">
                            <!-- Stars -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star.png')}}" alt=""> <!-- halfstar icon -->
                            <span>(15)</span>
                        </div>
                        <div class="card-footer">
                            <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt=""> <!-- mappin icon -->
                            <span>
                                76 km
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Section restaurants à proximité -->
        <section class="contentWrapper">
            <div class="sectionHead">
                <h1>Restaurants à proximité</h1>
                <a href="" class="flatLink color">Tout afficher</a>
            </div>
            <div class="row">
                <!-- Card Item -->
                <div class="card col">
                    <div class="card-img">
                        <img src="{{asset('img/dining-table.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="card-content">
                        <h4>Ivoire Golf Club</h4>
                        <div class="rating">
                            <!-- Stars -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star.png')}}" alt=""> <!-- halfstar icon -->
                            <span>(15)</span>
                        </div>
                        <div class="card-footer">
                            <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt=""> <!-- mappin icon -->
                            <span>
                                76 km
                            </span>
                        </div>
                    </div>
                </div>
                <!-- Card Item -->
                <div class="card col">
                    <div class="card-img">
                        <img src="{{asset('img/dining-table.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="card-content">
                        <h4>Hotel Golf Club</h4>
                        <div class="rating">
                            <!-- Stars -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star.png')}}" alt=""> <!-- halfstar icon -->
                            <span>(15)</span>
                        </div>
                        <div class="card-footer">
                            <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt=""> <!-- mappin icon -->
                            <span>
                                76 km
                            </span>
                        </div>
                    </div>
                </div>
                <!-- Card Item -->
                <div class="card col">
                    <div class="card-img">
                        <img src="{{asset('img/dining-table.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="card-content">
                        <h4>Hotel Golf Club</h4>
                        <div class="rating">
                            <!-- Stars -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star.png')}}" alt=""> <!-- halfstar icon -->
                            <span>(15)</span>
                        </div>
                        <div class="card-footer">
                            <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt=""> <!-- mappin icon -->
                            <span>
                                76 km
                            </span>
                        </div>
                    </div>
                </div>
                <!-- Card Item -->
                <div class="card col">
                    <div class="card-img">
                        <img src="{{asset('img/dining-table.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="card-content">
                        <h4>Hotel Golf Club</h4>
                        <div class="rating">
                            <!-- Stars -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star.png')}}" alt=""> <!-- halfstar icon -->
                            <span>(15)</span>
                        </div>
                        <div class="card-footer">
                            <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt=""> <!-- mappin icon -->
                            <span>
                                76 km
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Banner Ssection excursions -->
        <section id="excursions" class="banner">
            <div class="banner-head">

                <h1>Nos Suggestions</h1>
                <h3>Excursions en ce moments</h3>
            </div>
            <div class="banner-img">
                <!-- <img src="{{asset('img/excursion.jpg')}}" alt="" class="img-fluid"> -->
                <!-- <div class="dark-overlay">
                </div> -->

                <div class="banner-img-overlay">
                    <div class="banner-img-text">

                        <h1 class="title">Excursion au domaine bini</h1>
                        <div>Abidjan, Côte d'Ivoire</div>
                        <div>Trip in Africa</div>
                    </div>
                </div>


            </div>
        </section>

        <!-- Section Meilleures activités à abidjan -->
        <section class="contentWrapper">
            <div class="sectionHead">
                <h1>Meilleures activités à abidjan</h1>
                <!-- <a href="" class="flatLink color">Tout afficher</a> -->
                <span class="spacer-10">

                </span>
            </div>
            <div class="row">
                <!-- Card Item -->
                <div class="card col">
                    <div class="card-img">
                        <img src="{{asset('img/bedroom.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="card-content">
                        <h4>Hotel Golf Club</h4>
                        <div class="rating">
                            <!-- Stars -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star.png')}}" alt=""> <!-- halfstar icon -->
                            <span>(15)</span>
                        </div>
                        <div class="card-footer">
                            <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt=""> <!-- mappin icon -->
                            <span>
                                76 km
                            </span>
                        </div>
                    </div>
                </div>
                <!-- Card Item -->
                <div class="card col">
                    <div class="card-img">
                        <img src="{{asset('img/bedroom.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="card-content">
                        <h4>Hotel Golf Club</h4>
                        <div class="rating">
                            <!-- Stars -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star.png')}}" alt=""> <!-- halfstar icon -->
                            <span>(15)</span>
                        </div>
                        <div class="card-footer">
                            <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt=""> <!-- mappin icon -->
                            <span>
                                76 km
                            </span>
                        </div>
                    </div>
                </div>
                <!-- Card Item -->
                <div class="card col">
                    <div class="card-img">
                        <img src="{{asset('img/bedroom.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="card-content">
                        <h4>Hotel Golf Club</h4>
                        <div class="rating">
                            <!-- Stars -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star.png')}}" alt=""> <!-- halfstar icon -->
                            <span>(15)</span>
                        </div>
                        <div class="card-footer">
                            <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt=""> <!-- mappin icon -->
                            <span>
                                76 km
                            </span>
                        </div>
                    </div>
                </div>
                <!-- Card Item -->
                <div class="card col">
                    <div class="card-img">
                        <img src="{{asset('img/bedroom.jpg')}}" alt="" class="img-fluid">
                    </div>
                    <div class="card-content">
                        <h4>Hotel Golf Club</h4>
                        <div class="rating">
                            <!-- Stars -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star-full.png')}}" alt="" class=""> <!-- fullstar icons -->
                            <img src="{{asset('img/star.png')}}" alt=""> <!-- halfstar icon -->
                            <span>(15)</span>
                        </div>
                        <div class="card-footer">
                            <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt=""> <!-- mappin icon -->
                            <span>
                                76 km
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--  -->
    </main>
    <footer>
        <!--  -->
    </footer>
</body>

</html>
