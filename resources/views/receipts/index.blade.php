@extends('layouts.app')
@section("css")
    <link rel="stylesheet" type="text/css" href="https://coreui.io/demo/vendors/datatables.net-bs4/css/dataTables.bootstrap4.css">

@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Receipts</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
             @include('flash::message')
             <div class="row">
                 <div class="col-lg-12">
                     <div class="card">
                         <div class="card-header">
                             <i class="fa fa-align-justify"></i>
                             Receipts
                             <a class="pull-right" href="{{ route('receipts.create') }}"><i class="fa fa-plus-square fa-lg"></i></a>
                         </div>
                         <div class="card-body">
                             @include('receipts.table')
                              <div class="pull-right mr-3">
                                     
                              </div>
                         </div>
                     </div>
                  </div>
             </div>
         </div>
    </div>
@endsection

