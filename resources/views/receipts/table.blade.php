
<div class="box">

    <!-- /.box-header -->
    <div class="box-body" style="overflow-x: scroll;">
      <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive-sm">
        <thead>
            <tr>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($receipts as $receipt)
            <tr>
                
                <td>
                    {!! Form::open(['route' => ['receipts.destroy', $receipt->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('receipts.show', [$receipt->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('receipts.edit', [$receipt->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>