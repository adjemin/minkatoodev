{{-- <!-- First Name Field -->
<div class="form-group">
    {!! Form::label('first_name', 'Prénoms:') !!}
    <p>{{ $customer->first_name }}</p>
</div>

<!-- Last Name Field -->
<div class="form-group">
    {!! Form::label('last_name', 'Nom:') !!}
    <p>{{ $customer->last_name }}</p>
</div>


<!-- Phone Number Field -->
<div class="form-group">
    {!! Form::label('phone_number', 'Téléphone:') !!}
    <p>{{ $customer->phone_number }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $customer->email }}</p>
</div>

<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', 'Sexe:') !!}
    <p>{{ $customer->gender }}</p>
</div>

<!-- Bio Field -->
<div class="form-group">
    {!! Form::label('bio', 'Bio:') !!}
    <p>{{ $customer->bio }}</p>
</div>

<!-- Birthday Field -->
<div class="form-group">
    {!! Form::label('birthday', 'Date de naissance:') !!}
    <p>{{ $customer->birthday }}</p>
</div>

<!-- Language Field -->
<div class="form-group">
    {!! Form::label('language', 'Langue:') !!}
    <p>{{ $customer->language }}</p>
</div>

<!-- Customer Type Field -->
<div class="form-group">
    {!! Form::label('customer_type', 'Type du Client:') !!}
    <p>{{ $customer->customer_type }}</p>
</div>
 --}}


<div class="col-md-4">
    {{-- @php
        $customer = $customer->getCustomer();
    @endphp --}}
    {{-- @if(!is_null($customer)) --}}
        <div class="">
            <div class="content">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img
                            src="{{ $customer->picture ?? 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQEyza1-CKEJK6Hyq50yE8oOpyba5HbJaCqMioudHzaxVh-a9Is' }}"
                            width="100">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center"><span
                            class="badge badge-success text-{{ $customer->otp_verified == 1 ? 'success' : 'danger' }}">{{ $customer->otp_verified == 1 ? 'Client actif' : 'Client inactif' }}</span>
                    </div>
                </div>
                <div class="row">
                    <div
                        class="col-md-12 text-center">{{ !is_null($customer->name) ? $customer->name.' | ' : '' }} {{ !is_null($customer->language) ? $customer->language : 'Fr' }}</div>
                </div>
            </div>
        </div>
        
    <div class="">
        <div class="header">
            {{--                <h4 class="title">Client</h4>--}}
            <br>
            <p class="category">
                Détails du client
            </p>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-5">
                            <strong>Nom<span class="pull-right">:</span></strong>
                        </div>
                        <div class="col-md-7">
                            {{ $customer->name }}<br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <strong>Téléphone<span class="pull-right">:</span></strong>
                        </div>
                        <div class="col-md-7">
                            {{$customer->phone_number}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <strong>Email<span class="pull-right">:</span></strong>
                        </div>
                        <div class="col-md-7">
                            {{$customer->email}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="">
        <div class="header">
            <p class="category">
                Les dates de modifications.
            </p>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-5"><strong>Création<span class="pull-right">:</span></strong>
                </div>
                <div
                    class="col-md-7">{{ !is_null($customer->created_at) ? $customer->created_at : '---' }}</div>
            </div>
            <div class="row">
                <div class="col-md-5"><strong>Modification<span
                            class="pull-right">:</span></strong></div>
                <div
                    class="col-md-7">{{ !is_null($customer->updated_at) ? $customer->updated_at : '---' }}</div>
            </div>
            <div class="row">
                <div class="col-md-5"><strong>Suppression<span
                            class="pull-right">:</span></strong></div>
                <div
                    class="col-md-7">{{ !is_null($customer->deleted_at) ? $customer->deleted_at : '---' }}</div>
            </div>
        </div>
    </div>
</div>
