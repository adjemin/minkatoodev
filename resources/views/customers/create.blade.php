@extends('layouts.app')
@section("css")
    <link rel="stylesheet" href="{{asset('css/intlTelInput.css')}}">
    <link rel="stylesheet" href="{{asset('css/demo.css')}}">

@endsection

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('customers.index') !!}">Clients</a>
      </li>
      <li class="breadcrumb-item active">Créer</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Créer Client</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'customers.store']) !!}

                                   @include('customers.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
@section("scripts")
<script src="{{asset('js/intlTelInput.js')}}"></script>
<script>
  var input = document.querySelector("#phone");

        var   errorMsg = document.querySelector("#error-msg"),
            validMsg = document.querySelector("#valid-msg");

        // here, the index maps to the error code returned from getValidationError - see readme
        var errorMap = [ "&#65794; Numéro Invalide !", "&#65794; Code de pays invalide !", "&#65794; Numéro de téléphone Trop Court", "&#65794; Numéro de téléphone Trop Long", "&#65794; Numéro Invalide"];
        var iti = window.intlTelInput(input, {
            // allowDropdown: false,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            dropdownContainer: document.body,
            // excludeCountries: ["us"],
            // formatOnDisplay: false,
            initialCountry: "auto",
            separateDialCode: true,
            geoIpLookup: function(success, failure) {
                $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "CI";
                    success(countryCode);
                });
            },
            // hiddenInput: "full_number",
            // localizedCountries: { 'de': 'Deutschland' },
            //nationalMode: true,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            placeholderNumberType: "MOBILE",
             preferredCountries: ['ci'],
            utilsScript: "{{asset('js/utils.js?1562189064761')}}",
        });
        var reset = function() {
            input.classList.remove("error");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };

        // on blur: validate
        input.addEventListener('blur', function() {
            reset();
            if (input.value.trim()) {
                if (iti.isValidNumber()) {
                    $("#dial_code").val(iti.getSelectedCountryData().dialCode);
                   // $("#phone_number").val(iti.);
                    $("#country_code").val(iti.getSelectedCountryData().iso2.toUpperCase());
                    //console.log(iti.s.phone);
                    validMsg.classList.remove("hide");
                } else {
                    input.classList.add("error");
                    var errorCode = iti.getValidationError();
                    errorMsg.innerHTML = errorMap[errorCode];
                    errorMsg.classList.remove("hide");
                }
            }
        });

        // on keyup / change flag: reset
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);
    </script>
@endsection
