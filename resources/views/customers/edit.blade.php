@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('customers.index') !!}">Client</a>
          </li>
          <li class="breadcrumb-item active">Modifier</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Modifier Client</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($customer, ['route' => ['customers.update', $customer->id], 'method' => 'patch']) !!}

                              @include('customers.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection