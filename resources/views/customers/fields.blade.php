<!-- First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'Prénoms:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Nom:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Téléphone (*):') !!} <br>
    {!! Form::tel('phone', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <input type="hidden" name="dial_code" id="dial_code">
    <input type="hidden" name="phone_number" id="phone_number">
    <input type="hidden" name="country_code" id="country_code" value=""/>
    <span id="valid-msg" class="hide"></span>
    <span id="error-msg"  class="hide" style="color:red;"></span>
</div>

<!-- Photo Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo_url', 'Photo:') !!}
    <div class="input-group">
    <input accept="image/*" onchange="loadFile(event)" class="form-control" type="file" name="photo_url">
    </div>
    <img id="output" style="margin-top:15px;max-height:100px;">

    <script>
  var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };
</script>
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Gender Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gender', 'Sexe:') !!}
    {!! Form::select('gender',$genders, null, ['class' => 'form-control']) !!}
</div>

<!-- Bio Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bio', 'Bio:') !!}
    {!! Form::text('bio', null, ['class' => 'form-control']) !!}
</div>

<!-- Birthday Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthday', 'Date de naissance:') !!}
    {!! Form::date('birthday', null, ['class' => 'form-control']) !!}
</div>

<!-- Language Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language', 'Langage:') !!}
    {!! Form::text('language', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_type', 'Type du client:') !!}
    {!! Form::select('customer_type',$type, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('customers.index') }}" class="btn btn-secondary">Annuler</a>
</div>
