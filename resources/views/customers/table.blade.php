
<div class="box">

    <!-- /.box-header -->
    <div class="box-body" style="overflow-x: scroll;">
      <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive-sm">
        <thead>
            <tr>
            <th>Prénoms</th>
        <th>Nom</th>
        <th>Nom & Prénoms</th>
        <th>Numéro</th>
        <th>Téléphone</th>
        <th>Indicatif numéro</th>
        <th>Code pays</th>
        <th>Email</th>
        <th>Photo</th>
        <th>Sexe</th>
        <th>Bio</th>
        <th>Date de naissance</th>
        <th>Langue</th>
        <th>Type du Client</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($customers as $customer)
            <tr>
                <td>{{ $customer->first_name }}</td>
            <td>{{ $customer->last_name }}</td>
            <td>{{ $customer->name }}</td>
            <td>{{ $customer->phone_number }}</td>
            <td>{{ $customer->phone }}</td>
            <td>{{ $customer->dial_code }}</td>
            <td>{{ $customer->country_code }}</td>
            <td>{{ $customer->email }}</td>
            <td>{{ $customer->photo_url }}</td>
            <td>{{ $customer->gender }}</td>
            <td>{{ $customer->bio }}</td>
            <td>{{ $customer->birthday }}</td>
            <td>{{ $customer->language }}</td>
            <td>{{ $customer->customer_type }}</td>
                <td>
                    {!! Form::open(['route' => ['customers.destroy', $customer->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('customers.show', [$customer->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('customers.edit', [$customer->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Êtes vous sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>