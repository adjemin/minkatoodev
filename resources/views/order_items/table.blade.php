
<div class="box">

    <!-- /.box-header -->
    <div class="box-body" style="overflow-x: scroll;">
      <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive-sm">
        <thead>
            <tr>
            <th>Identifiant de la commande</th>
            <th>Identifiant de l'offre</th>
            <th>Quantité</th>
            <th>Prix unitaire</th>
            <th>Code de la devise</th>
            <th>Montant total</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            {{-- @dd($orderItems) --}}
            @forelse($orderItems as $orderItem)

                <tr>
                    <td>{{ $orderItem->order_id }}</td>
                    <td>{{ $orderItem->offer != null ? $orderItem->offer->title : "non renseigné"}}</td>
                    <td>{{ $orderItem->quantity }}</td>
                    <td>{{ $orderItem->unit_price }}</td>
                    <td>{{ $orderItem->currency_code }}</td>
                    <td>{{ $orderItem->total_amount }}</td>
                    <td>
                        {!! Form::open(['route' => ['orderItems.destroy', $orderItem->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('orderItems.show', [$orderItem->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                            <a href="{{ route('orderItems.edit', [$orderItem->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @empty
                <div class="d-flex justify-content-center">
                    Aucune commande éffectuée.
                </div>
            @endforelse
        </tbody>
    </table>
</div>
</div>
