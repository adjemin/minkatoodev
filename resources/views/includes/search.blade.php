<header>
    {{-- // Included connexion state from layouts.main --}}
    <!-- Main Header Image -->
    <div class="showcase">
        <img src="img/sea-dawn-nature-sky-127160.png" alt="" class="img-fluid" style="height: 490px">
        <div class="blackOverlay"></div>

    </div>
     <form action="" id="navBarSearch" type="input" class="col-12 col-sm-10 col-md-8 col-lg-6">
            <style>
                input {border:0;outline:0; width: 450px;}
                input:focus {outline:none!important;}
            </style>
        <div>
                <span class="">
                    <!-- Place Holder Icon -->
                    <span>
                        <img src="img/placeholder-for-map@1X.png" alt="" width="15px">
                    </span>
                <input type="text" placeholder="Abidjan, Côte d'ivoire" id="input_search">
            </span>
            <button type="submit">
                <!-- Search icon -->
                <img src="img/search (1)@1X.png" alt="" width="15px">
            </button>
        </div>
    </form>
</header>
