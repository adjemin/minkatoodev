

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('musees.index') }}" class="btn btn-secondary">Cancel</a>
</div>
