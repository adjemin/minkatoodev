<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Minkatoo - Payer Via AdjeminPay</title>
    </head>
    <body>
        <div class="demo">
            <div id="result">
                <h1 id="result-title"></h1>
                <p id="result-message"></p>
                <p id="result-status"></p>
            </div>

            <input type="hidden" id="amount" value="{{ $amount }}">
            <input type="hidden" id="currency" value="CFA">

            <!-- NB: La longeur maximum d'un id de transaction est de 191 caractères -->

            <input type="hidden" id="transaction_id"
            value="{{ $transactionId }}">
            <!-- Champ personnalisé où vous pourrez définir des informations supplémentaires à votre transaction  -->
            <!-- le champ custom est facultatif -->
            <input type="hidden" id="custom_field" value="custom text">

            <input type="hidden" id="designation" value="Réservation Pour Consultation">
            <input type="hidden" id="adp_signature" >

            <button id="payBtn" class="btn btn-success border-radius" style="display:none;">
                Payer
            </button>

        </div>

        <script src="https://api-dev.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript"></script>
            {{--
            <script src="https://api-dev.adjeminpay.net/release/seamless/latest/adjeminpay.min.js" type="text/javascript"></script>
            --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

        <script type="text/javascript">
            var AdjeminPay = AdjeminPay();
            var signature = "";

            AdjeminPay.on('init', function (e) {
                // retourne une erreur au cas où votre API_KEY ou APPLICATION_ID est incorrecte
                signature = e;
                //console.log(signature)
                $("#adp_signature").val(signature);

            });

            // Lance une requete ajax pour vérifier votre API_KEY et APPLICATION_ID et initie le paiement
            AdjeminPay.init({
                client_id : 99,
                client_secret : "8cMcVcLH3XPRSmP2OxDo7ZV5tmdVnIDg5uvHY5uD",
                transaction_id : $('#transaction_id').val(),
                designation :  $('#designation').val(),
                amount :  parseInt($('#amount').val()),

            });

            // Ecoute le feedback sur les erreurs
            AdjeminPay.on('error', function (e) {
                // la fonction que vous définirez ici sera exécutée en cas d'erreur
                console.log(e);
                $("#result-title").html(e.title);
                $("#result-message").html(e.message);
                $("#result-status").html(e.status);
            });

            // Lancer la procédure de paiement au click
            $('#payBtn').on('click', function () {
                // Vérifie vos informations et prépare le paiement
                // S'il y a une erreur à cette étape, AdjeminPay.on('error') sera exécuté

                AdjeminPay.preparePayment({
                    amount: parseInt($('#amount').val()),
                    transaction_id: $('#transaction_id').val(),
                    currency: $('#currency').val(),
                    designation: $('#designation').val(),
                    notify_url: "{{ route('api.payments.notify') }}",
                    return_url :"{{ url('customer/profil') }}",
                    cancel_url : "{{ url('/') }}",
                    signature : $('#adp_signature').val()
                    // le notify_url est TRES IMPORTANT
                    // c'est lui qui permettra de notifier votre backend
                });

                // Si l'étape précédante n'a pas d'erreur,
                // cette ligne génère et affiche l'interface de paiement AdjeminPay
                AdjeminPay.renderPaymentView();
            });

            $(document).ready(function() {
                setTimeout(() => {
                    $('#payBtn').click()
                }, 3000);
            });

        </script>

    </body>
</html>
