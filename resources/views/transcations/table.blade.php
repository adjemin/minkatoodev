<div class="table-responsive-sm">
    <table class="table table-striped" id="transcations-table">
        <thead>
            <tr>
                <th>Amount</th>
        <th>Currency</th>
        <th>Transaction Id</th>
        <th>Designation</th>
        <th>Notify Url</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($transcations as $transcation)
            <tr>
                <td>{{ $transcation->amount }}</td>
            <td>{{ $transcation->currency }}</td>
            <td>{{ $transcation->transaction_id }}</td>
            <td>{{ $transcation->designation }}</td>
            <td>{{ $transcation->notify_url }}</td>
                <td>
                    {!! Form::open(['route' => ['transcations.destroy', $transcation->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('transcations.show', [$transcation->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('transcations.edit', [$transcation->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>