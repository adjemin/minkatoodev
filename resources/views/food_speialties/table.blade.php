<div class="table-responsive-sm">
    <table class="table table-striped" id="foodSpeialties-table">
        <thead>
            <th>Image</th>
        <th>Name</th>
        <th>Slug</th>
            <th colspan="3">Action</th>
        </thead>
        <tbody>
        @foreach($foodSpeialties as $foodSpeialties)
            <tr>
                <td>
                <img src="{{ $foodSpeialties->image }}" height="70" width="70">
                </td>
            <td>{{ $foodSpeialties->name }}</td>
            <td>{{ $foodSpeialties->slug }}</td>
                <td>
                    {!! Form::open(['route' => ['foodSpeialties.destroy', $foodSpeialties->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('foodSpeialties.show', [$foodSpeialties->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('foodSpeialties.edit', [$foodSpeialties->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>