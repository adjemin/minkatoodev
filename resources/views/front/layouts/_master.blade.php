<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/png" href="{{asset('front/images/LOGO_MINKATOO.png')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>@yield('page_title') &#8211; {{ config('app.name') }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>

    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <link href="{{asset('front/css/style-freedom.css')}}" rel="stylesheet"/>

    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">

{{--    <link rel="stylesheet" href="{{ asset('front/css/bootstrap.min.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('front/css/perso.css') }}">

    <style>
        body {
            font-family: 'Montserrat', sans-serif;
        }
    </style>
@yield('css')


<body style="font-family: 'Montserrat', sans-serif;">
<div class="content">

    @include('front.layouts.common.navbar')
    <div class="justify-content-center">
        @yield('content')

    </div>
</div>
</body>
@yield('script')
{{--<script src="{{ asset('front/js/bootstrap.min.js') }}"></script>--}}
</html>

