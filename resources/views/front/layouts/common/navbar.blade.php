<div class="w3l-headers-9">
    <header>
        <div class="wrapper">
            <div class="header">
                <div>
                    <h1>
{{--                        <a href="https://minkatoo.adjemincloud.com" class="logo">Minkatoo</a>--}}
                        <!-- if logo is image enable this -->
                    <a class="logo" href="https://minkatoo.adjemincloud.com">
                        <img src="{{ asset('front/images/LOGO_MINKATOO.png') }}" alt="Minkatoo" title="Minkatoo" style="height: 70px" />
                    </a>
                    </h1>
                </div>
                <div class="bottom-menu-content">
                    <input type="checkbox" id="nav"/>
                    <label for="nav" class="menu-bar"></label>
                    <nav>
                        <ul class="menu">
                            <li><a href="#" class="link-nav">S'inscrire</a></li>
                            <li class="nav-right-sty">
                                <a href="#" class="actionbg">Se connecter</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- //header -->
</div>

