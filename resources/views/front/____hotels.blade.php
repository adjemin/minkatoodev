@extends('layouts.master')

@section('content')
	<section class="home-three bg-img-hotel">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="home3_home_content">
						<h1>Explorez, découvrez et économisez</h1>
						<h4>Découvrez les meilleures Hotel avec Minkatoo.</h4>
					</div>
				</div>
				<div class="col-lg-4">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 offset-lg-3 mt-3">
					<div class="home_adv_srch_opt home3">
						<div class="home1_adsrchfrm">
							<div class="home1-advnc-search home3">
								<ul class="h1ads_1st_list mb0">
									<li class="list-inline-item">
										<div class="form-group has-search">
											<span class="fa fa-search form-control-feedback"></span>
											<input type="text" class="form-control" id="exampleInputName1" placeholder="Abidjan, Côte d'ivoire">
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="maxw1600 m0a">
		<section class="mt20">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 col-sm-4">
						<nav id="iconNavbar">
							<ul class="">
								<li class="iconItem active">
									<a href="/hotels" class="">
										<p>Hotels</p>
										<div>
											<!-- Item image -->
											<img src="{{asset('img/hotel@1X.png')}}" alt="Hotels">

										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/restaurants">
										<p>Restaurants</p>
										<div>
											<!-- Item image -->
											<img src="{{asset('img/plate-fork-and-knife@1X.png')}}" alt="restaurants">

										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/excursion">

										<p>Excursion</p>
										<div>
											<!-- Item image -->
											<img src="{{asset('img/sunset@1X.png')}}" alt="Excursion">
										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/location">
										<p>Location Voiture</p>
										<div>
											<!-- Item image -->
											<img src="{{asset('img/car@1X.png')}}" alt="location Voiture">

										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/musees">
										<p>Musée</p>
										<div>
											<!-- Item image -->
											<img src="{{asset('img/museum@1X.png')}}" alt="musée">
										</div>
									</a>
								</li>
								<!--  -->
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</section>

	<!-- Hôtel à proximité -->
	<section id="best-property" class="best-property pt100 pb0">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="main-title">
						<h2>Hotels à proximité</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/hotel-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/17/5a/2c/52/guest-room.jpg?w=300&h=-1&s=1" alt="fp4.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
							<div class="details">
								<div class="tc_content">
									<h4>Pullman Abidjan</h4>
									<div class="_2NK4P3lO">
										<span class="sspd_review float-left">
											<ul>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</span>
										<span class="_28oqjHA2">210&nbsp;avis</span>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/hotel-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/03/2b/af/4e/la-piscine.jpg?w=400&h=300&s=1" alt="fp5.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
							<div class="details">
								<div class="tc_content">
									<h4>Villa Anakao</h4>
									<div class="_2NK4P3lO">
										<span class="sspd_review float-left">
											<ul>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</span>
										<span class="_28oqjHA2">210&nbsp;avis</span>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/hotel-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1b/46/46/a3/exterior-view.jpg?w=300&h=-1&s=1" alt="fp6.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
							<div class="details">
								<div class="tc_content">
									<h4>Sofitel Abidjan Hôtel Ivoire</h4>
									<div class="_2NK4P3lO">
										<span class="sspd_review float-left">
											<ul>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</span>
										<span class="_28oqjHA2">210&nbsp;avis</span>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/hotel-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/13/54/3e/39/hotel-tiama.jpg?w=400&h=300&s=1" alt="fp6.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
							<div class="details">
								<div class="tc_content">
									<h4>Hotel Tiama</h4>
									<div class="_2NK4P3lO">
										<span class="sspd_review float-left">
											<ul>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</span>
										<span class="_28oqjHA2">210&nbsp;avis</span>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/hotel-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/0a/b5/28/67/standard-room.jpg?w=400&h=300&s=1" alt="fp6.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
							<div class="details">
								<div class="tc_content">
									<h4>Radisson Blu Hotel, Abidjan Airport</h4>
									<div class="_2NK4P3lO">
										<span class="sspd_review float-left">
											<ul>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</span>
										<span class="_28oqjHA2">210&nbsp;avis</span>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/hotel-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/0d/61/ff/08/au-bord-de-l-eau.jpg?w=400&h=300&s=1" alt="fp6.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
							<div class="details">
								<div class="tc_content">
									<h4>Novotel Abidjan</h4>
									<div class="_2NK4P3lO">
										<span class="sspd_review float-left">
											<ul>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</span>
										<span class="_28oqjHA2">210&nbsp;avis</span>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- Residence à proximité -->
<section id="best-property" class="best-property pt100 pb0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-title">
					<h2>Résidence à proximité</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-lg-3">
				<div class="feat_property home3">
					<a href="/hotel-detail" class="_3uYDFt8_">
						<div class="thumb">
							<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/17/5a/2c/52/guest-room.jpg?w=300&h=-1&s=1" alt="fp4.jpg">
							<div class="thmb_cntnt">
								<ul class="icon mb0">
									<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
									<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
								</ul>
								<a class="fp_price" href="#"></a>
							</div>
						</div>
						<div class="details">
							<div class="tc_content">
								<h4>Pullman Abidjan</h4>
								<div class="_2NK4P3lO">
									<span class="sspd_review float-left">
										<ul>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
										</ul>
									</span>
									<span class="_28oqjHA2">210&nbsp;avis</span>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3">
				<div class="feat_property home3">
					<a href="/hotel-detail" class="_3uYDFt8_">
						<div class="thumb">
							<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/03/2b/af/4e/la-piscine.jpg?w=400&h=300&s=1" alt="fp5.jpg">
							<div class="thmb_cntnt">
								<ul class="icon mb0">
									<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
									<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
								</ul>
								<a class="fp_price" href="#"></a>
							</div>
						</div>
						<div class="details">
							<div class="tc_content">
								<h4>Villa Anakao</h4>
								<div class="_2NK4P3lO">
									<span class="sspd_review float-left">
										<ul>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
										</ul>
									</span>
									<span class="_28oqjHA2">210&nbsp;avis</span>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3">
				<div class="feat_property home3">
					<a href="/hotel-detail" class="_3uYDFt8_">
						<div class="thumb">
							<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1b/46/46/a3/exterior-view.jpg?w=300&h=-1&s=1" alt="fp6.jpg">
							<div class="thmb_cntnt">
								<ul class="icon mb0">
									<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
									<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
								</ul>
								<a class="fp_price" href="#"></a>
							</div>
						</div>
						<div class="details">
							<div class="tc_content">
								<h4>Sofitel Abidjan Hôtel Ivoire</h4>
								<div class="_2NK4P3lO">
									<span class="sspd_review float-left">
										<ul>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
										</ul>
									</span>
									<span class="_28oqjHA2">210&nbsp;avis</span>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3">
				<div class="feat_property home3">
					<a href="/hotel-detail" class="_3uYDFt8_">
						<div class="thumb">
							<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/13/54/3e/39/hotel-tiama.jpg?w=400&h=300&s=1" alt="fp6.jpg">
							<div class="thmb_cntnt">
								<ul class="icon mb0">
									<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
									<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
								</ul>
								<a class="fp_price" href="#"></a>
							</div>
						</div>
					</a>
					<div class="details">
						<div class="tc_content">
							<h4>Hotel Tiama</h4>
							<div class="_2NK4P3lO">
								<span class="sspd_review float-left">
									<ul>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
									</ul>
								</span>
								<span class="_28oqjHA2">210&nbsp;avis</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3">
				<div class="feat_property home3">
					<a href="/hotel-detail" class="_3uYDFt8_">
						<div class="thumb">
							<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/0a/b5/28/67/standard-room.jpg?w=400&h=300&s=1" alt="fp6.jpg">
							<div class="thmb_cntnt">
								<ul class="icon mb0">
									<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
									<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
								</ul>
								<a class="fp_price" href="#"></a>
							</div>
						</div>
					</a>
					<div class="details">
						<div class="tc_content">
							<h4>Radisson Blu Hotel, Abidjan Airport</h4>
							<div class="_2NK4P3lO">
								<span class="sspd_review float-left">
									<ul>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
									</ul>
								</span>
								<span class="_28oqjHA2">210&nbsp;avis</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-lg-3">
				<div class="feat_property home3">
					<a href="/hotel-detail" class="_3uYDFt8_">
						<div class="thumb">
							<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/0d/61/ff/08/au-bord-de-l-eau.jpg?w=400&h=300&s=1" alt="fp6.jpg">
							<div class="thmb_cntnt">
								<ul class="icon mb0">
									<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
									<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
								</ul>
								<a class="fp_price" href="#"></a>
							</div>
						</div>
					</a>
					<div class="details">
						<div class="tc_content">
							<h4>Novotel Abidjan</h4>
							<div class="_2NK4P3lO">
								<span class="sspd_review float-left">
									<ul>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
										<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
									</ul>
								</span>
								<span class="_28oqjHA2">210&nbsp;avis</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
	</div>
@endsection
