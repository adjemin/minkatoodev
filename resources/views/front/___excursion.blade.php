@extends('layouts.master')

@section('content')
	<section class="home-three bg-img-excu">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="home3_home_content">
						<h1>Explorez, découvrez et économisez</h1>
						<h4>Découvrez les meilleures excusions avec Minkatoo.</h4>
					</div>
				</div>
				<div class="col-lg-4">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 offset-lg-3 mt-3">
					<div class="home_adv_srch_opt home3">
						<div class="home1_adsrchfrm">
							<div class="home1-advnc-search home3">
								<ul class="h1ads_1st_list mb0">
									<li class="list-inline-item">
										<div class="form-group has-search">
											<span class="fa fa-search form-control-feedback"></span>
											<input type="text" class="form-control" id="exampleInputName1" placeholder="Abidjan, Côte d'ivoire">
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="maxw1600 m0a">
		<section class="mt20">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 col-sm-4">
						<nav id="iconNavbar">
							<ul class="">
								<li class="iconItem">
									<a href="/view/hotels" class="">
										<p>Hotels</p>
										<div>
											<!-- Item image -->
											<img src="img/hotel@1X.png" alt="Hotels">

										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/view/restaurants">

										<p>Restaurants</p>
										<div>
											<!-- Item image -->
											<img src="img/plate-fork-and-knife@1X.png" alt="restaurants">

										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem active">
									<a href="/excursion">

										<p>Excursion</p>
										<div>
											<!-- Item image -->
											<img src="img/sunset@1X.png" alt="Excursion">
										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/location">
										<p>Location Voiture</p>
										<div>
											<!-- Item image -->
											<img src="img/car@1X.png" alt="location Voiture">

										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/musees">
										<p>Musée</p>
										<div>
											<!-- Item image -->
											<img src="img/museum@1X.png" alt="musée">
										</div>
									</a>
								</li>
								<!--  -->
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</section>

    <section id="property-city" class="property-city pb30">
      <div class="container-fluid ovh">
        <div class="row">
          <div class="col-lg-12">
            <div class="main-title mb40">
              <h2>Nos Activités en ce moment</h2>
            </div>
          </div>
          <div class="col-lg-6 col-xl-6">
            <div class="properti_city">
              <div class="thumb"><img class="img-fluid w100" src="{{asset('images/Minkatoo/ID69480461_2747307518614738_17@2x.png')}}" alt="pc2.jpg"></div>
              <div class="overlay">
                <div class="details">
                  <h4>Excursions et visites</h4>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-xl-6">
            <div class="properti_city">
              <div class="thumb"><img class="img-fluid w100" src="{{asset('images/Minkatoo/ID69256094_2722453797766777_35.png')}}" alt="pc2.jpg" style="height: 350px;"></div>
              <div class="overlay">
                <div class="details">
                  <h4>Visites guidées</h4>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-xl-6">
            <div class="properti_city">
              <div class="thumb"><img class="img-fluid w100" src="{{asset('images/Minkatoo/site-historique.png')}}" alt="pc2.jpg"></div>
              <div class="overlay">
                <div class="details">
                  <h4>Sites historiques</h4>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-xl-6">
            <div class="properti_city">
              <div class="thumb"><img class="img-fluid w100" src="{{asset('images/Minkatoo/ID50144.png')}}" alt="pc2.jpg"></div>
              <div class="overlay">
                <div class="details">
                  <h4>Randonnées</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

	<!-- Nos recommandations -->
	<section id="best-property" class="best-property pb0">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="main-title">
						<h2>Nos recommandations</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/excursion-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="{{asset('images/Minkatoo/ID67744234_2696675700344587_54.png')}}" alt="fp4.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
							<div class="details">
								<div class="tc_content">
									<h4>Feu de bois</h4>
									<div class="_2NK4P3lO">
										<span class="sspd_review float-left">
											<ul>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</span>
										<span class="_28oqjHA2">210&nbsp;avis</span>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/excursion-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="{{asset('images/Minkatoo/ID66790144_2645099732168851_45.png')}}" alt="fp5.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
							<div class="details">
								<div class="tc_content">
									<h4>la caserne</h4>
									<div class="_2NK4P3lO">
										<span class="sspd_review float-left">
											<ul>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</span>
										<span class="_28oqjHA2">210&nbsp;avis</span>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/excursion-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="{{asset('images/Minkatoo/ID59285701_2521132871232205_55.png')}}" alt="fp6.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
						</a>
						<div class="details">
							<div class="tc_content">
								<h4>Ile Flottante</h4>
								<div class="_2NK4P3lO">
									<span class="sspd_review float-left">
										<ul>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
										</ul>
									</span>
									<span class="_28oqjHA2">210&nbsp;avis</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/hotel-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="{{asset('images/Minkatoo/ID84614242_3214706831874802_14.png')}}" alt="fp6.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
						</a>
						<div class="details">
							<div class="tc_content">
								<h4>Pont de Liane</h4>
								<div class="_2NK4P3lO">
									<span class="sspd_review float-left">
										<ul>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
										</ul>
									</span>
									<span class="_28oqjHA2">210&nbsp;avis</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>
@endsection
