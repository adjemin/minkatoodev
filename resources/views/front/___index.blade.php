@extends('layouts.master')

@section('content')
	<!-- Home Design --
	<section class="home-two p0">
		<div class="container-fluid p0">
			<div class="row">
				<div class="col-lg-12">
				</div>
			</div>
		</div>
	</section-->
	<section class="home-three bg-img3">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="home3_home_content">
						<h1>Explorez, découvrez et économisez</h1>
						<h4>Découvrez les meilleures offres avec Minkatoo.</h4>
					</div>
				</div>
				<div class="col-lg-4">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 offset-lg-3 mt-3">
					<div class="home_adv_srch_opt home3">
						<div class="home1_adsrchfrm">
							<div class="home1-advnc-search home3">
								<ul class="h1ads_1st_list mb0">
									<li class="list-inline-item">
										<div class="form-group has-search">
											<span class="fa fa-search form-control-feedback"></span>
											<input type="text" class="form-control" id="exampleInputName1" placeholder="Abidjan, Côte d'ivoire">
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="maxw1600 m0a">
		<section class="mt20">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 col-sm-4">
						<nav id="iconNavbar">
							<ul class="">
								<li class="iconItem">
									<a href="/view/hotels" class="">
										<p>Hotels</p>
										<div>
											<!-- Item image -->
											<img src="img/hotel@1X.png" alt="Hotels">
										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/view/restaurants">
										<p>Restaurants</p>
										<div>
											<!-- Item image -->
											<img src="img/plate-fork-and-knife@1X.png" alt="restaurants">
										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/excursion">

										<p>Excursion</p>
										<div>
											<!-- Item image -->
											<img src="img/sunset@1X.png" alt="Excursion">
										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/location">
										<p>Location Voiture</p>
										<div>
											<!-- Item image -->
											<img src="img/car@1X.png" alt="location Voiture">

										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/musees">
										<p>Musée</p>
										<div>
											<!-- Item image -->
											<img src="img/museum@1X.png" alt="musée">
										</div>
									</a>
								</li>
								<!--  -->
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</section>
		<!-- Hotel à proximité -->
		<section id="feature-property" class="feature-property" style="margin-top:-60px;">
			<div class="container-fluid ovh">
				<div class="row">
					<div class="col-lg-12">
						<div class="main-title mb40">
							<h2>Hotels à proximité</h2>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="feature_property_home3_slider">
							<div class="item">
								<div class="feat_property home3">
									<a href="/hotel-detail" class="_3uYDFt8_">
										<div class="thumb">
											<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/17/5a/2c/52/guest-room.jpg?w=300&h=-1&s=1" alt="fp1.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
												<a href="/hotel-detail">&nbsp;</a>
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>Pullman Abidjan</h4>
												<div class="_2NK4P3lO">
													<span class="sspd_review float-left">
														<ul>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														</ul>
													</span>
													<span class="_28oqjHA2">210&nbsp;avis</span>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<a href="/hotel-detail" class="_3uYDFt8_">
										<div class="thumb">
											<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/03/2b/af/4e/la-piscine.jpg?w=400&h=300&s=1" alt="fp2.jpg">
											<div class="thmb_cntnt">
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>Villa Anakao</h4>
												<div class="_2NK4P3lO">
													<span class="sspd_review float-left">
														<ul>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														</ul>
													</span>
													<span class="_28oqjHA2">210&nbsp;avis</span>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<a href="/hotel-detail" class="_3uYDFt8_">
										<div class="thumb">
											<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1b/46/46/a3/exterior-view.jpg?w=300&h=-1&s=1" alt="fp3.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>Sofitel Abidjan Hôtel Ivoire</h4>
												<div class="_2NK4P3lO">
													<span class="sspd_review float-left">
														<ul>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														</ul>
													</span>
													<span class="_28oqjHA2">210&nbsp;avis</span>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<a href="/hotel-detail" class="_3uYDFt8_">
										<div class="thumb">
											<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/13/54/3e/39/hotel-tiama.jpg?w=400&h=300&s=1" alt="fp1.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>Hotel Tiama</h4>
												<div class="_2NK4P3lO">
													<span class="sspd_review float-left">
														<ul>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														</ul>
													</span>
													<span class="_28oqjHA2">210&nbsp;avis</span>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<div class="thumb">
										<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/0f/61/04/ca/seen-hotel-abidjan-plateau.jpg?w=300&h=300&s=1" alt="fp1.jpg">
										<div class="thmb_cntnt">
											<ul class="icon mb0">
												<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
												<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
											</ul>
										</div>
									</div>
									<div class="details">
										<div class="tc_content">
											<h4>Seen Hotel Abidjan Plateau</h4>
											<div class="_2NK4P3lO">
												<span class="sspd_review float-left">
													<ul>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
													</ul>
												</span>
												<span class="_28oqjHA2">210&nbsp;avis</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<div class="thumb">
										<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/0d/61/ff/08/au-bord-de-l-eau.jpg?w=400&h=300&s=1" alt="fp2.jpg">
										<div class="thmb_cntnt">
											<ul class="icon mb0">
												<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
												<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
											</ul>
										</div>
									</div>
									<div class="details">
										<div class="tc_content">
											<h4>Novotel Abidjan</h4>
											<div class="_2NK4P3lO">
												<span class="sspd_review float-left">
													<ul>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
													</ul>
												</span>
												<span class="_28oqjHA2">210&nbsp;avis</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<div class="thumb">
										<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/0a/b5/28/67/standard-room.jpg?w=400&h=300&s=1" alt="fp3.jpg">
										<div class="thmb_cntnt">
											<ul class="icon mb0">
												<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
												<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
											</ul>
										</div>
									</div>
									<div class="details">
										<div class="tc_content">
											<h4>Radisson Blu Hotel, Abidjan Airport</h4>
											<div class="_2NK4P3lO">
												<span class="sspd_review float-left">
													<ul>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
													</ul>
												</span>
												<span class="_28oqjHA2">210&nbsp;avis</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section

		<!--  Restaurant à proximité -->
		<section id="feature-property" class="feature-property pb50" style="margin-top:-160px;">
			<div class="container-fluid ovh">
				<div class="row">
					<div class="col-lg-12">
						<div class="main-title mb40">
							<h2>Restaurants à proximité</h2>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="feature_property_home3_slider">
							<div class="item">
								<div class="feat_property home3">
									<a href="/restaurant-detail" class="_3uYDFt8_">
										<div class="thumb">
											<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-s/0f/f0/36/03/restaurant-regina-margherita.jpg" alt="fp1.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>Regina Margherita</h4>
												<p><span class="flaticon-placeholder"></span> II Plateaux Vallon, Abidjan Côte d&#39;Ivoire</p>
												<div class="_2NK4P3lO">
													<span class="sspd_review float-left">
														<ul>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														</ul>
													</span>
													<span class="_28oqjHA2">210&nbsp;avis</span>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<a href="/restaurant-detail" class="_3uYDFt8_">
										<div class="thumb">
											<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-f/18/9f/ae/a4/photo3jpg.jpg" alt="fp2.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>La Taverne Romaine</h4>
												<p><span class="flaticon-placeholder"></span> Boulevard Lagunaire, Abidjan Côte d&#39;Ivoire</p>
												<div class="_2NK4P3lO">
													<span class="sspd_review float-left">
														<ul>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														</ul>
													</span>
													<span class="_28oqjHA2">210&nbsp;avis</span>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<a href="/restaurant-detail" class="_3uYDFt8_">
										<div class="thumb">
											<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-s/0d/cf/be/f5/vue-de-la-terrasse-exterieur.jpg" alt="fp3.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>SAAKAN</h4>
												<div class="_2NK4P3lO">
													<span class="sspd_review float-left">
														<ul>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														</ul>
													</span>
													<span class="_28oqjHA2">210&nbsp;avis</span>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<a href="/restaurant-detail" class="_3uYDFt8_">
										<div class="thumb">
											<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-s/04/83/27/0c/le-grand-large-abidjan.jpg" alt="fp1.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>LE GRAND LARGE</h4>
												<div class="_2NK4P3lO">
													<span class="sspd_review float-left">
														<ul>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														</ul>
													</span>
													<span class="_28oqjHA2">210&nbsp;avis</span>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<a href="/restaurant-detail" class="_3uYDFt8_">
										<div class="thumb">
											<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-s/0d/cf/be/f5/vue-de-la-terrasse-exterieur.jpg" alt="fp1.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>Villa Savoia</h4>
												<div class="_2NK4P3lO">
													<span class="sspd_review float-left">
														<ul>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														</ul>
													</span>
													<span class="_28oqjHA2">210&nbsp;avis</span>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<a href="/restaurant-detail" class="_3uYDFt8_">
										<div class="thumb">
											<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-s/0d/39/31/a7/photo1jpg.jpg" alt="fp2.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>Le Débarcadère</h4>
												<div class="_2NK4P3lO">
													<span class="sspd_review float-left">
														<ul>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														</ul>
													</span>
													<span class="_28oqjHA2">210&nbsp;avis</span>
												</div>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="property-search" class="property-search mb50" style="background-attachment: scroll;background-image: url('../images/Minkatoo/domaine_bini_cYzGVD877R.png'); background-position: center; background-repeat: no-repeat; height: 450px;">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<div class="search_smart_property">
							<h2 style="color:#fff; font-size:45px;">Excusion Domaine Bini</h2>
							<p style="color:#fff; font-size:25px;">Abidjan, Côte d'Ivoire <br/>
								Trip in Africa
							</p>
							<button class="btn ssp_btn">En savoir plus</button>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Property Cities -->
		<section id="best-property" class="best-property bgc-thm">
			<div class="container ovh">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<div class="main-title text-center">
							<h2 class="color-white">Meilleurs activités à Abidjan</h2>
							<p class="color-white">Des lieux à voir, des rues à explorer et des expériences emblématiques.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="best_property_slider">
							<div class="item">
								<div class="feat_property">
									<a href="#">
										<div class="thumb">
											<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/0b/a8/c2/b4/les-batiments-a-l-entree.jpg?w=400&h=300&s=1" alt="fp1.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>PARC NATIONAL DU BANCO</h4>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="item">
								<div class="feat_property">
									<a href="#">
										<div class="thumb">
											<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/05/2c/79/eb/st-paul-s-cathedral.jpg?w=300&h=300&s=1" alt="fp2.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>Cathédrale Saint-Paul d'Abidjan</h4>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="item">
								<div class="feat_property">
									<a href="#">
										<div class="thumb">
											<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/05/4d/82/40/the-plateau.jpg?w=300&h=300&s=1" alt="fp3.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>The Plateau</h4>
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="item">
								<div class="feat_property">
									<a href="#">
										<div class="thumb">
											<img class="img-whp" src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/0a/c5/dc/69/les-richesses-de-chez.jpg?w=300&h=-1&s=1" alt="fp3.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
											</div>
										</div>
										<div class="details">
											<div class="tc_content">
												<h4>Jardin botanique de Bingerville</h4>
											</div>
										</div>
									</a>
								</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<!-- Our Blog -->
		<section class="our-blog bgc-f7 pb30">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<div class="main-title text-center">
							<h2>Articles et conseils</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-lg-4 col-xl-4">
						<div class="for_blog feat_property">
							<div class="thumb">
								<img class="img-whp" src="https://r-ak.bstatic.com/xdata/images/xphoto/540x405/65098319.webp?k=827b777f5c7cab733a5b196e154c174b136977d2bf505489c3d8b29d6922c771&o=" alt="bh1.jpg">
							</div>
							<div class="details">
								<div class="tc_content">
									<h4>Premier voyage sans les enfants : où partir ?</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection
