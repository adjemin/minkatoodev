@extends('layouts.main')

@section('css')
<style>
    .loader{
        display: none;
        margin: 20px 50%;
        margin-top: -30px;
    }
    
</style>
@endsection

@section('title')
    Nos Restaurants
@endsection


@section('header')

<header>
    {{-- // Included connexion state from layouts.main --}}
    <!-- Main Header Image -->
        <div class="showcase">
            <img src="{{ asset('images/restaurant_showcase.png') }}" alt="" class="img-fluid">
            <div class="blackOverlay"></div>

        </div>
        
        @include('front.partials.header')

    </header>
@endsection
@section('main')
    <main>
        <!-- Navigation catégories -->
        <nav id="iconNavbar" class="container">
            <ul class="">
                {{-- // TODO turn into TabView --}}
                <li class="iconItem">
                    <a href="/hotels" class="">
                        <p>Hotels</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/hotel@1X.png" alt="Hotels">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem active">
                    <a href="/restaurants">

                        <p>Restaurants</p>
                        <div>
                            <!-- Item image -->
                            <img src="{{asset('images/icones/restaurant_active.png')}}" alt="restaurants">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="{{ route('excursion') }}">

                        <p>Excursion</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/sunset@1X.png" alt="Excursion">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="/locations">

                        <p>Location Voiture</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/car@1X.png" alt="location Voiture">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="musees">

                        <p>Musée</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/museum@1X.png" alt="musée">

                        </div>
                    </a>
                </li>
                <!--  -->
            </ul>
        </nav>
        <!--  -->
        <!-- Section restaurants à proximité -->
        <section class="contentWrapper container" style="margin-top: 65px;">
            <div class="sectionHead">
                <h2>Restaurants à proximité</h2>
                <a href="{{ route('All-restaurant') }}" class="flatLink color">Tout afficher</a>
            </div>
            <div class="row" id="dynamicRestaurant">
                @foreach($restaurants as $restaurant)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                    <a href="{{ route('restaurant-detail',$restaurant->id) }}">
                        <div class="card">
                            <div class="card-img">
                                <img src="{{ $restaurant->slide_url }}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                            </div>
                            <div class="card-content">
                                <h4 style="font-size: 15px; font-weight: bold;">{{$restaurant->title}}</h4>
                                <div class="rating">
                                    <!-- Stars -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                    <span>(15)</span>
                                </div>
                                <div class="card-footer-text">
                                    <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px "> <!-- mappin icon -->
                                    <span>
                                        76 km
                                    </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
                <!-- Card Item -->
            </div>
        </section>
        {{-- Section Parcourir nos spécialités --}}
        <section id="property-city" class="feature-property" style="margin-top: 20px;">
			<div class="container-fluid ovh">
				<div class="col-lg-12">
					<div class="sectionHead">
                        <h2>Parcourir nos spécialités</h2>
                    </div>
				</div>
				<div class="row">
					<div class="col-md-6 pb-1 pb-md-0 mr-auto pr-md-2 p-0">
                        {{-- <a href="{{ route('restaurant-detail',$restaurant->id) }}"> --}}
                            <a href="/restaurant_by_specification/cusisine_africaine">
                            <div class="properti_city">
                                <div class="thumb">
                                    <img class="img-fluid w100" src="{{asset('images/cuisine_locale.png')}}" alt="pc2.jpg">
                                </div>
                                <div class="overlay">
                                    <div class="details">
                                        <h4>Cuisine Locale</h4>
                                        {{-- <p>18 Restaurants</p> --}}
                                    </div>
                                </div>
                            </div>
                        </a>
					</div>
					<div class="col-md-6 pt-3 pt-md-0 p-0 pl-md-2">
                        {{-- <a href="{{ route('restaurant-detail',$restaurant->id) }}"> --}}
                        <a href="/restaurant_by_specification/cuisine_asiatique">
                            <div class="properti_city">
                                <div class="thumb">
                                    <img class="img-fluid w100" src="{{asset('images/Minkatoo/recette_asiatique_kimbap_sushi.png')}}" alt="pc2.jpg">
                                </div>
                                <div class="overlay">
                                    <div class="details">
                                        <h4>Cuisine asiatique</h4>
                                        {{-- <p>18 Restaurants</p> --}}
                                    </div>
                                </div>
                            </div>
                        </a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 pb-1 pb-md-0 mr-auto pr-md-2 p-0">
                        {{-- <a href="{{ route('restaurant-detail',$restaurant->id) }}"> --}}
                        <a href="/restaurant_by_specification/cuisine-orientale">
                            <div class="properti_city">
                                <div class="thumb">
                                    <img class="img-fluid w100" src="{{asset('images/sushi.png')}}" alt="pc2.jpg">
                                </div>
                                <div class="overlay">
                                    <div class="details">
                                        <h4>Cuisine rafinée</h4>
                                        {{-- <p>18 Restaurants</p> --}}
                                    </div>
                                </div>
                            </div>
                        </a>
					</div>
					<div class="col-md-6 pt-3 pt-md-0 p-0 pl-md-2">
                        {{-- <a href="{{ route('restaurant-detail',$restaurant->id) }}"> --}}
                            <a href="/restaurant_by_specification/cuisine-francaise">
                            <div class="properti_city">
                                <div class="thumb"><img class="img-fluid w100" src="{{asset('images/Minkatoo/meal_918639_1920.png')}}" alt="pc2.jpg"></div>
                                <div class="overlay">
                                    <div class="details">
                                        <h4>Cuisine européenne</h4>
                                        {{-- <p>18 Restaurants</p> --}}
                                    </div>
                                </div>
                            </div>
                        </a>
					</div>
				</div>
			</div>
		</section>
        <!-- Section Nos recommandations à proximité -->
        <section class="contentWrapper container">
            <div class="sectionHead">
                {{-- // TODO set responsive font for title --}}
                <h3>Nos recommandations</h3>
                <a href="{{ route('All-restaurant') }}" class="flatLink color">Tout afficher</a>
            </div>
            <div class="row">
                @foreach($restaurants as $restaurant)
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                        <a href="{{ route('restaurant-detail',$restaurant->id) }}">

                            <div class="card">
                                <div class="card-img">
                                    <img src="{{ $restaurant->slide_url }}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                                </div>
                                <div class="card-content">
                                    <h4 style="font-size: 15px; font-weight: bold;">{{$restaurant->title}}</h4>
                                    <div class="rating">
                                        <!-- Stars -->
                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                        <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                        <span>(15)</span>
                                    </div>
                                    <div class="card-footer-text">
                                        <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px "> <!-- mappin icon -->
                                        <span>
                                            76 km
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    @endforeach
                    <!-- Card Item -->
                </div>
        </section>

        <!--  -->
        <script src="{{ asset('/asset/js/searchScript.js') }}"></script>
    </main>
@endsection
