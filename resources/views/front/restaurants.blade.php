@extends('layouts.main')

@section('header')

<header>
    {{-- // Included connexion state from layouts.main --}}
    <!-- Main Header Image -->
    <div class="showcase">
        <img src="{{asset('images/restaurant_showcase.png')}}" alt="" class="img-fluid">
        <div class="blackOverlay"></div>

    </div>
    <div id="navBarSearch" class="col-12 col-sm-10 col-md-8 col-lg-6">
        <!-- <form action=""> -->
            <span class="">
                <!-- Place Holder Icon -->
                <span>
                    <img src="img/placeholder-for-map@1X.png" alt="" width="15px">
                </span>
                <input type="text" placeholder="Abidjan, Côte d'ivoire">
            </span>
            <button type="submit">
                <!-- Search icon -->
                <img src="img/search (1)@1X.png" alt="" width="15px">
            </button>
            <!-- </form> -->
        </div>
    </header>
@endsection
@section('main')
    <main>
        <!-- Navigation catégories -->
        <nav id="iconNavbar" class="container">
            <ul class="">
                {{-- // TODO turn into TabView --}}
                <li class="iconItem">
                    <a href="/hotels" class="">
                        <p>Hotels</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/hotel@1X.png" alt="Hotels">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem active">
                    <a href="/restaurants">

                        <p>Restaurants</p>
                        <div>
                            <!-- Item image -->
                            <img src="{{asset('images/icones/restaurant_active.png')}}" alt="restaurants">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="excursion">

                        <p>Excursion</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/sunset@1X.png" alt="Excursion">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="location">

                        <p>Location Voiture</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/car@1X.png" alt="location Voiture">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="musees">

                        <p>Musée</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/museum@1X.png" alt="musée">

                        </div>
                    </a>
                </li>
                <!--  -->
            </ul>
        </nav>
        <!--  -->
        <!-- Section restaurants à proximité -->
        <section class="contentWrapper container">
            <div class="sectionHead">
                <h2>Restaurants à proximité</h2>
                <a href="" class="flatLink color">Tout afficher</a>
            </div>
            <div class="row">
                @for ($i = 0; $i < 4; $i++)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                    <a href="/restaurant-detail">
                        <div class="card">
                            <div class="card-img">
                                <img src="img/bedroom.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="card-content">
                                <h4>Hotel Golf Club</h4>
                                <div class="rating">
                                    <!-- Stars -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                    <span>(15)</span>
                                </div>
                                <div class="card-footer-text">
                                    <img src="img/placeholder-for-map@1X.png" alt=""> <!-- mappin icon -->
                                    <span>
                                        76 km
                                    </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endfor
                <!-- Card Item -->
            </div>
        </section>
        {{-- Section Parcourir nos spécialités --}}
        <section class="container">
            <div class="sectionHead">
                <h2>Parcourir nos spécialités</h2>
            </div>
            <div class="container-fluid">
                <div class="row">
                    @for ($i = 0; $i < 4; $i++)
                    <div class="col-md-6 p-0
                        @if($i==0) pr-md-1 pr-lg-2 pb-2 pb-md-1 pb-lg-2  @endif
                        @if($i==1) pl-md-1 pl-lg-2 pb-2 pb-md-1 pb-lg-2  @endif
                        @if($i==2) pr-md-1 pr-lg-2 pt-2 pt-md-1 pt-lg-1  @endif
                        @if($i==3) pl-md-1 pl-lg-2 pt-2 pt-md-1 pt-lg-2  @endif">
                        <div class="">
                            <div class="display-container">
                                <img src="images/cuisine_locale.png" class="img-fluid" alt="">
                                <div class="blackOverlay"></div>
                                <div class="display-center">

                                    <h3 class="white display-center">Cuisine Locale</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endfor
                </div>
            </div>
        </section>
        <!-- Section Nos recommandations à proximité -->
        <section class="contentWrapper container">
            <div class="sectionHead">
                {{-- // TODO set responsive font for title --}}
                <h3>Nos recommandations</h3>
                <a href="" class="flatLink color">Tout afficher</a>
            </div>
            <div class="row">
                @for ($i = 0; $i < 4; $i++)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                    <a href="/restaurant-detail">

                        <div class="card">
                            <div class="card-img">
                                <img src="img/dining-table.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="card-content">
                                <h4>Ivoire Golf Club</h4>
                                <div class="rating">
                                    <!-- Stars -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                    <span>(15)</span>
                                </div>
                                <div class="card-footer-text">
                                    <img src="img/placeholder-for-map@1X.png" alt=""> <!-- mappin icon -->
                                    <span>
                                        76 km
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                    @endfor
                    <!-- Card Item -->
                </div>
        </section>

        <!--  -->
    </main>
@endsection
