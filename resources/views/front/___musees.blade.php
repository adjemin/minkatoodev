@extends('layouts.master')

@section('content')
	<section class="home-three bg-img-muse">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="home3_home_content">
						<h1>Explorez, découvrez et économisez</h1>
						<h4>Découvrez les musées avec Minkatoo.</h4>
					</div>
				</div>
				<div class="col-lg-4">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 offset-lg-3 mt-3">
					<div class="home_adv_srch_opt home3">
						<div class="home1_adsrchfrm">
							<div class="home1-advnc-search home3">
								<ul class="h1ads_1st_list mb0">
									<li class="list-inline-item">
										<div class="form-group has-search">
											<span class="fa fa-search form-control-feedback"></span>
											<input type="text" class="form-control" id="exampleInputName1" placeholder="Abidjan, Côte d'ivoire">
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="maxw1600 m0a">
		<section class="mt20">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 col-sm-4">
						<nav id="iconNavbar">
							<ul class="">
								<li class="iconItem">
									<a href="/view/hotels" class="">
										<p>Hotels</p>
										<div>
											<!-- Item image -->
											<img src="img/hotel@1X.png" alt="Hotels">

										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/view/restaurants">

										<p>Restaurants</p>
										<div>
											<!-- Item image -->
											<img src="img/plate-fork-and-knife@1X.png" alt="restaurants">

										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/excursion">

										<p>Excursion</p>
										<div>
											<!-- Item image -->
											<img src="img/sunset@1X.png" alt="Excursion">
										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/location">
										<p>Location Voiture</p>
										<div>
											<!-- Item image -->
											<img src="img/car@1X.png" alt="location Voiture">

										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem active">
									<a href="/musees">
										<p>Musée</p>
										<div>
											<!-- Item image -->
											<img src="img/museum@1X.png" alt="musée">
										</div>
									</a>
								</li>
								<!--  -->
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</section>

	<!-- Hôtel à proximité -->
	<section id="best-property" class="best-property pb0">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="main-title">
						<h2>Nos musées</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/musee-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="{{asset('images/Minkatoo/src_47025235_largejpg.png')}}" alt="fp4.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
						</a>
						<div class="details">
							<div class="tc_content">
								<h4>Musée national des costumes</h4>
								<div class="_2NK4P3lO">
									<span class="sspd_review float-left">
										<ul>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
										</ul>
									</span>
									<span class="_28oqjHA2">210&nbsp;avis</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/musee-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="{{asset('images/Minkatoo/musee_des_civilisations.png')}}" alt="fp5.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
						</a>
						<div class="details">
							<div class="tc_content">
								<h4>Musée des civilisations</h4>
								<div class="_2NK4P3lO">
									<span class="sspd_review float-left">
										<ul>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
										</ul>
									</span>
									<span class="_28oqjHA2">210&nbsp;avis</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/musee-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="{{asset('images/Minkatoo/t_l_chargement.png')}}" alt="fp6.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
						</a>
						<div class="details">
							<div class="tc_content">
								<h4>Galerie Dominantes</h4>
								<div class="_2NK4P3lO">
									<span class="sspd_review float-left">
										<ul>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
										</ul>
									</span>
									<span class="_28oqjHA2">210&nbsp;avis</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="feat_property home3">
						<a href="/musee-detail" class="_3uYDFt8_">
							<div class="thumb">
								<img class="img-whp" src="{{asset('images/Minkatoo/photo2jpg.png')}}" alt="fp6.jpg">
								<div class="thmb_cntnt">
									<ul class="icon mb0">
										<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
										<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
									</ul>
									<a class="fp_price" href="#"></a>
								</div>
							</div>
						</a>
						<div class="details">
							<div class="tc_content">
								<h4>Musée péléfero gbon coulibaly</h4>
								<div class="_2NK4P3lO">
									<span class="sspd_review float-left">
										<ul>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
										</ul>
									</span>
									<span class="_28oqjHA2">210&nbsp;avis</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	</div>
@endsection
