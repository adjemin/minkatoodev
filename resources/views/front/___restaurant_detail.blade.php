@extends('layouts.master')

@section('styles')

@endsection

@section('content')
<div class="single_page_listing_style">
		<div class="container-fluid p0">
			<div class="row">
				<div class="col-sm-6 col-lg-6 p0">
					<div class="row m0">
						<div class="col-lg-12 p0">
							<div class="spls_style_one pr1 1px">
								<img class="img-fluid w100" src="{{asset('images/Minkatoo/_a1a8429@2x.png')}}" alt="ls1.jpg">
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-6 p0">
					<div class="row m0">
						<div class="col-sm-6 col-lg-6 p0">
							<div class="spls_style_one">
								<a class="popup-img" href="{{asset('images/Minkatoo/_a1a8429@2x.png')}}"><img class="img-fluid w100" src="{{asset('images/Minkatoo/_a1a8429@2x.png')}}" alt="ls2.jpg"></a>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6 p0">
							<div class="spls_style_one">
								<a class="popup-img" href="{{asset('images/Minkatoo/_a1a8429@2x.png')}}"><img class="img-fluid w100" src="{{asset('images/Minkatoo/_a1a8429@2x.png')}}" alt="ls3.jpg"></a>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6 p0">
							<div class="spls_style_one">
								<a class="popup-img" href="{{asset('images/Minkatoo/_a1a8429@2x.png')}}"><img class="img-fluid w100" src="{{asset('images/Minkatoo/_a1a8429@2x.png')}}" alt="ls4.jpg"></a>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6 p0">
							<div class="spls_style_one">
								<a class="popup-img" href="{{asset('images/Minkatoo/_a1a8429@2x.png')}}"><img class="img-fluid w100" src="{{asset('images/Minkatoo/_a1a8429@2x.png')}}" alt="ls5.jpg"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="p0">
		<div class="container">
			<div class="row listing_single_row">
				<div class="col-sm-6 col-lg-7 col-xl-8">
					<div class="single_property_title">
						<a href="{{asset('images/Minkatoo/_a1a8429@2x.png')}}" class="upload_btn popup-img"><span class="flaticon-photo-camera"></span> Afficher toutes les photos</a>
					</div>
				</div>
				<div class="col-sm-6 col-lg-5 col-xl-4">
					<div class="single_property_social_share">
						<div class="spss style2 mt10 text-right tal-400">
							<ul class="mb0">
								<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
								<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
								<li class="list-inline-item"><a href="#"><span class="flaticon-share"></span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
  <section class="our-agent-single bgc-f7 pb30-991">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-8">
						<div class="row">
							<div class="col-lg-12">
								<div class="application_statics mt30-767 mb30-767">
									<div class="single_property_title">
										<h2>Regina Margherita</h2>
	                  <div class="_2NK4P3lO">
	                    <span class="sspd_review float-left">
	                      <ul>
	                        <li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
	                        <li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
	                        <li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
	                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
	                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
	                      </ul>
	                    </span>
	                    <span class="_28oqjHA2">210&nbsp;avis</span>
	                  </div>
										<p>Rue Abdoulaye Fadiga, Avenue Delafosse Prolongee 01 BP 2185, Abidjan 01 Côte d'Ivoire</p>
									</div>
								</div>
							</div>
						   <div class="col-lg-12">
									<div class="application_statics mt30">
										<div class="row">
											<div class="col-lg-12">
												<h4 class="mb10">Informations</h4>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="application_statics mt30">
										 <h4 class="mb30">Emplacement</h4>
										 <div class="property_video p0">
											 <div class="thumb">
												 <div class="h400" id="map-canvas"></div>
											 </div>
										 </div>
									 </div>
								</div>
								<div class="col-lg-12">
							<div class="product_single_content">
								<div class="mbp_pagination_comments mt30">
									<ul class="total_reivew_view">
										<li class="list-inline-item sub_titles">896 Avis</li>
										<li class="list-inline-item">
											<ul class="star_list">
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</li>
										<li class="list-inline-item avrg_review">( 4.5 out of 5 )</li>

<<<<<<< HEAD
									</ul>
									<div class="mbp_first media">
										<img src="https://media-cdn.tripadvisor.com/media/photo-t/1a/f6/e2/11/default-avatar-2020-41.jpg" class="mr-3" alt="1.png">
										<div class="media-body">
									    	<h4 class="sub_title mt-0">Diana Cooper
												<div class="sspd_review dif">
													<ul class="ml10">
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"></li>
													</ul>
												</div>
									    	</h4>
									    	<a class="sspd_postdate fz14" href="#">December 28, 2020</a>
									    	<p class="mt10">Un hôtel au cœur du quartier d’affaire, pratique pour nos rdv. Les chambres sont modernes, propres et confortables. Le service était efficace malgré que l’hôtel était plein. Nous avons été surclassé en suites que je vous recommande vivement. Surtout celles avec la vue imprenable côté lagune. On était hors du temps en pleine centre ville</p>
										</div>
									</div>
									<div class="custom_hr"></div>
									<div class="mbp_first media">
										<img src="https://media-cdn.tripadvisor.com/media/photo-t/1a/f6/e2/11/default-avatar-2020-41.jpg" class="mr-3" alt="2.png">
										<div class="media-body">
									    	<h4 class="sub_title mt-0">Ali Tufan
												<div class="sspd_review dif">
													<ul class="ml10">
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"></li>
													</ul>
												</div>
									    	</h4>
									    	<a class="sspd_postdate fz14" href="#">December 28, 2020</a>
									    	<p class="mt10">Un service à la hauteur de mes attentes dans un cadre moderne et agréable.Petit clin d'œil a François,le directeur,pour sa disponibilité et sa bonne humeur. Merci a toute l'équipe pour votre accueil.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
        <div class="col-lg-4 col-xl-4 _164veh0">
					<div class="sidebar_listing_list">
						<div class="sidebar_advanced_search_widget">
              <div class="col-lg-12">
                <h4 class="mb10">Description</h4>
              </div>
						</div>
					</div>
        </div>
      </div>
    </div>
  </section>
=======
                                            <img src="images/icones/coffee_cup.png" width="22" alt="">
                                            Visiter le site web
                                        </a>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <img src="images/icones/coffee_cup.png" width="22" alt="">
                                        <a href="">Appeler</a>
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <img src="images/icones/coffee_cup.png" width="22" alt="">
                                        Attestation d'excellence
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- Price container --}}
                    <div class="col-md-4 p-0 h-100">
                        <div class="price-container p-3">
                            <h2>OUVERT <small></small>
                            </h2>
                            <hr>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row">
                                            Horaires :
                                        </div>
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-6">
                                                    11:30
                                                </div>
                                                <div class="col-6">
                                                    15:00
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-6">
                                                    18:30
                                                </div>
                                                <div class="col-6">
                                                    13:00
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                Addresse :
                                            </div>
                                            <div class="col-12">
                                                <span>
                                                    Addresse, rue, intersection
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                Telephone :
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            +225 25 52 30 26
                                        </div>
                                    </div>
                                </div>
                                <div class="pt-3">
                                    <a href="" class="btn btn-primary w-100">
                                        Réserver
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- A propos --}}
            <hr/>
            {{-- // TODO add specialités later mb --}}
            {{-- <div class="container-fluid">
                <h3>Spécialités</h3>
                <div class="row">
                    @for ($i = 0; $i < 6; $i++)
                    <div class="col-6 col-md-4 my-1">
                        <div>
                            <img src="images/icones/coffee_cup.png" class="" width="22px" alt="">
                            Spécialité
                        </div>
                    </div>
                    @endfor
                </div>
            </div> --}}
        </section>
        {{--  --}}
        {{-- Section Carte --}}
        <section id="maps-container container-fluid">
            <div id="map" class="container-fluid">
                {{-- // --}}
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.568345380635!2d-3.9750283493462137!3d5.329812137345493!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc1ec65e37c61ff%3A0xbfebce7780c6f4fb!2sHeden%20Golf%20Hotel!5e0!3m2!1sfr!2sci!4v1590769060246!5m2!1sfr!2sci" width="100%" height="450px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </section>
    </main>
>>>>>>> f52a52caf69d050a05d0d1738ed24648563b1249
@endsection
