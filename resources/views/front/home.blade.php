@extends('front.layouts.master')

@section('css')

@endsection

@section('content')

    <section class="w3l-covers-9-main">
        <div class="covers-9">
            <div class="csslider infinity" id="slider1">
                <ul class="banner_slide_bg">
                    <li>
                        <div class="wrapper">
                            <div class="cover-top-center-9">
                                <div class="w3ls_cover_txt-9">
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- //covers -->

    <!-- form -->
    <section class="w3l-form-my-mian">
        <div class="form-botm-cont">
            <div class="">
                <div class="form-inner-cont">
                    <div class="col align-self-center">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- //form -->

    <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;"></div>

    <!-- Section item hotel et autre -->

    <section class="w3l-features-6">
        <div class="features">
            <div class="wrapper">
                <div class="d-grid">
                    <div class="feature">
                        <a href="#">
                            <h4>Hôtels</h4>
                            <div class="icon-bg">
                                <span><img src="{{ asset('front/images/HOTEL.png') }}" alt=""></span>
                            </div>
                        </a>
                    </div>
                    <div class="feature">
                        <a href="#">
                            <h4>Restaurants</h4>
                            <div class="icon-bg">
                                <span><img src="{{ asset('front/images/plate_fork_and_knife.png') }}" alt=""></span>
                            </div>
                        </a>
                    </div>
                    <div class="feature">
                        <a href="#">
                            <h4>Excursion</h4>
                            <div class="icon-bg">
                                <span><img src="{{ asset('front/images/sunset.png') }}" alt=""></span></span>
                            </div>
                        </a>
                    </div>
                    <div class="feature">
                        <a href="#">
                            <h4>Location Voiture</h4>
                            <div class="icon-bg">
                                <span><img src="{{ asset('front/images/car.png') }}" alt=""></span>
                            </div>
                        </a>
                    </div>
                    <div class="feature">
                        <a href="#">
                            <h4>Musée</h4>
                            <div class="icon-bg">
                                <span><img src="{{ asset('front/images/museum.png') }}" alt=""></span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- //Fin section hotel -->

    <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;"></div>

    <!-- Section hotels à proximité -->

    <section class="w3l-services-1">
        <div class="services1">
            <div class="wrapper">
                <div class="row">
                    <div class="col-md-3">
                        <div class="d-grid top-content-7">
                            <h4 class="left-text-inti">Hotels à proximité</h4>
                        </div>
                    </div>
                    <div class="col-md-3 offset-md-6">
                        <div class="d-grid top-content-7">
                            <a href="#"><h6 class="left-text-inti">Tout afficher</h6></a>
                        </div>
                    </div>
                </div>
                <br>
                <div class="services5-content">
                    <div class="column">
                        <a href="#">
                            <img src="{{ asset('front/images/_a1a8429.png') }}" alt="" class="img-responsive"/>
                            <div class="service-info">
                                <h4>Sed eiusmod tempor</h4>
                                <p>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span>  (75)</span>
                                </p>
                                <p><span class="fa fa-map-marker" aria-hidden="true"></span> 7,6 Km</p>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#">
                            <img src="{{ asset('front/images/_a1a8429.png') }}" alt="" class="img-responsive"/>
                            <div class="service-info">
                                <h4>Sed eiusmod tempor</h4>
                                <p>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span>  (75)</span>
                                </p>
                                <p><span class="fa fa-map-marker" aria-hidden="true"></span> 7,6 Km</p>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#">
                            <img src="{{ asset('front/images/_a1a8429.png') }}" alt="" class="img-responsive"/>
                            <div class="service-info">
                                <h4>Sed eiusmod tempor</h4>
                                <p>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span>  (75)</span>
                                </p>
                                <p><span class="fa fa-map-marker" aria-hidden="true"></span> 7,6 Km</p>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#">
                            <img src="{{ asset('front/images/_a1a8429.png') }}" alt="" class="img-responsive"/>
                            <div class="service-info">
                                <h4>Sed eiusmod tempor</h4>
                                <p>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span>  (75)</span>
                                </p>
                                <p><span class="fa fa-map-marker" aria-hidden="true"></span> 7,6 Km</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;"></div>

    <!-- Section Restaurant à proximité -->

    <section class="w3l-services-1">
        <div class="services1">
            <div class="wrapper">
                <div class="row">
                    <div class="col-md-3">
                        <div class="d-grid top-content-7">
                            <h4 class="left-text-inti">Hotels à proximité</h4>
                        </div>
                    </div>
                    <div class="col-md-3 offset-md-6">
                        <div class="d-grid top-content-7">
                            <a href="#"><h6 class="left-text-inti">Tout afficher</h6></a>
                        </div>
                    </div>
                </div>
                <br>
                <div class="services5-content">
                    <div class="column">
                        <a href="#">
                            <img src="{{ asset('front/images/_a1a8429.png') }}" alt="" class="img-responsive"/>
                            <div class="service-info">
                                <h4>Sed eiusmod tempor</h4>
                                <p>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span>  (75)</span>
                                </p>
                                <p><span class="fa fa-map-marker" aria-hidden="true"></span> 7,6 Km</p>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#">
                            <img src="{{ asset('front/images/_a1a8429.png') }}" alt="" class="img-responsive"/>
                            <div class="service-info">
                                <h4>Sed eiusmod tempor</h4>
                                <p>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span>  (75)</span>
                                </p>
                                <p><span class="fa fa-map-marker" aria-hidden="true"></span> 7,6 Km</p>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#">
                            <img src="{{ asset('front/images/_a1a8429.png') }}" alt="" class="img-responsive"/>
                            <div class="service-info">
                                <h4>Sed eiusmod tempor</h4>
                                <p>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span>  (75)</span>
                                </p>
                                <p><span class="fa fa-map-marker" aria-hidden="true"></span> 7,6 Km</p>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#">
                            <img src="{{ asset('front/images/_a1a8429.png') }}" alt="" class="img-responsive"/>
                            <div class="service-info">
                                <h4>Sed eiusmod tempor</h4>
                                <p>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span>  (75)</span>
                                </p>
                                <p><span class="fa fa-map-marker" aria-hidden="true"></span> 7,6 Km</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;"></div>


    <div class="wrapper">
        <div class="row">
            <div class="col-md-3">
                <div class="d-grid top-content-7">
                    <h4 class="left-text-inti">Nos Suggestions</h4>
                    <p>Excursion en ce moments</p>
                </div>
            </div>
        </div>
        <br>
        <div class="cover-top-center-9">
            <img src="{{ asset('front/images/domaine_bini_cYzGVD877R.png') }}" alt="" style="width: 100%">
            <div class="w3ls_cover_txt-9" style="position: relative;
  top: 50%;
  left: 50%;
  transform: translate(-45%, -150%);">

                <h3 class="title-cover-9" style="color: white">Excursion au domaine bini</h3>
                <p class="para-cover-9" style="color: white">Abidjan, Côte d'Ivoire</p>
                <p class="para-cover-9" style="color: white">Trip in africa</p>
            </div>
        </div>
    </div>
    <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;"></div>

    <section class="w3l-services-1">
        <div class="services1">
            <div class="wrapper">
                <div class="row">
                    <div class="col-md-3">
                        <div class="d-grid top-content-7">
                            <h4 class="left-text-inti">Meilleurs activités à Abidjan</h4>
                        </div>
                    </div>
                </div>
                <br>
                <div class="services5-content">
                    <div class="column">
                        <a href="#">
                            <img src="{{ asset('front/images/_a1a8429.png') }}" alt="" class="img-responsive"/>
                            <div class="service-info">
                                <h4>Sed eiusmod tempor</h4>
                                <p>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span>  (75)</span>
                                </p>
                                <p><span class="fa fa-map-marker" aria-hidden="true"></span> 7,6 Km</p>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#">
                            <img src="{{ asset('front/images/_a1a8429.png') }}" alt="" class="img-responsive"/>
                            <div class="service-info">
                                <h4>Sed eiusmod tempor</h4>
                                <p>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span>  (75)</span>
                                </p>
                                <p><span class="fa fa-map-marker" aria-hidden="true"></span> 7,6 Km</p>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#">
                            <img src="{{ asset('front/images/_a1a8429.png') }}" alt="" class="img-responsive"/>
                            <div class="service-info">
                                <h4>Sed eiusmod tempor</h4>
                                <p>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span>  (75)</span>
                                </p>
                                <p><span class="fa fa-map-marker" aria-hidden="true"></span> 7,6 Km</p>
                            </div>
                        </a>
                    </div>
                    <div class="column">
                        <a href="#">
                            <img src="{{ asset('front/images/_a1a8429.png') }}" alt="" class="img-responsive"/>
                            <div class="service-info">
                                <h4>Sed eiusmod tempor</h4>
                                <p>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span class="fa fa-star" aria-hidden="true"></span>
                                    <span>  (75)</span>
                                </p>
                                <p><span class="fa fa-map-marker" aria-hidden="true"></span> 7,6 Km</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="display-ad" style="margin: 8px auto; display: block; text-align:center;"></div>


@endsection
