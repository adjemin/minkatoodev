{{-- Page Header --}}
{{--
    // Contains Page Logo, isUserSignedIn ? Sign In/Up : User Tab Blocks
    //
    // --}}


@php
    $customer = Auth::guard('customer')->user();
@endphp

<div id="header" class="container">
    <div id="brand">
        <a href="/">
            <img src="{{asset('img/LOGO-MINKATOO-1@1X.png')}}" alt="" class="img-fluid" style="width: 85px; height:85;">
        </a>
    </div>
    <div>
        {{-- <ul class="navbar-nav ml-auto"> --}}
            <!-- Authentication Links -->
            @if (Auth::guard('customer')->guest())

                <style>
                    #register a, #register a:hover, #register a:focus, #register a:active {
                          text-decoration: none;
                          color: inherit;
                     }
                </style>


                @if (Route::has('customer.register'))
                    <a id='register' class="btn-link flatLink" href="{{ route('customer.register') }}"><span class="dn-lg">{{ __("S'inscrire") }}</span></a>
                @endif
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-primary" href="{{ route('customer.login') }}">{{ __('Se connecter') }}</a>

            @else
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" style="color:black" href="#" id="navbarDropdownMenuLink" role="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ strtoupper(Auth::guard('customer')->user()->last_name) }} {{ Auth::guard('customer')->user()->first_name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url('/')}}">{{ __('Accueil') }}</a>
                        <a class="dropdown-item" href="{{ route('customerProfile', [ Auth::guard('customer')->user()->id ]) }}">{{ __('Mon Profil') }}</a>
                        <a class="dropdown-item" href="{{ route('customerRequest', $customer->id ) }}">{{ __('Mes Commandes') }}</a>
                        <a class="dropdown-item" href="{{ route('customer.logout') }}"
                             onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                            {{ __('Déconnexion') }}
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>


                        {{-- <div class="col-12 text-left">
                            <a class="dropdown-item" href="{!! url('/logout') !!}" class="btn btn-default btn-flat"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Déconnexion
                            </a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                            </form>
                        </div> --}}

                    </div>
                </li>
            @endguest
        {{-- </ul> --}}
    </div>
</div>


