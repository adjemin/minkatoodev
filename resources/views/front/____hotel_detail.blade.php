@extends('layouts.master')

@section('styles')

@endsection

@section('content')
<!--div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex justify-content-start">
      <a class="p-4 text-muted ui_tab_header" href="#">Abidjan</a>
      <a class="p-4 text-muted ui_tab_header" href="#">Hôtels</a>
      <a class="p-4 text-muted ui_tab_header" href="#">Activités</a>
      <a class="p-4 text-muted ui_tab_header" href="#">Restaurants</a>
    </nav>
  </div-->
<div class="single_page_listing_style">
		<div class="container-fluid p0">
			<div class="row">
				<div class="col-sm-6 col-lg-6 p0">
					<div class="row m0">
						<div class="col-lg-12 p0">
							<div class="spls_style_one pr1 1px">
								<img class="img-fluid w100" src="https://grandetest.com/theme/findhouse-html/images/property/ls1.jpg" alt="ls1.jpg">
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-6 p0">
					<div class="row m0">
						<div class="col-sm-6 col-lg-6 p0">
							<div class="spls_style_one">
								<a class="popup-img" href="https://grandetest.com/theme/findhouse-html/images/property/ls2.jpg"><img class="img-fluid w100" src="https://grandetest.com/theme/findhouse-html/images/property/ls2.jpg" alt="ls2.jpg"></a>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6 p0">
							<div class="spls_style_one">
								<a class="popup-img" href="https://grandetest.com/theme/findhouse-html/images/property/ls3.jpg"><img class="img-fluid w100" src="https://grandetest.com/theme/findhouse-html/images/property/ls3.jpg" alt="ls3.jpg"></a>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6 p0">
							<div class="spls_style_one">
								<a class="popup-img" href="https://grandetest.com/theme/findhouse-html/images/property/ls4.jpg"><img class="img-fluid w100" src="https://grandetest.com/theme/findhouse-html/images/property/ls4.jpg" alt="ls4.jpg"></a>
							</div>
						</div>
						<div class="col-sm-6 col-lg-6 p0">
							<div class="spls_style_one">
								<a class="popup-img" href="https://grandetest.com/theme/findhouse-html/images/property/ls5.jpg"><img class="img-fluid w100" src="https://grandetest.com/theme/findhouse-html/images/property/ls5.jpg" alt="ls5.jpg"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="p0">
		<div class="container">
			<div class="row listing_single_row">
				<div class="col-sm-6 col-lg-7 col-xl-8">
					<div class="single_property_title">
						<a href="https://grandetest.com/theme/findhouse-html/images/property/ls1.jpg" class="upload_btn popup-img"><span class="flaticon-photo-camera"></span> Afficher toutes les photos</a>
					</div>
				</div>
				<div class="col-sm-6 col-lg-5 col-xl-4">
					<div class="single_property_social_share">
						<div class="spss style2 mt10 text-right tal-400">
							<ul class="mb0">
								<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
								<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
								<li class="list-inline-item"><a href="#"><span class="flaticon-share"></span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
  <section class="our-agent-single bgc-f7 pb30-991">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-lg-8">
						<div class="row">
							<div class="col-lg-12">
								<div class="application_statics mt30-767 mb30-767">
									<div class="single_property_title">
										<h2>Pullman Abidjan</h2>
	                  <div class="_2NK4P3lO">
	                    <span class="sspd_review float-left">
	                      <ul>
	                        <li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
	                        <li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
	                        <li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
	                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
	                        <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
	                      </ul>
	                    </span>
	                    <span class="_28oqjHA2">210&nbsp;avis</span>
	                  </div>
										<p>Rue Abdoulaye Fadiga, Avenue Delafosse Prolongee 01 BP 2185, Abidjan 01 Côte d'Ivoire</p>
									</div>
								</div>
							</div>
						<div class="col-lg-12">
									<div class="application_statics mt30">
										<div class="row">
											<div class="col-lg-12">
												<h4 class="mb10">À propos</h4>
											</div>
												<div class="col-sm-12">
													<h5>Équipements de l'établissement</h5>
												</div>
												<div class="col-sm-6 col-md-6 col-lg-4">
													<ul class="order_list list-inline-item">
														<li><a href="#"><span class="flaticon-tick"></span>Air Conditioning</a></li>
														<li><a href="#"><span class="flaticon-tick"></span>Barbeque</a></li>
														<li><a href="#"><span class="flaticon-tick"></span>Dryer</a></li>
														<li><a href="#"><span class="flaticon-tick"></span>Gym</a></li>
														<li><a href="#"><span class="flaticon-tick"></span>Laundry</a></li>
													</ul>
												</div>
												<div class="col-sm-6 col-md-6 col-lg-4">
													<ul class="order_list list-inline-item">
														<li><a href="#"><span class="flaticon-tick"></span>Lawn</a></li>
														<li><a href="#"><span class="flaticon-tick"></span>Microwave</a></li>
														<li><a href="#"><span class="flaticon-tick"></span>Outdoor Shower</a></li>
														<li><a href="#"><span class="flaticon-tick"></span>Refrigerator</a></li>
														<li><a href="#"><span class="flaticon-tick"></span>Sauna</a></li>
													</ul>
												</div>
												<div class="col-sm-6 col-md-6 col-lg-4">
													<ul class="order_list list-inline-item">
														<li><a href="#"><span class="flaticon-tick"></span>Swimming Pool</a></li>
														<li><a href="#"><span class="flaticon-tick"></span>TV Cable</a></li>
														<li><a href="#"><span class="flaticon-tick"></span>Washer</a></li>
														<li><a href="#"><span class="flaticon-tick"></span>WiFi</a></li>
														<li><a href="#"><span class="flaticon-tick"></span>Window Coverings</a></li>
													</ul>
												</div>
											</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="application_statics mt30">
										 <h4 class="mb30">Emplacement</h4>
										 <div class="property_video p0">
											 <div class="thumb">
												 <div class="h400" id="map-canvas"></div>
											 </div>
										 </div>
									 </div>
								</div>
								<div class="col-lg-12">
							<div class="product_single_content">
								<div class="mbp_pagination_comments mt30">
									<ul class="total_reivew_view">
										<li class="list-inline-item sub_titles">896 Avis</li>
										<li class="list-inline-item">
											<ul class="star_list">
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</li>
										<li class="list-inline-item avrg_review">( 4.5 out of 5 )</li>

									</ul>
									<div class="mbp_first media">
										<img src="https://media-cdn.tripadvisor.com/media/photo-t/1a/f6/e2/11/default-avatar-2020-41.jpg" class="mr-3" alt="1.png">
										<div class="media-body">
									    	<h4 class="sub_title mt-0">Diana Cooper
												<div class="sspd_review dif">
													<ul class="ml10">
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"></li>
													</ul>
												</div>
									    	</h4>
									    	<a class="sspd_postdate fz14" href="#">December 28, 2020</a>
									    	<p class="mt10">Un hôtel au cœur du quartier d’affaire, pratique pour nos rdv. Les chambres sont modernes, propres et confortables. Le service était efficace malgré que l’hôtel était plein. Nous avons été surclassé en suites que je vous recommande vivement. Surtout celles avec la vue imprenable côté lagune. On était hors du temps en pleine centre ville</p>
										</div>
									</div>
									<div class="custom_hr"></div>
									<div class="mbp_first media">
										<img src="https://media-cdn.tripadvisor.com/media/photo-t/1a/f6/e2/11/default-avatar-2020-41.jpg" class="mr-3" alt="2.png">
										<div class="media-body">
									    	<h4 class="sub_title mt-0">Ali Tufan
												<div class="sspd_review dif">
													<ul class="ml10">
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"></li>
													</ul>
												</div>
									    	</h4>
									    	<a class="sspd_postdate fz14" href="#">December 28, 2020</a>
									    	<p class="mt10">Un service à la hauteur de mes attentes dans un cadre moderne et agréable.Petit clin d'œil a François,le directeur,pour sa disponibilité et sa bonne humeur. Merci a toute l'équipe pour votre accueil.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
        <div class="col-lg-4 col-xl-4 _164veh0">
					<div class="sidebar_listing_list">
						<div class="sidebar_advanced_search_widget">
							<div class="price">
										<h2>CFA 150 000<small> / nuit</small></h2>
							</div>
							<ul class="sasw_list mb0">
								<div class="search_option_button">
										<button type="submit" class="btn btn-block btn-thm">Réserver</button>
								</div>
							</ul>
						</div>
					</div>
        </div>
      </div>
    </div>
  </section>
@endsection
