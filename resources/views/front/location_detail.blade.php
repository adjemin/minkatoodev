@extends('layouts.main')
@section('title', "Details location de voiture")
    
@endsection

@section('styles')
    <style>
        /* Hotel detail page */
        #map{

        }
        .price-container{
            border: 1px solid lightGrey;
        }
        /* .slider-container{
            height: 150px !important;
        } */
        .slider-container .showcase-item{
            width: 25%;
        }
        /* .jumbotron {
            margin-bottom: 0 !important;
        } */
        .description-container{
            height: 100% !important;
        }
    </style>
@endsection

@section('header')
    <header class="">
        <div class="showcase">
            <img src="images/hyundai.png" class="img-fluid" alt="">
        </div>
        {{-- Slider Elements Section --}}
        {{-- // TODO add javascript --}}
        <div class="container-fluid col-md-10 slider-container mx-auto my-2">
            <div class="row d-md-flex px-md-4 justify-content-center">
                <img src="images/hyundai.png" class="img-fluid showcase-item col-md mr-1 mr-md-0 p-md-2" alt="">

                <img src="images/hyundai.png" class="img-fluid showcase-item col-md mr-1 mr-md-0 p-md-2" alt="">

                <img src="images/hyundai.png" class="img-fluid showcase-item col-md mr-1 mr-md-0 p-md-2" alt="">

                <img src="images/hyundai.png" class="img-fluid d-none d-md-inline col-md showcase-item p-md-2" alt="">
            </div>
        </div>
    </header>
@endsection

@section('main')
    <main>
        {{-- Presentation section --}}
        <section class="container mt-2 mt-md-3">
            <div class="sectionHead">

                <div class="row align-content-md-stretch h-100">
                    {{-- // TODO make dis container full height to match its price container conterpart --}}
                    {{-- Title and summary container --}}
                    <div class="col-md-8 p-0 pr-md-3 h-100 description-container">
                        <div class="jumbotron jumbotron-fluid mb-2 mb-md-0 p-3 pb-md-3 h-100">
                            <div class="d-md-flex flex-column justify-content-between">
                                <div>
                                    <h2>Hyundai 4x4
                                        <br>
                                        <small>
                                            Abidjan, Cocody center
                                        </small>
                                    </h2>
                                    <hr>
                                </div>
                                {{-- Summary --}}
                                <div class="h-100">
                                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Possimus labore officia magnam eligendi assumenda, sunt atque quia explicabo praesentium nisi. Totam dolorum libero suscipit, distinctio eos sed modi illo temporibus.
                                </div>
                                {{--  --}}
                                {{-- <div class="my-lg-2"></div> --}}
                            </div>
                        </div>
                    </div>
                    {{-- Price container --}}
                    <div class="col-md-4 p-0 h-100">
                        <div class="price-container p-3">
                            <h2>70 000 <small>FCFA
                                /Jours</small>
                            </h2>
                            <hr>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-6">
                                        <img src="images/icones/coffee_cup.png" width="22" alt="">
                                        13 Février
                                    </div>
                                    <div class="col-6">
                                        <img src="images/icones/coffee_cup.png" width="22" alt="">
                                        14 Février
                                    </div>
                                    <div class="col-12 d-flex justify-content-end">

                                        <div class="ml-auto">
                                            1 jour
                                        </div>
                                    </div>
                                </div>
                                <div class="container">

                                    <div class="row justify-content-between align-items-baseline">
                                        <div>Total :</div>
                                        <div>
                                            <h3>

                                                70 000 <small>
                                                    Frcs
                                                    </small>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="pt-3">
                                    <a href="" class="btn btn-primary w-100">
                                        Réserver
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- A propos --}}
            <hr/>
            <div class="container-fluid">
                <h3>Caractérisques</h3>
                <div class="row">
                    {{-- @for ($i = 0; $i < 6; $i++) --}}
                    <div class="col-6 col-md-4 my-1">
                        <div>
                            <img src="images/icones/transmission.png" class="" width="22px" alt="">
                            Manuelle
                        </div>
                    </div>
                    <div class="col-6 col-md-4 my-1">
                        <div>
                            <img src="images/icones/baggage.png" class="" width="22px" alt="">
                            2 Bagages
                        </div>
                    </div>
                    <div class="col-6 col-md-4 my-1">
                        <div>
                            <img src="images/icones/neige.png" class="" width="22px" alt="">
                            Climatisation
                        </div>
                    </div>
                    <div class="col-6 col-md-4 my-1">
                        <div>
                            <img src="images/icones/passager.png" class="" width="22px" alt="">
                            2 Passagers
                        </div>
                    </div>
                    <div class="col-6 col-md-4 my-1">
                        <div>
                            <img src="images/icones/porte.png" class="" width="22px" alt="">
                            5 Portes
                        </div>
                    </div>
                    {{-- @endfor --}}

                </div>
            </div>
        </section>
        {{--  --}}
        {{-- Section Carte --}}
        <section id="maps-container container">
            <div id="map" class="container">
                {{-- // --}}
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.568345380635!2d-3.9750283493462137!3d5.329812137345493!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc1ec65e37c61ff%3A0xbfebce7780c6f4fb!2sHeden%20Golf%20Hotel!5e0!3m2!1sfr!2sci!4v1590769060246!5m2!1sfr!2sci" width="100%" height="450px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </section>
    </main>
@endsection
