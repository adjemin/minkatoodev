@extends('layouts.master')

@section('content')
	<section class="home-three bg-img-resto">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="home3_home_content">
						<h1>Explorez, découvrez les meilleures restaurants</h1>
						<h4>Découvrez les meilleures offres avec Minkatoo.</h4>
					</div>
				</div>
				<div class="col-lg-4">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 offset-lg-3 mt-3">
					<div class="home_adv_srch_opt home3">
						<div class="home1_adsrchfrm">
							<div class="home1-advnc-search home3">
								<ul class="h1ads_1st_list mb0">
									<li class="list-inline-item">
										<div class="form-group has-search">
											<span class="fa fa-search form-control-feedback"></span>
											<input type="text" class="form-control" id="exampleInputName1" placeholder="Abidjan, Côte d'ivoire">
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="maxw1600 m0a">
		<section class="mt20">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 col-sm-4">
						<nav id="iconNavbar">
							<ul class="">
								<li class="iconItem">
									<a href="/view/hotels" class="">
										<p>Hotels</p>
										<div>
											<!-- Item image -->
											<img src="{{asset('img/hotel@1X.png')}}" alt="Hotels">

										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem active">
									<a href="/view/restaurants">

										<p>Restaurants</p>
										<div>
											<!-- Item image -->
											<img src="{{asset('img/plate-fork-and-knife@1X.png')}}" alt="restaurants">

										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/excursion">

										<p>Excursion</p>
										<div>
											<!-- Item image -->
											<img src="{{asset('img/sunset@1X.png')}}" alt="Excursion">
										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/location">
										<p>Location Voiture</p>
										<div>
											<!-- Item image -->
											<img src="{{asset('img/car@1X.png')}}" alt="location Voiture">

										</div>
									</a>
								</li>
								<!--  -->
								<li class="iconItem">
									<a href="/musees">
										<p>Musée</p>
										<div>
											<!-- Item image -->
											<img src="{{asset('img/museum@1X.png')}}" alt="musée">
										</div>
									</a>
								</li>
								<!--  -->
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</section>
		<!--  Restaurant à proximité -->
		<section id="feature-property" class="feature-property" style="margin-top:-50px; padding-bottom:10px ! important">
			<div class="container-fluid ovh">
				<div class="row">
					<div class="col-lg-12">
						<div class="main-title mb40">
							<h2>Restaurants à proximité</h2>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="feature_property_home3_slider">
							<div class="item">
								<div class="feat_property home3">
									<a href="/restaurant-detail" class="_3uYDFt8_">
										<div class="thumb">
											<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-s/0f/f0/36/03/restaurant-regina-margherita.jpg" alt="fp1.jpg">
											<div class="thmb_cntnt">
												<ul class="icon mb0">
													<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
													<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
												</ul>
											</div>
										</div>
									</a>
									<div class="details">
										<div class="tc_content">
											<h4>Regina Margherita</h4>
											<p><span class="flaticon-placeholder"></span> II Plateaux Vallon, Abidjan Côte d&#39;Ivoire</p>
											<div class="_2NK4P3lO">
												<span class="sspd_review float-left">
													<ul>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
													</ul>
												</span>
												<span class="_28oqjHA2">210&nbsp;avis</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<div class="thumb">
										<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-f/18/9f/ae/a4/photo3jpg.jpg" alt="fp2.jpg">
										<div class="thmb_cntnt">
											<ul class="icon mb0">
												<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
												<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
											</ul>
										</div>
									</div>
									<div class="details">
										<div class="tc_content">
											<h4>La Taverne Romaine</h4>
											<p><span class="flaticon-placeholder"></span> Boulevard Lagunaire, Abidjan Côte d&#39;Ivoire</p>
											<div class="_2NK4P3lO">
												<span class="sspd_review float-left">
													<ul>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
													</ul>
												</span>
												<span class="_28oqjHA2">210&nbsp;avis</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<div class="thumb">
										<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-s/0d/cf/be/f5/vue-de-la-terrasse-exterieur.jpg" alt="fp3.jpg">
										<div class="thmb_cntnt">
											<ul class="icon mb0">
												<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
												<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
											</ul>
										</div>
									</div>
									<div class="details">
										<div class="tc_content">
											<h4>SAAKAN</h4>
											<div class="_2NK4P3lO">
												<span class="sspd_review float-left">
													<ul>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
													</ul>
												</span>
												<span class="_28oqjHA2">210&nbsp;avis</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<div class="thumb">
										<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-s/04/83/27/0c/le-grand-large-abidjan.jpg" alt="fp1.jpg">
										<div class="thmb_cntnt">
											<ul class="icon mb0">
												<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
												<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
											</ul>
										</div>
									</div>
									<div class="details">
										<div class="tc_content">
											<h4>LE GRAND LARGE</h4>
											<div class="_2NK4P3lO">
												<span class="sspd_review float-left">
													<ul>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
													</ul>
												</span>
												<span class="_28oqjHA2">210&nbsp;avis</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<div class="thumb">
										<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-s/0d/cf/be/f5/vue-de-la-terrasse-exterieur.jpg" alt="fp1.jpg">
										<div class="thmb_cntnt">
											<ul class="icon mb0">
												<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
												<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
											</ul>
										</div>
									</div>
									<div class="details">
										<div class="tc_content">
											<h4>Villa Savoia</h4>
											<div class="_2NK4P3lO">
												<span class="sspd_review float-left">
													<ul>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
													</ul>
												</span>
												<span class="_28oqjHA2">210&nbsp;avis</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="feat_property home3">
									<div class="thumb">
										<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-s/0d/39/31/a7/photo1jpg.jpg" alt="fp2.jpg">
										<div class="thmb_cntnt">
											<ul class="icon mb0">
												<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
												<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
											</ul>
										</div>
									</div>
									<div class="details">
										<div class="tc_content">
											<h4>Le Débarcadère</h4>
											<div class="_2NK4P3lO">
												<span class="sspd_review float-left">
													<ul>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
													</ul>
												</span>
												<span class="_28oqjHA2">210&nbsp;avis</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="property-city" class="property-city pb30">
			<div class="container-fluid ovh">
				<div class="row">
					<div class="col-lg-12">
						<div class="main-title mb40">
							<h2>Parcourir nos spécialités</h2>
						</div>
					</div>
					<div class="col-lg-6 col-xl-6">
						<div class="properti_city">
							<div class="thumb"><img class="img-fluid w100" src="{{asset('images/cuisine_locale.png')}}" alt="pc2.jpg"></div>
							<div class="overlay">
								<div class="details">
									<h4>Cuisine Locale</h4>
									<p>18 Restaurants</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-xl-6">
						<div class="properti_city">
							<div class="thumb"><img class="img-fluid w100" src="{{asset('images/Minkatoo/recette_asiatique_kimbap_sushi.png')}}" alt="pc2.jpg"></div>
							<div class="overlay">
								<div class="details">
									<h4>Cuisine asiatique</h4>
									<p>18 Restaurants</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-xl-6">
						<div class="properti_city">
							<div class="thumb"><img class="img-fluid w100" src="{{asset('images/sushi.png')}}" alt="pc2.jpg"></div>
							<div class="overlay">
								<div class="details">
									<h4>Cuisine rafinée</h4>
									<p>18 Restaurants</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-xl-6">
						<div class="properti_city">
							<div class="thumb"><img class="img-fluid w100" src="{{asset('images/Minkatoo/meal_918639_1920.png')}}" alt="pc2.jpg"></div>
							<div class="overlay">
								<div class="details">
									<h4>Cuisine européenne</h4>
									<p>18 Restaurants</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Nos recommandations -->
		<section id="best-property" class="best-property pt100 pb0">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="main-title">
							<h2>Nos recommandations</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-lg-3">
						<div class="feat_property home3">
							<a href="/restaurant-detail" class="_3uYDFt8_">
								<div class="thumb">
									<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-s/0f/f0/36/03/restaurant-regina-margherita.jpg" alt="fp1.jpg">
									<div class="thmb_cntnt">
										<ul class="icon mb0">
											<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
											<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
										</ul>
										<a class="fp_price" href="#"></a>
									</div>
								</div>
							</a>
							<div class="details">
								<div class="tc_content">
									<h4>Ivoire Golf Club</h4>
									<div class="_2NK4P3lO">
										<span class="sspd_review float-left">
											<ul>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</span>
										<span class="_28oqjHA2">210&nbsp;avis</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3">
						<div class="feat_property home3">
							<a href="/hotel-detail" class="_3uYDFt8_">
								<div class="thumb">
									<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-f/18/9f/ae/a4/photo3jpg.jpg" alt="fp2.jpg">
									<div class="thmb_cntnt">
										<ul class="icon mb0">
											<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
											<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
										</ul>
										<a class="fp_price" href="#"></a>
									</div>
								</div>
							</a>
							<div class="details">
								<div class="tc_content">
									<h4>La Taverne Romaine</h4>
									<div class="_2NK4P3lO">
										<span class="sspd_review float-left">
											<ul>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</span>
										<span class="_28oqjHA2">210&nbsp;avis</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3">
						<div class="feat_property home3">
							<a href="/hotel-detail" class="_3uYDFt8_">
								<div class="thumb">
									<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-s/0d/cf/be/f5/vue-de-la-terrasse-exterieur.jpg" alt="fp3.jpg">
									<div class="thmb_cntnt">
										<ul class="icon mb0">
											<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
											<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
										</ul>
										<a class="fp_price" href="#"></a>
									</div>
								</div>
							</a>
							<div class="details">
								<div class="tc_content">
									<h4>SAAKAN</h4>
									<div class="_2NK4P3lO">
										<span class="sspd_review float-left">
											<ul>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</span>
										<span class="_28oqjHA2">210&nbsp;avis</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-lg-3">
						<div class="feat_property home3">
							<a href="/hotel-detail" class="_3uYDFt8_">
								<div class="thumb">
									<img class="img-whp" src="https://media-cdn.tripadvisor.com/media/photo-s/04/83/27/0c/le-grand-large-abidjan.jpg" alt="fp1.jpg">
									<div class="thmb_cntnt">
										<ul class="icon mb0">
											<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
											<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
										</ul>
										<a class="fp_price" href="#"></a>
									</div>
								</div>
							</a>
							<div class="details">
								<div class="tc_content">
									<h4>LE GRAND LARGE</h4>
									<div class="_2NK4P3lO">
										<span class="sspd_review float-left">
											<ul>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
												<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
											</ul>
										</span>
										<span class="_28oqjHA2">210&nbsp;avis</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection
