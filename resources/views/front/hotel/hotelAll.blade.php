<!DOCTYPE html>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <title>Tous les hotels- {{ config('app.name') }}</title>

        {{-- @section('title', 'Tous les hotels') --}}


        <link rel="icon" type="image/png" href="{{asset('front/images/LOGO_MINKATOO.png')}}">

        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;0,800;0,900;1,500&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">

        <!-- <link rel="stylesheet" href="css/responsive-grid.css"> -->

        {{-- Bootstrap CDN --}}

        <link rel="stylesheet" href="{{asset('css/bootstrap-grid.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        {{-- W3.CSS CDN --}}
        {{-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> --}}

        {{-- // Main Page Style CSS --}}
        <link rel="stylesheet" href="{{asset('css/main.css')}}">
        <link rel="stylesheet" href="{{asset('css/media-queries.css')}}">
        {{--  --}}

        @section('title', 'Tous les hotels')

        <script src="{{asset('js/app.js')}}"></script>
        {{--  --}}
        @yield('styles')
    </head>

    <body>
        <!-- Logo plus connexion state -->
        {{-- Page Header --}}
        {{--// Contains Page Logo, isUserSignedIn ? Sign In/Up : User Tab Blocks// --}}
        @include('front.inc.page_header')

        <header>
            {{-- // Included connexion state from layouts.main --}}
            <!-- Main Header Image -->
            <div class="showcase">
                <img src="{{asset('images/hotel_showcase.png')}}" alt="" class="img-fluid">
                <div class="blackOverlay"></div>
            </div>
            <form action="" id="navBarSearch" type="input" class="col-12 col-sm-10 col-md-8 col-lg-6">
                <div>
                        <span class="">
                            <!-- Place Holder Icon -->
                            <span>
                                <img src="{{asset('img/placeholder-for-map@1X.png')}}" alt="" width="15px">
                            </span>
                        <input type="text" placeholder="Abidjan, Côte d'ivoire" id="input_search">
                    </span>
                    <button type="submit">
                        <!-- Search icon -->
                        <img src="{{ asset('img/search (1)@1X.png') }}" alt="" width="15px">
                    </button>
                </div>
            </form>
        </header>
        {{-- <main> --}}
        <main>
            <!-- Navigation catégories -->
            <nav id="iconNavbar" class="container">
                <ul class="">
                    {{-- // TODO turn into TabView --}}
                    <li class="iconItem active">
                        <a href="/hotels" class="">
                            <p>Hotels</p>
                            <div>
                                <!-- Item image -->
                                <img src="{{ asset('img/hotel@1X.png') }}" alt="Hotels">
                            </div>
                        </a>
                    </li>
                    <!--  -->
                    <li class="iconItem">
                        <a href="/restaurants">
                            <p>Restaurants</p>
                            <div>
                                <!-- Item image -->
                                <img src="{{ asset('img/plate-fork-and-knife@1X.png') }}" alt="restaurants">

                            </div>
                        </a>
                    </li>
                    <!--  -->
                    <li class="iconItem">
                    <a href="{{ route('excursion') }}">

                            <p>Excursion</p>
                            <div>
                                <!-- Item image -->
                                <img src="{{ asset('img/sunset@1X.png') }}" alt="Excursion">

                            </div>
                        </a>
                    </li>
                    <!--  -->
                    <li class="iconItem">
                        <a href="/locations">

                            <p>Location Voiture</p>
                            <div>
                                <!-- Item image -->
                                <img src="{{ asset('img/car@1X.png') }}" alt="location Voiture">

                            </div>
                        </a>
                    </li>
                    <!--  -->
                    <li class="iconItem">
                        <a href="{{ route('musee') }}">

                            <p>Musée</p>
                            <div>
                                <!-- Item image -->
                                <img src="{{ asset('img/museum@1X.png') }}" alt="musée">

                            </div>
                        </a>
                    </li>
                    <!--  -->
                </ul>
            </nav>
            <!--  -->
            <!-- Section hotels à proximité -->
            <section class="contentWrapper container">
                <div class="sectionHead">
                    <h2>Tous nos Hotels</h2>
                </div>
                <div class="row">
                    @foreach ($hotels as $hotel)
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                        {{-- // TODO add hover styling --}}
                    <a href="/hotels/{{$hotel->id}}" >
                            <div class="card">
                                <div class="card-img">
                                    <img src="{{ $hotel->slide_url }}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                                </div>
                                <div class="card-content">
                                    <h4 style="font-size: 15px; font-weight: bold;">{{$hotel->title}}</h4>
                                    <div class="rating">
                                        <!-- Stars -->
                                        <img src={{ asset('img/star-full.png')}} alt="" class=""> <!-- fullstar icons -->
                                        <img src={{ asset('img/star-full.png')}} alt="" class=""> <!-- fullstar icons -->
                                        <img src={{ asset('img/star-full.png')}} alt="" class=""> <!-- fullstar icons -->
                                        <img src={{ asset('img/star-full.png')}} alt="" class=""> <!-- fullstar icons -->
                                        <img src={{ asset('img/star-full.png')}} alt="" class=""> <!-- fullstar icons -->
                                        <img src={{ asset('img/star.png')}} alt=""> <!-- halfstar icon -->
                                        <span>(15)</span>
                                    </div>
                                    <div class="card-footer-text">
                                        <img src="{{ asset('img/placeholder-for-map@1X.png')}}" alt="" style="height: 19px; width: 15px "> <!-- mappin icon -->
                                        <span>
                                        76 km
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    </div>
                    @endforeach
                    <!-- Card Item -->
                </div>
            </section>
                <!--  -->
        </main>
        <div class="wrapper">
            <section class="footer_one">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 pr0 pl0">
                            <div class="footer_about_widget">
                                <h4>A propos de nous</h4>
                                <p>Nous désirons vous faciliter la vie en vous permettant de decouvrir sur notre plateforme toutes les meilleurs offres du moment en Côte d'ivoire en seul lieu.</p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                            <div class="footer_qlink_widget">
                                <h4>Liens Utiles</h4>
                                <ul class="list-unstyled">
                                    <li><a href="/hotels">Hotels</a></li>
                                    <li><a href="/restaurants">Restaurants</a></li>
                                    <li><a href="/excursion">Excursion</a></li>
                                    <li><a href="/location">Location voiture</a></li>
                                    <li><a href="/musees">Musée</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                            <div class="footer_contact_widget">
                                <h4>Nous Contacter</h4>
                                <ul class="list-unstyled">
                                    <li><a href="#"><span class="__cf_email__" data-cfemail="4f262129200f2926212b27203a3c2a612c2022">[email&#160;protected]</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                            <div class="footer_social_widget">
                                <h4>Follow us</h4>
                                <ul class="mb30">
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                                <h4>S'abonner à la newsletter</h4>
                                <form class="footer_mailchimp_form">
                                    <div class="form-row align-items-center">
                                        <div class="col-auto">
                                            <input type="email" class="form-control mb-2" id="inlineFormInput" placeholder="Entrez votre adresse email">
                                        </div>
                                        <div class="col-auto">
                                            <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-angle-right"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Our Footer Bottom Area -->
            <section class="footer_middle_area pt30 pb30" style="margin: -32px  0px !important;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">

                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <div class="copyright-widget text-right">
                                <p>© 2020 Minkatoo.All Rights Reserved
                                    Designed by <a href="https://adjemin.com/Products_Services" target="_blank" style="color: chocolate"> Adjemin</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <a class="scrollToHome" href="#" style="display: inline;"><i class="flaticon-arrows" style="box-sizing: border-box; font-style: italic;"></i></a>
        </div>

        <script type="text/javascript">
            /* ----- Scroll To top ----- */
            function scrollToTop() {
                $(window).scroll(function(){
                    if ($(this).scrollTop() > 600) {
                        $('.scrollToHome').fadeIn();
                    } else {
                        $('.scrollToHome').fadeOut();
                    }
                });

                //Click event to scroll to top
                $('.scrollToHome').on('click',function(){
                    $('html, body').animate({scrollTop : 0},800);
                    return false;
                });
            }
        </script>
    </body>
</html>
