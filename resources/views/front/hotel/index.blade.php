@extends('layouts.main')


@section('title')
    Tous Nos Hotels
@endsection


@section('css')
<style>
    .loader{
        display: none;
        margin: 20px 50%;
        margin-top: -30px;
    }
</style>
@endsection


@section('header')

<header>
    {{-- // Included connexion state from layouts.main --}}
    <!-- Main Header Image -->
    <div class="showcase">
        <img src="{{asset('images/hotel_showcase.png')}}" alt="" class="img-fluid">
        <div class="blackOverlay"></div>

    </div>
    @include('front.partials.header')
    
</header>
@endsection
@section('main')
    <main>
        <!-- Navigation catégories -->
        <nav id="iconNavbar" class="container">
            <ul class="">
                {{-- // TODO turn into TabView --}}
                <li class="iconItem active">
                    <a href="/hotels" class="">
                        <p>Hotels</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/hotel@1X.png" alt="Hotels">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="/restaurants">
                        <p>Restaurants</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/plate-fork-and-knife@1X.png" alt="restaurants">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="excursion">

                        <p>Excursion</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/sunset@1X.png" alt="Excursion">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="/locations">

                        <p>Location Voiture</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/car@1X.png" alt="location Voiture">
                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="musees">
                        <p>Musée</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/museum@1X.png" alt="musée">
                        </div>
                    </a>
                    <!--  -->
            </ul>
        </nav>
        <!--  -->
    </li>
        <!-- Section hotels à proximité -->
        <section class="contentWrapper container">
            <div class="sectionHead">
                <h2>Hotels à proximité</h2>
                <a href="{{ route('All-Hotel') }}" class="flatLink color">Tout afficher</a>
            </div>
            <div class="row"  id="dynamicHotel">
                @foreach ($hotels as $hotel)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                    {{-- // TODO add hover styling --}}
                <a href="/hotels/{{$hotel->id}}" >
                        <div class="card">
                            <div class="card-img">
                                <img src="{{ $hotel->slide_url }}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                            </div>
                            <div class="card-content">
                                <h4 style="font-size: 15px; font-weight: bold;">{{$hotel->title}}</h4>
                                <div class="rating">
                                    <!-- Stars -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                    <span>(15)</span>
                                </div>
                                <div class="card-footer-text">
                                    <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px "> <!-- mappin icon -->
                                    <span>
                                    76 km
                                </span>
                            </div>
                        </div>
                    </a>
                </div>
                </div>
                @endforeach
                <!-- Card Item -->
            </div>
        </section>


        <!-- Section Résidences  à proximité -->
        <section class="contentWrapper container">
            <div class="sectionHead">
                {{-- // TODO set responsive font for title --}}
                {{-- // TODO create model --}}
                <h3>Résidences à proximité</h3>
                <a href="{{ route('All-Residences') }}" class="flatLink color">Tout afficher</a>
            </div>
            <div class="row" id="dynamicResidence">
                {{-- @for ($i = 0; $i < 8; $i++) --}}

                @foreach ($residence as $residence)

                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                        <a href="{{ route('residence-detail',$residence->id) }}">
                            <div class="card">
                                <div class="card-img">
                                    <img src="{{ $residence->getPictures()[0] }}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                                </div>
                                <div class="card-content" >
                                    <h4 style="font-size: 15px; font-weight: bold;">{{ $residence->title }}</h4>
                                    <div class="rating">
                                        <!-- Stars -->
                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                        <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                        <span>(15)</span>
                                    </div>
                                    <div class="card-footer-text">
                                        <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px "> <!-- mappin icon -->
                                        <span>
                                            76 km
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                @endforeach
                    {{-- @endfor --}}
                    <!-- Card Item -->
                </div>
            </section>
            <!--  -->
        </main>
        <footer>
            <!--  -->
        </footer>    
        <script src="{{ asset('/asset/js/searchScript.js') }}"></script>
    </body>

    </html>
@endsection
