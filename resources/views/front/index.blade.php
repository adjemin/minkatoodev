@extends('layouts.main')

@section('title', 'Accueil')

@section('css')
<style>
    span {text-decoration:none;}
    .loader{
        display: none;
        margin: 20px 50%;
        margin-top: -30px;
    }
</style>
@endsection

@section('header')
<header>
    {{-- // Included connexion state from layouts.main --}}
    <!-- Main Header Image -->
    <div class="showcase">
        <img src="img/sea-dawn-nature-sky-127160.png" alt="" class="img-fluid" style="height: 490px">
        <div class="blackOverlay"></div>

    </div>

    @include('front.partials.header')

</header>
@endsection
@section('main')
    <main style="margin-top: 30px;">
        <!-- Navigation catégories -->
        <nav id="iconNavbar" class="container">
            <ul class="">
                {{-- // TODO turn into TabView --}}
                <li class="iconItem">
                    <a href="/hotels" class="">
                        <p>Hotels</p>
                        <div>
                            <!-- Item offer -->
                            <img src="img/hotel@1X.png" alt="Hotels">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="/restaurants">

                        <p>Restaurants</p>
                        <div>
                            <!-- Item offer -->
                            <img src="img/plate-fork-and-knife@1X.png" alt="restaurants">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="excursion">

                        <p>Excursions</p>
                        <div>
                            <!-- Item offer -->
                            <img src="img/sunset@1X.png" alt="Excursion">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="location">

                        <p>Location de Voitures</p>
                        <div>
                            <!-- Item offer -->
                            <img src="img/car@1X.png" alt="location Voiture">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="musees">

                        <p>Musées</p>
                        <div>
                            <!-- Item offer -->
                            <img src="img/museum@1X.png" alt="musée">

                        </div>
                    </a>
                </li>
                <!--  -->
            </ul>
        </nav>
        <!--  -->
        <!-- Section hotels à proximité -->
        <section class="contentWrapper container">
            <div class="sectionHead">
                <h2>Hotels à proximité</h2>
                <a href="{{ route('All-Hotel') }}" class="flatLink color">Tout afficher</a>
            </div>
            <div class="row" id='dynamicHotel'>
               @foreach($hotels as $hotel)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                    <div class="card">
                        <a href="{{ route('hotel-detail',$hotel->id) }}">
                            <div class="card-img">
                                {{-- @if(strpos(json_decode($hotel->meta_data)->pictures[0], 'http') !== false)  --}}
                                   <img src="{{ $hotel->getPictures()[0] }}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                                {{-- @else
                                    <img src="{{ asset('storage/'.$hotel->slide_url) }}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                                @endif --}}
                            </div>
                        </a>
                        <div class="card-content">
                            <a href="{{ route('hotel-detail',$hotel->id) }}">
                                <h4 style="font-size: 15px; font-weight: bold;">{{$hotel->title}}</h4>
                            </a>
                            <div class="rating">
                                <!-- Stars -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                <span>(15)</span>
                            </div>
                            <div class="card-footer-text">
                                <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px;"> <!-- mappin icon -->
                                <span>
                                    76 km
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                {{-- <div id='dynamic'>
                </div> --}}


                <!-- Card Item -->
            </div>
        </section>
        <!-- Section restaurants à proximité -->
        <section class="contentWrapper container">
            <div class="sectionHead">
                {{-- // TODO set responsive font for title --}}
                <h3>Restaurants à proximité</h3>
                <a href="{{ route('All-restaurant') }}" class="flatLink color">Tout afficher</a>
            </div>
            <div class="row" id="dynamicRestaurant">
                @foreach($restaurants as $restaurant)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                    <div class="card">
                        <a href="{{ route('restaurant-detail',$restaurant->id) }}">
                            <div class="card-img">
                            {{-- {{ dd($restaurant->getPictures()[0]) --}}
                            {{-- @if(strpos( json_decode($restaurant->meta_data)->pictures[0], 'http') !== false)  --}}
                                <img src="{{ $restaurant->getPictures()[1] }}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                             {{-- @else
                                 <img src="{{ asset('storage/'.$restaurant->slide_url) }}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                             @endif --}}
                            </div>
                        </a>
                        <div class="card-content">
                            <a href="{{ route('restaurant-detail',$restaurant->id) }}">
                                <h4 style="font-size: 15px; font-weight: bold;">{{$restaurant->title}}</h4>
                            </a>
                            <div class="rating">
                                <!-- Stars -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                <span>(15)</span>
                            </div>
                            {{-- <div class="card-footer-text">
                                <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px;"> <!-- mappin icon -->
                                <span>
                                    76 km
                                </span>
                            </div> --}}
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- Card Item -->
            </div>
        </section>
        <!-- Banner Ssection excursions -->

            <section id="excursions" class="banner">
                <div class="banner-head container">
                    <h1>Nos Suggestions</h1>
                    <h3>Excursions en ce moments</h3>
                </div>
                <div class="banner-img">
                    <div class="banner-img-overlay">
                        <div class="banner-img-text container">
                            <h1 class="title" style="color:white;">Excursion au domaine bini</h1>
                            <div>Abidjan, Côte d'Ivoire</div>
                            <div>Trip in Africa</div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Section Meilleures activités à abidjan -->

            <section class="contentWrapper container">
                <div class="sectionHead">
                    <h1>Meilleures activités à abidjan</h1>
                    <!-- <a href="" class="flatLink color">Tout afficher</a> -->
                    <span class="spacer-10">

                    </span>
                </div>
                <div class="row">
                    @foreach($excursion as $excursion)
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                        <div class="card">
                            <a href="{{ route('excursion-detail',$excursion->id) }}">
                                <div class="card-img">
                                    @if(strpos(json_decode($excursion->meta_data)->pictures[0], 'http') !== false)
                                        <img src="{{ $excursion->getPictures()[0] }}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                                    {{-- @else
                                        <img src="{{ asset('storage/'.$excursion->slide_url) }}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                                         --}}
                                    @endif
                                </div>
                            </a>
                            <div class="card-content">
                                <a href="{{ route('excursion-detail',$excursion->id) }}">
                                    <h4 style="font-size: 15px; font-weight: bold;">{{$excursion->title}}</h4>
                                </a>
                                <div class="rating">
                                    <!-- Stars -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                    <span>(15)</span>
                                </div>
                                {{-- <div class="card-footer-text">
                                    <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px;"> <!-- mappin icon -->
                                    <span>
                                        76 km
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!-- Card Item -->
                </div>
            </section>

            <!--  -->
        </main>
        <footer>
            <!--  -->
        </footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="{{ asset('/asset/js/searchScript.js') }}"></script>

    </body>

    </html>
@endsection

