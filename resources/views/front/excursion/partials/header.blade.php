<nav id="iconNavbar" class="container">
    <ul class="">
        {{-- // TODO turn into TabView --}}
        <li class="iconItem">
            <a href="/hotels" class="">
                <p>Hotels</p>
                <div>
                    <!-- Item image -->
                    <img src="{{asset('img/hotel@1X.png')}}" alt="Hotels">
                </div>
            </a>
        </li>
        <!--  -->
        <li class="iconItem">
            <a href="/restaurants">

                <p>Restaurants</p>
                <div>
                    <!-- Item image -->
                    <img src="{{asset('img/plate-fork-and-knife@1X.png')}}" alt="restaurants">
                </div>
            </a>
        </li>
        <!--  -->
        <li class="iconItem active">
            <a href="/excursion">
                <p>Excursion</p>
                <div>
                    <!-- Item image -->
                    <img src="{{asset('images/icones/excursion_active.png')}}" alt="Excursion">
                </div>
            </a>
        </li>
        <!--  -->
        <li class="iconItem">
            <a href="/locations">
                <p>Location Voiture</p>
                <div>
                    <!-- Item image -->
                    <img src="{{asset('img/car@1X.png')}}" alt="location Voiture">

                </div>
            </a>
        </li>
        <!--  -->
        <li class="iconItem">
            <a href="musees">

                <p>Musée</p>
                <div>
                    <!-- Item image -->
                    <img src="{{asset('img/museum@1X.png')}}" alt="musée">

                </div>
            </a>
        </li>
        <!--  -->
    </ul>
</nav>