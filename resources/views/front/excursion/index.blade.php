@extends('layouts.main')

@section('title')
    Nos Excursions
@endsection

@section('css')
<style>
    .loader{
        display: none;
        margin: 20px 50%;
        margin-top: -30px;
    }
</style>
@endsection


@section('header')

<header>
    {{-- // Included connexion state from layouts.main --}}
    <!-- Main Header Image -->
    <div class="showcase">
        <img src="images/excursion_showcase.png" alt="" class="img-fluid">
        <div class="blackOverlay"></div>
    </div>
    @include('front.partials.header')
    </header>
@endsection
@section('main')
    <main>
        <!-- Navigation catégories -->
        <nav id="iconNavbar" class="container">
            <ul class="">
                {{-- // TODO turn into TabView --}}
                <li class="iconItem">
                    <a href="/hotels" class="">
                        <p>Hotels</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/hotel@1X.png" alt="Hotels">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="/restaurants">

                        <p>Restaurants</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/plate-fork-and-knife@1X.png" alt="restaurants">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem active">
                    <a href="excursion">

                        <p>Excursion</p>
                        <div>
                            <!-- Item image -->
                            <img src="{{asset('images/icones/excursion_active.png')}}" alt="Excursion">
                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="location">

                        <p>Location Voiture</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/car@1X.png" alt="location Voiture">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="musees">

                        <p>Musée</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/museum@1X.png" alt="musée">

                        </div>
                    </a>
                </li>
                <!--  -->
            </ul>
        </nav>
        <!--  -->
        
        
        {{-- /**********************************New Section*****************************************************/ --}}
        <section class="contentWrapper container">
            <div class="sectionHead">
                <h2>Excursion</h2>
            </div>
            <div class="row" id="dynamicExcursion">
                @foreach ($excursions as $excursion)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                    <a href="{{ route('musee-detail',$excursion->id) }}">
                        <div class="card">
                            <div class="card-img">
                                <img src="{{ $excursion->getPictures()[0] }}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                            </div>
                            <div class="card-content">
                                <h4 style="font-size: 15px; font-weight: bold;">{{ $excursion->title}}</h4>
                            </a>
                                <div class="rating">
                                    <!-- Stars -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                    <span>(15)</span>
                                </div>
                                {{-- <div class="card-footer-text">
                                    <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px "> <!-- mappin icon -->
                                    <span>
                                        76 km
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                </div>
                @endforeach
                <!-- Card Item -->
            </div>
        </section>
        
        <!-- Section Nos activités en ce momment -->
        <section id="property-city" class="feature-property" style="margin-top:60px;">
			<div class="container-fluid ovh">
				<div class="col-lg-12">
					<div class="sectionHead">
                        <h2>Nos activités en ce moment</h2>
                    </div>
				</div>
				<div class="row">

					<div class="col-md-6 pb-1 pb-md-0 mr-auto pr-md-2 p-0">
                        <a href="/excursion_by_specification/excursions_et_visites">
                            <div class="properti_city">
                                <div class="thumb">
                                    <img class="img-fluid w100" src="{{asset('images/Minkatoo/ID69480461_2747307518614738_17@2x.png')}}" alt="pc2.jpg">
                                </div>
                                <div class="overlay">
                                    <div class="details">
                                        <h4>Excursions et visites</h4>
                                        {{-- <p>18 Restaurants</p> --}}
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

					<div class="col-md-6 pt-3 pt-md-0 p-0 pl-md-2">
                        <a href="/excursion_by_specification/visites_guidées">
                            <div class="properti_city">
                                <div class="thumb">
                                    <img class="img-fluid w100" src="{{asset('images/Minkatoo/ID69256094_2722453797766777_35.png')}}" alt="pc2.jpg">
                                </div>
                                <div class="overlay">
                                    <div class="details">
                                        <h4>Visites guidées</h4>
                                        {{-- <p>18 Restaurants</p> --}}
                                    </div>
                                </div>
                            </div>
                        </a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 pb-1 pb-md-0 mr-auto pr-md-2 p-0">
                        <a href="/excursion_by_specification/sites_historiques">
                            <div class="properti_city">
                                <div class="thumb">
                                    <img class="img-fluid w100" src="{{asset('images/Minkatoo/site-historique.png')}}" alt="pc2.jpg">
                                </div>
                                <div class="overlay">
                                    <div class="details">
                                        <h4>Sites historiques</h4>
                                        {{-- <p>18 Restaurants</p> --}}
                                    </div>
                                </div>
                            </div>
                        </a>
					</div>
					<div class="col-md-6 pt-3 pt-md-0 p-0 pl-md-2">
                        <a href="/excursion_by_specification/randonnees">
                            <div class="properti_city">
                                <div class="thumb"><img class="img-fluid w100" src="{{asset('images/Minkatoo/ID50144.png')}}" alt="pc2.jpg"></div>
                                <div class="overlay">
                                    <div class="details">
                                        <h4>Randonnées</h4>
                                        {{-- <p>18 Restaurants</p> --}}
                                    </div>
                                </div>
                            </div>
                        </a>
					</div>
				</div>
			</div>
		</section>
        <!-- Section Nos Recommandations -->
        <section class="contentWrapper container">
            <div class="sectionHead">
                {{-- // TODO set responsive font for title --}}
                <h3>Nos recommandations</h3>
            </div>
            <div class="row">
                {{-- @for ($i = 0; $i < 4; $i++) --}}
                {{-- @foreach ($excursion as $excursion) --}}
                @foreach ($excursions as $excursion)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                    <a href="{{ route('excursion-detail',$excursion->id)  }}">
                        <div class="card">
                            <div class="card-img">
                                <img src="{{ $excursion->getPictures()[0]}}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                            </div>
                            <div class="card-content">
                                <h4 style="font-size: 15px; font-weight: bold;">{{ $excursion->title}}</h4>
                                <div class="rating">
                                    <!-- Stars -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                    <span>(15)</span>
                                </div>
                                {{-- <div class="card-footer-text">
                                    <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px "> <!-- mappin icon -->
                                    <span>
                                        76 km
                                    </span>
                                </div> --}}
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
                
                {{-- @else
                    <div class="col-md-10">
                        <h4></h4>
                    </div>
                @endif --}}
                
                
                
                {{-- @endfor --}}
                <!-- Card Item -->
            </div>
        </section>
            <!--  -->
    </main>
    <script src="{{ asset('/asset/js/searchScript.js') }}"></script>
@endsection
