@extends('layouts.master')

@section('title')
    Reservation excursion
@endsection


@section('content')
{{-- // TODO add error alert --}}
<style>
    :root{
        --form-bg-color: #dd6e13;
        --form-bg-accent-color: #efc500;
        --error-color: #e3342f;
    }
    * {
        margin: 0;
        padding: 0
    }

    html {
        height: 100%
    }
    #grad1 {
        /* background-color: : lightslategray; */
        /* background-image: linear-gradient(120deg, lightslategray, lightslategray) */
    }

    #msform {
        text-align: center;
        position: relative;
        margin-top: 10px
    }

    #msform fieldset .form-card {
        background: white;
        border: 0 none;
        border-radius: 0px;
        box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
        padding: 10px 40px 30px 40px;
        box-sizing: border-box;
        width: 94%;
        margin: 0 3% 20px 3%;
        position: relative
    }

    #msform fieldset {
        background: white;
        border: 0 none;
        border-radius: 0.5rem;
        box-sizing: border-box;
        width: 100%;
        margin: 0;
        padding-bottom: 20px;
        position: relative
    }

    #msform fieldset:not(:first-of-type) {
        display: none
    }

    #msform fieldset .form-card {
        text-align: left;
        /* color: #9E9E9E */
        color: #333
    }

    #msform input,
    #msform textarea {
        padding: 0px 8px 4px 8px;
        border: none;
        border-bottom: 1px solid #ccc;
        border-radius: 0px;
        margin-bottom: 25px;
        margin-top: 2px;
        width: 100%;
        box-sizing: border-box;
        font-family: montserrat;
        /* color: #2C3E50; */
        color: #333;
        font-size: 16px;
        letter-spacing: 1px
    }
    #msform input.error,
    #msform textarea.error{
        border-color: var(--error-color);
    }

    #msform input:focus,
    #msform textarea:focus {
        -moz-box-shadow: none !important;
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
        border: none;
        font-weight: bold;
        border-bottom: 2px solid var(--form-bg-color);
        outline-width: 0
    }

    #msform .action-button {
        width: 100px;
        background: var(--form-bg-color);
        font-weight: bold;
        color: white;
        border: 0 none;
        border-radius: 0px;
        cursor: pointer;
        padding: 10px 5px;
        margin: 10px 5px
    }

    #msform .action-button:hover,
    #msform .action-button:focus {
        box-shadow: 0 0 0 2px white, 0 0 0 3px var(--form-bg-color)
    }

    #msform .action-button-previous {
        width: 100px;
        /* background: #616161; */
        font-weight: bold;
        color: white;
        border: 0 none;
        border-radius: 0px;
        cursor: pointer;
        padding: 10px 5px;
        margin: 10px 5px
    }
    .action-button-previous{
        background: #333;
    }

    #msform .action-button-previous:hover,
    #msform .action-button-previous:focus {
        box-shadow: 0 0 0 2px white, 0 0 0 3px #616161
    }
    #maincard{
        margin: auto !important;
    }
    #mainrow {
        display: flex;
        justify-content: center;
    }
    select.list-dt {
        border: none;
        outline: 0;
        border-bottom: 1px solid #ccc;
        padding: 2px 5px 3px 5px;
        margin: 2px
    }

    select.list-dt:focus {
        border-bottom: 2px solid var(--form-bg-color)
    }

    .card {
        z-index: 0;
        border: none;
        border-radius: 0.5rem;
        position: relative
    }

    .fs-title {
        font-size: 25px;
        color: #333;
        margin-bottom: 10px;
        font-weight: bold;
        text-align: left
    }
    .fs-titler {
        font-size: 18px;
        color: #333;
        margin-bottom: 10px;
        font-weight: bold;
        text-align: left
    }

    #progressbar {
        margin-bottom: 30px;
        overflow: hidden;
        /* color: lightgrey */
        color: #333
    }

    #progressbar .active {
        color: #000000
    }

    #progressbar li {
        list-style-type: none;
        font-size: 12px;
        width: 33.3%;
        /* width: 20%; */
        /* width: 16.666%; */
        float: left;
        position: relative
    }

    #progressbar #customer:before {
        font-family: FontAwesome;
        content: "\f007"
    }

    #progressbar #services:before {
        font-family: FontAwesome;
        content: "\f1e9"
    }

    #progressbar #invoice:before {
        font-family: FontAwesome;
        content: "\f09d"
    }
    #progressbar #delivery:before {
        font-family: FontAwesome;
        content: "\f1d8"
    }
    #progressbar #order:before {
        font-family: FontAwesome;
        content: "\f15c"
    }
    #progressbar #checkout:before {
        font-family: FontAwesome;
        content: "\f00c"
    }

    #progressbar li:before {
        width: 50px;
        height: 50px;
        line-height: 45px;
        display: block;
        font-size: 18px;
        color: #ffffff;
        /* background: lightgray; */
        background: #333;
        border-radius: 50%;
        margin: 0 auto 10px auto;
        padding: 2px
    }

    #progressbar li:after {
        content: '';
        width: 100%;
        height: 2px;
        background: lightgray;
        position: absolute;
        left: 0;
        /* right: 0; */
        top: 25px;
        z-index: -1
    }

    #progressbar li.active:before,
    #progressbar li.active:after {
        background: var(--form-bg-color)
    }

    /*  */
    #typeahead_clothes span{
        width: 100%;
    }
    /*  */
    .radio-group {
        position: relative;
        margin-bottom: 25px
    }

    .error {
        border-color: red;
    }

    .radio {
        display: inline-block;
        width: 204;
        height: 104;
        border-radius: 0;
        background: lightblue;
        box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
        box-sizing: border-box;
        cursor: pointer;
        margin: 8px 2px
    }

    .radio:hover {
        box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3)
    }

    .radio.selected {
        box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.1)
    }

    .fit-image {
        width: 100%;
        object-fit: cover
    }
</style>


@section('css')
    <link rel="stylesheet" href="{{ asset('customer/css/perso.css') }}">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"/>
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"></script>
@endsection


    @if(Auth::guard('customer')->user())


    <div class="container-fluid" id="grad1">
        <div id="mainrow" class="row mt-0">
            <div id="maincard" class="col-11 col-sm-9 col-md-9 col-lg-9 col-xl-9 text-center p-0 mt-3 mb-2">
                <div class="card px-0 pt-4 pb-0 mt-3 mb-3">

                    @if(Request::is("formulaireReservationResidence/*"))
                        <h2><strong>Réservez votre Redicence dès maintenant</strong></h2>
                        <p>Veuillez remplir les champs suivants.</p>
                    @else
                        <h2><strong>Réservez votre excursion dès maintenant</strong></h2>
                        <p>Veuillez remplir les champs suivants.</p>
                    @endif


                    <div class="row">
                        <div id='stepperHeader' class="col-md-12">
                            <ul id="progressbar">
                                {{-- <li class="active" id="customer"><strong>Client</strong></li> --}}
                                <li id="customer" class="active"><strong>Informations sur la Réservation</strong></li>
                                {{-- <li id="invoice"><strong>Résumé services</strong></li> --}}
                                <li id="invoice"><strong>Résumé de la Réservation</strong></li>
                                {{-- <li id="order"><strong>Paiement</strong></li> --}}
                                <li id="checkout"><strong></strong></li>
                            </ul>
                        </div>

                        <div class="col-md-12 mx-0">
                            <form id="msform" action="" method="post" enctype="multipart/form-data">
                                    {{-- {{ csrf_field() }} {{ route('orders.storeAjax') }} --}}
                                <input type="hidden" id="csrf" value ="{{ csrf_token() }}">
                            <input type="hidden" id="customer_id" value="{{ Auth::guard('customer')->user()->id }}">
                                <!-- progressbar -->



                              <!-- fieldsets -->
                                {{-- <fieldset id="fieldset_customer">
                                    <div class="form-card">
                                        <h2 class="fs-title">Informations du client</h2>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" name="last_name" id="input_last_name" placeholder="Nom" value=""/>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="name" id="input_name" placeholder="Prénom" value=""/>
                                            </div>
                                        </div>
                                        <input type="email" name="email" type="email" id="input_email" placeholder="Email" value="" />
                                        <div class="row">
                                            <div class="col-md-3">
                                                <select name="dial_code" id="input_dial_code" class="form-control">
                                                    <option value="+225">+225</option>
                                                    <option value="+226">+226</option>
                                                </select>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="tel" id="input_phone_number" name="phone_number" placeholder="Contact" value="" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Vous êtes</label>
                                                <select id="input_location_type" name="location_type" class='form-control'>
                                                    <option value="seul">Seul(e)</option>
                                                    <option value="couple">En couple</option>
                                                    <option value="famille">En famille</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="button" id="customer_tab_next_button" name="next" class="action-button" value="Suivant" />
                                </fieldset> --}}
                                <fieldset id="fieldset_service">
                                    <div class="form-card">
                                        <h2 class="fs-title">Informations sur la Réservation</h2>
                                        <br>

                                        {{-- Service Form --}}
                                        <div class="row">
                                            <div class="container-fluid table-responsive table-full-width">
                                                <table class="table table-hover table-striped">
                                                    <thead>

                                                            <th>Numéro ou Nom de excursion</th>
                                                            <th>Coût de la excursion</th>
                                                            <th>Nombre d'adultes</th>
                                                            <th>Nombre d'enfants</th>
                                                            <th>Montant total</th>

                                                        {{-- <th>Actions</th> --}}
                                                    </thead>
                                                    <tbody id="table_review_body">
                                                        <td>
                                                            <input type="text" readonly id="bedroom" value="{{ $excursion->title }}">
                                                        </td>

                                                        <td>
                                                            <input type="text" readonly id="bedroom_price" value="{{ $excursion->price }}"/>
                                                        </td>

                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th colspan="6" style="text-align: right;">
                                                                Total <span id="table_review_total_amount">0</span> Frcs
                                                                {{-- total <span id="table_review_total_amount">0</span> Frcs --}}
                                                            </th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>


                                        {{-- Not display --}}
                                        <div class="service-container" style="display: none">

                                            <div class="row">
                                                <div class="col-md-3">

                                                @if(Request::is("formulaireReservationResidence/*"))
                                                    <label class="pay" style="font-weight: bold;" >Nom de la résidence</label>
                                                @else
                                                    <label class="pay" style="font-weight: bold;" >Type de excursion</label>
                                                @endif


                                                    {{-- <label class="pay" style="font-weight: bold;" >Type de excursion</label> --}}
                                                    <div class="">
                                                        {{-- <input type="text" readonly id="select_name_car" value="{{$excursion->title}}"/> --}}
                                                        <input type="text" readonly id="bedroom" value="{{ $excursion->title }}"/>
                                                        <input type="text" readonly id="bedroom_id" value="{{ $excursion->id }}"/>

                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    @if(Request::is("formulaireReservationResidence/*"))
                                                        <label class="pay" style="font-weight: bold">Coût de la résidence</label> <br>
                                                    @else
                                                        <label class="pay" style="font-weight: bold">Coût de la excursion</label> <br>
                                                    @endif

                                                    <div class="">
                                                        {{-- <input type="text" readonly id="select_price_car" value="{{$excursion->price}}"/> --}}
                                                        <input type="text" readonly id="bedroom_price" value="{{ $excursion->price }}"/>
                                                    </div>
                                                </div>
                                                {{-- <div class="col-md-3">
                                                    <label class="pay" style="font-weight: bold"> Nombre de Jour</label> <br>
                                                    <div class="">
                                                    // TODO typehead search
                                                        <input type="number" name="quantity_number_day" id="input_number_day" placeholder="1" min="1" value="0" onchange="">
                                                    </div>
                                                </div> --}}
                                                <div class="col-md-3">
                                                    <label class="pay" style="font-weight: bold">Montant à Payer</label> <br>
                                                    <span id="display_hotel_amount">0</span> <span>F</span>
                                                </div>
                                            </div>
                                            {{-- <div class="row">
                                                <div class="col-md-2 ml-auto">
                                                    <input type="button" name="next" id="add_bedroom" class="action-button" value="Ajouter"/>
                                                </div>
                                            </div> --}}
                                        </div>

                                        {{-- end Not display --}}

                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="fs-title">
                                                        <b>Nombre d'adultes</b>
                                                    </p>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="number" id='val_p' min="1" placeholder="adultes" step="1" name='val_p' value="">
                                                        </div>
                                                    </div>
                                                    <span id='error' style="display: none">Invalide</span>
                                                </div>

                                                <div class="col-md-6">
                                                    <p class="fs-title">
                                                        <b>Nombre d'enfants</b>
                                                    </p>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="number" id='val_en' min="0" placeholder="enfants" step="1" name='val_en' value="">
                                                        </div>
                                                    </div>
                                                    <span id='error' style="display: none">Invalide</span>
                                                </div>

                                            </div>
                                        </div>



                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p class="fs-title">
                                                        Date d'arrivée
                                                    </p>
                                                    <input type="datetime-local" id="delivery_date" name="delivery_date" placeholder="Entrez votre jour d'arrivé" />
                                                    {{-- <input type="time" id="delivery_time" name="delivery_time" placeholder="Votre heure d'arrivée" /> --}}
                                                </div>

                                                <div  class="col-md-6">
                                                    <p class="fs-title">
                                                        Date de départ
                                                    </p>
                                                    <input type="datetime-local" id="delivery_date_bed" name="delivery_date_bed" placeholder="Entrez votre jour de départ" />
                                                    {{-- <input type="time" id="delivery_time_bed" name="delivery_time_bed" placeholder="Votre heure de départ" /> --}}
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    {{-- <input type="button" name="previous" class="previous action-button-previous" value="Précédent" /> --}}
                                    <input type="button" name="next" id="delivery_tab_next_button" class="action-button" value="Suivant" />
                                </fieldset>

                                <fieldset id="fieldset_order">
                                    <div class="form-card">
                                        <h2 class="fs-title">Facturation de la Réservation</h2>
                                        <br>
                                        <h4 class="fs-titler">Client</h4>
                                        <div class="container">
                                            {{-- Customer info from $form_data --}}
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span>
                                                        Nom:
                                                    </span>
                                                <span id="input_name">{{ Auth::guard('customer')->user()->first_name }}<span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span>
                                                        Prénom:
                                                    </span>
                                                    <span id="input_last_name">{{ Auth::guard('customer')->user()->last_name }}</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span>
                                                        Email:
                                                    </span>
                                                    <span id="input_email">{{ Auth::guard('customer')->user()->email }}</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span>
                                                        Contact:
                                                    </span>
                                                    <span id="input_phone_number">{{ Auth::guard('customer')->user()->phone }}<span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span>
                                                        Nombre d'adultes:
                                                    </span>
                                                    <span id="display_val_p"></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <span>
                                                        Nombre d'enfants:
                                                    </span>
                                                    <span id="display_val_en"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <h4 class="fs-titler">Services</h4>
                                        <div class="table-responsive table-full-width" >
                                            <table class="table table-hover table-striped">
                                                <thead>

                                                    <th>Numéro ou Nom de excursion</th>
                                                    <th>Coût de la excursion</th>
                                                    <th>Nombre de jours</th>
                                                    <th>Montant total</th>
                                                </thead>
                                                    {{-- Services info from $form_data --}}
                                                    <tbody>
                                                        <tr>

                                                            <td id="bedroom_name">

                                                            </td>

                                                            <td id="bedroom_unity_price">

                                                            </td>

                                                            <td id="number_day">

                                                            </td>

                                                            <td id="total_bed_amount">

                                                            </td>

                                                        </tr>
                                                    </tbody>

                                                    <tfoot>
                                                        <tr>
                                                            <th colspan="6" style="text-align: right;">
                                                                Total <span id="order_review_total_amount">0</span> Frcs
                                                            </th>
                                                        </tr>
                                                    </tfoot>
                                            </table>
                                        </div>
                                        {{--  --}}
                                        <h4 class="fs-titler">Période et Durée</h4>
                                            {{-- Delivery info from $form_data --}}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>
                                                    Arrivée le <i id="display_delivery_date"></i>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p>
                                                    Départ le <i id="display_delivery_date_bed"></i>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="button" name="previous" class="previous action-button-previous" value="Précédent" />
                                    <input type="button" name="next" id="order_tab_next_button" class="action-button" value="Suivant" />
                                </fieldset>
                               
                                <fieldset id="fieldset_checkout">
                                    <div class="form-card">
                                        <h2 class="fs-title text-center">Success !</h2> <br><br>
                                        <div class="row justify-content-center">
                                            <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>
                                        </div> <br><br>
                                        <div class="row justify-content-center">
                                            <div class="col-7 text-center">
                                                <h5>Réservation enregistrée avec success</h5>
                                            </div>
                                        </div>
                                        <div class="row">

                                        </div>
                                    </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="{{ asset('asset/commandeExcursion.js') }}"></script>
@endsection
