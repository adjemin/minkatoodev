@extends('layouts.master')

@section('title', 'Customer Profil')
    
@section('content')    
    @php
        $customer = Auth::guard('customer')->user();
    @endphp

    <div class="container" style="padding: 5%;">
            <h1>Mon Profil</h1>
        <form method="post">
            <div class="row">
                <div class="col-md-3">
                    <div class="profile-img">
                        {{-- <img src="storage/'.$customer->photo_url" alt=""> --}}

                        <img src="{{ !is_null($customer->photo_url) ? asset('storage/'.$customer->photo_url) : 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQEyza1-CKEJK6Hyq50yE8oOpyba5HbJaCqMioudHzaxVh-a9Is'}}"
                        id="output" style="width: 175px; height: 175px;">

                        {{-- <div class="file btn btn-sm btn-secondary" style="width: 101%">
                            Modifier la photo de profil<input type="file" name="photo_url"/>
                        </div> --}}
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="profile-head">
                                <h5>
                                    {{ Auth::guard('customer')->user()->first_name }} {{ Auth::guard('customer')->user()->last_name }}
                                </h5>
                                {{-- <h6> --}}
                                    <div class="tab-content profile-tab" id="myTabContent">
                                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Nom</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                        <p>
                                                            {{ Auth::guard('customer')->user()->last_name }}
                                                        </p>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Prénom</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                        <p>
                                                            {{ Auth::guard('customer')->user()->first_name }}
                                                        </p>
                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Email</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <p>
                                                                {{ Auth::guard('customer')->user()->email }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label>Phone</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <p>
                                                                {{ Auth::guard('customer')->user()->phone }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        {{-- <div class="col-md-6">
                                                            <label>Profession</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <p>
                                                                {{ Auth::guard('customer')->user()->email }}
                                                            
                                                            </p>
                                                        </div> --}}
                                                </div>
                                        </div>
                                        {{-- <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Experience</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <p>Expert</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Hourly Rate</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <p>10$/hr</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Total Projects</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <p>230</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="col-md-6">
                                                            <label>English Level</label>
                                                        </div>
                                                            <p>Expert</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Availability</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <p>6 months</p>
                                                        </div>
                                                    </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>Your Bio</label><br/>
                                                    <p>Your detail description</p>
                                                </div>
                                            </div>
                                        </div> --}}
                                    </div>
                                {{-- </h6> --}}
                                {{-- <p class="proile-rating">RANKINGS : <span>8/10</span></p> --}}
                       
                    </div>
                </div>
                <div class="col-md-2">
                    {{-- <input type="submit" class="profile-edit-btn" name="btnAddMore" value="Edit Profile"/> --}}
                <a class="btn btn-primary text-white" href="{{ route('editProfil', [ Auth::guard('customer')->user()->id ]) }}">
                        Modifier le profil
                    </a>    
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    {{-- <div class="profile-work"> --}}
                        {{-- <p>WORK LINK</p>
                        <a href="">Website Link</a><br/>
                        <a href="">Bootsnipp Profile</a><br/>
                        <a href="">Bootply Profile</a>
                        <p>SKILLS</p>
                        <a href="">Web Designer</a><br/>
                        <a href="">Web Developer</a><br/>
                        <a href="">WordPress</a><br/>
                        <a href="">WooCommerce</a><br/>
                        <a href="">PHP, .Net</a><br/> --}}
                    {{-- </div> --}}
                </div>
            </div>
        </form>           
    </div>
@endsection