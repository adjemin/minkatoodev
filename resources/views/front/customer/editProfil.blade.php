@extends('layouts.master')

@section('title', 'Customer Profil')
    
@section('content')    

    @php
        $customer =  Auth::guard('customer')->user();
    @endphp

    <div class="container" style="padding: 5%;">
    <form method="post"  enctype="multipart/form-data" action="{{ route('updateProfile', [$customer->id] )}}">
            @csrf
            @method('PUT')
            <h1 class="text-center">
               Modifier Mon Profil 
            </h1>

            <div class="row">
                <div class="col-md-3">
                    <div class="profile-img">
                        <img src="{{ !is_null($customer->photo_url) ? asset('storage/'.$customer->photo_url) : 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQEyza1-CKEJK6Hyq50yE8oOpyba5HbJaCqMioudHzaxVh-a9Is'}}"
                        id="output" style="width: 250px; height: 175px;">


                      
                        <div class="file btn btn-sm btn-secondary" style="width: 101%">
                            Modifier la photo de profil
                            <input accept="image/*" onchange="loadFile(event)" type="file" name="photo_url"/>

                            <script>
                                var loadFile = function (event) {
                                    var reader = new FileReader();
                                    reader.onload = function () {
                                        var output = document.getElementById('output');
                                        output.src = reader.result;
                                    };
                                    reader.readAsDataURL(event.target.files[0]);
                                };
                            </script>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="home"  role="tabpanel" aria-labelledby="home-tab">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top:20px;">Nom</label>
                                        </div>
                                        <div class="col-md-8" style="margin-top:20px;">
                                            <input type="text" name='last_name' class='form-control' value="{{ Auth::guard('customer')->user()->last_name }}">
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top:20px;">Prénom(s)</label>
                                        </div>
                                        <div class="col-md-8" style="margin-top:20px;">
                                            <input type="text" name='first_name' class='form-control' value="{{ Auth::guard('customer')->user()->first_name }}">
                                        </div>
                                    </div>
                                    

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label 
                                            style="margin-top:20px;">Email</label>
                                        </div>
                                        <div class="col-md-8" style="margin-top:20px;">
                                            <input type="text" name='email' class='form-control' value="{{ Auth::guard('customer')->user()->email }}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label style="margin-top:20px;">Phone</label>
                                        </div>
                                        <div class="col-md-8" style="margin-top:20px;">
                                            <input type="text" name="phone" class='form-control' value="{{ Auth::guard('customer')->user()->phone }}">
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-center" style="margin-top: 30px;">
                                        <button class="btn btn-primary text-white" type="submit" name="btnAddMore">
                                            Mettre à jour le profil
                                        </button>
                                    </div>

                                        
                        
                                    <div class="row">
                                        {{-- <div class="col-md-6">
                                            <label>Profession</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>
                                                {{ Auth::guard('customer')->user()->email }}
                                            
                                            </p>
                                        </div> --}}
                                </div>
                        </div>
                        {{-- <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Experience</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>Expert</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Hourly Rate</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>10$/hr</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Total Projects</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>230</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="col-md-6">
                                            <label>English Level</label>
                                        </div>
                                            <p>Expert</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Availability</label>
                                        </div>
                                        <div class="col-md-6">
                                            <p>6 months</p>
                                        </div>
                                    </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Your Bio</label><br/>
                                    <p>Your detail description</p>
                                </div>
                            </div> --}}
                    </div>

                    
                </div>
                <div class="col-md-2">
            
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    {{-- <div class="profile-work"> --}}
                        {{-- <p>WORK LINK</p>
                        <a href="">Website Link</a><br/>
                        <a href="">Bootsnipp Profile</a><br/>
                        <a href="">Bootply Profile</a>
                        <p>SKILLS</p>
                        <a href="">Web Designer</a><br/>
                        <a href="">Web Developer</a><br/>
                        <a href="">WordPress</a><br/>
                        <a href="">WooCommerce</a><br/>
                        <a href="">PHP, .Net</a><br/> --}}
                    {{-- </div> --}}
                </div>
                
                <div class="col-md-8">
                    
                </div>
            </div>
        </form>           
    </div>
@endsection