@extends('layouts.master')

@section('title')
    Mes Réservations
@endsection

@section('css')
    <style>
         body {
            background-color: #ccd8ec;
        }

        .card-1 {
            border: none;
            border-radius: 10px;
            width: 100%;
            background-color: #fff
        }

        .icons i {
            margin-left: 20px
        }
    </style>
@endsection

@section('content')


@php
    use Carbon\Carbon;
    Carbon::setLocale('fr');
@endphp



<div class="container mt-5">
    <div class="row">
        <div class="col-md-12 d-flex justify-content-center">
            <h1 class="text-center">Mes Reservations</h1>
        </div>

        {{-- <a class="col-md-4" href="{{ route() }}">
            <div class="btn btn-primary">Retour </div>
        </a> --}}
    </div>


    <table class="table table-borderless table-responsive card-1 p-4" >
        <thead>
            <tr class="border-bottom">
                {{-- <th> <span class="ml-2">En attente </span> </th> --}}
                {{-- <th> <span class="ml-2">Nom de client</span> </th> --}}
                <th> <span class="ml-2">Type de Reservation</span> </th>
                <th> <span class="ml-2">Titre et description</span> </th>
                <th> <span class="ml-2">Status</span> </th>
                <th> <span class="ml-2">Prix</span> </th>
                <th> <span class="ml-2">Adresse</span> </th>
                {{-- <th> <span class="ml-2">Contact</span> </th> --}}
                <th> <span class="ml-2">Date de création </span> </th>
                <th> <span class="ml-2">Date de début </span> </th>
                <th> <span class="ml-2">Date d'expiration </span> </th>
                {{-- <th> <span class="ml-2">Date de creation</span> </th>
                <th> <span class="ml-2">Date de creation</span> </th> --}}
                {{-- <th> <span class="ml-4">Action</span> </th> --}}
            </tr>
        </thead>
        <tbody>

            @if(count($orders) < 1 )
                <h2 class="text-center">Vous n'avez effectué aucune Reservation pour le moment.</h2>
            @endif

            @foreach ($orders as $order)

            {{-- {{ dd(json_decode($order->getItemsAttribute()[0])->start_date) }}
            {{ dd(json_decode($order->getItemsAttribute()[0])->end_date) }}  --}}
            {{-- {{ dd(json_decode($order->getItemsAttribute()[0]->meta_data)->picture1) }}  --}}
            {{-- {{ dd(json_decode($order->getItemsAttribute()[0]->meta_data)->description) }}  --}}
            {{-- {{ dd(json_decode($order->getItemsAttribute()[0]->meta_data)->location_name) }}  --}}
            {{-- {{ dd(json_decode($order->getItemsAttribute()[0]->meta_data)->slide_url) }}  --}}
            {{-- {{ dd(json_decode($order->getItemsAttribute()[0]->meta_data)->phone_number) }}  --}}




        <tr class="border-bottom">
                {{-- <td>
                    <div class="p-2"> <span class="d-block ">
                        {{  $order->getCustomer()->name }}</span>  {{--<small>2:30PM</small>
                    </div>
                </td>    --}}

                <td>
                    <div class="p-2"> <span class="d-block font-weight-bold text-uppercase">
                        @if(count($order->getItemsAttribute()) > 0)
                            {{ json_decode($order->getItemsAttribute()[0]->meta_data)->type }}
                        @else
                            {{ "Objet vide !" }}
                        @endif

                        @if(count($order->getItemsAttribute()) > 0)
            <img src="{{ json_decode($order->getItemsAttribute()[0]->meta_data)->picture1 }}" width="90" height ="100" class="img-thumbnail">
                   @endif
                    </div>
                </td>

                <td>

            <div class="p-2 d-flex flex-row align-items-center mb-2">
                        {{-- @if(count($order->getItemsAttribute()) > 0)
                            <img src=" {{ asset( 'storage/'.json_decode($order->getItemsAttribute()[0]->meta_data)->picture1)}} " width="90" height ="100" class="img-thumbnail">
                         @endif --}}


                        <div class="d-flex flex-column ml-2"> <span class="d-block font-weight-bold">

                        @if(count($order->getItemsAttribute()) > 0)
                            {{ json_decode($order->getItemsAttribute()[0]->meta_data)->title }}
                        @else
                            {{ "Objet vide ! " }}
                        @endif

                    </span>

                    <small class="text-muted">

                        @if(count($order->getItemsAttribute()) > 0)
                            {!! Str::limit(json_decode($order->getItemsAttribute()[0]->meta_data)->description, 30)  !!}
                        @else
                            {{ "Objet vide ! " }}
                        @endif

                    </small> </div>
                </div>
                </td>

                <td>
                    <div class="p-2"> <span class="font-weight-bold">
                        @php
                        switch ($order->current_status) {
                            case 'waiting':
                                echo '<button type="button" class="btn btn-info btn-sm"> Impayé </button>';
                                break;
                                case 'unpaid':
                                echo '<button type="button" class="btn btn-info btn-sm"> Impayé </button>';
                                break;
                            case 'confirmed':
                            echo '<button type="button" class="btn btn-primary btn-sm"> Confirmé </button>';
                                break;
                            case 'edited':
                            echo '<button type="button" class="btn btn-default btn-sm"> Modifié </button>';
                                break;
                            case 'refused':
                            echo '<button type="button" class="btn btn-danger btn-sm"> Refused </button>';
                                break;
                            case 'failed':
                            echo '<button type="button" class="btn btn-danger btn-sm"> échoué </button>';
                                break;
                            case 'report':
                            echo '<button type="button" class="btn btn-warning btn-sm"> Reporté </button>';
                                break;
                            case 'noted':
                            echo '<button type="button" class="btn btn-secondary btn-sm"> Noted </button>';
                                break;
                            case 'waiting_customer_pickup':
                            echo '<button type="button" class="btn btn-warning btn-sm"> Waiting Customer Pickup </button>';
                                break;
                            case 'waiting_customer_delivery':
                            echo '<button type="button" class="btn btn-warning btn-sm"> Waiting Customer Delivery </button>';
                                break;
                            case 'delivered':
                            echo '<button type="button" class="btn btn-success btn-sm"> Delivered </button>';
                                break;
                            case 'reported':
                            echo '<button type="button" class="btn btn-danger btn-sm"> Reporté </button>';
                                break;
                            case 'customer_paid':
                            echo '<button type="button" class="btn btn-success btn-sm"> Payé </button>';
                                break;
                            case 'has_seller_paid':
                            echo '<button type="button" class="btn btn-success btn-sm"> Has Seller Paid </button>';
                                break;
                            case 'canceled':
                            echo '<button type="button" class="btn btn-danger btn-sm">Annulé</button>';
                                break;
                            default:
                            echo '<button type="button" class="btn btn-default btn-sm"> Must add to status field </button>';
                        }
                     @endphp
                    </span>
                </div>
                </td>

                <td>
                    <div class="p-2"> <span class="">
                        @if(count($order->getItemsAttribute()) > 0)
                            {{ $order->amount != null ?  $order->amount ." "."FCFA"
                                    :
                                json_decode($order->getItemsAttribute()[0]->meta_data)->price ." "."FCFA" }}
                        @else
                            {{ "Objet vide ! " }}
                        @endif
                    </span> </div>
                </td>

                <td>
                    <div class="p-2"> <span class="">
                        @if(count($order->getItemsAttribute()) > 0)
                            {{  Str::limit(json_decode($order->getItemsAttribute()[0]->meta_data)->location_name, 30) }}
                        @else
                            {{ "Champ vide ! " }}
                        @endif
                        </span>
                    </div>
                </td>
                 {{-- <td>
                    <div class="p-2"> <span class="font-weight-bold">
                        @if(count($order->getItemsAttribute()) > 0)
                            +{{ json_decode($order->getItemsAttribute()[0]->meta_data)->phone_number }}
                        @else
                            {{ "Champ vide ! " }}
                        @endif
                        </span>
                    </div>
                </td>    --}}

                {{-- <td>
                    <div class="p-2"> <span class="font-weight-bold">
                        @if(count($order->getItemsAttribute()) > 0)
                            {{ json_decode($order->getItemsAttribute()[0]->meta_data) }}
                        @else
                            {{ "Champ vide ! " }}
                        @endif
                        </span>
                    </div>
                </td> --}}

                <td>
                    <div class="p-2"> <span class="font-weight-bold">
                                {{ $order->created_at->diffForHumans() }}
                        </span>
                    </div>
                </td>
                <td>
                    <div class="p-2"> <span class="">
                        @if(count($order->getItemsAttribute()) > 0)
                            {{ Carbon::parse(json_decode($order->getItemsAttribute()[0])->start_date)  }}
                        @else
                            {{ "Champ vide ! " }}
                        @endif
                        </span>
                    </div>
                </td>

                <td>
                    <div class="p-2"> <span class="">
                        @if(count($order->getItemsAttribute()) > 0)
                            {{ json_decode($order->getItemsAttribute()[0])->end_date }}
                        @else
                            {{ "Champ vide ! " }}
                        @endif
                        </span>
                    </div>
                </td>

                {{-- <td>
                    <div class="p-2 d-flex flex-column"> <span>1 City point,#2A</span> <span> Brooklyn,NY</span> </div>
                </td> --}}
                {{-- <td>
                    <div class="p-2 icons"> <i class="fa fa-phone text-danger"></i> <i class="fa fa-adjust text-danger"></i> <i class="fa fa-share"></i> </div>
                </td> --}}
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
