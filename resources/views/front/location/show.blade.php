@extends('layouts.master')

@section('title')
    Details Voitures à louer
@endsection

@section('styles')

@endsection

@section('content')
    <div class="single_page_listing_style">
		<div class="container-fluid p0">

		@if(count($car->getPictures())>= 4)
                    <div class="row">
                        <div class="col-sm-6 col-lg-6 p0">
                            <div class="row m0">
                                <div class="col-lg-12 p0">
                                    <div class="spls_style_one pr1 1px">
                                        <img class="img-fluid w100" src="{{ $car->getPictures()[0] }}" alt="ls1.jpg" style="width: 100%; height: 475px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-6 p0">
                            <div class="row m0">
                                <div class="col-sm-6 col-lg-6 p0">
                                    <div class="spls_style_one">
                                        <a class="popup-img" href="{{$car->getPictures()[0]}}"><img class="img-fluid w100" src="{{$car->getPictures()[0]}}" alt="ls2.jpg" style="height: 250px; width: 250px;"></a>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6 p0">
                                    <div class="spls_style_one">
                                        <a class="popup-img" href="{{$car->getPictures()[1]}}"><img class="img-fluid w100" src="{{$car->getPictures()[1]}}" alt="ls3.jpg" style="height: 250px; width: 250px;"></a>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6 p0">
                                    <div class="spls_style_one">
                                        <a class="popup-img" href="{{$car->getPictures()[2]}}"><img class="img-fluid w100" src="{{$car->getPictures()[2]}}" alt="ls4.jpg" style="height: 250px; width: 250px;"></a>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6 p0">
                                    <div class="spls_style_one">
                                        <a class="popup-img" href="{{$car->getPictures()[3]}}"><img class="img-fluid w100" src="{{$car->getPictures()[3]}}" alt="ls5.jpg" style="height: 250px; width: 250px;"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            @else
                <div class='row'>
                    <div class="col-sm-12 col-lg-12 p0">
                        <div class="row m0">
                            <div class="col-lg-12 p0">
                                <div class="spls_style_one pr1 1px">
                                    <img class="img-fluid w100" src="{{ $car->getPictures()[0] }}" alt="" style="width: 100%; height: 100%;">
                            </div>
                        </div>
                    </div>
                </div>
            @endif


		</div>
	</div>
	<section class="p0">
		<div class="container">
			<div class="row listing_single_row">
				<div class="col-sm-6 col-lg-7 col-xl-8">
					<div class="single_property_title">
						<a href="{{$car->slide_url}}" class="upload_btn popup-img"><span class="flaticon-photo-camera"></span> Afficher toutes les photos</a>
					</div>
				</div>
				<div class="col-sm-6 col-lg-5 col-xl-4">
					<div class="single_property_social_share">
						<div class="spss style2 mt10 text-right tal-400">
							<ul class="mb0">
								<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
								<li class="list-inline-item"><a href="#"><span class="flaticon-share"></span></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
    <section class="our-agent-single bgc-f7 pb30-991">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-lg-8">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="application_statics mt30-767 mb30-767">
                                <div class="single_property_title">
                                    <h2>{{ $car->title}}</h2>
	                                <div class="_2NK4P3lO">
	                                    <span class="sspd_review float-left">
	                                        <ul>
	                                            <li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
	                                            <li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
	                                            <li class="list-inline-item"><a href="#"><i class="fa fa-star active"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
                                                <li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
	                                        </ul>
	                                    </span>
	                                    <span class="_28oqjHA2">210&nbsp;avis</span>
	                                </div>
										<p>{{$car->location_name}}</p>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
                            <div class="application_statics mt30">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4 class="mb10">Caractéristique de la voiture</h4>
                                    </div>

                                    @php
                                        $car_spe = json_decode($car->meta_data);
                                        // dd( $car_spe);
                                    @endphp                                    
                                    {{-- @if($car_spe != null && count($car_spe) > 0)  --}}
                                        {{-- @foreach ($car_spe as $item) --}}
                                            <div class="col-sm-6 col-md-6 col-lg-4">
                                                <ul class="order_list list-inline-item">
                                                <li>
                                                    <a href="#"><span class="flaticon-tick"></span> {{$car_spe->model }} </a>
                                                </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-4">
                                                <ul class="order_list list-inline-item">
                                                <li>
                                                    <a href="#"><span class="flaticon-tick"></span> {{ $car_spe->model }} </a>
                                                </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-4">
                                                <ul class="order_list list-inline-item">
                                                <li>
                                                    <a href="#"><span class="flaticon-tick"></span> {{ $car_spe->brand }} </a>
                                                </li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-4">
                                                <ul class="order_list list-inline-item">
                                                <li>
                                                    <a href="#"><span class="flaticon-tick"></span> {{ $car_spe->year }} </a>
                                                </li>
                                                </ul>
                                            </div>
                                           
                                            {{-- @endforeach --}}
                                        {{-- @endif --}}
{{-- <li><a href="#"><span class="flaticon-tick"></span>Barbeque</a></li> 
<li><a href="#"><span class="flaticon-tick"></span>Dryer</a></li>
<li><a href="#"><span class="flaticon-tick"></span>Gym</a></li>
<li><a href="#"><span class="flaticon-tick"></span>Laundry</a></li> --}}
{{-- <div class="col-sm-6 col-md-6 col-lg-4">
    <ul class="order_list list-inline-item">
                                            <li><a href="#"><span class="flaticon-tick"></span>Lawn</a></li>
                                            <li><a href="#"><span class="flaticon-tick"></span>Microwave</a></li>
                                            <li><a href="#"><span class="flaticon-tick"></span>Outdoor Shower</a></li>
                                            <li><a href="#"><span class="flaticon-tick"></span>Refrigerator</a></li>
                                            <li><a href="#"><span class="flaticon-tick"></span>Sauna</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-4">
                                        <ul class="order_list list-inline-item">
                                            <li><a href="#"><span class="flaticon-tick"></span>Swimming Pool</a></li>
                                            <li><a href="#"><span class="flaticon-tick"></span>TV Cable</a></li>
                                            <li><a href="#"><span class="flaticon-tick"></span>Washer</a></li>
                                            <li><a href="#"><span class="flaticon-tick"></span>WiFi</a></li>
                                            <li><a href="#"><span class="flaticon-tick"></span>Window Coverings</a></li>
                                        </ul>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="application_statics mt30">
                                <h4 class="mb30">Emplacement</h4>
                                <div class="property_video p0">
                                    <div class="thumb">
                                        <div class="h400" id="map" style="height:400px; width:100%"></div>
                                        <!-- Location Latitude Field -->
                                        <input type="text" name="location_lat" id="location_lat" value={{ $car->location_lat }} hidden>
                                        <!-- Location Longitude Field -->
                                        <input type="text" name="location_lng" id="location_lng" value={{ $car->location_lng }} hidden>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
                <div class="col-lg-4 col-xl-4 _164veh0">
					<div class="sidebar_listing_list">
						<div class="sidebar_advanced_search_widget">
							<div class="price">
                                <h2 style="text-align: center">{{ $car->price }} CFA <small> / Jour</small></h2>
							</div>
							<ul class="sasw_list mb0">
								<div class="search_option_button">
                                    <a href="{{ route('request_form', [$car->id]) }}">
                                        <button type="submit" class="btn btn-block btn-thm">Réserver</button>
                                    </a>
								</div>
							</ul>
						</div>
					</div>
                </div>
            </div>
        </div>
    </section>
    <script>
        // Abidjan
        // var startlat = 5.40911790;
        // var startlon = -4.04220990;
        var startlat = document.getElementById('location_lat').value;
        var startlon = document.getElementById('location_lng').value;

        var options = {
            center: [startlat, startlon],
            zoom: 10
        }

        console.log(startlat, startlon);
        var map = L.map('map', options);

        var nzoom = 12;

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'OSM'}).addTo(map);

        var myMarker = L.marker([startlat, startlon], {
            title: "Coordinates",
            alt: "Coordinates",
            draggable: true
        }).addTo(map).on('dragend', function () {
            var lat = myMarker.getLatLng().lat.toFixed(8);
            var lon = myMarker.getLatLng().lng.toFixed(8);
            var czoom = map.getZoom();

            if (czoom < 18) {
                nzoom = czoom + 2;
            }
            if (nzoom > 18) {
                nzoom = 18;
            }
            if (czoom != 18) {
                map.setView([lat, lon], nzoom);
            } else {
                map.setView([lat, lon]);
            }
            document.getElementById('location_lat').value = lat;
            document.getElementById('location_lng').value = lon;
            myMarker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();
        });

        function chooseAddr(lat1, lng1) {
            myMarker.closePopup();
            map.setView([lat1, lng1], 18);
            myMarker.setLatLng([lat1, lng1]);
            lat = lat1.toFixed(8);
            lon = lng1.toFixed(8);
            document.getElementById('location_lat').value = lat;
            document.getElementById('location_lng').value = lon;
            //document.getElementById('locat').value = addrn;
            //alert(addrn);
            myMarker.bindPopup("Lat " + lat + "<br />Lon " + lon).openPopup();

            $.get('https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=' + lat + '&lon=' + lon, function (data) {
                //console.log(data);
                // document.getElementById('locat').value = data.address.city + ',' + data.address.state + ',' + data.address.country;
                document.getElementById('locat').value = data.display_name;
            });
            //alert(i);
        //document.getElementById('location').submit();
        }

        function lolp(ii) {
            alert(ii);
        }

        function loca(i) {
            alert(i);
        }

        function myFunction(arr) {
            var out = "<br />";
            var i;

            if (arr.length > 0) {
                for (i = 0; i < arr.length; i++) {
                    out += "<div class='address' title='Show Location and Coordinates' style='cursor:pointer' value='" + arr[i].display_name + "' onclick='chooseAddr(" + arr[i].lat + ", " + arr[i].lon + ");return false;'>" + arr[i].display_name + "</div>";
                }
                document.getElementById('results').innerHTML = out;
            } else {
                document.getElementById('results').innerHTML = "Sorry, no results...";
            }

        }

        function addr_search() {
            var inp = document.getElementById("addr");
            var xmlhttp = new XMLHttpRequest();
            var url = "https://nominatim.openstreetmap.org/search?format=json&limit=3&q=" + inp.value;
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    var myArr = JSON.parse(this.responseText);
                    myFunction(myArr);
                }
            };
            xmlhttp.open("GET", url, true);
            xmlhttp.send();
        }

        //onclick='"+loca(arr[i].display_name);"'
        //"+arr[i].display_name+",
        //  onclick='loca(\""+arr[i].display_name+"\")'

    </script>
@endsection

