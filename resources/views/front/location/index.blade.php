@extends('layouts.main')

@section('css')
<style>
    .loader{
        display: none;
        margin: 20px 50%;
        margin-top: -30px;
    }
    
</style>
@endsection

@section('title')
    Voitures à louer
@endsection

@section('header')
<header>
    {{-- // Included connexion state from layouts.main --}}
    <!-- Main Header Image -->
    <div class="showcase">
        <img src="{{ asset('images/location_showcase.png') }}" alt="" class="img-fluid">
        <div class="blackOverlay"></div>
    </div>
    
    @include('front.partials.header')
    
    </header>
@endsection
@section('main')
    <main>
        <!-- Navigation catégories -->
        <nav id="iconNavbar" class="container">
            <ul class="">
                {{-- // TODO turn into TabView --}}
                <li class="iconItem">
                    <a href="/hotels" class="">
                        <p>Hotels</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/hotel@1X.png" alt="Hotels">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="/restaurants">

                        <p>Restaurants</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/plate-fork-and-knife@1X.png" alt="restaurants">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="excursion">

                        <p>Excursion</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/sunset@1X.png" alt="Excursion">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem active">
                    <a href="location">
                        <p>Location Voiture</p>
                        <div>
                            <!-- Item image -->
                            <img src="{{asset('images/icones/location_active.png')}}" alt="location Voiture">

                        </div>
                    </a>
                </li>
                <!--  -->
                <li class="iconItem">
                    <a href="musees">

                        <p>Musée</p>
                        <div>
                            <!-- Item image -->
                            <img src="img/museum@1X.png" alt="musée">

                        </div>
                    </a>
                </li>
                <!--  -->
            </ul>
        </nav>
        <!--  -->
        <!-- Section Voitures en location à proximité -->
        <section class="contentWrapper container">
            <div class="sectionHead">
                <h2>Véhicules en location</h2>
                {{-- <a href="" class="flatLink color">Tout afficher</a> --}}
            </div>
            <div class="row" id="dynamicCar">
                @foreach ($cars as $car)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-4">
                    <a href="{{ route('location-detail',$car->id)  }}">
                        <div class="card">
                            <div class="card-img">
                                <img src="{{$car->slide_url}}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                            </div>
                            <div class="card-content">
                                <h4 style="font-size: 15px; font-weight: bold;">{{$car->title}}</h4>
                            </a>
                                <div class="rating">
                                    <!-- Stars -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                    <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                    <span>(15)</span>
                                </div>
                                <div class="card-footer-text">
                                    <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px "> <!-- mappin icon -->
                                    <span>
                                        76 km
                                    </span>
                                </div>
                            </div>
                        </div>

                </div>
                @endforeach
                <!-- Card Item -->
            </div>
        </section>
        <!-- Section restaurants à proximité -->
        <script src="{{ asset('/asset/js/searchScript.js') }}"></script>
    </main>
@endsection
