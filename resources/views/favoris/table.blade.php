
<div class="box">

    <!-- /.box-header -->
    <div class="box-body" style="overflow-x: scroll;">
      <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive-sm">
        <thead>
            <tr>
            <th>Client</th>
        <th>Identifiant de l'offre</th>
        <th>Offre</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($favoris as $favori)
            <tr>
                <td>{{ $favori->customer->name }}</td>
            <td>{{ $favori->offer->title }}</td>
            <td>{{ $favori->offer }}</td>
                <td>
                    {!! Form::open(['route' => ['favoris.destroy', $favori->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('favoris.show', [$favori->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('favoris.edit', [$favori->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Êtes vous sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>