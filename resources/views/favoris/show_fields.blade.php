<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Identifiant de client:') !!}
    <p>{{ $favori->customer_id }}</p>
</div>

<!-- Offer Id Field -->
<div class="form-group">
    {!! Form::label('offer_id', 'Identifiant de l'offre:') !!}
    <p>{{ $favori->offer_id }}</p>
</div>

