<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Identifiant du client:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Offer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('offer_id', 'Identifiant de l'offre:') !!}
    {!! Form::text('offer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Offer Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('offer', 'l'offre:') !!}
    {!! Form::textarea('offer', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Enregistrer', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('favoris.index') }}" class="btn btn-secondary">Annuler</a>
</div>
