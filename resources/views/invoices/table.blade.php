
<div class="box">

    <!-- /.box-header -->
    <div class="box-body" style="overflow-x: scroll;">
      <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive-sm">
        <thead>
            <tr>
            <th>Identifiant de la commande</th>
        <th>Identifiant du client</th>
        <th>Reference</th>
        <th>Lien</th>
        <th>Sous-total</th>
        <th>Tax</th>
        <th>Frais de livraison</th>
        <th>Total</th>
        <th>Status</th>
        <th>Payé par le client</th>
        <th>Payé par le service de livraison</th>
        <th>Code de la devise</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($invoices as $invoice)
            <tr>
                <td>{{ $invoice->order_id }}</td>
            <td>{{ $invoice->customer_id }}</td>
            <td>{{ $invoice->reference }}</td>
            <td>{{ $invoice->link }}</td>
            <td>{{ $invoice->subtotal }}</td>
            <td>{{ $invoice->tax }}</td>
            <td>{{ $invoice->fees_delivery }}</td>
            <td>{{ $invoice->total }}</td>
            <td>{{ $invoice->status }}</td>
            <td>
                @if($invoice->is_paid_by_customer == 0)
                    Non
                @else
                    Oui
                @endif
            </td>
            <td>
                @if($invoice->is_paid_by_delivery_service == 0)
                    Non
                @else
                    Oui
                @endif
            </td>
            <td>{{ $invoice->currency_code }}</td>
                <td>
                    {!! Form::open(['route' => ['invoices.destroy', $invoice->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>