
<div class="box">

    <!-- /.box-header -->
    <div class="box-body" style="overflow-x: scroll;">
      <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive-sm">
        <thead>
            <tr>
            <th>Token</th>
        <th>Customer Id</th>
        <th>Is Active</th>
        <th>Location Address</th>
        <th>Location Latitude</th>
        <th>Location Longitude</th>
        <th>Battery</th>
        <th>Version</th>
        <th>Device</th>
        <th>Ip Address</th>
        <th>Network</th>
        <th>Ismobile</th>
        <th>Istesting</th>
        <th>Deviceid</th>
        <th>Devicesosversion</th>
        <th>Devicesname</th>
        <th>W</th>
        <th>H</th>
        <th>Ms</th>
        <th>Idapp</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($customerSessions as $customerSession)
            <tr>
                <td>{{ $customerSession->token }}</td>
            <td>{{ $customerSession->customer_id }}</td>
            <td>{{ $customerSession->is_active }}</td>
            <td>{{ $customerSession->location_address }}</td>
            <td>{{ $customerSession->location_latitude }}</td>
            <td>{{ $customerSession->location_longitude }}</td>
            <td>{{ $customerSession->battery }}</td>
            <td>{{ $customerSession->version }}</td>
            <td>{{ $customerSession->device }}</td>
            <td>{{ $customerSession->ip_address }}</td>
            <td>{{ $customerSession->network }}</td>
            <td>{{ $customerSession->isMobile }}</td>
            <td>{{ $customerSession->isTesting }}</td>
            <td>{{ $customerSession->deviceId }}</td>
            <td>{{ $customerSession->devicesOSVersion }}</td>
            <td>{{ $customerSession->devicesName }}</td>
            <td>{{ $customerSession->w }}</td>
            <td>{{ $customerSession->h }}</td>
            <td>{{ $customerSession->ms }}</td>
            <td>{{ $customerSession->idapp }}</td>
                <td>
                    {!! Form::open(['route' => ['customerSessions.destroy', $customerSession->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('customerSessions.show', [$customerSession->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('customerSessions.edit', [$customerSession->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>