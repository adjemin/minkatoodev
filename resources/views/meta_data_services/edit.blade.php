@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
          <li class="breadcrumb-item">
             <a href="{!! route('metaDataServices.index') !!}">Meta Data Service</a>
          </li>
          <li class="breadcrumb-item active">Modifier</li>
        </ol>
    <div class="container-fluid">
         <div class="animated fadeIn">
             <div class="row">
                 <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              <i class="fa fa-edit fa-lg"></i>
                              <strong>Modifier Meta Data Service</strong>
                          </div>
                          <div class="card-body">
                              {!! Form::model($metaDataService, ['route' => ['metaDataServices.update', $metaDataService->id], 'method' => 'patch']) !!}

                              @include('meta_data_services.fields')

                              {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
         </div>
    </div>
@endsection