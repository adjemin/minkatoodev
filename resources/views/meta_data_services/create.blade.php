@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
         <a href="{!! route('metaDataServices.index') !!}">Meta Data Service</a>
      </li>
      <li class="breadcrumb-item active">Créer</li>
    </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-plus-square-o fa-lg"></i>
                                <strong>Create Meta Data Service</strong>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['route' => 'metaDataServices.store']) !!}

                                   @include('meta_data_services.fields')

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
           </div>
    </div>
@endsection
