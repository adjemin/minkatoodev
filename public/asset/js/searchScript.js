            var localisation = {};
            var urlPath = "https://"+window.location.hostname+"/";
            
            // alert(urlPath);
            navigator.geolocation.getCurrentPosition(function(position) {
                console.log(position.coords.latitude, position.coords.longitude);
                localisation.latitude = position.coords.latitude;
                localisation.longitude = position.coords.longitude;

                $.get(`/api/v1/search_offers_by_geofence?location_latitude=${position.coords.latitude}&location_longitude=${position.coords.longitude}`,function( data ) {
                    // alert('ok')
                    // alert( "Data Loaded: " + data );
                    // print(data)
                    console.log(data)
                    var res = data.data;
                    //Return resonse in console
                    console.log("Response : "+res)
                    var BigDiv ='';
                        
                        // ${value.id}
                    if( res != null ){
                        //Empty div which content this id
                            $("#dynamicHotel").html('');
                            $("#dynamicRestaurant").html('');
                            $("#dynamicResidence").html('');

                            $.each (res, function(index, value){
                            // console.log("recup Type : "+value.type)
                            //Show the type before to display
                            if(value.type =='hotel'){
                            console.log('Compte les hotels pour moi')
                                    BigDiv = `<div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                                    <div class="card">
                                        <a href="${urlPath}hotel-detail/${value.id}">
                                            <div class="card-img">
                                                <img src="${JSON.parse(value.meta_data).pictures[0]}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                                            </div>
                                        </a>
                                        <div class="card-content">
                                            <a href="${urlPath}hotel-detail/${value.id}">
                                                <h4 style="font-size: 15px; font-weight: bold;">${value.title}</h4>
                                            </a>
                                            <div class="rating">
                                                <!-- Stars -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                                <span>(15)</span>
                                            </div>
                                            <div class="card-footer-text">
                                                <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px;"> <!-- mappin icon -->
                                                    <span>
                                                        ${Math.floor(value.distance)} km
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;

                                //Display a the html
                                $('#dynamicHotel').append(BigDiv);
                                }else if(value.type == 'restaurant'){
                                    var BigDivRestaurant='';
                                    console.log('Compte les resto pour moi '+ value.id)
                                    BigDivRestaurant= `<div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                                    <div class="card">
                                        <a href="${urlPath}restaurant-detail/${value.id}">
                                            <div class="card-img">
                                                <img src="${JSON.parse(value.meta_data).pictures[0]}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                                            </div>
                                        </a>
                                        <div class="card-content">
                                            <a href="${urlPath}restaurant-detail/${value.id}">
                                                <h4 style="font-size: 15px; font-weight: bold;"> ${value.title} </h4>
                                            </a>
                                            <div class="rating">
                                                <!-- Stars -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                                <span>(15)</span>
                                            </div>
                                            <div class="card-footer-text">
                                                <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px;"> <!-- mappin icon -->
                                                    <span>
                                                        ${Math.floor(value.distance)} km
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;

                                    $('#dynamicRestaurant').append(BigDivRestaurant);
                                } 
                                // endelseif
                                
                                else if(value.type == 'residence'){
    /***********************************************Residence********************************************************/
                                        var BigDivResidence='';
                                        console.log('Compte les resto pour moi '+ value.id)
                                        BigDivResidence= `<div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                                        <div class="card">
                                <a href="${urlPath}residence-detail/${value.id}">
                                    <div class="card-img">
                                        <img src="${JSON.parse(value.meta_data).pictures[0]}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                                    </div>
                                </a>
                                            <div class="card-content">
                                                <a href="${urlPath}residence-detail/${value.id}">
                                                    <h4 style="font-size: 15px; font-weight: bold;"> ${value.title} </h4>
                                                </a>
                                                <div class="rating">
                                                    <!-- Stars -->
                                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                    <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                    <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                                    <span>(15)</span>
                                                    </div>
                                                <div class="card-footer-text">
                                                    <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px;"> <!-- mappin icon -->
                                                    <span>
                                                        ${Math.floor(value.distance)} km
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>`;
                                    $('#dynamicResidence').append(BigDivResidence);
                                    }
                            });
                            // endEach
                        } 
                        // endFirstCondition
        });
    })

            const search = document.getElementById("input_search");
            const matchList = document.getElementById("match-list")
            //Search Address to api
            const searchStates = async searchText =>{
                const response = await fetch(`https://nominatim.openstreetmap.org/search?q=${search.value}&countrycodes=CI&format=json&addressdetails=1&limit=100`);
                
                const states = await response.json();
                // console.log(states); 
                
                let matches = states.filter(state =>{
                    const regex= new RegExp(`^${searchText}`, 'gi');
                    // console.log(state.display_name);
                    return state.display_name.match(regex);
                });
                
                if(searchText.length === 0){
                    matches = [];
                    matchList.innerHTML = "";
                }
                
                console.log(matches);
                outputHtml(matches);
            };
            
            const outputHtml = matches =>{
                if(matches.length >0){
                    const html = matches.map(
                    match=>`
                    <div class="d-flex justify-content-center card card-body w-150" 
                    id='${match.place_id}' style='margin-top :-20px; cursor: pointer;' onmouseover="this.style.backgroundColor='#555';" onmouseout="this.style.backgroundColor='white';"  onclick="startSearch(this.id);" >
                            <h6>
                            <span class='text-primary'>${match.display_name}</span>
                            ${(match.address.country_code).toUpperCase()}  <br/>
                            <small>Lat : ${match.lat} /Lon :${match.lon} <small/>
                        </h6>
                    </div>`
                    ).join('');
                    
                    console.log(html)
                    matchList.innerHTML = html;
                }
            }          
            
            search.addEventListener('input', ()=>searchStates(search.value));
            var _search='';
            
/******************************************Start Search ****************************************************/
            function startSearch(clicked_id){
                // alert("ok")
                // alert(cliked_id);
                if(clicked_id !== null && typeof(clicked_id)=='undefined'){

                    autoVal = document.getElementById(clicked_id).textContent.trim();
                
                function stringToSlug(str) {
                    str = str.replace(/^\s+|\s+$/g, ''); // trim
                    str = str.toLowerCase();
                    // remove accents, swap ñ for n, etc
                    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
                    var to   = "aaaaeeeeiiiioooouuuunc------";
                    for (var i=0, l=from.length ; i<l ; i++) {
                        str = str.replace(new RegExp(from.charAt(i), 'gi'), to.charAt(i));
                    }
                    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                        .replace(/\s+/g, '') // collapse whitespace and replace by -
                        .replace(/-+/g, ''); // collapse dashes
                    return str;
                    // document.getElementById("slug").value = str;
                }
                // stringToSlug(_search)
 
                // alert(autoVal);
                search.value = autoVal;
                _search = stringToSlug(autoVal).substring(0,7);
                
                }else{
                    _search = search.value;
                }
                // alert(_search);
                
                // console.log();
                
                $.ajax({
                    method:'GET',
                    url:`/api/v1/search_offers/${_search}`,
                    dataType:"json",
                    beforeSend:function(){
                        $('.loader').show();
                    },
                    data:{
                        search:_search
                },
                    
                success: function(res){
                    // console.log(res.data.parent)
                    // alert(res);
                    var res = res.data.parent;
                    //Return resonse in console
                    // console.log(res)
                    var BigDiv ='';
                    // alert("alert")

                    if( res != null ){
                        //Empty div which content this id
                        $("#dynamicHotel").html('');
                        $("#dynamicRestaurant").html('');
                        $("#dynamicCar").html('');
                        $("#dynamicMusee").html('');
                        $("#dynamicExcursion").html('');
                        
                        $.each (res, function(index, value){
                            // console.log("recup Type : "+value.type)
                            //Show the type before to display
                            if(value.type =='hotel'){
 /***********************************************Hotel********************************************************/
                            console.log('Compte les hotels pour moi')
                                    BigDiv = `<div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                                    <div class="card">
                                        <a href='${urlPath}hotel-detail/${value.id}'>
                                            <div class="card-img">
                                                <img src="${JSON.parse(value.meta_data).pictures[0]}" alt="" class="img-fluid" style="width:585px; height: 215px;">
                                            </div>
                                        </a>
                                        <div class="card-content">
                                            <a href="${urlPath}hotel-detail/${value.id}">
                                                <h4 style="font-size: 15px; font-weight: bold;">
                                                    ${value.title} </h4>
                                            </a>
                                            <div class="rating">
                                                <!-- Stars -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                                <span>(15)</span>
                                            </div>
                                            <div class="card-footer-text">
                                                <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px;"> <!-- mappin icon -->
                                                    <span>
                                                        ${value.location_name}    
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;
                                    
                                    // if( document.location)

                                //Display a the html
                                // $('.loader').hide(3000);   
                                setTimeout(function() {
                                    $('.loader').fadeOut('slow');
                                }, 700); // <-- time in milliseconds                             // $('.loader').hide();
                                $('#dynamicHotel').append(BigDiv);
                                matchList.innerHTML = "";
                                
                                }else if(value.type == 'restaurant'){
  /***********************************************Restaurant********************************************************/
                                    
                                    var BigDivRestaurant=''
                                    console.log('Compte les resto pour moi '+ value.id)
                                    BigDivRestaurant= `<div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                                    <div class="card">
                                        <a href="${urlPath}restaurant-detail/${value.id}">
                                            <div class="card-img">
                                                <img src="${JSON.parse(value.meta_data).pictures[0]}" alt="" class="img-fluid" style="width:585px; height: 215px;">                                            </div>
                                        </a>
                                        <div class="card-content">
                                            <a href='${urlPath}restaurant-detail/${value.id}'>
                                                <h4 style="font-size: 15px; font-weight: bold;"> ${value.title} </h4>


                                            </a>
                                            <div class="rating">
                                                <!-- Stars -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                                <span>(15)</span>
                                               </div>
                                            <div class="card-footer-text">
                                                <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px;"> <!-- mappin icon -->
                                                    <span>
                                                        ${value.location_name}... 
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;
                                    
                                    setTimeout(function() {
                                        $('.loader').fadeOut('slow');
                                    }, 700); // <-- time in milliseconds
                                    // $('.loader').hide(3000);                                    // $('.loader').hide();
                                    $('#dynamicRestaurant').append(BigDivRestaurant);
                                    matchList.innerHTML = "";
                                    
                                } else if(value.type == 'residence'){
  /***********************************************Residence********************************************************/
                                    var BigDivResidence='';

                                    console.log('Compte les resto pour moi '+ value.id)
                                    BigDivResidence= `<div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                                    <div class="card">
                                        <a href='${urlPath}residence-detail/${value.id}'>
                                            <div class="card-img">
                                                <img src="${JSON.parse(value.meta_data).pictures[0]}" alt="" class="img-fluid" style="width:585px; height: 215px;">                                            
                                            </div>
                                        </a>
                                        <div class="card-content">
                                            <a href="${urlPath}residence-detail/${value.id}">
                                                <h4 style="font-size: 15px; font-weight: bold;"> ${value.title} </h4>
                                            </a>
                                            <div class="rating">
                                                <!-- Stars -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                                <span>(15)</span>
                                               </div>
                                            <div class="card-footer-text">
                                                <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px;"> <!-- mappin icon -->
                                                    <span>
                                                        ${value.location_name}... 
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;
                                    setTimeout(function() {
                                        $('.loader').fadeOut('slow');
                                    }, 700); // <-- time in milliseconds
                                    
                                    $('#dynamicResidence').append(BigDivResidence);
                                    matchList.innerHTML = "";
                                    
                                } else if(value.type == 'car'){
  /***********************************************car********************************************************/
                                    var BigDivCar='';

                                    console.log('Compte les resto pour moi '+ value.id)
                                    BigDivCar= `<div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                                    <div class="card">
                                        <a href='${urlPath}location-detail/${value.id}'>
                                            <div class="card-img">
                                                <img src="${JSON.parse(value.meta_data).pictures[0]}" alt="" class="img-fluid" style="width:585px; height: 215px;">                                            </div>
                                        </a>
                                        <div class="card-content">
                                            <a href="${urlPath}location-detail/${value.id}">
                                                <h4 style="font-size: 15px; font-weight: bold;"> ${value.title} </h4>


                                            </a>
                                            <div class="rating">
                                                <!-- Stars -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                                <span>(15)</span>
                                               </div>
                                            <div class="card-footer-text">
                                                <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px;"> <!-- mappin icon -->
                                                    <span>
                                                        ${value.location_name}... 
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`;
                                    setTimeout(function() {
                                        $('.loader').fadeOut('slow');
                                    }, 700); // <-- time in milliseconds
                                    
                                    $('#dynamicCar').append(BigDivCar);
                                    matchList.innerHTML = "";
                                }
                             // endelseif
                                    else if(value.type == 'musee'){
        /***********************************************musee********************************************************/
                                            var BigDivMusee='';
                                            // alert("ok musee")

                                            console.log('Compte les resto pour moi '+ value.id)
                                            BigDivMusee= `<div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                                            <div class="card">
                                                <a href='${urlPath}musee-detail/${value.id}'>
                                                    <div class="card-img">
                                <img src="${ JSON.parse(value.meta_data) !== null || JSON.parse(value.meta_data)!= "undefined" ? JSON.parse(value.meta_data).pictures[0] : "" }" alt="" class="img-fluid" style="width:585px; height: 215px;">                                            </div>
                                                </a>
                                                <div class="card-content">
                                                    <a href="${urlPath}musee-detail/${value.id}">
                                                        <h4 style="font-size: 15px; font-weight: bold;"> ${value.title} </h4>


                                                    </a>
                                                    <div class="rating">
                                                        <!-- Stars -->
                                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                        <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                                        <span>(15)</span>
                                                    </div>
                                                    <div class="card-footer-text">
                                                        <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px;"> <!-- mappin icon -->
                                                            <span>
                                                                ${value.location_name !== null ? value.location_name: ""}... 
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>`;
                                            setTimeout(function() {
                                                $('.loader').fadeOut('slow');
                                            }, 700); // <-- time in milliseconds
                                            
                                            $('#dynamicMusee').append(BigDivMusee);
                                            matchList.innerHTML = "";
                                        }else if(value.type == 'excursion'){
        /***********************************************excursion********************************************************/
                                            var BigDivExcursion='';
                                            // alert("ok excursion")

                                            console.log('Compte les resto pour moi '+ value.id)
                                            BigDivExcursion= `<div class="col-12 col-sm-6 col-md-4 col-lg-3 py-2">
                                            <div class="card">
                                                <a href='${urlPath}musee-detail/${value.id}'>
                                                    <div class="card-img">
                                <img src="${ JSON.parse(value.meta_data) !== null || JSON.parse(value.meta_data)!= "undefined" ? JSON.parse(value.meta_data).pictures[0] : "" }" alt="" class="img-fluid" style="width:585px; height: 215px;">                                            </div>
                                                </a>
                                                <div class="card-content">
                                                    <a href="${urlPath}musee-detail/${value.id}">
                                                        <h4 style="font-size: 15px; font-weight: bold;"> ${value.title} </h4>


                                                    </a>
                                                    <div class="rating">
                                                        <!-- Stars -->
                                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                        <img src="img/star-full.png" alt="" class=""> <!-- fullstar icons -->
                                                        <img src="img/star.png" alt=""> <!-- halfstar icon -->
                                                        <span>(15)</span>
                                                    </div>
                                                    <div class="card-footer-text">
                                                        <img src="img/placeholder-for-map@1X.png" alt="" style="height: 19px; width: 15px;"> <!-- mappin icon -->
                                                            <span>
                                                                ${value.location_name !== null ? value.location_name: "" }... 
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>`;
                                            setTimeout(function() {
                                                $('.loader').fadeOut('slow');
                                            }, 700); // <-- time in milliseconds
                                            
                                            $('#dynamicExcursion').append(BigDivExcursion);
                                            matchList.innerHTML = "";
                                        }/*else{
                                            alert('not result')
                                        }*/
                                    // endelseif
                                    }
                                );// endEach
                            // matchList.innerHTML = '';
                            
                        }else{
                            alert('rien dedans')
                        }// endFirstCondition                    
                    },
                    error:function(){
                        // alert('errorrrrrrrrrrrrrrrr')
                        matchList.innerHTML = ""
                        
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text:  "Pas d'offre disponible à l'adresse selectionnée !",
                        }),
                        
                        setTimeout(function() {
                            $('.loader').fadeOut('slow');
                        }, 700); // <-- time in milliseconds
                        matchList.html("")
                    }
                });   
            }
    /*********************************************************Search Script***************************************/

 