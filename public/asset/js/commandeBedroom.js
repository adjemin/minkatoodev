$(document).ready(function () {

    // *** FORM VALIDATION
    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;

    // All the infos about the form:
        // customer
        // delivery

    var $form_data = {};
    // All the infos about the customer
    var $customer = {};
    //** Delivery Tab Validation /
    var $delivery_data = {};
 

    $("#customer_tab_next_button").click(function () {
        // Validator
        var input_name = $("#input_name").val();
        var input_last_name = $("#input_last_name").val();
        var input_email = $("#input_email").val();
        var input_phone_number = $("#input_phone_number").val();
        var input_dial_code = $("#input_dial_code").val();
        var input_location_type = $("#input_location_type").val();
        // var input_company_name = $("#input_company_name").val();

        function raiseError(input_id){
            if (input_id == '') {
                input_id.addClass('error');
                alert("error")
            }
            alert("not empty")
        }

        if (input_name == '' || input_last_name == '' || input_email == '' || input_dial_code == '' || input_phone_number == '') {
            alert("champs vides");

            raiseError(input_name);
            raiseError(input_last_name);
            raiseError(input_email);
            raiseError(input_phone_number);

            return false;
        } else {
            // var input_data = [];
            $customer = {
                'name': input_name,
                'last_name': input_last_name,
                'email': input_email,
                'phone_number': input_phone_number,
                'dial_code': input_dial_code,
                'phone': input_dial_code + input_phone_number,
                'location_type': input_location_type,
                // 'company_name': input_company_name,
            }

            // Show next tab
            showNextTab("#customer_tab_next_button");
            console.log($customer);
            $form_data['customer'] = $customer;
            console.log($form_data);

        }
    });
  

    var $current_car_slug = $("#select_name_car").val();
    var $current_price_car = $("#select_price_car").val();
    // $current_clothe_price = "";
    // Total of all the clothes services
    $review_total_amount = 0;
    // $review_total_amount = parseInt($("#table_review_total_price"));
    // $review_total_amount_html = $("#table_review_total_price").html();
    $display_amount = $("#display_amount");


    // Rendering the selected_services as a disposable array in the htmls
    $services_table_review_body = $("#table_review_body");



    // calculate price
    $("#input_number_day", "#val_p").on("change", function () {
        // $clothe_slug = $("#input_clothes_typehead").val();
        $price_slug = $current_price_car;
        amount = 0;
        $quantity = $("#input_number_day").val();
        // $price_pattern =
        if ($price_slug != null) {
            amount = $current_price_car * $quantity;
            $display_amount.html(amount);
        }
    });
    // print current_clothe_price when typeahead is changed
    // $("#input_clothes_typehead").on("change", function () {
    //     alert("changed ! ")
    // });

    // **
    $("#add_clothers").click(function () {
        // Service
        // var service = $current_service_slug;
        var service = $("#select_name_car").val();
        // Quantity
        var quantity = parseInt($("#input_number_day").val());
        // Slug
        var price_slug = $("#select_price_car").val();
        // Price
        // Amount
        var amount = parseInt($("#display_amount").html());
        // check if set
        if (service == null || quantity == null || price_slug == null || amount == 0) {
            alert("Champs vides !");
            // TODO add fields error validation
            return false;
        }

        // Checks for service duplicate
        // search for duplicate service slug
        // function checkDuplicateServiceSlug(services) {
        //     for (var t = 0; t < services.length; t++){
        //         if (services[t]['type'] == service) {
        //             return t;
        //         }
        //     }
        //     return -1;
        // }
        // // search for duplicate Cars
        function checkDuplicateCarSlug(Cars, current_Car) {
            for (var t = 0; t < Cars.length; t++) {
                if (Cars[t]['slug'] == current_Car) {
                    return t;
                }
            }
            return -1;
        }
        // adds a new service with initial Car
        function addNewServiceWithInitialCar(){
            $selected_services.push({
                'type': service,
                'car': [{
                    'slug': $current_car_slug,
                    'prix': $current_price_car,
                    'quantite': quantity,
                    'montant': $current_price_car * quantity
                }],
                'montant_total': amount
            });
        }
        // add a new Car to a service
        function addNewCarToService(service_index) {

            $selected_services[service_index]['cars'].push({
                'slug': $current_car_slug,
                'prix': $current_price_car,
                'quantite': quantity,
                'montant': $current_price_car * quantity
            });
            // update total_amount of service
            $selected_services[service_index]['montant_total'] += $current_price_car * quantity;
        }
        // increments Car quantity and recalculate amount of specified service Car
        function incrementQuantityAndRecalculateAmount(service_index, clothe_index) {
            // quantity += 1;
            // $display_amount.html(amount);
            $selected_services[service_index]['cars'][clothe_index]['quantite'] += quantity;
            amount = $current_price_car * $selected_services[service_index]['cars'][clothe_index]['quantite'];
            $selected_services[service_index]['cars'][clothe_index]['montant'] = amount;
            $selected_services[service_index]['montant_total'] += amount;

        }
        // *** Deletion functions
        // TODO !!!
        function deleteCar(service_index, clothe_index) {
            alert("delete " + clothe_index);
        }



        // If there is already a selected service entry
        if ($selected_services > 0) {

            // Check for duplicate service
            var duplicate_service_index = checkDuplicateServiceSlug($selected_services);
            // check if the new service slug doesnt already exists
            if (duplicate_service_index != -1) {
                // If duplicate service is found
                // Check for duplicate Car
                if ($selected_services[duplicate_service_index]['cars'] > 0) {
                    var duplicate_clothe_index = checkDuplicateCarSlug($selected_services[duplicate_service_index]['cars'], price_slug);

                    // if duplicate Car is found
                    if (duplicate_clothe_index != -1) {
                        // alert("duplicate at : " + duplicate_clothe_index);
                        incrementQuantityAndRecalculateAmount(duplicate_service_index, duplicate_clothe_index);
                    } else {
                        // else if duplicate is not found : add a new
                        addNewCarToService(duplicate_service_index);
                    }
                } else {
                    // Else if there's no clothes at all
                    addNewCarToService(duplicate_service_index);
                }
            } else {
                // If he doesnt find any duplicate
                addNewServiceWithInitialCar();
            }
            // Display in the html
            // If the services has 0 record then add anew
        } else {
            addNewServiceWithInitialCar();
        }
        // console.log($selected_services);


        // $services_table_review_body.html("<>")
        // For each selected service a table row like this
            // $tr_services (
            // <tr>
            //     <td rowspan="2">Repassage</td>

            //     <td>Veste</td>
            //     <td>qty</td>
            //     <td>prix</td>
            //     <td>amount</td>
            //     <td>action</td>
            // </tr> )
            // $tr_Cars (
            //     <tr>
            //         <td>Veste</td>
            //         <td>qty</td>
            //         <td>prix</td>
            //         <td>amount</td>
            //         <td>action</td>
            //     </tr> )
            //

        $tr_services = "";
        $tr_car = "";
        console.log($selected_services)
        for (var s = 0; s < $selected_services.length; s++) {
            $tr_services += `
            <tr>
                <td rowspan="${$selected_services[s]['car']}">
                    ${$selected_services[s]['type']}
                </td>
                <td>
                    ${$selected_services[s]['car'][0]['prix']}
                </td>
                <td>
                    ${$selected_services[s]['car'][0]['quantite']}
                </td>
                <td>
                    ${$selected_services[s]['car'][0]['quantite'] * $selected_services[s]['car'][0]['prix']}
                </td>
                <td>
                    <i class="fa fa-trash text-danger" onClick="deleteCar(${s},0);">

                    </i>
                </td>
            </tr>`;
            
            // console.log("service : " + $selected_services[s]['type'] + " nb v : " + $selected_services[s]['Cars'].length);
            // Cars list
            // console.log($selected_services[s]['Cars']);

            cars = $selected_services[s]['car'];
            if (cars.order_review_total_amount > 1) {
                for (var v = 1; v < cars.length; v++){
                    $tr_cars = `
                        <tr>
                            <td>
                                ${cars[v]['prix']}
                            </td>
                            <td>
                                ${cars[v]['quantite']}
                            </td>
                            <td>
                                ${cars[v]['quantite'] * cars[v]['prix']}
                            </td>
                            <td>
                                <i class="fa fa-trash text-danger"></i>
                            </td>
                        </tr>`;
                        $tr_services += $tr_cars;
                }
            }
            // Making a separation
            $tr_services += "<!-- Fin type de service -->";
            $review_total_amount = $selected_services[s]['montant_total'];
            $services_table_review_body.html($tr_services);
            // reset fields
                // Service
                // Quantity
                $("#quantity_number_day").val(0);
                // Price
                $("#display_amount").html(0);
        }
        // print selected_services
        console.log($selected_services);

        $form_data['services'] = $selected_services;
        console.log($form_data);

        // Display the total amount at the bottom of the review table
        $("#table_review_total_amount").html($review_total_amount);
        // console.log($selected_services)
        // $("#console").html($selected_services);

    });

    // Validation
    $("#services_tab_next_button").click(function () {
        // Validator
        //
        var v = $("#table_review_total_amount").text();

        // TODO uncomment !!!!!!!!
        // TODO uncomment !!!!!!!!
        // TODO uncomment !!!!!!!!
        // TODO uncomment !!!!!!!!
        if (v == '0') {
            alert("Aucun service sélectionné");
            return false;
        } else {
            // Show next tab
            showNextTab("#services_tab_next_button");
        }
    });




    //
    // var price = $("#input_price");
    $("#delivery_tab_next_button").click(function () {
        // alert(ok)

        // var formula = $("#select_formula").val();
        // var agency_name = $("#select_business").val();
        // //
        // var pickup_address = $("#locat_pickup").val();
        // var pickup_lat = $("#lat_pickup").val();
        // var pickup_lon = $("#lon_pickup").val();
        // var pickup_date = $("#pickup_date").val();
        // var pickup_time = $("#pickup_time").val();
        //
        var delivery_address = $("#locat_delivery").val();
        var delivery_lat = $("#lat_delivery").val();
        var delivery_lon = $("#lon_delivery").val();
        var delivery_date = $("#delivery_date").val();
        var delivery_time = $("#delivery_time").val();
        var delivery_date_bed = $("#delivery_date_bed").val();
        var delivery_time_bed = $("#delivery_time_bed").val();
        //

        if ( delivery_address == '' || delivery_date == '' || delivery_time == '' || delivery_date_bed == '' || delivery_time_bed == '') {
            //
            alert("champs requis");
            return false;
        } else {
            // set up delivery data
            $delivery_data = {
                // 'formula': formula,
                // 'agency_name': agency_name,
                'delivery_address': delivery_address,
                'delivery_lat': delivery_lat,
                'delivery_lon': delivery_lon,
                // 'delivery_place': ,
                // 'delivery_town': ,
                'delivery_date': delivery_date,
                'delivery_time': delivery_time,
                'delivery_date_bed': delivery_date_bed,
                'delivery_time_bed': delivery_time_bed,
                //
                // 'pickup_address': pickup_address,
                // 'pickup_lat': pickup_lat,
                // 'pickup_lon': pickup_lon,
                // 'pickup_place': ,
                // 'pickup_town': ,
                // 'pickup_date': pickup_date,
                // 'pickup_time': pickup_time,
                // 'price': ,
            }
            console.log($delivery_data);

            $form_data['delivery'] = $delivery_data;
            console.log($form_data);

            // *** Populate the customer review section with info from the form_info
            $("#display_name").html($form_data['customer']['name']);
            $("#display_last_name").html($form_data['customer']['last_name']);
            $("#display_email").html($form_data['customer']['email']);
            $("#display_phone").html($form_data['customer']['dial_code'] + $form_data['customer']['phone_number']);
            $("#display_location_type").html($form_data['customer']['location_type']);
            // $("#display_company_name").html($form_data['customer']['company_name']);

            // *** Populate the order review table with all the form_info
            // $("#order_review_table").
            populateOrderReviewTable();

            function populateOrderReviewTable() {
                $order_review_table_body = $("#order_review_body");

                $tr_services = "";
                $tr_car = "";
                $ss = $form_data['services'];
                for (var s = 0; s < $ss.length; s++) {
                    $tr_services += `<tr>
                    <td rowspan="${$ss[s]['car']}">
                        ${$ss[s]['type']}
                    </td>
                    <td>
                        ${$ss[s]['car'][0]['prix']}
                    </td>
                    <td>
                        ${$ss[s]['car'][0]['quantite']}
                    </td>
                    <td>
                        ${$ss[s]['car'][0]['quantite'] * $ss[s]['car'][0]['prix']}
                    </td>
                </tr>`;
                    // console.log("service : " + $selected_services[s]['type'] + " nb v : " + $selected_services[s]['Cars'].length);
                    // Cars list
                    // console.log($selected_services[s]['Cars']);

                    sv = $ss[s]['cars'];
                    if (s > 1) {
                        for (var v = 1; v < sv.length; v++) {
                            $tr_cars = `
                            <tr>
                                <td>
                                    ${sv[v]['prix']}
                                </td>
                                <td>
                                    ${sv[v]['quantite']}
                                </td>
                                <td>
                                    ${sv[v]['quantite'] * sv[v]['prix']}
                                </td>
                            </tr>`;
                            $tr_services += $tr_cars;
                        }
                    }
                    // Making a separation
                    $tr_services += "<!-- Fin type de service -->";
                    $review_total_amount = $ss[s]['montant_total'];
                    $order_review_table_body.html($tr_services);
                }


                // Display the total amount at the bottom of the review table
                $("#order_review_total_amount").html($review_total_amount);
            }

            // *** Populate the delivery review section

            // $("#display_formula").html($form_data['delivery']['formula']);
            // $("#display_agency").html($form_data['delivery']['agency_name']);

            // $("#display_pickup_adress").html($form_data['delivery']['pickup_address']);
            // $("#display_pickup_date").html($form_data['delivery']['pickup_date']);
            // $("#display_pickup_time").html($form_data['delivery']['pickup_time']);

            $("#display_delivery_adress").html($form_data['delivery']['delivery_address']);
            $("#display_delivery_date").html($form_data['delivery']['delivery_date']);
            $("#display_delivery_time").html($form_data['delivery']['delivery_time']);
            $("#display_delivery_date_bed").html($form_data['delivery']['delivery_date_bed']);
            $("#display_delivery_time_bed").html($form_data['delivery']['delivery_time_bed']);
        // console.log($selected_services)
            //
            showNextTab("#delivery_tab_next_button");
        }
    });

    // *** End of Validation
    // Sending the $form_data to the server
    $("#order_tab_next_button").click(function () {

        sendData($form_data);

        function sendData(data) {
            var _token = $('#csrf').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': _token
                }
            });

            $.ajax({
                url: "/orders/storeAjax",
                method: "POST",
                data: data
            }).done((response, textStatus, jqXHR) => {
                console.log("success");
                console.log(response);
                // to next page
                showNextTab("#order_tab_next_button");

            }).fail((response) => {
                alert("Erreur, veuillez reprendre");
                console.log("failed, here's why :")
                console.log(response);
            });
        }
        //
    });


        //
        // *** Tab navigation functions
        // Shows next tab
    function showNextTab(currentButton) {
            // current_fs = $(this).parent();
            // next_fs = $(this).parent().next();
            current_fs = $(currentButton).parent();
            next_fs = $(currentButton).parent().next();

            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function (now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                next_fs.css({
                    'opacity': opacity
                });
            },
            duration: 600
        });
    }
    $(".next").click(function () {
        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //Add Class Active
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function (now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                next_fs.css({
                    'opacity': opacity
                });
            },
            duration: 600
        });
    });

    $(".previous").click(function () {

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();

        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function (now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                previous_fs.css({
                    'opacity': opacity
                });
            },
            duration: 600
        });
    });

    $('.radio-group .radio').click(function () {
        $(this).parent().find('.radio').removeClass('selected');
        $(this).addClass('selected');
    });

    $(".submit").click(function () {
        return false;
    })

    // $("#bedroom"){
// }

                                                                                                      //When User choose end Date
                                                                                                                                             $("#delivery_date_bed").change(
        // $("#add_bedroom").click(                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
        function(){
        // alert('ok')
        var bedroom_price = $("#bedroom_price").val();
        var delivery_date = $("#delivery_date").val();
        var bedroom_price = $("#bedroom_price").val();

        var delivery_date_bed = $("#delivery_date_bed").val();

        var dateDebut = new Date(delivery_date);
        var dateFin = new Date(delivery_date_bed);
        var dateDiff =dateFin.getTime() - dateDebut.getTime();
        var date = dateDiff/(1000 * 3600 * 24);
        var amount =date*bedroom_price;

        $display_hotel_amount = $('#display_hotel_amount');

        $display_hotel_amount.html(amount);
            console.log(amount)
        } 
    );

    $('#delivery_tab_next_button').on('click', function(){
        alert('ok')
    })

});
