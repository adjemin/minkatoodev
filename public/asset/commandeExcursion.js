$('document').ready(function () {

    var bedroom_price;
    var delivery_date;
    var bedroom_price;
    var delivery_address;
    var delivery_lat;
    var delivery_lon;
    var delivery_date;
    var delivery_time;
    var delivery_date_bed;
    var delivery_time_bed;
    var resume;
    var bedroom;
    var amount;
    var date;
    var table_review = '';
    var bedroom_id;

    var number__inc;
    var number_people_de;
    var val_p;
    var val_en;
    var $form_data = {};
    var $delivery_data;
    var total_amount;

    var dateDebut
    var dateFin

    //When end date is choose
    $("#delivery_date_bed").change(
        // alert('ok')
        // $("#add_bedroom").click(
        function () {
            bedroom_price = $("#bedroom_price").val();
            delivery_date_bed = $("#delivery_date_bed").val();
            val_p = $('#val_p').val();
            val_en = $('#val_en').val();
            delivery_date = $("#delivery_date").val();

            console.log(val_p)
            console.log(val_en)


            dateDebut = new Date(delivery_date);
            dateFin = new Date(delivery_date_bed);

            var dateDiff = dateFin.getTime() - dateDebut.getTime();
            date = Math.floor(dateDiff / (1000 * 3600 * 24)) + 1;// Calcule nombre de
            // date = Math.round(dateDiff / (1000 * 3600 * 24));
            var amount = date * bedroom_price;

            total_amount = amount * val_p + (amount/2) * val_en;

            // Tax
            var tax = total_amount * 0.18;
            _amount =total_amount + tax;

            console.log("Montant" + amount )
            console.log("Montant total" + total_amount )


            $display_hotel_amount = $('#display_hotel_amount');

            $display_hotel_amount.html(_amount);
            bedroom = $("#bedroom").val();
            bedroom_id = parseInt($("#bedroom_id").val());
            console.log(amount)
            console.log(total_amount)

            //Resume
            resume = {
                'display_montant':total_amount,
                'bedroom': bedroom,
                'date': date,
                'bedroom_price': bedroom_price,
                'val_p': val_p,
                'val_en': val_en,
                'bedroom_id':bedroom_id,
                'dateDebut':dateDebut,
                'dateFin':dateFin
            }

            $form_data['resume'] = resume;

            console.log(resume)

            //Reservation informations display
            table_review =
            ` <tr>
                <td>${resume.bedroom}</td>
                <td>${resume.bedroom_price}</td>
                <td>${ isNaN(resume.val_p) ? "": resume.val_p}</td>
                <td>${ isNaN(resume.val_en) ? "": resume.val_en}</td>
                <td>${resume.display_montant}</td>
            </tr>
            `;

            if(dateDebut == dateFin ){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Date Invalide, veuillez rentrez des date correctes !',
                })

                $("#delivery_date_bed").val("");
                $("#delivery_date").val("");
                $("#val").val("");
                return false;

            }else if(dateDebut < new Date() ){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Date Invalide, impossible de reserver a une date passée !',
                })

                $("#delivery_date_bed").val("");
                $("#delivery_date").val("");
                $("#val_p").val("");
            }else if(dateDebut.getDate() == dateFin.getDate() && dateDebut.getHours() == dateDebut.getHours ){
                // alert('Error same day')
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Date Invalide, la date de debut et la date de fin sont identiques !',
                })

                $("#delivery_date_bed").val("");
                $("#delivery_date").val("");
                $("#val_p").val("");

            }else if(val_p == null || val_p==""){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Nombre de personne Invalide ou non défini !',
                })

                $("#delivery_date_bed").val("");
                $("#delivery_date").val("");
                $("#val_p").val("");
            }else{
                $('#table_review_body').html(table_review);
                $('#table_review_total_amount').html(resume.display_montant)
                $('#display_val_p').html(resume.val_p)
                $('#display_val_en').html(resume.val_en)
            }



            //Displaying
            // if(dateDebut === dateFin  || dateDebut < dateFin || dateDebut < now() ){
            //     $('#table_review_body').html(table_review);
            //     $('#table_review_total_amount').html(resume.display_montant)
            //     $('#display_val_p').html(resume.val_p)
            //     $('#display_val_en').html(resume.val_en)
            // }else{
            //     alert('Date error')
            //     return false
            // }


    });


    $('#delivery_tab_next_button').on('click', function () {
        // alert('ok')
        var delivery_address = $("#locat_delivery").val();
        var delivery_lat = $("#lat_delivery").val();
        var delivery_lon = $("#lon_delivery").val();
        var delivery_date = $("#delivery_date").val();
        var delivery_time = $("#delivery_time").val();
        var delivery_date_bed = $("#delivery_date_bed").val();
        var delivery_time_bed = $("#delivery_time_bed").val();

        $delivery_data = {
            'delivery_address': delivery_address,
            'delivery_lat': delivery_lat,
            'delivery_lon': delivery_lon,
            'delivery_date': delivery_date,
            'delivery_time': delivery_time,
            'delivery_date_bed': delivery_date_bed,
            'delivery_time_bed': delivery_time_bed,
        }

        $form_data['delivery_data'] = $delivery_data;
        console.log($form_data);

        // $("#display_val_p").append(resume.val_p);
        console.log($delivery_data.delivery_date);

        showNextTab("#delivery_tab_next_button");
        // showNextTab("#order_tab_next_button");

        function showNextTab(currentButton) {
            // current_fs = $(this).parent();
            // next_fs = $(this).parent().next();
            current_fs = $(currentButton).parent();
            next_fs = $(currentButton).parent().next();
            //show the next fieldset


            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({
                opacity: 0
            },
                {
                    step: function (now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        next_fs.css({
                            'opacity': opacity
                        });
                    },
                    duration: 600
                });
        }

        $(".next").click(function () {
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({
                opacity: 0
            }, {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({
                        'opacity': opacity
                    });
                },
                duration: 600
            });
        });

        $("#display_delivery_date").html($delivery_data.delivery_date);
        $("#display_delivery_date_bed").append($delivery_data.delivery_date_bed);
        // var display_montant = $("#display_hotel_amount").text();
        amount = $("#display_hotel_amount").text();


        // resume = {
        //     'display_montant': amount,
        //     'bedroom': bedroom,
        //     'date': date,
        //     'bedroom_price': bedroom_price,
        //     'bedroom_id': bedroom_id
        // }



        console.log(resume)

        $("#order_review_total_amount").html(resume.display_montant);
        $('#bedroom_name').html(resume.bedroom);
        $('#number_day').html(resume.date);
        $('#bedroom_unity_price').html(resume.bedroom_price);
        $('#total_bed_amount').html(resume.display_montant);
    });



    $("#order_tab_next_button").click(function () {
        // alert("Excursion !")

        function showNextTab(currentButton) {
            // current_fs = $(this).parent();
            // next_fs = $(this).parent().next();
            current_fs = $(currentButton).parent();
            next_fs = $(currentButton).parent().next();
            //show the next fieldset


            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({
                opacity: 0
            },
            {
                step: function (now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({
                        'opacity': opacity
                    });
                },
                duration: 600
            }
            );
        }


        var input_name = $("#input_name").text();
        var input_last_name = $("#input_last_name").text();
        var input_email = $("#input_email").text();
        var input_phone_number = $("#input_phone_number").text();
        var input_dial_code = $("#input_dial_code").text();
        var input_location_type = $("#input_location_type").text();
        var customer_id = $("#customer_id").text();


        //  Customer validation and recuparation of variable
        $customer = {
            'name': input_name,
            'last_name': input_last_name,
            'email': input_email,
            'phone_number': input_phone_number,
            'dial_code': input_dial_code,
            'phone': input_dial_code + input_phone_number,
            'location_type': input_location_type,
            'customer_id': customer_id,
            // 'company_name': input_company_name,
        }

        //console.log($customer);

        $form_data['customer'] = $customer;



        // $delivery_data = {
        //     'delivery_address': delivery_address,
        //     'delivery_lat': delivery_lat,
        //     'delivery_lon': delivery_lon,
        //     'delivery_date': delivery_date,
        //     'delivery_time': delivery_time,
        //     'delivery_date_bed': delivery_date_bed,
        //     'delivery_time_bed': delivery_time_bed,
        // }


        // $form_data['delivery_data'] = $delivery_data;

        console.log($form_data);

        sendData($form_data);

        function sendData(data) {
            var _token = $('#csrf').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': _token
                }
            });

            $.ajax({
                url: "/orders/storeAjax",
                method: "POST",
                data: data,
            }).done((response, textStatus, jqXHR) => {
                // alert('on Ajax Excursion !')

                console.log("success");
                console.log(response);

                if(response.code == 11){
                    window.location.assign('/adjeminPay/'+response.data);
                    // window.location.assign('/cinetPay/'+response.data);
                }

                // to next page
                showNextTab("#order_tab_next_button");

            }).fail((response) => {
                alert("Erreur, veuillez reprendre");
                console.log("failed, here's why :")
                console.log(response);
            });
        }

    });


    //next
    $(".next").click(function () {
        current_fs = $(this).parent();
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        next_fs = $(this).parent().next();        //Add Class Active

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function (now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                next_fs.css({
                    'opacity': opacity
                });
            },
            duration: 600
        });
    });

    //Previous fieldset
    $(".previous").click(function () {

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();

        //hide the current fieldset with style
        current_fs.animate({
            opacity: 0
        }, {
            step: function (now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                previous_fs.css({
                    'opacity': opacity
                });
            },
            duration: 600
        });
    });

});


//
