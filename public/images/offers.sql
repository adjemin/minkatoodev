-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 24, 2020 at 06:26 AM
-- Server version: 10.3.24-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qznotdwx_minkatoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_source_id` bigint(20) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  `publisher_id` bigint(20) DEFAULT NULL,
  `publisher_name` bigint(20) DEFAULT NULL,
  `has_children` tinyint(1) DEFAULT 0,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `etablissement` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specification` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_price` tinyint(1) DEFAULT 0,
  `rating` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_lng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_gps` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slide_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slide_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_escort` tinyint(1) DEFAULT NULL,
  `escort_fees` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `parent_source_id`, `title`, `meta_data`, `type`, `owner_id`, `publisher_id`, `publisher_name`, `has_children`, `description`, `picture1`, `picture2`, `picture3`, `picture4`, `phone_number`, `website`, `room`, `etablissement`, `specification`, `original_price`, `price`, `currency_code`, `has_price`, `rating`, `location_name`, `location_lat`, `location_lng`, `location_gps`, `slide_url`, `slide_title`, `is_escort`, `escort_fees`, `created_at`, `updated_at`, `deleted_at`) VALUES
(85, 0, 'Restaurant Le Saakan', '{\"food_specialities\":[\"cusisine_africaine\",\"cuisine-orientale\"],\"pictures\":[\"https://i.imgur.com/wjrptXx.jpg\",\"https://i.imgur.com/e1eOd7V.jpg\",\"https://i.imgur.com/Zmd3ux2.jpg\",\"https://i.imgur.com/TQC0EM7.jpg\"]}', 'restaurant', 0, 20, NULL, 0, NULL, 'https://i.imgur.com/wjrptXx.jpg', 'https://i.imgur.com/e1eOd7V.jpg', 'https://i.imgur.com/Zmd3ux2.jpg', 'https://i.imgur.com/TQC0EM7.jpg', '08453194', 'https://cap.org', ',,', NULL, NULL, NULL, NULL, 'XOF', 0, NULL, 'Restaurant Saakan, Avenue Chardy, Indénié, Le Plateau, Abidjan, 01 BP 10605 ABIDJAN 01, Côte d\'Ivoire', '5.32360330', '-4.01653780', '5.32360330,-4.01653780', 'https://i.imgur.com/RFQ9vBf.jpg', NULL, NULL, NULL, '2020-09-21 14:02:58', '2020-09-21 14:02:58', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
