
INSERT INTO `customers` (`id`, `first_name`, `last_name`, `name`, `phone_number`, `phone`, `dial_code`, `country_code`, `email`, `photo_url`, `gender`, `bio`, `birthday`, `language`, `customer_type`, `otp`, `otp_verified`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mohamed', 'Soro', 'Mohamed Soro', '87015892', '22587015892', '225', 'CI', 'mohamedsoro@adjemin.com', NULL, 'HOMME', NULL, NULL, NULL, 'client', '123456', 1, '2020-02-13 18:31:27', '2020-02-17 15:39:25', NULL),
(2, 'Mohamed', 'Soro', 'Mohamed Soro', '77925078', '22577925078', '225', 'CI', 'mohamedsoro@adjemin.com', NULL, 'HOMME', NULL, NULL, NULL, 'client', '067221', 1, '2020-02-17 15:00:53', '2020-02-17 15:38:32', NULL),
(7, 'Abd', 'Simpson', 'Abd Simpson', '01020304', '22501020304', '225', 'CI', NULL, NULL, 'HOMME', NULL, NULL, NULL, 'client', '857019', 0, '2020-02-18 18:04:39', '2020-02-18 18:04:39', NULL),
(6, 'Frank', 'Brou', 'Frank Brou', '44908473', '22544908473', '225', 'CI', NULL, NULL, 'HOMME', NULL, NULL, NULL, 'client', NULL, 0, '2020-02-18 17:41:27', '2020-02-18 17:41:27', NULL),
(12, 'Ange', 'Bagui', 'Ange Bagui', '56888385', '22556888385', '225', 'CI', 'angebagui@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-07 00:20:24', '2020-03-07 00:20:24', NULL),
(13, 'Dikado', 'Ban', 'Dikado Ban', '08175784', '22508175784', '225', 'CI', 'dikadoban@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-07 21:31:48', '2020-03-07 21:31:48', NULL),
(14, 'Nicole', 'Yao', 'Nicole Yao', '77385080', '22577385080', '225', 'CI', 'nicole.adjoua@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-07 22:45:51', '2020-03-07 22:45:51', NULL),
(15, 'Casimir', 'Daple', 'Casimir Daple', '47694913', '22547694913', '225', 'CI', 'daplecasimir1970@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-08 17:15:15', '2020-03-08 17:15:15', NULL),
(16, 'Gba', 'Sinde franck', 'Gba Sinde franck', '59510446', '22559510446', '225', 'CI', 'francksinde@minkatoo.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-09 11:41:17', '2020-03-16 17:30:56', '2020-03-16 17:30:56'),
(17, 'Blandine', 'Bamba', 'Blandine Bamba', NULL, '22558125834', NULL, NULL, 'blandine.bamba74@gmail.com', NULL, 'FEMME', NULL, NULL, NULL, 'client', NULL, 0, '2020-03-13 19:10:48', '2020-03-16 15:10:48', NULL),
(18, 'Franck', 'Gba', 'Franck Gba', '44849587', '22544849587', '225', 'CI', 'francksinde@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-16 13:51:29', '2020-03-16 17:30:33', '2020-03-16 17:30:33'),
(19, 'Franck sinde', 'Gba', 'Franck sinde Gba', NULL, '59510446', NULL, NULL, 'francksinde@minkatoo.com', 'WIN_20170221_17_47_48_Pro.jpg', 'HOMME', NULL, NULL, 'Français', 'client', '383119', 0, '2020-03-16 17:48:52', '2020-03-16 18:21:40', '2020-03-16 18:21:40'),
(20, 'Franck sinde', 'Gba', 'Franck sinde Gba', NULL, '225 44849587', NULL, NULL, 'francksinde@minkatoo.com', 'WIN_20170221_17_47_48_Pro.jpg', 'HOMME', NULL, NULL, NULL, 'client', '511314', 0, '2020-03-17 11:32:02', '2020-05-25 18:17:11', NULL),
(21, 'Too', 'Taa', 'Too Taa', '84400312', '22584400312', '225', 'CI', 'angebagui@adjemin.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-05-29 22:11:33', '2020-05-29 22:11:33', NULL);

-- --------------------------------------------------------



INSERT INTO `customer_session` (`id`, `token`, `customer_id`, `is_active`, `location_address`, `location_latitude`, `location_longitude`, `battery`, `version`, `device`, `ip_address`, `network`, `isMobile`, `isTesting`, `deviceId`, `devicesOSVersion`, `devicesName`, `w`, `h`, `ms`, `idapp`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-02-17 09:56:55', '2020-02-17 09:56:55', NULL),
(2, NULL, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-02-28 01:03:28', '2020-02-28 01:03:28', NULL),
(3, NULL, 9, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 10:20:25', '2020-03-04 10:20:25', NULL),
(4, NULL, 10, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 22:11:10', '2020-03-04 22:11:10', NULL),
(5, NULL, 11, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 22:49:31', '2020-03-04 22:49:31', NULL),
(6, NULL, 12, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-07 00:20:24', '2020-03-07 00:20:24', NULL),
(7, NULL, 13, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-07 21:31:48', '2020-03-07 21:31:48', NULL),
(8, NULL, 14, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-07 22:45:51', '2020-03-07 22:45:51', NULL),
(9, NULL, 15, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-08 17:15:15', '2020-03-08 17:15:15', NULL),
(10, NULL, 16, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-09 11:41:17', '2020-03-09 11:41:17', NULL),
(11, NULL, 17, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-13 19:10:48', '2020-03-13 19:10:48', NULL),
(12, NULL, 18, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-16 13:51:29', '2020-03-16 13:51:29', NULL),
(13, NULL, 21, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-29 22:11:33', '2020-05-29 22:11:33', NULL);

-- --------------------------------------------------------



INSERT INTO `excursion_activities` (`
id`,
`image
`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'https://i.imgur.com/RbrqUS8.png', 'Excursions et visites', 'excursions_et_visites', '2020-03-04 08:25:11', '2020-03-04 13:23:41', NULL),
(2, 'https://i.imgur.com/DPHmpcn.png', 'Visites Guidées', 'visites_guidées', '2020-03-04 08:25:11', '2020-03-04 13:23:41', NULL),
(3, 'https://i.imgur.com/a7NZR6Q.png', 'Sites historiques', 'sites_historiques', '2020-03-04 08:26:01', '2020-03-04 13:25:15', NULL),
(4, 'https://i.imgur.com/ssKvbyq.png', 'Randonnées', 'randonnees', '2020-03-04 08:26:01', '2020-03-04 13:25:15', NULL);



INSERT INTO `food_specialties` (`
id`,
`image
`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'https://i.imgur.com/mJY3XGH.png', 'Cuisine Africaine', 'cusisine_africaine', '2020-03-04 08:20:35', '2020-03-04 13:19:22', NULL),
(2, 'https://i.imgur.com/FvE5oqJ.png', 'Cuisine Asiatique', 'cuisine_asiatique', '2020-03-04 08:20:35', '2020-03-04 13:19:22', NULL),
(3, 'https://i.imgur.com/ocki9Mb.png', 'Cuisine Francaise', 'cuisine-francaise', '2020-03-04 10:46:23', '2020-03-04 15:44:50', NULL),
(4, 'https://i.imgur.com/YJrzF3E.png', 'Cuisine Orientale', 'cuisine-orientale', '2020-03-04 10:46:23', '2020-03-04 15:44:50', NULL);



INSERT INTO `invoices` (`
id`,
`order_id`,
`customer_id`,
`reference`,
`link`,
`subtotal`,
`tax`,
`fees_delivery
`, `total`, `status`, `is_paid_by_customer`, `is_paid_by_delivery_service`, `currency_code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 1, 'BOOKING-1-6-1-1583597215', '#', '100', '18', NULL, '118', 'unpaid', 0, 0, 'XOF', '2020-03-07 21:06:55', '2020-03-07 21:06:55', NULL);
