<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('locations')->insert([


            [
                'title'=> "Hyundai 4x",
                'description'=> "Voiture familiale Hyundai",
                'location_name' => "Cocody, Abidjan",
                'slider' => "location_slide1.png",
                'location_map' => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.568345380635!2d-3.9750283493462137!3d5.329812137345493!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc1ec65e37c61ff%3A0xbfebce7780c6f4fb!2sHeden%20Golf%20Hotel!5e0!3m2!1sfr!2sci!4v1590769060246!5m2!1sfr!2sci",
            ],

            [
                'title'=> "hotel de la peninsule",
                'description'=> "Hotel de la peninsule",

                'slider' => "location_slide1.png",

                'location_name' => "Cocody, Abidjan",
                'location_map' => "",
            ],

            [
                'title'=> "hotel de ville",
                'description'=> "Hotel de ville",
                'slider' => "location_slide1.png",

                'location_name' => "Cocody, Abidjan"
                ,
                'location_map' => "",
            ],
        ]);

        DB::table('specification_locations')->insert([
            [
                'icon' => "neige.png",
                'name' => "climatisation",
            ],
            [
                'icon' => "porte.png",
                'name' => "porte",
            ],
            [
                'icon' => "transmission.png",
                'name' => "transmission",
            ],
            [
                'icon' => "bagage.png",
                'name' => "bagage",
            ],
            [
                'icon' => "passager.png",
                'name' => "passager",
            ],
        ]);


        DB::table('Location_specification')->insert([
            [
                'location_id' => 1,
                'specification_id' => 1,
            ],
            [
                'location_id' => 1,
                'specification_id' => 2,
            ],
            [
                'location_id' => 1,
                'specification_id' => 3,
            ],
            [
                'location_id' => 1,
                'specification_id' => 4,
            ],
            [
                'location_id' => 1,
                'specification_id' => 5,
            ],
        ]);

        DB::table('media')->insert([
            [
                'url' => "https://minkatoo.adjemincloud.com/images/Location_showcase.png",
                'Location_id' => 1,
            ],
            [
                'url' => "https://minkatoo.adjemincloud.com/images/hyundai.png",
                'Location_id' => 1,
            ],
            [
                'url' => "https://minkatoo.adjemincloud.com/images/Location_showcase.png",
                'Location_id' => 1,
            ],
        ]);

    }
}