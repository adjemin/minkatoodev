<?php

use App\Models\Customer;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call(OfferSeeder::class);
        // $this->call(HotelSeeder::class);
        // $this->call(LocationSeeder::class);
        // $this->call(RestaurantSeeder::class);
        // $this->call(_A_::class);
        // $this->call(RatingsTableSeeder::class);

        $users = [
            [
                "name"=> "Ouattara Aboubakar Sidik",
                "email"=> "assidikouattara@gmail.com",
                "phone"=> "+2250779920203",
            ],
            [
                "name"=> "Ben Imad Kouakan",
                "email"=> "bindeben@gmail.com",
                "phone"=> "+2250708453194",
            ],
            [
                "name"=> "Nelly Niamson",
                "email"=> "bindeben@gmail.com",
                "phone"=> "+2250768955872",
            ],
        ];
            // "`password`"=> bcrypt("password"),
        DB::table('customers')->insert($users);
    }
}
