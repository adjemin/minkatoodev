<?php
use Illuminate\Database\Seeder;
class RatingsTableSeeder extends Seeder
{
    public function run()
    {
        \DB::table('offers_customers')->insert([
            0 => [
                'offers_id' => 39,
                'customers_id' => 3,
                'rating' => 1,
            ],
            1 => [
                'offers_id' => 40,
                'customers_id' => 3,
                'rating' => 2,
            ],
            2 => [
                'offers_id' => 37,
                'customers_id' => 3,
                'rating' => 2,
            ],
            3 => [
                'offers_id' => 43,
                'customers_id' => 3,
                'rating' => 2,
            ],
            4 => [
                'offers_id' => 39,
                'customers_id' => 2,
                'rating' => 5,
            ],
            5 => [
                'offers_id' => 37,
                'customers_id' => 2,
                'rating' => 5,
            ],
            6 => [
                'offers_id' => 41,
                'customers_id' => 2,
                'rating' => 3,
            ],
            7 => [
                'offers_id' => 36,
                'customers_id' => 2,
                'rating' => 2,
            ],
            7 => [
                'offers_id' => 31,
                'customers_id' => 3,
                'rating' => 3,
            ],
            8 => [
                'offers_id' => 32,
                'customers_id' => 3,
                'rating' => 3,
            ]
        ]);
    }
}
