<?php

use App\Models\Customer;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class _A_ extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // Customer::create([
        //     "name" => "John Doe",
        //     "email" => "john@doe.com",
        //     "password" => Hash::make("password")
        // ]);

        // User::create([
        //     "name" => "John Lark",
        //     "email" => "john@lark.com",
        //     "password" => Hash::make("password")
        // ]);
        User::create([
            "name" => "John Domingo",
            "email" => "john@doming.com",
            "password" => Hash::make("password")
        ]);

    }
}