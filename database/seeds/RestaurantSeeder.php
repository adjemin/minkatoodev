<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('restaurants')->insert([
            [
                'title'=> "Regina Margherita",
                'description'=> "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Possimus labore officia magnam eligendi assumenda, sunt atque quia explicabo praesentium nisi. Totam dolorum libero suscipit, distinctio eos sed modi illo temporibus.",
                'location_name' => "Abidjan, Cocody Center",
                'slider' => "restaurant_slide1.png",
                'location_map' => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.568345380635!2d-3.9750283493462137!3d5.329812137345493!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc1ec65e37c61ff%3A0xbfebce7780c6f4fb!2sHeden%20Golf%20Hotel!5e0!3m2!1sfr!2sci!4v1590769060246!5m2!1sfr!2sci",
            ],
            [
                'title'=> "El Pacio",
                'description'=> "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Possimus labore officia magnam eligendi assumenda, sunt atque quia explicabo praesentium nisi. Totam dolorum libero suscipit, distinctio eos sed modi illo temporibus.",
                'location_name' => "Abidjan, Cocody Center",
                'slider' => "restaurant_slide1.png",
                'location_map' => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.568345380635!2d-3.9750283493462137!3d5.329812137345493!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc1ec65e37c61ff%3A0xbfebce7780c6f4fb!2sHeden%20Golf%20Hotel!5e0!3m2!1sfr!2sci!4v1590769060246!5m2!1sfr!2sci",
            ],
        ]);

    }
}