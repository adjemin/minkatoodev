<!-- <?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OfferSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('customers')->insert([
[
    "id" => 1,
    "first_name" => 'Mohamed',
    "last_name" => 'Soro',
    "name" => 'Mohamed Soro',
    `phone_number` =>'87015892',
    `phone` => '22587015892',
    `dial_code`=> '225',
    `country_code` => 'CI',
    `email` => 'mohamedsoro@adjemin.com',
    `photo_url` => NULL,
    `gender` => 'HOMME',
    `bio`=> NULL ,
    `birthday` => NULL,
    `language` => NULL,
    `customer_type` =>'client',
    `otp`=> '123456',
    `otp_verified` => 1,
    `created_at` => '2020-02-13 18:31:27',
    `updated_at` => '2020-02-17 15:39:25',
    "deleted_at" => NULL
    ],,
[
    "id" => 2,
    "first_name" => 'Mohamed',
    "last_name" =>  'Soro',
    "name" => 'Mohamed Soro', '77925078', '22577925078', '225', 'CI', 'mohamedsoro@adjemin.com', NULL, 'HOMME', NULL, NULL, NULL, 'client', '067221', 1, '2020-02-17 15:00:53', '2020-02-17 15:38:32', NULL],,
[
    "id" => 7,
    "first_name" => 'Abd',
    "last_name" =>  'Simpson',
    "name" => 'Abd Simpson', '01020304', '22501020304', '225', 'CI', NULL, NULL, 'HOMME', NULL, NULL, NULL, 'client', '857019', 0, '2020-02-18 18:04:39', '2020-02-18 18:04:39', NULL],,
[
    "id" => 6,
    "first_name" => 'Frank',
    "last_name" =>  'Brou',
    "name" => 'Frank Brou', '44908473', '22544908473', '225', 'CI', NULL, NULL, 'HOMME', NULL, NULL, NULL, 'client', NULL, 0, '2020-02-18 17:41:27', '2020-02-18 17:41:27', NULL],,
[
    "id" => 12,
    "first_name" => 'Ange',
    "last_name" =>  'Bagui',
    "name" => 'Ange Bagui', '56888385', '22556888385', '225', 'CI', 'angebagui@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-07 00:20:24', '2020-03-07 00:20:24', NULL],,
[
    "id" => 13,
    "first_name" => 'Dikado',
    "last_name" =>  'Ban',
    "name" => 'Dikado Ban', '08175784', '22508175784', '225', 'CI', 'dikadoban@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-07 21:31:48', '2020-03-07 21:31:48', NULL],,
[
    "id" => 14,
    "first_name" => 'Nicole',
    "last_name" =>  'Yao',
    "name" => 'Nicole Yao', '77385080', '22577385080', '225', 'CI', 'nicole.adjoua@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-07 22:45:51', '2020-03-07 22:45:51', NULL],,
[
    "id" => 15,
    "first_name" => 'Casimir',
    "last_name" =>  'Daple',
    "name" => 'Casimir Daple', '47694913', '22547694913', '225', 'CI', 'daplecasimir1970@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-08 17:15:15', '2020-03-08 17:15:15', NULL],,
[
    "id" => 16,
    "first_name" => 'Gba',
    "last_name" =>  'Sinde franck',
    "name" => 'Gba Sinde franck', '59510446', '22559510446', '225', 'CI', 'francksinde@minkatoo.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-09 11:41:17', '2020-03-16 17:30:56', '2020-03-16 17:30:56'],,
[
    "id" => 17,
    "first_name" => 'Blandine',
    "last_name" =>  'Bamba',
    "name" => 'Blandine Bamba', NULL, '22558125834', NULL, NULL, 'blandine.bamba74@gmail.com', NULL, 'FEMME', NULL, NULL, NULL, 'client', NULL, 0, '2020-03-13 19:10:48', '2020-03-16 15:10:48', NULL],,
[
    "id" => 18,
    "first_name" => 'Franck',
    "last_name" =>  'Gba',
    "name" => 'Franck Gba', '44849587', '22544849587', '225', 'CI', 'francksinde@gmail.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-03-16 13:51:29', '2020-03-16 17:30:33', '2020-03-16 17:30:33'],,
[
    "id" => 19,
    "first_name" => 'Franck sinde',
    "last_name" =>  'Gba',
    "name" => 'Franck sinde Gba', NULL, '59510446', NULL, NULL, 'francksinde@minkatoo.com', 'WIN_20170221_17_47_48_Pro.jpg', 'HOMME', NULL, NULL, 'Français', 'client', '383119', 0, '2020-03-16 17:48:52', '2020-03-16 18:21:40', '2020-03-16 18:21:40'],,
[
    "id" => 20,
    "first_name" => 'Franck sinde',
    "last_name" => 'Gba',
    "name" => 'Franck sinde Gba', NULL, '225 44849587', NULL, NULL, 'francksinde@minkatoo.com', 'WIN_20170221_17_47_48_Pro.jpg', 'HOMME', NULL, NULL, NULL, 'client', '511314', 0, '2020-03-17 11:32:02', '2020-05-25 18:17:11', NULL],,
[
    "id" => 21,
    "first_name" => 'Too',
    "last_name" =>  'Taa',
    "name" => 'Too Taa', '84400312', '22584400312', '225', 'CI', 'angebagui@adjemin.com', NULL, NULL, NULL, NULL, NULL, 'client', NULL, 0, '2020-05-29 22:11:33', '2020-05-29 22:11:33', NULL],,
]);


-- --------------------------------------------------------

--
-- テーブルの構造 `customer_session`
--

CREATE TABLE `customer_session` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `location_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_latitude` double DEFAULT NULL,
  `location_longitude` double DEFAULT NULL,
  `battery` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `version` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `network` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isMobile` tinyint(1) NOT NULL DEFAULT 0,
  `isTesting` tinyint(1) NOT NULL DEFAULT 1,
  `deviceId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `devicesOSVersion` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `devicesName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `w` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ms` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idapp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `customer_session`
--

INSERT INTO `customer_session` (`id`, `token`, `customer_id`, `is_active`, `location_address`, `location_latitude`, `location_longitude`, `battery`, `version`, `device`, `ip_address`, `network`, `isMobile`, `isTesting`, `deviceId`, `devicesOSVersion`, `devicesName`, `w`, `h`, `ms`, `idapp`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-02-17 09:56:55', '2020-02-17 09:56:55', NULL),
(2, NULL, 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-02-28 01:03:28', '2020-02-28 01:03:28', NULL),
(3, NULL, 9, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 10:20:25', '2020-03-04 10:20:25', NULL),
(4, NULL, 10, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 22:11:10', '2020-03-04 22:11:10', NULL),
(5, NULL, 11, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-04 22:49:31', '2020-03-04 22:49:31', NULL),
(6, NULL, 12, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-07 00:20:24', '2020-03-07 00:20:24', NULL),
(7, NULL, 13, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-07 21:31:48', '2020-03-07 21:31:48', NULL),
(8, NULL, 14, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-07 22:45:51', '2020-03-07 22:45:51', NULL),
(9, NULL, 15, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-08 17:15:15', '2020-03-08 17:15:15', NULL),
(10, NULL, 16, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-09 11:41:17', '2020-03-09 11:41:17', NULL),
(11, NULL, 17, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-13 19:10:48', '2020-03-13 19:10:48', NULL),
(12, NULL, 18, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-16 13:51:29', '2020-03-16 13:51:29', NULL),
(13, NULL, 21, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-29 22:11:33', '2020-05-29 22:11:33', NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `excursion_activities`
--

CREATE TABLE `excursion_activities` (
  `id` int(11) NOT NULL,
  `image` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `slug` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `excursion_activities`
--

INSERT INTO `excursion_activities` (`id`, `image`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'https://i.imgur.com/RbrqUS8.png', 'Excursions et visites', 'excursions_et_visites', '2020-03-04 08:25:11', '2020-03-04 13:23:41', NULL),
(2, 'https://i.imgur.com/DPHmpcn.png', 'Visites Guidées', 'visites_guidées', '2020-03-04 08:25:11', '2020-03-04 13:23:41', NULL),
(3, 'https://i.imgur.com/a7NZR6Q.png', 'Sites historiques', 'sites_historiques', '2020-03-04 08:26:01', '2020-03-04 13:25:15', NULL),
(4, 'https://i.imgur.com/ssKvbyq.png', 'Randonnées', 'randonnees', '2020-03-04 08:26:01', '2020-03-04 13:25:15', NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- テーブルの構造 `favoris`
--

CREATE TABLE `favoris` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `offer_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `offer` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- テーブルの構造 `food_specialties`
--

CREATE TABLE `food_specialties` (
  `id` int(11) NOT NULL,
  `image` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `slug` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- テーブルのデータのダンプ `food_specialties`
--

INSERT INTO `food_specialties` (`id`, `image`, `name`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'https://i.imgur.com/mJY3XGH.png', 'Cuisine Africaine', 'cusisine_africaine', '2020-03-04 08:20:35', '2020-03-04 13:19:22', NULL),
(2, 'https://i.imgur.com/FvE5oqJ.png', 'Cuisine Asiatique', 'cuisine_asiatique', '2020-03-04 08:20:35', '2020-03-04 13:19:22', NULL),
(3, 'https://i.imgur.com/ocki9Mb.png', 'Cuisine Francaise', 'cuisine-francaise', '2020-03-04 10:46:23', '2020-03-04 15:44:50', NULL),
(4, 'https://i.imgur.com/YJrzF3E.png', 'Cuisine Orientale', 'cuisine-orientale', '2020-03-04 10:46:23', '2020-03-04 15:44:50', NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtotal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fees_delivery` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_paid_by_customer` tinyint(1) NOT NULL DEFAULT 0,
  `is_paid_by_delivery_service` tinyint(1) NOT NULL DEFAULT 0,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `invoices`
--

INSERT INTO `invoices` (`id`, `order_id`, `customer_id`, `reference`, `link`, `subtotal`, `tax`, `fees_delivery`, `total`, `status`, `is_paid_by_customer`, `is_paid_by_delivery_service`, `currency_code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 1, 'BOOKING-1-6-1-1583597215', '#', '100', '18', NULL, '118', 'unpaid', 0, 0, 'XOF', '2020-03-07 21:06:55', '2020-03-07 21:06:55', NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `invoice_payments`
--

CREATE TABLE `invoice_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) DEFAULT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cp',
  `payment_reference` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'XOF',
  `creator_id` bigint(20) DEFAULT NULL,
  `creator_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creator` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'waiting',
  `is_waiting` tinyint(1) NOT NULL DEFAULT 1,
  `is_completed` tinyint(1) NOT NULL DEFAULT 0,
  `payment_gateway_trans_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_custom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_currency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_payid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_payment_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_payment_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_error_message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_payment_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_phone_prefixe` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_cel_phone_num` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_ipn_ack` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_created_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_updated_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_cpm_result` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_trans_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_designation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_buyer_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway_signature` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `invoice_payments`
--

INSERT INTO `invoice_payments` (`id`, `invoice_id`, `payment_method`, `payment_reference`, `amount`, `currency_code`, `creator_id`, `creator_name`, `creator`, `status`, `is_waiting`, `is_completed`, `payment_gateway_trans_id`, `payment_gateway_custom`, `payment_gateway_currency`, `payment_gateway_amount`, `payment_gateway_payid`, `payment_gateway_payment_date`, `payment_gateway_payment_time`, `payment_gateway_error_message`, `payment_gateway_payment_method`, `payment_gateway_phone_prefixe`, `payment_gateway_cel_phone_num`, `payment_gateway_ipn_ack`, `payment_gateway_created_at`, `payment_gateway_updated_at`, `payment_gateway_cpm_result`, `payment_gateway_trans_status`, `payment_gateway_designation`, `payment_gateway_buyer_name`, `payment_gateway_signature`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'cp', '580156448-1-1-6', '118', 'XOF', 1, 'Mohamed Soro', 'customer', 'waiting', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-03-07 21:06:55', '2020-03-07 21:06:55', NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `meta_data_service`
--

CREATE TABLE `meta_data_service` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- テーブルの構造 `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_02_06_094440_create_customers_table', 1),
(5, '2020_02_13_111742_create_offers_table', 2),
(6, '2020_02_13_112037_create_orders_table', 2),
(7, '2020_02_13_112144_create_invoices_table', 2),
(8, '2020_02_13_112222_create_invoice_payments_table', 2),
(9, '2020_02_13_112255_create_order_items_table', 2),
(10, '2020_02_13_112324_create_receipts_table', 2),
(11, '2020_02_13_115645_create_owner_sources_table', 2),
(12, '2020_02_13_115710_create_meta_data_service_table', 2),
(13, '2020_02_13_190647_create_customer_session_table', 3),
(14, '2020_02_17_102314_create_favoris_table', 4);

-- --------------------------------------------------------

--
-- テーブルの構造 `offers`
--

CREATE TABLE `offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_source_id` bigint(20) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  `publisher_id` bigint(20) DEFAULT NULL,
  `publisher_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_children` tinyint(1) NOT NULL DEFAULT 0,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `has_price` tinyint(1) NOT NULL DEFAULT 0,
  `rating` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `room` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `etalissement` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specification` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_lng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_gps` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slide_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slide_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_escort` tinyint(1) DEFAULT 0,
  `escort_fees` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- テーブルのデータのダンプ `offers`
--

INSERT INTO `offers` (`id`, `parent_source_id`, `title`, `meta_data`, `type`, `owner_id`, `publisher_id`, `publisher_name`, `has_children`, `description`, `original_price`, `price`, `currency_code`, `has_price`, `rating`, `location_name`, `picture1`, `picture2`, `picture3`, `room`, `etalissement`, `specification`, `location_lat`, `location_lng`, `location_gps`, `slide_url`, `slide_title`, `is_escort`, `escort_fees`, `created_at`, `updated_at`, `deleted_at`) VALUES
(16, 0, 'Musée national du costume de Grand-Bassam', '{\r\n  \"specifications\": [\r\n    {\r\n      \"label_name\": \"Visiter le Site Web\",\r\n      \"label_value\": \"https://adjemincloud.com\"\r\n    },\r\n    {\r\n      \"label_name\": \"Téléphone\",\r\n      \"label_value\": \"+22546888385\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://blogs.roux-f.com.fr/blog-mcs3-47/wp-content/uploads/sites/136/2020/01/2014-03-29.jpg\",\r\n    \"https://www.fratmat.info/media/k2/items/cache/954834345dc80d3be639ada52ee49261_XL.jpg\",\r\n    \"https://ivoirhotel.com/storage/images/800x540/63507761de855d6f4e788898d4a69faaf71a2a80.jpg\",\r\n    \"https://media-cdn.tripadvisor.com/media/photo-s/0e/b8/6e/49/src-47025235-largejpg.jpg\"\r\n  ]\r\n}', 'musee', 0, 1, NULL, 0, 'Le musée national du costume de Grand-Bassam', '100', '100', 'XOF', 1, '3', 'Comoé,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.41350405', '-3.51696377', NULL, 'https://i.imgur.com/iLxeE4n.jpg', 'Muséede Grand-Bassam', 0, NULL, '2020-03-04 19:07:24', '2020-03-04 19:07:24', NULL),
(22, 0, 'Chez murphy', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Visiter le Site Web\",\r\n      \"label_value\": \"https://adjemincloud.com\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Téléphone\",\r\n      \"label_value\": \"+22546888385\"\r\n    }\r\n  ],\r\n  \"specialties\": [\r\n    {\r\n      \"image\": \"https://i2.wp.com/negronews.fr/wp-content/uploads/2019/12/africanfood.jpg?fit=900%2C600\",\r\n      \"name\": \"Cusisine Africaine\",\r\n      \"slug\": \"cusisine_africaine\",\r\n      \"id\": 1\r\n    },\r\n    {\r\n      \"image\": \"https://i.f1g.fr/media/madame/1900x1900/sites/default/files/img/2016/08/pays-photo-28.jpg\",\r\n      \"name\": \"Cusisine Asiatique\",\r\n      \"slug\": \"cusisine_asiatique\",\r\n      \"id\": 2\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Francaise\",\r\n      \"slug\": \"cusisine_francaise\",\r\n      \"id\": 3\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Orientale\",\r\n      \"slug\": \"cusisine_orientale\",\r\n      \"id\": 4\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/51150921_1816304761812546_8234552071462846464_o.jpg?_nc_cat=101&_nc_sid=110474&_nc_ohc=f9sJDgLNzUUAX9u4jV_&_nc_ht=scontent.fabj3-1.fna&oh=a45ee74da33d7646bb4744e6a0a0fc95&oe=5E8219D5\",\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/51060103_1812131532229869_6901307138027552768_o.jpg?_nc_cat=104&_nc_sid=110474&_nc_ohc=iIHDMO-ll80AX_QWQbJ&_nc_ht=scontent.fabj3-1.fna&oh=7e4a2632077055707326adbb5b389d18&oe=5E98ECA0\"\r\n  ]\r\n}', 'restaurant', 0, 1, NULL, 0, NULL, NULL, NULL, NULL, 0, '4', 'Cocody,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.39731620', '-3.97778040', NULL, 'https://i.imgur.com/cXV6nFn.jpg', 'murphy banniere', 0, NULL, '2020-03-04 19:51:16', '2020-03-04 19:51:17', NULL),
(21, 0, 'la case blue', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Visiter le Site Web\",\r\n      \"label_value\": \"https://adjemincloud.com\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Téléphone\",\r\n      \"label_value\": \"+22546888385\"\r\n    }\r\n  ],\r\n  \"specialties\": [\r\n    {\r\n      \"image\": \"https://i2.wp.com/negronews.fr/wp-content/uploads/2019/12/africanfood.jpg?fit=900%2C600\",\r\n      \"name\": \"Cusisine Africaine\",\r\n      \"slug\": \"cusisine_africaine\",\r\n      \"id\": 1\r\n    },\r\n    {\r\n      \"image\": \"https://i.f1g.fr/media/madame/1900x1900/sites/default/files/img/2016/08/pays-photo-28.jpg\",\r\n      \"name\": \"Cusisine Asiatique\",\r\n      \"slug\": \"cusisine_asiatique\",\r\n      \"id\": 2\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Francaise\",\r\n      \"slug\": \"cusisine_francaise\",\r\n      \"id\": 3\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Orientale\",\r\n      \"slug\": \"cusisine_orientale\",\r\n      \"id\": 4\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/60082553_1989291577842984_7372145126874808320_o.jpg?_nc_cat=107&_nc_sid=8024bb&_nc_ohc=9ld6EImk27QAX8_Six_&_nc_ht=scontent.fabj3-1.fna&oh=c27a3fe85ff8be131a99362d1bc3fa3c&oe=5E803052\",\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/60174714_1989290701176405_7166748168598585344_n.jpg?_nc_cat=106&_nc_sid=8024bb&_nc_ohc=i7i3-nzr_mcAX9Wlikv&_nc_ht=scontent.fabj3-1.fna&oh=f02af75037573e4d201325e96adf8c61&oe=5E815FA1\",\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/59543735_1977728928999249_2935943092680785920_n.jpg?_nc_cat=111&_nc_sid=8024bb&_nc_ohc=0xEt7Xp1EZQAX_KDa49&_nc_ht=scontent.fabj3-1.fna&oh=418a9a43135100315e31e114d3408f4a&oe=5E8037A3\",\r\n    \"https://scontent.fabj3-1.fna.fbcdn.net/v/t1.0-9/56424394_1927778663994276_2294723480618270720_o.jpg?_nc_cat=109&_nc_sid=8024bb&_nc_ohc=Y2P1Y3OtFe4AX-LEzEB&_nc_ht=scontent.fabj3-1.fna&oh=9147d504b80d2be0f99ce00909134338&oe=5E7FF19F\"\r\n  ]\r\n}', 'restaurant', 0, 1, NULL, 0, 'case blue restaurant situé a bassam', NULL, NULL, NULL, 0, '4', 'undefined,Comoé,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.41350405', '-3.51696377', NULL, 'https://i.imgur.com/lbOa1PA.jpg', 'case blue banniere', 0, NULL, '2020-03-04 19:42:20', '2020-03-04 19:42:21', NULL),
(20, 0, 'KKT Resi', '{\r\n  \"room_equipments\": [\r\n    {\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"name\": \"Piscine\"\r\n    },\r\n    {\r\n      \"name\": \"Piscine\"\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://resize-elle.ladmedia.fr/rcrop/638,,forcex/img/var/plain_site/storage/images/deco/reportages/city-guide/airbnb-biarritz-les-appartements-lofts-et-maisons-de-reve-a-biarritz/83299433-3-fre-FR/Airbnb-Biarritz-20-appartements-lofts-et-maisons-de-reve-a-Biarritz.jpg\",\r\n    \"https://www.biarritzsothebysrealty.com/theme/_siteSIR1812.sirbiarri/datas/articles/images/212/212_00-2019-11-07-1719.jpg\",\r\n    \"https://oliviergerber.fr/wp-content/uploads/photographeimmobilier-oliviergerber-easycles-elena-appartement-t3-location-vacances-biarritzcotebasque-7756.jpg\",\r\n    \"https://4.bp.blogspot.com/-2pWCcjfQgCQ/Vd8FS0NKssI/AAAAAAAAjSk/Im-_K2tVm5o/s1600/appart-location%2Bairbnb%2BBiarritz.jpg\",\r\n    \"https://static.trip101.com/paragraph_media/pictures/001/135/097/large/30e14bf7-582a-438f-b455-7839e6bd84ac.jpg?1527498277\",\r\n    \"https://www.toutsurmesfinances.com/impots/wp-content/uploads/sites/6/2019/04/meuble-location-airbnb.jpg\"\r\n  ]\r\n}', 'residence', 0, 1, NULL, 0, 'Residence top etoile', '100', '100', 'XOF', 1, '4', 'Le Plateau,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.32781449', '-4.02116776', NULL, 'https://i.imgur.com/wqALkjV.jpg', 'Residence KK', 0, NULL, '2020-03-04 19:42:07', '2020-03-04 19:42:09', NULL),
(19, 0, 'la terrasse', '{\r\n  \"specifications\": [\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Visiter le Site Web\",\r\n      \"label_value\": \"https://adjemincloud.com\"\r\n    },\r\n    {\r\n      \"icon\": \"\",\r\n      \"label_name\": \"Téléphone\",\r\n      \"label_value\": \"+22546888385\"\r\n    }\r\n  ],\r\n  \"specialties\": [\r\n  {\r\n      \"image\": \"https://i2.wp.com/negronews.fr/wp-content/uploads/2019/12/africanfood.jpg?fit=900%2C600\",\r\n      \"name\": \"Cusisine Africaine\",\r\n      \"slug\": \"cusisine_africaine\",\r\n      \"id\": 1\r\n    },\r\n    {\r\n      \"image\": \"https://i.f1g.fr/media/madame/1900x1900/sites/default/files/img/2016/08/pays-photo-28.jpg\",\r\n      \"name\": \"Cusisine Asiatique\",\r\n      \"slug\": \"cusisine_asiatique\",\r\n      \"id\": 2\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Francaise\",\r\n      \"slug\": \"cusisine_francaise\",\r\n      \"id\": 3\r\n    },\r\n    {\r\n      \"image\": \"https://www.sunlocation.com//images/zones/1/gastronomie-francaise.jpg\",\r\n      \"name\": \"Cusisine Orientale\",\r\n      \"slug\": \"cusisine_orientale\",\r\n      \"id\": 4\r\n    }\r\n  ],\r\n  \"pictures\": [\r\n    \"https://www.abidjanresto.net/assets/images/la-terrasse4-700x389.jpg\",\r\n    \"https://www.abidjanresto.net/assets/images/la-terrasse2-960x365.jpg\",\r\n    \"https://www.abidjanresto.net/assets/images/la-terrasse3-699x381.jpg\"\r\n  ]\r\n}', 'restaurant', 0, 1, NULL, 0, 'la terrasse zone 4', NULL, NULL, NULL, 0, '4', 'Marcory,Abidjan,Côte d\'Ivoire', NULL, NULL, NULL, NULL, NULL, NULL, '5.28819560', '-3.97865900', NULL, 'https://i.imgur.com/ZwmvWSG.jpg', 'banniere la terrasse', 0, NULL, '2020-03-04 19:33:20', '2020-03-04 19:33:21', NULL);

    }
} -->