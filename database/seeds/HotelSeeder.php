<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HotelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('hotels')->insert([


            [
                'title'=> "hotel du golf",
                'description'=> "Hotel du golf",
                'location_name' => "Cocody, Abidjan",
                'slider' => "hotel_slide1.png",
                'location_map' => "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.568345380635!2d-3.9750283493462137!3d5.329812137345493!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xfc1ec65e37c61ff%3A0xbfebce7780c6f4fb!2sHeden%20Golf%20Hotel!5e0!3m2!1sfr!2sci!4v1590769060246!5m2!1sfr!2sci",
            ],

            [
                'title'=> "hotel de la peninsule",
                'description'=> "Hotel de la peninsule",

                'slider' => "hotel_slide1.png",

                'location_name' => "Cocody, Abidjan",
                'location_map' => "",
            ],

            [
                'title'=> "hotel de ville",
                'description'=> "Hotel de ville",
                'slider' => "hotel_slide1.png",

                'location_name' => "Cocody, Abidjan"
                ,
                'location_map' => "",
            ],
        ]);

        DB::table('specification_hotels')->insert([
            [
                'icon' => "wifi.png",
                'name' => "wifi",
            ],
            [
                'icon' => "plate.png",
                'name' => "plate",
            ],
            [
                'icon' => "hanger.png",
                'name' => "hanger",
            ],
            [
                'icon' => "spa.png",
                'name' => "spa",
            ],
            [
                'icon' => "food.png",
                'name' => "food",
            ],
            [
                'icon' => "bus.png",
                'name' => "bus",
            ],
            [
                'icon' => "support.png",
                'name' => "support",
            ],
            [
                'icon' => "plane.png",
                'name' => "plane",
            ],
            [
                'icon' => "carousel.png",
                'name' => "carousel",
            ],
        ]);


        DB::table('hotel_specification')->insert([
            [
                'hotel_id' => 1,
                'specification_id' => 1,
            ],
            [
                'hotel_id' => 1,
                'specification_id' => 2,
            ],
            [
                'hotel_id' => 1,
                'specification_id' => 3,
            ],
            [
                'hotel_id' => 1,
                'specification_id' => 4,
            ],
            [
                'hotel_id' => 1,
                'specification_id' => 5,
            ],
        ]);

        DB::table('media')->insert([
            [
                'url' => "https://minkatoo.adjemincloud.com/images/hotel_showcase.png",
                'hotel_id' => 1,
            ],
            [
                'url' => "https://minkatoo.adjemincloud.com/images/hyundai.png",
                'hotel_id' => 1,
            ],
            [
                'url' => "https://minkatoo.adjemincloud.com/images/hotel_showcase.png",
                'hotel_id' => 1,
            ],
        ]);

    }
}