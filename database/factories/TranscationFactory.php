<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Transcation;
use Faker\Generator as Faker;

$factory->define(Transcation::class, function (Faker $faker) {

    return [
        'amount' => $faker->randomDigitNotNull,
        'currency' => $faker->word,
        'transaction_id' => $faker->word,
        'designation' => $faker->word,
        'notify_url' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
