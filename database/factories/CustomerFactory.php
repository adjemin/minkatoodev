<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {

    return [
        'first_name' => $faker->word,
        'last_name' => $faker->word,
        'name' => $faker->word,
        'phone_number' => $faker->word,
        'phone' => $faker->word,
        'dial_code' => $faker->word,
        'country_code' => $faker->word,
        'email' => $faker->word,
        'photo_url' => $faker->word,
        'gender' => $faker->word,
        'bio' => $faker->word,
        'birthday' => $faker->word,
        'language' => $faker->word,
        'customer_type' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
