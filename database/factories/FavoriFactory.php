<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Favori;
use Faker\Generator as Faker;

$factory->define(Favori::class, function (Faker $faker) {

    return [
        'customer_id' => $faker->word,
        'offer_id' => $faker->word,
        'offer' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
