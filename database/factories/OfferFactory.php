<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Offer;
use Faker\Generator as Faker;

$factory->define(Offer::class, function (Faker $faker) {

    return [
        'parent_source_id' => $faker->word,
        'title' => $faker->word,
        'meta_data' => $faker->text,
        'type' => $faker->word,
        'owner_id' => $faker->word,
        'publisher_id' => $faker->word,
        'has_children' => $faker->word,
        'description' => $faker->word,
        'original_price' => $faker->word,
        'price' => $faker->word,
        'currency_code' => $faker->word,
        'has_price' => $faker->word,
        'rating' => $faker->word,
        'location_name' => $faker->word,
        'location_lat' => $faker->word,
        'location_lng' => $faker->word,
        'location_gps' => $faker->word,
        'slide_url' => $faker->word,
        'slide_title' => $faker->word,
        'is_escort' => $faker->word,
        'escort_fees' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
