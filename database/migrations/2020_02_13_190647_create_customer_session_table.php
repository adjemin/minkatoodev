<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerSessionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_session', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('token')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->boolean('is_active')->default(true);
            $table->string('location_address')->nullable();
            $table->double('location_latitude')->nullable();
            $table->double('location_longitude')->nullable();
            $table->string('battery')->nullable();
            $table->string('version')->nullable();
            $table->text('device')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('network')->nullable();
            $table->boolean('isMobile')->default(false);
            $table->boolean('isTesting')->default(true);
            $table->string('deviceId')->nullable();
            $table->string('devicesOSVersion')->nullable();
            $table->string('devicesName')->nullable();
            $table->string('w')->nullable();
            $table->string('h')->nullable();
            $table->string('ms')->nullable();
            $table->string('idapp')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_session');
    }
}
