<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->nullable();
            $table->boolean('is_waiting')->default(true);
            $table->string('current_status')->nullable();
            $table->string('payment_method_slug')->default('cash');
            $table->string('delivery_fees')->nullable();
            $table->boolean('is_delivered')->default(false);
            $table->timestamp('delivery_date')->nullable();
            $table->string('amount')->nullable();
            $table->string('currency_code')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
