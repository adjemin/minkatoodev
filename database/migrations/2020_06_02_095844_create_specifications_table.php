<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specification_hotels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('icon');
            $table->string('name');
            $table->timestamps();
            //
            // $table->unsignedBigInteger('hotel_id')->nullable();
            // $table->index("hotel_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specifications');
    }
}