<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',225)->nullable();
            $table->string('subtitle',225)->nullable();
            $table->string('action',225)->nullable();
            $table->string('action_by',225)->nullable();
            $table->text('meta_data',225)->nullable();
            $table->string('type_notification',225)->nullable();
            $table->boolean('is_read')->default(false);
            $table->boolean('is_received')->default(false);
            $table->text('data',225)->nullable();
            $table->bigInteger('user_id')->nullable();;
            $table->bigInteger('data_id')->nullable();;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_notifications');
    }
}
