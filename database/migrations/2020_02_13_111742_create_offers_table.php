<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parent_source_id')->nullable();
            $table->string('title')->nullable();
            $table->text('meta_data')->nullable();
            $table->string('type')->nullable();
            $table->bigInteger('owner_id')->nullable();
            $table->bigInteger('publisher_id')->nullable();
            $table->bigInteger('publisher_name')->nullable(); //
            $table->boolean('has_children')->nullable()->default(false);
            $table->text('description', )->nullable();
            $table->string('picture1')->nullable(); //
            $table->string('picture2')->nullable(); //
            $table->string('picture3')->nullable(); //
            $table->string('picture4')->nullable(); //
            $table->string('phone_number')->nullable();
            $table->string('website')->nullable();
            $table->string('room')->nullable(); //
            $table->string('etablissement')->nullable(); //
            // $table->string('specification')->nullable(); //
            $table->string('original_price')->nullable();
            $table->string('price')->nullable();
            $table->string('currency_code')->nullable();
            $table->boolean('has_price')->nullable()->default(false);
            $table->string('rating')->nullable();
            $table->string('location_name')->nullable();
            $table->string('location_lat')->nullable();
            $table->string('location_lng')->nullable();
            $table->string('location_gps')->nullable();
            $table->string('slide_url')->nullable();
            $table->string('slide_title')->nullable();
            $table->boolean('is_escort')->nullable();
            $table->string('escort_fees')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offres');
    }
}
