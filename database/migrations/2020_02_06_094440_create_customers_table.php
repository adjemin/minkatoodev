<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('name')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('phone')->unique()->nullable(); // TODO
            $table->string('dial_code')->nullable();
            $table->string('country_code')->nullable();
            $table->string('email')->nullable();
            $table->string('photo_url')->nullable();
            $table->string('gender')->nullable();
            $table->string('bio')->nullable();
            $table->string('birthday')->nullable();
            $table->string('language')->nullable();
            $table->string('customer_type')->default('client');
            $table->string('otp')->nullable();
            $table->string('password')->nullable();
            // $table->boolean('otp_verify')->nullable();
            $table->boolean('otp_verified')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}