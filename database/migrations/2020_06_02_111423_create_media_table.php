<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('url');
            //
            $table->unsignedBigInteger('hotel_id')->nullable();
            $table->index('hotel_id')->nullable();
            //
            $table->unsignedBigInteger('location_id')->nullable();
            $table->index('location_id')->nullable();
            //
            $table->unsignedBigInteger('restaurant_id')->nullable();
            $table->index('restaurant_id')->nullable();
            //
            $table->unsignedBigInteger('excursion_id')->nullable();
            $table->index('excursion_id')->nullable();

            $table->unsignedBigInteger('meuseum_id')->nullable();
            $table->index('meuseum_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}