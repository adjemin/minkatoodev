<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Otp;

class OtpApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_otp()
    {
        $otp = factory(Otp::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/otps', $otp
        );

        $this->assertApiResponse($otp);
    }

    /**
     * @test
     */
    public function test_read_otp()
    {
        $otp = factory(Otp::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/otps/'.$otp->id
        );

        $this->assertApiResponse($otp->toArray());
    }

    /**
     * @test
     */
    public function test_update_otp()
    {
        $otp = factory(Otp::class)->create();
        $editedOtp = factory(Otp::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/otps/'.$otp->id,
            $editedOtp
        );

        $this->assertApiResponse($editedOtp);
    }

    /**
     * @test
     */
    public function test_delete_otp()
    {
        $otp = factory(Otp::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/otps/'.$otp->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/otps/'.$otp->id
        );

        $this->response->assertStatus(404);
    }
}
