<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MetaDataService;

class MetaDataServiceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_meta_data_service()
    {
        $metaDataService = factory(MetaDataService::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/meta_data_services', $metaDataService
        );

        $this->assertApiResponse($metaDataService);
    }

    /**
     * @test
     */
    public function test_read_meta_data_service()
    {
        $metaDataService = factory(MetaDataService::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/meta_data_services/'.$metaDataService->id
        );

        $this->assertApiResponse($metaDataService->toArray());
    }

    /**
     * @test
     */
    public function test_update_meta_data_service()
    {
        $metaDataService = factory(MetaDataService::class)->create();
        $editedMetaDataService = factory(MetaDataService::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/meta_data_services/'.$metaDataService->id,
            $editedMetaDataService
        );

        $this->assertApiResponse($editedMetaDataService);
    }

    /**
     * @test
     */
    public function test_delete_meta_data_service()
    {
        $metaDataService = factory(MetaDataService::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/meta_data_services/'.$metaDataService->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/meta_data_services/'.$metaDataService->id
        );

        $this->response->assertStatus(404);
    }
}
