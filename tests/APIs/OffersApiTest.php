<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Offers;

class OffersApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_offers()
    {
        $offers = factory(Offers::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/offers', $offers
        );

        $this->assertApiResponse($offers);
    }

    /**
     * @test
     */
    public function test_read_offers()
    {
        $offers = factory(Offers::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/offers/'.$offers->id
        );

        $this->assertApiResponse($offers->toArray());
    }

    /**
     * @test
     */
    public function test_update_offers()
    {
        $offers = factory(Offers::class)->create();
        $editedOffers = factory(Offers::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/offers/'.$offers->id,
            $editedOffers
        );

        $this->assertApiResponse($editedOffers);
    }

    /**
     * @test
     */
    public function test_delete_offers()
    {
        $offers = factory(Offers::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/offers/'.$offers->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/offers/'.$offers->id
        );

        $this->response->assertStatus(404);
    }
}
