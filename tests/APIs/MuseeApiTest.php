<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Musee;

class MuseeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_musee()
    {
        $musee = factory(Musee::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/musees', $musee
        );

        $this->assertApiResponse($musee);
    }

    /**
     * @test
     */
    public function test_read_musee()
    {
        $musee = factory(Musee::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/musees/'.$musee->id
        );

        $this->assertApiResponse($musee->toArray());
    }

    /**
     * @test
     */
    public function test_update_musee()
    {
        $musee = factory(Musee::class)->create();
        $editedMusee = factory(Musee::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/musees/'.$musee->id,
            $editedMusee
        );

        $this->assertApiResponse($editedMusee);
    }

    /**
     * @test
     */
    public function test_delete_musee()
    {
        $musee = factory(Musee::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/musees/'.$musee->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/musees/'.$musee->id
        );

        $this->response->assertStatus(404);
    }
}
