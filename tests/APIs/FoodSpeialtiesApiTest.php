<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\FoodSpeialties;

class FoodSpeialtiesApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_food_speialties()
    {
        $foodSpeialties = factory(FoodSpeialties::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/food_speialties', $foodSpeialties
        );

        $this->assertApiResponse($foodSpeialties);
    }

    /**
     * @test
     */
    public function test_read_food_speialties()
    {
        $foodSpeialties = factory(FoodSpeialties::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/food_speialties/'.$foodSpeialties->id
        );

        $this->assertApiResponse($foodSpeialties->toArray());
    }

    /**
     * @test
     */
    public function test_update_food_speialties()
    {
        $foodSpeialties = factory(FoodSpeialties::class)->create();
        $editedFoodSpeialties = factory(FoodSpeialties::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/food_speialties/'.$foodSpeialties->id,
            $editedFoodSpeialties
        );

        $this->assertApiResponse($editedFoodSpeialties);
    }

    /**
     * @test
     */
    public function test_delete_food_speialties()
    {
        $foodSpeialties = factory(FoodSpeialties::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/food_speialties/'.$foodSpeialties->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/food_speialties/'.$foodSpeialties->id
        );

        $this->response->assertStatus(404);
    }
}
