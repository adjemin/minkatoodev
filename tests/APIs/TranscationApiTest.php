<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Transcation;

class TranscationApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_transcation()
    {
        $transcation = factory(Transcation::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/transcations', $transcation
        );

        $this->assertApiResponse($transcation);
    }

    /**
     * @test
     */
    public function test_read_transcation()
    {
        $transcation = factory(Transcation::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/transcations/'.$transcation->id
        );

        $this->assertApiResponse($transcation->toArray());
    }

    /**
     * @test
     */
    public function test_update_transcation()
    {
        $transcation = factory(Transcation::class)->create();
        $editedTranscation = factory(Transcation::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/transcations/'.$transcation->id,
            $editedTranscation
        );

        $this->assertApiResponse($editedTranscation);
    }

    /**
     * @test
     */
    public function test_delete_transcation()
    {
        $transcation = factory(Transcation::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/transcations/'.$transcation->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/transcations/'.$transcation->id
        );

        $this->response->assertStatus(404);
    }
}
