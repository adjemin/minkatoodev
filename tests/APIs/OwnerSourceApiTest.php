<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\OwnerSource;

class OwnerSourceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_owner_source()
    {
        $ownerSource = factory(OwnerSource::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/owner_sources', $ownerSource
        );

        $this->assertApiResponse($ownerSource);
    }

    /**
     * @test
     */
    public function test_read_owner_source()
    {
        $ownerSource = factory(OwnerSource::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/owner_sources/'.$ownerSource->id
        );

        $this->assertApiResponse($ownerSource->toArray());
    }

    /**
     * @test
     */
    public function test_update_owner_source()
    {
        $ownerSource = factory(OwnerSource::class)->create();
        $editedOwnerSource = factory(OwnerSource::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/owner_sources/'.$ownerSource->id,
            $editedOwnerSource
        );

        $this->assertApiResponse($editedOwnerSource);
    }

    /**
     * @test
     */
    public function test_delete_owner_source()
    {
        $ownerSource = factory(OwnerSource::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/owner_sources/'.$ownerSource->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/owner_sources/'.$ownerSource->id
        );

        $this->response->assertStatus(404);
    }
}
