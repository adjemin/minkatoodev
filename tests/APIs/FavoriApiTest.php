<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Favori;

class FavoriApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_favori()
    {
        $favori = factory(Favori::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/favoris', $favori
        );

        $this->assertApiResponse($favori);
    }

    /**
     * @test
     */
    public function test_read_favori()
    {
        $favori = factory(Favori::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/favoris/'.$favori->id
        );

        $this->assertApiResponse($favori->toArray());
    }

    /**
     * @test
     */
    public function test_update_favori()
    {
        $favori = factory(Favori::class)->create();
        $editedFavori = factory(Favori::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/favoris/'.$favori->id,
            $editedFavori
        );

        $this->assertApiResponse($editedFavori);
    }

    /**
     * @test
     */
    public function test_delete_favori()
    {
        $favori = factory(Favori::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/favoris/'.$favori->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/favoris/'.$favori->id
        );

        $this->response->assertStatus(404);
    }
}
