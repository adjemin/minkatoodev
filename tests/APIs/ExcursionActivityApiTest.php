<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ExcursionActivity;

class ExcursionActivityApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_excursion_activity()
    {
        $excursionActivity = factory(ExcursionActivity::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/excursion_activities', $excursionActivity
        );

        $this->assertApiResponse($excursionActivity);
    }

    /**
     * @test
     */
    public function test_read_excursion_activity()
    {
        $excursionActivity = factory(ExcursionActivity::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/excursion_activities/'.$excursionActivity->id
        );

        $this->assertApiResponse($excursionActivity->toArray());
    }

    /**
     * @test
     */
    public function test_update_excursion_activity()
    {
        $excursionActivity = factory(ExcursionActivity::class)->create();
        $editedExcursionActivity = factory(ExcursionActivity::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/excursion_activities/'.$excursionActivity->id,
            $editedExcursionActivity
        );

        $this->assertApiResponse($editedExcursionActivity);
    }

    /**
     * @test
     */
    public function test_delete_excursion_activity()
    {
        $excursionActivity = factory(ExcursionActivity::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/excursion_activities/'.$excursionActivity->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/excursion_activities/'.$excursionActivity->id
        );

        $this->response->assertStatus(404);
    }
}
