<?php namespace Tests\Repositories;

use App\Models\Transcation;
use App\Repositories\TranscationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TranscationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TranscationRepository
     */
    protected $transcationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->transcationRepo = \App::make(TranscationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_transcation()
    {
        $transcation = factory(Transcation::class)->make()->toArray();

        $createdTranscation = $this->transcationRepo->create($transcation);

        $createdTranscation = $createdTranscation->toArray();
        $this->assertArrayHasKey('id', $createdTranscation);
        $this->assertNotNull($createdTranscation['id'], 'Created Transcation must have id specified');
        $this->assertNotNull(Transcation::find($createdTranscation['id']), 'Transcation with given id must be in DB');
        $this->assertModelData($transcation, $createdTranscation);
    }

    /**
     * @test read
     */
    public function test_read_transcation()
    {
        $transcation = factory(Transcation::class)->create();

        $dbTranscation = $this->transcationRepo->find($transcation->id);

        $dbTranscation = $dbTranscation->toArray();
        $this->assertModelData($transcation->toArray(), $dbTranscation);
    }

    /**
     * @test update
     */
    public function test_update_transcation()
    {
        $transcation = factory(Transcation::class)->create();
        $fakeTranscation = factory(Transcation::class)->make()->toArray();

        $updatedTranscation = $this->transcationRepo->update($fakeTranscation, $transcation->id);

        $this->assertModelData($fakeTranscation, $updatedTranscation->toArray());
        $dbTranscation = $this->transcationRepo->find($transcation->id);
        $this->assertModelData($fakeTranscation, $dbTranscation->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_transcation()
    {
        $transcation = factory(Transcation::class)->create();

        $resp = $this->transcationRepo->delete($transcation->id);

        $this->assertTrue($resp);
        $this->assertNull(Transcation::find($transcation->id), 'Transcation should not exist in DB');
    }
}
