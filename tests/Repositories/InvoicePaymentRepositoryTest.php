<?php namespace Tests\Repositories;

use App\Models\InvoicePayment;
use App\Repositories\InvoicePaymentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class InvoicePaymentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var InvoicePaymentRepository
     */
    protected $invoicePaymentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->invoicePaymentRepo = \App::make(InvoicePaymentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_invoice_payment()
    {
        $invoicePayment = factory(InvoicePayment::class)->make()->toArray();

        $createdInvoicePayment = $this->invoicePaymentRepo->create($invoicePayment);

        $createdInvoicePayment = $createdInvoicePayment->toArray();
        $this->assertArrayHasKey('id', $createdInvoicePayment);
        $this->assertNotNull($createdInvoicePayment['id'], 'Created InvoicePayment must have id specified');
        $this->assertNotNull(InvoicePayment::find($createdInvoicePayment['id']), 'InvoicePayment with given id must be in DB');
        $this->assertModelData($invoicePayment, $createdInvoicePayment);
    }

    /**
     * @test read
     */
    public function test_read_invoice_payment()
    {
        $invoicePayment = factory(InvoicePayment::class)->create();

        $dbInvoicePayment = $this->invoicePaymentRepo->find($invoicePayment->id);

        $dbInvoicePayment = $dbInvoicePayment->toArray();
        $this->assertModelData($invoicePayment->toArray(), $dbInvoicePayment);
    }

    /**
     * @test update
     */
    public function test_update_invoice_payment()
    {
        $invoicePayment = factory(InvoicePayment::class)->create();
        $fakeInvoicePayment = factory(InvoicePayment::class)->make()->toArray();

        $updatedInvoicePayment = $this->invoicePaymentRepo->update($fakeInvoicePayment, $invoicePayment->id);

        $this->assertModelData($fakeInvoicePayment, $updatedInvoicePayment->toArray());
        $dbInvoicePayment = $this->invoicePaymentRepo->find($invoicePayment->id);
        $this->assertModelData($fakeInvoicePayment, $dbInvoicePayment->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_invoice_payment()
    {
        $invoicePayment = factory(InvoicePayment::class)->create();

        $resp = $this->invoicePaymentRepo->delete($invoicePayment->id);

        $this->assertTrue($resp);
        $this->assertNull(InvoicePayment::find($invoicePayment->id), 'InvoicePayment should not exist in DB');
    }
}
