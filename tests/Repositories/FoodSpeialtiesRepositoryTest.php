<?php namespace Tests\Repositories;

use App\Models\FoodSpeialties;
use App\Repositories\FoodSpeialtiesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class FoodSpeialtiesRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var FoodSpeialtiesRepository
     */
    protected $foodSpeialtiesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->foodSpeialtiesRepo = \App::make(FoodSpeialtiesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_food_speialties()
    {
        $foodSpeialties = factory(FoodSpeialties::class)->make()->toArray();

        $createdFoodSpeialties = $this->foodSpeialtiesRepo->create($foodSpeialties);

        $createdFoodSpeialties = $createdFoodSpeialties->toArray();
        $this->assertArrayHasKey('id', $createdFoodSpeialties);
        $this->assertNotNull($createdFoodSpeialties['id'], 'Created FoodSpeialties must have id specified');
        $this->assertNotNull(FoodSpeialties::find($createdFoodSpeialties['id']), 'FoodSpeialties with given id must be in DB');
        $this->assertModelData($foodSpeialties, $createdFoodSpeialties);
    }

    /**
     * @test read
     */
    public function test_read_food_speialties()
    {
        $foodSpeialties = factory(FoodSpeialties::class)->create();

        $dbFoodSpeialties = $this->foodSpeialtiesRepo->find($foodSpeialties->id);

        $dbFoodSpeialties = $dbFoodSpeialties->toArray();
        $this->assertModelData($foodSpeialties->toArray(), $dbFoodSpeialties);
    }

    /**
     * @test update
     */
    public function test_update_food_speialties()
    {
        $foodSpeialties = factory(FoodSpeialties::class)->create();
        $fakeFoodSpeialties = factory(FoodSpeialties::class)->make()->toArray();

        $updatedFoodSpeialties = $this->foodSpeialtiesRepo->update($fakeFoodSpeialties, $foodSpeialties->id);

        $this->assertModelData($fakeFoodSpeialties, $updatedFoodSpeialties->toArray());
        $dbFoodSpeialties = $this->foodSpeialtiesRepo->find($foodSpeialties->id);
        $this->assertModelData($fakeFoodSpeialties, $dbFoodSpeialties->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_food_speialties()
    {
        $foodSpeialties = factory(FoodSpeialties::class)->create();

        $resp = $this->foodSpeialtiesRepo->delete($foodSpeialties->id);

        $this->assertTrue($resp);
        $this->assertNull(FoodSpeialties::find($foodSpeialties->id), 'FoodSpeialties should not exist in DB');
    }
}
