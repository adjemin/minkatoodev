<?php namespace Tests\Repositories;

use App\Models\Otp;
use App\Repositories\OtpRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OtpRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OtpRepository
     */
    protected $otpRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->otpRepo = \App::make(OtpRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_otp()
    {
        $otp = factory(Otp::class)->make()->toArray();

        $createdOtp = $this->otpRepo->create($otp);

        $createdOtp = $createdOtp->toArray();
        $this->assertArrayHasKey('id', $createdOtp);
        $this->assertNotNull($createdOtp['id'], 'Created Otp must have id specified');
        $this->assertNotNull(Otp::find($createdOtp['id']), 'Otp with given id must be in DB');
        $this->assertModelData($otp, $createdOtp);
    }

    /**
     * @test read
     */
    public function test_read_otp()
    {
        $otp = factory(Otp::class)->create();

        $dbOtp = $this->otpRepo->find($otp->id);

        $dbOtp = $dbOtp->toArray();
        $this->assertModelData($otp->toArray(), $dbOtp);
    }

    /**
     * @test update
     */
    public function test_update_otp()
    {
        $otp = factory(Otp::class)->create();
        $fakeOtp = factory(Otp::class)->make()->toArray();

        $updatedOtp = $this->otpRepo->update($fakeOtp, $otp->id);

        $this->assertModelData($fakeOtp, $updatedOtp->toArray());
        $dbOtp = $this->otpRepo->find($otp->id);
        $this->assertModelData($fakeOtp, $dbOtp->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_otp()
    {
        $otp = factory(Otp::class)->create();

        $resp = $this->otpRepo->delete($otp->id);

        $this->assertTrue($resp);
        $this->assertNull(Otp::find($otp->id), 'Otp should not exist in DB');
    }
}
