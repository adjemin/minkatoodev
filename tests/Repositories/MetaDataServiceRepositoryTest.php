<?php namespace Tests\Repositories;

use App\Models\MetaDataService;
use App\Repositories\MetaDataServiceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MetaDataServiceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MetaDataServiceRepository
     */
    protected $metaDataServiceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->metaDataServiceRepo = \App::make(MetaDataServiceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_meta_data_service()
    {
        $metaDataService = factory(MetaDataService::class)->make()->toArray();

        $createdMetaDataService = $this->metaDataServiceRepo->create($metaDataService);

        $createdMetaDataService = $createdMetaDataService->toArray();
        $this->assertArrayHasKey('id', $createdMetaDataService);
        $this->assertNotNull($createdMetaDataService['id'], 'Created MetaDataService must have id specified');
        $this->assertNotNull(MetaDataService::find($createdMetaDataService['id']), 'MetaDataService with given id must be in DB');
        $this->assertModelData($metaDataService, $createdMetaDataService);
    }

    /**
     * @test read
     */
    public function test_read_meta_data_service()
    {
        $metaDataService = factory(MetaDataService::class)->create();

        $dbMetaDataService = $this->metaDataServiceRepo->find($metaDataService->id);

        $dbMetaDataService = $dbMetaDataService->toArray();
        $this->assertModelData($metaDataService->toArray(), $dbMetaDataService);
    }

    /**
     * @test update
     */
    public function test_update_meta_data_service()
    {
        $metaDataService = factory(MetaDataService::class)->create();
        $fakeMetaDataService = factory(MetaDataService::class)->make()->toArray();

        $updatedMetaDataService = $this->metaDataServiceRepo->update($fakeMetaDataService, $metaDataService->id);

        $this->assertModelData($fakeMetaDataService, $updatedMetaDataService->toArray());
        $dbMetaDataService = $this->metaDataServiceRepo->find($metaDataService->id);
        $this->assertModelData($fakeMetaDataService, $dbMetaDataService->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_meta_data_service()
    {
        $metaDataService = factory(MetaDataService::class)->create();

        $resp = $this->metaDataServiceRepo->delete($metaDataService->id);

        $this->assertTrue($resp);
        $this->assertNull(MetaDataService::find($metaDataService->id), 'MetaDataService should not exist in DB');
    }
}
