<?php namespace Tests\Repositories;

use App\Models\Favori;
use App\Repositories\FavoriRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class FavoriRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var FavoriRepository
     */
    protected $favoriRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->favoriRepo = \App::make(FavoriRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_favori()
    {
        $favori = factory(Favori::class)->make()->toArray();

        $createdFavori = $this->favoriRepo->create($favori);

        $createdFavori = $createdFavori->toArray();
        $this->assertArrayHasKey('id', $createdFavori);
        $this->assertNotNull($createdFavori['id'], 'Created Favori must have id specified');
        $this->assertNotNull(Favori::find($createdFavori['id']), 'Favori with given id must be in DB');
        $this->assertModelData($favori, $createdFavori);
    }

    /**
     * @test read
     */
    public function test_read_favori()
    {
        $favori = factory(Favori::class)->create();

        $dbFavori = $this->favoriRepo->find($favori->id);

        $dbFavori = $dbFavori->toArray();
        $this->assertModelData($favori->toArray(), $dbFavori);
    }

    /**
     * @test update
     */
    public function test_update_favori()
    {
        $favori = factory(Favori::class)->create();
        $fakeFavori = factory(Favori::class)->make()->toArray();

        $updatedFavori = $this->favoriRepo->update($fakeFavori, $favori->id);

        $this->assertModelData($fakeFavori, $updatedFavori->toArray());
        $dbFavori = $this->favoriRepo->find($favori->id);
        $this->assertModelData($fakeFavori, $dbFavori->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_favori()
    {
        $favori = factory(Favori::class)->create();

        $resp = $this->favoriRepo->delete($favori->id);

        $this->assertTrue($resp);
        $this->assertNull(Favori::find($favori->id), 'Favori should not exist in DB');
    }
}
