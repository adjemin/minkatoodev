<?php namespace Tests\Repositories;

use App\Models\Musee;
use App\Repositories\MuseeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MuseeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MuseeRepository
     */
    protected $museeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->museeRepo = \App::make(MuseeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_musee()
    {
        $musee = factory(Musee::class)->make()->toArray();

        $createdMusee = $this->museeRepo->create($musee);

        $createdMusee = $createdMusee->toArray();
        $this->assertArrayHasKey('id', $createdMusee);
        $this->assertNotNull($createdMusee['id'], 'Created Musee must have id specified');
        $this->assertNotNull(Musee::find($createdMusee['id']), 'Musee with given id must be in DB');
        $this->assertModelData($musee, $createdMusee);
    }

    /**
     * @test read
     */
    public function test_read_musee()
    {
        $musee = factory(Musee::class)->create();

        $dbMusee = $this->museeRepo->find($musee->id);

        $dbMusee = $dbMusee->toArray();
        $this->assertModelData($musee->toArray(), $dbMusee);
    }

    /**
     * @test update
     */
    public function test_update_musee()
    {
        $musee = factory(Musee::class)->create();
        $fakeMusee = factory(Musee::class)->make()->toArray();

        $updatedMusee = $this->museeRepo->update($fakeMusee, $musee->id);

        $this->assertModelData($fakeMusee, $updatedMusee->toArray());
        $dbMusee = $this->museeRepo->find($musee->id);
        $this->assertModelData($fakeMusee, $dbMusee->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_musee()
    {
        $musee = factory(Musee::class)->create();

        $resp = $this->museeRepo->delete($musee->id);

        $this->assertTrue($resp);
        $this->assertNull(Musee::find($musee->id), 'Musee should not exist in DB');
    }
}
