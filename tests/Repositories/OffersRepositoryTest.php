<?php namespace Tests\Repositories;

use App\Models\Offers;
use App\Repositories\OffersRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OffersRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OffersRepository
     */
    protected $offersRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->offersRepo = \App::make(OffersRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_offers()
    {
        $offers = factory(Offers::class)->make()->toArray();

        $createdOffers = $this->offersRepo->create($offers);

        $createdOffers = $createdOffers->toArray();
        $this->assertArrayHasKey('id', $createdOffers);
        $this->assertNotNull($createdOffers['id'], 'Created Offers must have id specified');
        $this->assertNotNull(Offers::find($createdOffers['id']), 'Offers with given id must be in DB');
        $this->assertModelData($offers, $createdOffers);
    }

    /**
     * @test read
     */
    public function test_read_offers()
    {
        $offers = factory(Offers::class)->create();

        $dbOffers = $this->offersRepo->find($offers->id);

        $dbOffers = $dbOffers->toArray();
        $this->assertModelData($offers->toArray(), $dbOffers);
    }

    /**
     * @test update
     */
    public function test_update_offers()
    {
        $offers = factory(Offers::class)->create();
        $fakeOffers = factory(Offers::class)->make()->toArray();

        $updatedOffers = $this->offersRepo->update($fakeOffers, $offers->id);

        $this->assertModelData($fakeOffers, $updatedOffers->toArray());
        $dbOffers = $this->offersRepo->find($offers->id);
        $this->assertModelData($fakeOffers, $dbOffers->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_offers()
    {
        $offers = factory(Offers::class)->create();

        $resp = $this->offersRepo->delete($offers->id);

        $this->assertTrue($resp);
        $this->assertNull(Offers::find($offers->id), 'Offers should not exist in DB');
    }
}
