<?php namespace Tests\Repositories;

use App\Models\OwnerSource;
use App\Repositories\OwnerSourceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class OwnerSourceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var OwnerSourceRepository
     */
    protected $ownerSourceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->ownerSourceRepo = \App::make(OwnerSourceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_owner_source()
    {
        $ownerSource = factory(OwnerSource::class)->make()->toArray();

        $createdOwnerSource = $this->ownerSourceRepo->create($ownerSource);

        $createdOwnerSource = $createdOwnerSource->toArray();
        $this->assertArrayHasKey('id', $createdOwnerSource);
        $this->assertNotNull($createdOwnerSource['id'], 'Created OwnerSource must have id specified');
        $this->assertNotNull(OwnerSource::find($createdOwnerSource['id']), 'OwnerSource with given id must be in DB');
        $this->assertModelData($ownerSource, $createdOwnerSource);
    }

    /**
     * @test read
     */
    public function test_read_owner_source()
    {
        $ownerSource = factory(OwnerSource::class)->create();

        $dbOwnerSource = $this->ownerSourceRepo->find($ownerSource->id);

        $dbOwnerSource = $dbOwnerSource->toArray();
        $this->assertModelData($ownerSource->toArray(), $dbOwnerSource);
    }

    /**
     * @test update
     */
    public function test_update_owner_source()
    {
        $ownerSource = factory(OwnerSource::class)->create();
        $fakeOwnerSource = factory(OwnerSource::class)->make()->toArray();

        $updatedOwnerSource = $this->ownerSourceRepo->update($fakeOwnerSource, $ownerSource->id);

        $this->assertModelData($fakeOwnerSource, $updatedOwnerSource->toArray());
        $dbOwnerSource = $this->ownerSourceRepo->find($ownerSource->id);
        $this->assertModelData($fakeOwnerSource, $dbOwnerSource->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_owner_source()
    {
        $ownerSource = factory(OwnerSource::class)->create();

        $resp = $this->ownerSourceRepo->delete($ownerSource->id);

        $this->assertTrue($resp);
        $this->assertNull(OwnerSource::find($ownerSource->id), 'OwnerSource should not exist in DB');
    }
}
