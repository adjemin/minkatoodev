<?php namespace Tests\Repositories;

use App\Models\ExcursionActivity;
use App\Repositories\ExcursionActivityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ExcursionActivityRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ExcursionActivityRepository
     */
    protected $excursionActivityRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->excursionActivityRepo = \App::make(ExcursionActivityRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_excursion_activity()
    {
        $excursionActivity = factory(ExcursionActivity::class)->make()->toArray();

        $createdExcursionActivity = $this->excursionActivityRepo->create($excursionActivity);

        $createdExcursionActivity = $createdExcursionActivity->toArray();
        $this->assertArrayHasKey('id', $createdExcursionActivity);
        $this->assertNotNull($createdExcursionActivity['id'], 'Created ExcursionActivity must have id specified');
        $this->assertNotNull(ExcursionActivity::find($createdExcursionActivity['id']), 'ExcursionActivity with given id must be in DB');
        $this->assertModelData($excursionActivity, $createdExcursionActivity);
    }

    /**
     * @test read
     */
    public function test_read_excursion_activity()
    {
        $excursionActivity = factory(ExcursionActivity::class)->create();

        $dbExcursionActivity = $this->excursionActivityRepo->find($excursionActivity->id);

        $dbExcursionActivity = $dbExcursionActivity->toArray();
        $this->assertModelData($excursionActivity->toArray(), $dbExcursionActivity);
    }

    /**
     * @test update
     */
    public function test_update_excursion_activity()
    {
        $excursionActivity = factory(ExcursionActivity::class)->create();
        $fakeExcursionActivity = factory(ExcursionActivity::class)->make()->toArray();

        $updatedExcursionActivity = $this->excursionActivityRepo->update($fakeExcursionActivity, $excursionActivity->id);

        $this->assertModelData($fakeExcursionActivity, $updatedExcursionActivity->toArray());
        $dbExcursionActivity = $this->excursionActivityRepo->find($excursionActivity->id);
        $this->assertModelData($fakeExcursionActivity, $dbExcursionActivity->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_excursion_activity()
    {
        $excursionActivity = factory(ExcursionActivity::class)->create();

        $resp = $this->excursionActivityRepo->delete($excursionActivity->id);

        $this->assertTrue($resp);
        $this->assertNull(ExcursionActivity::find($excursionActivity->id), 'ExcursionActivity should not exist in DB');
    }
}
