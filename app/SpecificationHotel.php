<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecificationHotel extends Model
{
    //
    protected $guarded = [];

    //
    public function hotel()
    {
        return $this->belongsToMany(Hotel::class, 'hotel_specification', 'specification_id', 'hotel_id');
    }
}