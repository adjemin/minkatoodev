<?php

namespace App\Models;

use Symfony\Component\HttpFoundation\ParameterBag;
use App\Models\RestaurantSpecification;
use App\Models\Offer;

class Restaurant
{

    private $offer;

    private $title;

    private $description;

    private $pictures;

    private $location_name;

    private $location_lat;

    private $location_lng;

    private $location_gps;

    /** @var array of RestaurantSpecification */
    private $specifications;

    /**
     * Restaurant constructor.
     * @param $offer
     * @param $title
     * @param $description
     * @param $pictures
     * @param $location_name
     * @param $location_lat
     * @param $location_lng
     * @param $location_gps
     * @param array $specifications
     * @param array $specialties
     */
    public function __construct($offer, $title, $description, $pictures, $location_name, $location_lat, $location_lng, $location_gps, $specifications)
    {
        $this->offer = $offer;
        $this->title = $title;
        $this->description = $description;
        $this->pictures = $pictures;
        $this->location_name = $location_name;
        $this->location_lat = $location_lat;
        $this->location_lng = $location_lng;
        $this->location_gps = $location_gps;
        $this->specifications = $specifications;
    }


    /**
     * Create a new instance.
     *
     * @return void
     */


    public static function fromJson(Offer $offer)
    {

        $meta_data = $offer->meta_data;
        $result = (array)json_decode($meta_data, true);

        if (is_array($result)) {

            $json = new ParameterBag($result);

            $json_pictures = (array)$json->get('pictures');

            $json_specifications = (array)$json->get('specifications');
            $restaurant_spectifications = [];
            foreach ($json_specifications as $item) {

                if (is_array($item)) {
                    $icon = $item['icon'];
                    $label_name = $item['label_name'];
                    $label_value = $item['label_value'];
                } else {
                    $icon = $item->icon;
                    $label_name = $item->label_name;
                    $label_value = $item->label_value;
                }

                array_push($restaurant_spectifications, new RestaurantSpectification($icon, $label_name, $label_value));
            }


            return new Restaurant (
                $offer,
                $offer->title,
                $offer->description,
                $json_pictures,
                $restaurant_spectifications,
                $offer->location_name,
                $offer->location_lat,
                $offer->location_lng,
                $offer->location_gps
            );
        } else {
            return null;
        }

    }


    public function getTitle()
    {

        if (empty($this->title)) {
            return "";
        } else {
            return $this->title;
        }
    }

    public function getDescription()
    {
        if (empty($this->description)) {
            return "";
        } else {
            return $this->description;
        }
    }

    public function getLocationName()
    {

        if (empty($this->location_name)) {
            return "";
        } else {
            return $this->location_name;
        }
    }

    public function getLocationLat()
    {
        if (empty($this->location_lat)) {
            return "";
        } else {
            return $this->location_lat;
        }
    }

    public function getLocationLng()
    {

        if (empty($this->location_lng)) {
            return "";
        } else {
            return $this->location_lng;
        }
    }

    public function getLocationGps()
    {
        if (empty($this->location_gps)) {
            return "";
        } else {
            return $this->location_gps;
        }
    }

    public function getPictures()
    {

        if (empty($this->pictures)) {
            return [];
        } else {
            return $this->pictures;
        }
    }

    public function getSpecifications()
    {

        if (empty($this->specifications)) {
            return [];
        } else {
            return $this->specifications;
        }
    }



    public function getRecipes()
    {

        $offers = Offer::where('parent_source_id', $this->offer->id)->get();
        if (empty($offers)) {
            return [];
        }

        $recipes = [];
        foreach ($offers as $offer) {
            $recipe = FoodSpeialties::fromJson($offer);
            array_push($recipes, $recipe);
        }
    }

    public function getOffer()
    {
        return $this->offer;
    }
}
