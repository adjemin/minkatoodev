<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="InvoicePayment",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="invoice_id",
 *          description="invoice_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="payment_method",
 *          description="payment_method",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_reference",
 *          description="payment_reference",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="amount",
 *          description="amount",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="currency_code",
 *          description="currency_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="creator_id",
 *          description="creator_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="creator_name",
 *          description="creator_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="creator",
 *          description="creator",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_waiting",
 *          description="is_waiting",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="is_completed",
 *          description="is_completed",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_trans_id",
 *          description="payment_gateway_trans_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_custom",
 *          description="payment_gateway_custom",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_currency",
 *          description="payment_gateway_currency",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_amount",
 *          description="payment_gateway_amount",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_payid",
 *          description="payment_gateway_payid",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_payment_date",
 *          description="payment_gateway_payment_date",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_payment_time",
 *          description="payment_gateway_payment_time",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_error_message",
 *          description="payment_gateway_error_message",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_payment_method",
 *          description="payment_gateway_payment_method",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_phone_prefixe",
 *          description="payment_gateway_phone_prefixe",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_cel_phone_num",
 *          description="payment_gateway_cel_phone_num",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_ipn_ack",
 *          description="payment_gateway_ipn_ack",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_created_at",
 *          description="payment_gateway_created_at",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_updated_at",
 *          description="payment_gateway_updated_at",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_cpm_result",
 *          description="payment_gateway_cpm_result",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_trans_status",
 *          description="payment_gateway_trans_status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_designation",
 *          description="payment_gateway_designation",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_buyer_name",
 *          description="payment_gateway_buyer_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_gateway_signature",
 *          description="payment_gateway_signature",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class InvoicePayment extends Model
{
    use SoftDeletes;

    public $table = 'invoice_payments';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'invoice_id',
        'payment_method',
        'payment_reference',
        'amount',
        'currency_code',
        'creator_id',
        'creator_name',
        'creator',
        'status',
        'is_waiting',
        'is_completed',
        'payment_gateway_trans_id',
        'payment_gateway_custom',
        'payment_gateway_currency',
        'payment_gateway_amount',
        'payment_gateway_payid',
        'payment_gateway_payment_date',
        'payment_gateway_payment_time',
        'payment_gateway_error_message',
        'payment_gateway_payment_method',
        'payment_gateway_phone_prefixe',
        'payment_gateway_cel_phone_num',
        'payment_gateway_ipn_ack',
        'payment_gateway_created_at',
        'payment_gateway_updated_at',
        'payment_gateway_cpm_result',
        'payment_gateway_trans_status',
        'payment_gateway_designation',
        'payment_gateway_buyer_name',
        'payment_gateway_signature'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'invoice_id' => 'integer',
        'payment_method' => 'string',
        'payment_reference' => 'string',
        'amount' => 'string',
        'currency_code' => 'string',
        'creator_id' => 'integer',
        'creator_name' => 'string',
        'creator' => 'string',
        'status' => 'string',
        'is_waiting' => 'boolean',
        'is_completed' => 'boolean',
        'payment_gateway_trans_id' => 'string',
        'payment_gateway_custom' => 'string',
        'payment_gateway_currency' => 'string',
        'payment_gateway_amount' => 'string',
        'payment_gateway_payid' => 'string',
        'payment_gateway_payment_date' => 'string',
        'payment_gateway_payment_time' => 'string',
        'payment_gateway_error_message' => 'string',
        'payment_gateway_payment_method' => 'string',
        'payment_gateway_phone_prefixe' => 'string',
        'payment_gateway_cel_phone_num' => 'string',
        'payment_gateway_ipn_ack' => 'string',
        'payment_gateway_created_at' => 'string',
        'payment_gateway_updated_at' => 'string',
        'payment_gateway_cpm_result' => 'string',
        'payment_gateway_trans_status' => 'string',
        'payment_gateway_designation' => 'string',
        'payment_gateway_buyer_name' => 'string',
        'payment_gateway_signature' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
