<?php
///*
//namespace App\Models;
//
//use Symfony\Component\HttpFoundation\ParameterBag;
//use App\Models\HotelSpecification;
//use App\Models\Offer;
//
//
//class Hotel
//{
//
//
//    private $offer;
//
//    private $title;
//
//    private $description;
//
//    private $model;
//
//    private $model_label;
//
//    private $brand;
//
//    private $brand_label;
//    private $year;
//    private $year_label;
//    private $body_type;
//    private $body_type_label;
//    private $color;
//    private $color_label;
//    private $doors;
//    private $doors_label;
//    private $motors;
//    private $motors_label;
//    private $gearbox;
//    private $gearbox_label;
//
//    private $pictures;
//    private $location_name;
//
//    private $location_lat;
//
//    private $location_lng;
//
//    private $location_gps;
//
//    /**
//     * Hotel constructor.
//     * @param $offer
//     * @param $title
//     * @param $description
//     * @param $model
//     * @param $model_label
//     * @param $brand
//     * @param $brand_label
//     * @param $year
//     * @param $year_label
//     * @param $body_type
//     * @param $body_type_label
//     * @param $color
//     * @param $color_label
//     * @param $doors
//     * @param $doors_label
//     * @param $motors
//     * @param $motors_label
//     * @param $gearbox
//     * @param $gearbox_label
//     * @param $pictures
//     * @param $location_name
//     * @param $location_lat
//     * @param $location_lng
//     * @param $location_gps
//     */
//    public function __construct($offer, $title, $description, $model, $model_label, $brand, $brand_label, $year, $year_label, $body_type, $body_type_label, $color, $color_label, $doors, $doors_label, $motors, $motors_label, $gearbox, $gearbox_label, $pictures, $location_name, $location_lat, $location_lng, $location_gps)
//    {
//        $this->offer = $offer;
//        $this->title = $title;
//        $this->description = $description;
//        $this->model = $model;
//        $this->model_label = $model_label;
//        $this->brand = $brand;
//        $this->brand_label = $brand_label;
//        $this->year = $year;
//        $this->year_label = $year_label;
//        $this->body_type = $body_type;
//        $this->body_type_label = $body_type_label;
//        $this->color = $color;
//        $this->color_label = $color_label;
//        $this->doors = $doors;
//        $this->doors_label = $doors_label;
//        $this->motors = $motors;
//        $this->motors_label = $motors_label;
//        $this->gearbox = $gearbox;
//        $this->gearbox_label = $gearbox_label;
//        $this->pictures = $pictures;
//        $this->location_name = $location_name;
//        $this->location_lat = $location_lat;
//        $this->location_lng = $location_lng;
//        $this->location_gps = $location_gps;
//    }
//
//
//    /**
//     * Create a new instance.
//     *
//     * @return void
//     */
//
//
//    public static function fromJson(Offer $offer)
//    {
//
//        $meta_data = $offer->meta_data;
//        $result = (array)json_decode($meta_data, true);
//
//        if (is_array($result)) {
//
//            $json = new ParameterBag($result);
//
//            $json_pictures = (array)$json->get('pictures');
//
//
//            return new Car (
//                $offer,
//                $offer->title,
//                $offer->description,
//                $model = $this->model,
//                $model_label = $this->model_label,
//                $brand = $this->brand,
//                $brand_label = $this->brand_label
//                $year = $this->year,
//                $year_label = $this->year_label,
//                $body_type = $this->body_type,
//                $body_type_label = $this->>body_type_label,
//                $color = $this->color,
//                $color_label = $this->color_label,
//                $doors = $this->doors,
//                $doors_label = $this->doors_label,
//                $motors = $this->motors,
//                $motors_label = $this->motors_label,
//                $gearbox = $this->gearbox,
//                $gearbox_label = $this->gearbox_label
//
//
//            );
//        } else {
//            return null;
//        }
//
//    }
//
//
//    public function getTitle()
//    {
//
//        if (empty($this->title)) {
//            return "";
//        } else {
//            return $this->title;
//        }
//    }
//
//    public function getDescription()
//    {
//        if (empty($this->description)) {
//            return "";
//        } else {
//            return $this->description;
//        }
//    }
//
//    public function getLocationName()
//    {
//
//        if (empty($this->location_name)) {
//            return "";
//        } else {
//            return $this->location_name;
//        }
//    }
//
//    public function getLocationLat()
//    {
//        if (empty($this->location_lat)) {
//            return "";
//        } else {
//            return $this->location_lat;
//        }
//    }
//
//    public function getLocationLng()
//    {
//
//        if (empty($this->location_lng)) {
//            return "";
//        } else {
//            return $this->location_lng;
//        }
//    }
//
//    public function getLocationGps()
//    {
//        if (empty($this->location_gps)) {
//            return "";
//        } else {
//            return $this->location_gps;
//        }
//    }
//
//    public function getPictures()
//    {
//
//        if (empty($this->pictures)) {
//            return [];
//        } else {
//            return $this->pictures;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getModel()
//    {
//        return $this->model;
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getModelLabel()
//    {
//        if (empty($this->model_label)) {
//            return "";
//        } else {
//            return $this->model_label;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getBrand()
//    {
//        if (empty($this->brand)) {
//            return "";
//        } else {
//            return $this->brand;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getBrandLabel()
//    {
//        if ($this->brand_label) {
//            return "";
//        } else {
//            return $this->brand_label;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getYear()
//    {
//        if (empty($this->year)) {
//            return "";
//        } else {
//            return $this->year;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getYearLabel()
//    {
//        if (empty($this->year_label)) {
//            return "";
//        } else {
//            return $this->year_label;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getBodyType()
//    {
//        if (empty($this->body_type)) {
//            return "";
//        } else {
//            return $this->body_type;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getBodyTypeLabel()
//    {
//        if (empty($this->body_type_label)) {
//            return "";
//        } else {
//            return $this->body_type_label;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getColor()
//    {
//        if (empty($this->color)) {
//            return "";
//        } else {
//            return $this->color;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getColorLabel()
//    {
//        if (empty($this->color_label)) {
//            return "";
//        } else {
//            return $this->color_label;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getDoors()
//    {
//        if (empty($this->doors)) {
//            return "";
//        } else {
//            return $this->doors;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getDoorsLabel()
//    {
//        if (empty($this->doors_label)) {
//            return "";
//        } else {
//            return $this->doors_label;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getMotors()
//    {
//        if (empty($this->motors)) {
//            return "";
//        } else {
//            return $this->motors;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getMotorsLabel()
//    {
//        if (empty($this->motors_label)) {
//            return "";
//        } else {
//            return $this->motors_label;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getGearbox()
//    {
//        if (empty($this->gearbox)) {
//            return "";
//        } else {
//            return $this->gearbox;
//        }
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getGearboxLabel()
//    {
//        if (empty($this->gearbox_label)) {
//            return "";
//        } else {
//            return $this->gearbox_label;
//        }
//    }
//
//
//    public function getOffer()
//    {
//        return $this->offer;
//    }
//}*/
