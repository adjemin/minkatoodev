<?php

namespace App\Models;

use Symfony\Component\HttpFoundation\ParameterBag;

class RoomEquipment{


    private $icon;
    private $name;

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct($icon, $name)
    {
        $this->icon = $icon;
        $this->name = $name;

    }

    public function getIcon(){

        if(empty($this->icon)){
            return "";
        }else{
            return $this->icon;
        }
    }

    public function getName(){
        if(empty($this->name)){
            return "";
        }else{
            return $this->name;
        }
    }


}
