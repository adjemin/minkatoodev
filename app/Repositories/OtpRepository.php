<?php

namespace App\Repositories;

use App\Models\Otp;
use App\Repositories\BaseRepository;

/**
 * Class OtpRepository
 * @package App\Repositories
 * @version September 7, 2020, 3:21 pm UTC
*/

class OtpRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Otp::class;
    }
}
