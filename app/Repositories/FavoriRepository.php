<?php

namespace App\Repositories;

use App\Models\Favori;
use App\Repositories\BaseRepository;

/**
 * Class FavoriRepository
 * @package App\Repositories
 * @version February 17, 2020, 10:41 am UTC
*/

class FavoriRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'offer_id',
        'offer'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Favori::class;
    }
}
