<?php

namespace App\Repositories;

use App\Models\Musee;
use App\Repositories\BaseRepository;

/**
 * Class MuseeRepository
 * @package App\Repositories
 * @version September 2, 2020, 4:41 pm UTC
*/

class MuseeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Musee::class;
    }
}
