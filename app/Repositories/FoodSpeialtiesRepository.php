<?php

namespace App\Repositories;

use App\Models\FoodSpeialties;
use App\Repositories\BaseRepository;

/**
 * Class FoodSpeialtiesRepository
 * @package App\Repositories
 * @version March 4, 2020, 8:56 am UTC
*/

class FoodSpeialtiesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'image',
        'name',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FoodSpeialties::class;
    }
}
