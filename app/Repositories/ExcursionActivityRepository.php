<?php

namespace App\Repositories;

use App\Models\ExcursionActivity;
use App\Repositories\BaseRepository;

/**
 * Class ExcursionActivityRepository
 * @package App\Repositories
 * @version March 4, 2020, 8:57 am UTC
*/

class ExcursionActivityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'image',
        'name',
        'slug'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ExcursionActivity::class;
    }
}
