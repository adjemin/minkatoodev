<?php

namespace App\Repositories;

use App\Models\OwnerSource;
use App\Repositories\BaseRepository;

/**
 * Class OwnerSourceRepository
 * @package App\Repositories
 * @version February 13, 2020, 1:43 pm UTC
*/

class OwnerSourceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OwnerSource::class;
    }
}
