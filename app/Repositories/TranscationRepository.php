<?php

namespace App\Repositories;

use App\Models\Transcation;
use App\Repositories\BaseRepository;

/**
 * Class TranscationRepository
 * @package App\Repositories
 * @version December 3, 2020, 9:30 am UTC
*/

class TranscationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'amount',
        'currency',
        'transaction_id',
        'designation',
        'notify_url'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Transcation::class;
    }
}
