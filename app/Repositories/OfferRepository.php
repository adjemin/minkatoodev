<?php

namespace App\Repositories;

use App\Models\Offer;
use App\Repositories\BaseRepository;

/**
 * Class OfferRepository
 * @package App\Repositories
 * @version February 13, 2020, 1:42 pm UTC
*/

class OfferRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'parent_source_id',
        'title',
        'meta_data',
        'type',
        'owner_id',
        'publisher_id',
        'has_children',
        'description',
        'original_price',
        'price',
        'currency_code',
        'has_price',
        'rating',
        'location_name',
        'location_lat',
        'location_lng',
        'location_gps',
        'slide_url',
        'slide_title',
        'is_escort',
        'escort_fees'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Offer::class;
    }

    public function rateOffer($customer, $offer, $value){

        $rate = $offer->customers()->where('customers.id', $customer->id)->pluck('rating')->first();
        if($rate) {
            if($rate !== $value) {
                $offer->customers()->updateExistingPivot ($customer->id, ['rating' => $value]);
            }
        } else {
            $offer->customers ()->attach ($customer->id, ['rating' => $value]);
        }
        return $rate;
    }

    public function isOwner($customer, $offer)
    {
        return $offer->customer()->where('customers.id', $customer->id)->exists();
    }

    public function paginateAndRate($query)
    {
        $offers = $query->paginate (config ('app.pagination'));
        return $this->setRating ($offers);
    }

    public function setRating($offers)
    {
        $offers->transform(function ($offer) {
            $this->setOfferRate ($offer);
            return $offer;
        });
        return $offers;
    }

    public function setOfferRate($offer)
    {
        $number = $offer->customers->count();
        $offer->rate = $number ? $offer->customers->pluck ('pivot.rating')->sum () / $number : 0;
    }

    public function getAllOffers()
    {
        return $this->paginateAndRate (Offer::latestWithCustomer());
    }

    public function getOffersForCategory($slug)
    {
        $query = Offer::latestWithCustomer ()->whereHas ('category', function ($query) use ($slug) {
            $query->whereSlug ($slug);
        });
        return $this->paginateAndRate ($query);
    }

    public function getOffersForUser($id)
    {
        $query = Offer::latestWithCustomer ()->whereHas ('user', function ($query) use ($id) {
            $query->whereId ($id);
        });
        return $this->paginateAndRate ($query);
    }

    public function getOffersForAlbum($slug)
    {
        $query = Offer::latestWithCustomer ()->whereHas ('albums', function ($query) use ($slug) {
            $query->whereSlug ($slug);
        });
        return $this->paginateAndRate ($query);
    }

}
