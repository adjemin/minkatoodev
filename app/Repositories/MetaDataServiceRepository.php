<?php

namespace App\Repositories;

use App\Models\MetaDataService;
use App\Repositories\BaseRepository;

/**
 * Class MetaDataServiceRepository
 * @package App\Repositories
 * @version February 13, 2020, 1:41 pm UTC
*/

class MetaDataServiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MetaDataService::class;
    }
}
