<?php

namespace App\Repositories;

use App\Models\CustomerNotification;
use App\Repositories\BaseRepository;

/**
 * Class CustomerNotifications
 * @package App\Repositories
 * @version June 4, 2020, 2:37 pm UTC
*/

class CustomerNotificationsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'subtitle',
        'action',
        'action_by',
        'meta_data',
        'type_notification',
        'is_read',
        'is_received',
        'data',
        'user_id',
        'data_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerNotification::class;
    }
}
