<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="CustomerDevice",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="customer_id",
 *          description="customer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="firebase_id",
 *          description="firebase_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="device_id",
 *          description="device_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="device_model",
 *          description="device_model",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="device_os",
 *          description="device_os",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="device_os_version",
 *          description="device_os_version",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="device_model_type",
 *          description="device_model_type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="device_meta_data",
 *          description="device_meta_data",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CustomerDevice extends Model
{
    use SoftDeletes;

    public $table = 'customer_devices';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'customer_id',
        'firebase_id',
        'device_id',
        'device_model',
        'device_os',
        'device_os_version',
        'device_model_type',
        'device_meta_data'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'firebase_id' => 'string',
        'device_id' => 'string',
        'device_model' => 'string',
        'device_os' => 'string',
        'device_os_version' => 'string',
        'device_model_type' => 'string',
        'device_meta_data' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
