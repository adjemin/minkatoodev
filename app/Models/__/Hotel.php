<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Symfony\Component\HttpFoundation\ParameterBag;
use App\Models\HotelSpecification;
use App\Models\Offer;


class Hotel extends Model
{
    private $offer;
    private $title;
    private $description;
    private $pictures;
    private $location_name;
    private $location_lat;
    private $location_lng;
    private $location_gps;

    /** @var array of HotelSpecification */
    private $specifications;


    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct($offer, $title, $description, $pictures, $specifications, $location_name, $location_lat, $location_lng, $location_gps)
    {
        $this->offer = $offer;
        $this->title = $title;
        $this->description = $description;
        $this->pictures = $pictures;
        $this->specifications = $specifications;
        $this->location_name = $location_name;
        $this->location_lat = $location_lat;
        $this->location_lng = $location_lng;
        $this->location_gps = $location_gps;

    }


    static function fromJson(Offer $offer)
    {

        $meta_data = $offer->meta_data;
        $result = (array)json_decode($meta_data, true);

        if (is_array($result)) {

            $json = new ParameterBag($result);

            $json_pictures = (array)$json->get('pictures');
            $json_specifications = (array)$json->get('specifications');
            $hotel_spectifications = [];

                foreach ($json_specifications as $item) {

                    if (is_array($item)) {
                        $icon = $item['icon'];
                        $name = $item['name'];
                    } else {
                        $icon = $item->icon;
                        $name = $item->name;
                    }

                    array_push($hotel_spectifications, new HotelSpecification($icon, $name));
                }

            return new Hotel (
                $offer,
                $offer->title,
                $offer->description,
                $json_pictures,
                $hotel_spectifications,
                $offer->location_name,
                $offer->location_lat,
                $offer->location_lng,
                $offer->location_gps
            );
            
        } else {
            return null;
        }

    }


    public function getTitle()
    {

        if (empty($this->title)) {
            return "";
        } else {
            return $this->title;
        }
    }

    public function getDescription()
    {
        if (empty($this->description)) {
            return "";
        } else {
            return $this->description;
        }
    }

    public function getLocationName()
    {

        if (empty($this->location_name)) {
            return "";
        } else {
            return $this->location_name;
        }
    }

    public function getLocationLat()
    {
        if (empty($this->location_lat)) {
            return "";
        } else {
            return $this->location_lat;
        }
    }

    public function getLocationLng()
    {

        if (empty($this->location_lng)) {
            return "";
        } else {
            return $this->location_lng;
        }
    }

    public function getLocationGps()
    {
        if (empty($this->location_gps)) {
            return "";
        } else {
            return $this->location_gps;
        }
    }

    public function getPictures()
    {

        if (empty($this->pictures)) {
            return [];
        } else {
            return $this->pictures;
        }
    }

    public function getSpecifications()
    {

        if (empty($this->specifications)) {
            return [];
        } else {
            return $this->specifications;
        }
    }

    public function getRooms()
    {

        $offers = Offer::where('parent_source_id', $this->offer->id)->get();
        if (empty($offers)) {
            return [];
        }

        $rooms = [];
        foreach ($offers as $offer) {
            $room = Room::fromJson($offer);
            array_push($rooms, $room);
        }
    }

    public function getOffer()
    {
        return $this->offer;
    }
}