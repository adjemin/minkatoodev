<?php

namespace App\Models;

use Eloquent as Model;
use App\Models\Favori;
use App\Models\OrderItem;
use App\Models\Customer;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\HotelSpecification;
use App\Models\Hotel;
use App\Models\Geographical;


/**
 * @SWG\Definition(
 *      definition="Offer",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="parent_source_id",
 *          description="parent_source_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="meta_data",
 *          description="meta_data",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="owner_id",
 *          description="owner_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="publisher_id",
 *          description="publisher_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="has_children",
 *          description="has_children",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="original_price",
 *          description="original_price",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="price",
 *          description="price",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="currency_code",
 *          description="currency_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="has_price",
 *          description="has_price",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="rating",
 *          description="rating",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_name",
 *          description="location_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_lat",
 *          description="location_lat",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_lng",
 *          description="location_lng",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_gps",
 *          description="location_gps",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="slide_url",
 *          description="slide_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="slide_title",
 *          description="slide_title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_escort",
 *          description="is_escort",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="escort_fees",
 *          description="escort_fees",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Offer extends Model
{
    use SoftDeletes;

    use Geographical;

    public $table = 'offers';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const LATITUDE  = 'location_lat';
    const LONGITUDE = 'location_lng';


    protected $dates = ['deleted_at'];

    protected static $kilometers = true;

    public function getRouteKeyName(){
        return 'slug';
    }

    public $fillable = [
        'parent_source_id',
        'title',
        'slug',
        'meta_data',
        'type',
        'owner_id',
        'publisher_id',
        'publisher_name',
        'has_children',
        'description',
        'original_price',
        'price',
        'currency_code',
        'has_price',
        'rating',
        'location_name',
        'picture1',
        'picture2',
        'picture3',
        'picture4',
        'website',
        'phone_number',
        'room',
        'etalissement',
        'location_lat',
        'location_lng',
        'location_gps',
        'slide_url',
        'slide_title',
        'is_escort',
        'escort_fees'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'parent_source_id' => 'integer',
        'title' => 'string',
        'meta_data' => 'string',
        'type' => 'string',
        'owner_id' => 'integer',
        'publisher_id' => 'integer',
        'publisher_name' => 'string',
        'has_children' => 'boolean',
        'description' => 'string',
        'original_price' => 'string',
        'price' => 'string',
        'currency_code' => 'string',
        'has_price' => 'boolean',
        'rating' => 'string',
        'location_name' => 'string',
        'picture1' => 'string',
        'picture2' => 'string',
        'picture3' => 'string',
        'picture4' => 'string',
        'website' => 'string',
        'phone_number' => 'string',
        'room' => 'string',
        'etalissement' => 'string',
        'location_lat' => 'string',
        'location_lng' => 'string',
        'location_gps' => 'string',
        'slide_url' => 'string',
        'slide_title' => 'string',
        'is_escort' => 'boolean',
        'escort_fees' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    public function favoris()
    {
        return $this->hasMany('App\Models\Favori', 'id');
    }

    public function orderitem()
    {
        return $this->hasMany('App\Models\OrderItem', 'id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Offer', 'owner_id');
    }

    public function bedrooms(){
        return $this->hasMany(Bedroom::class);
    }

    public function spec(){
        return $this->hasMany(HotelSpecification::class);
    }

    public function getSpecifications()
    {

        if (empty($this->specifications)) {
            return [];
        } else {
            return $this->specifications;
        }
    }

    public function FoodSpeialties(){
        return $this->belongsToMany('App\Models\FoodSpeialties');
    }

    public function getPictures(){
         $offer = Offer::orWhere(["id"=>$this->id])->get();

        //  dd(json_decode($offer[0]->meta_data));
         if($offer != null && count($offer) > 0) {
            $pictures = json_decode($offer[0]->meta_data)->pictures;
            return $pictures;
         }else{
             return 'not image found';
         }
    }
    // public function Customer()
    // {
    //     return $this->belongsToMany (customer::class)->withPivot('rating');
    // }
}
