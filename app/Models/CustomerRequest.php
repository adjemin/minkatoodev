<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CustomerRequest
 * @package App\Models
 * @version June 16, 2020, 3:52 pm UTC
 *
 * @property integer $customer_id
 * @property string $service_slug
 * @property integer $town_id
 * @property string $delivery_address
 * @property string $delivery_latitude
 * @property string $delivery_longitude
 * @property string $status
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $dial_code
 * @property string $phone_number
 * @property string $delivery_details
 * @property bool $is_waiting
 * @property string $payment_method_slug
 * @property number $delivery_fees
 * @property number $amount
 * @property string $currency_code
 * @property bool $is_delivery
 * @property integer $task_id
 * @property string $merchant_assignements
 * @property bool $merchants_assignement_accepted
 * @property string $reference
 */
class CustomerRequest extends Model
{
    use SoftDeletes;

    public $table = 'customer_requests';


    protected $dates = ['deleted_at'];

    const AMOUNT_FOR_WEDDING = 50000;
    const AMOUNT_FOR_STAMP = 500;
    const AMOUNT_NULL = 0;
    const DELIVERY_FEES = 500;
    const DELIVERY_FEES_NULL = 0;

    public $fillable = [
        'customer_id',
        'service_slug',
        'town_id',
        'delivery_address',
        'delivery_latitude',
        'delivery_longitude',
        'status',
        'first_name',
        'last_name',
        'phone',
        'dial_code',
        'phone_number',
        'delivery_details',
        'is_waiting',
        'payment_method_slug',
        'delivery_fees',
        'amount',
        'currency_code',
        'is_delivery',
        'task_id',
        'merchant_assignements',
        'merchants_assignement_accepted',
        'reference'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'service_slug' => 'string',
        'town_id' => 'integer',
        'delivery_address' => 'string',
        'delivery_latitude' => 'string',
        'delivery_longitude' => 'string',
        'status' => 'string',
        'first_name' => 'string',
        'last_name' => 'string',
        'phone' => 'string',
        'dial_code' => 'string',
        'phone_number' => 'string',
        'delivery_details' => 'string',
        'payment_method_slug' => 'string',
        'delivery_fees' => 'double',
        'amount' => 'double',
        'currency_code' => 'string',
        'task_id' => 'integer',
        'merchant_assignements' => 'string',
        'reference' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function town()
    {
        return $this->belongsTo(Town::class);
    }
}
