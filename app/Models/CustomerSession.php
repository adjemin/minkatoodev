<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="CustomerSession",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="token",
 *          description="token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="customer_id",
 *          description="customer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="is_active",
 *          description="is_active",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="location_address",
 *          description="location_address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="location_latitude",
 *          description="location_latitude",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="location_longitude",
 *          description="location_longitude",
 *          type="number",
 *          format="number"
 *      ),
 *      @SWG\Property(
 *          property="battery",
 *          description="battery",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="version",
 *          description="version",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="device",
 *          description="device",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ip_address",
 *          description="ip_address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="network",
 *          description="network",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="isMobile",
 *          description="isMobile",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="isTesting",
 *          description="isTesting",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="deviceId",
 *          description="deviceId",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="devicesOSVersion",
 *          description="devicesOSVersion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="devicesName",
 *          description="devicesName",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="w",
 *          description="w",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="h",
 *          description="h",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ms",
 *          description="ms",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="idapp",
 *          description="idapp",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CustomerSession extends Model
{
    use SoftDeletes;

    public $table = 'customer_session';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'token',
        'customer_id',
        'is_active',
        'location_address',
        'location_latitude',
        'location_longitude',
        'battery',
        'version',
        'device',
        'ip_address',
        'network',
        'isMobile',
        'isTesting',
        'deviceId',
        'devicesOSVersion',
        'devicesName',
        'w',
        'h',
        'ms',
        'idapp'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'token' => 'string',
        'customer_id' => 'integer',
        'is_active' => 'boolean',
        'location_address' => 'string',
        'location_latitude' => 'float',
        'location_longitude' => 'float',
        'battery' => 'string',
        'version' => 'string',
        'device' => 'string',
        'ip_address' => 'string',
        'network' => 'string',
        'isMobile' => 'boolean',
        'isTesting' => 'boolean',
        'deviceId' => 'string',
        'devicesOSVersion' => 'string',
        'devicesName' => 'string',
        'w' => 'string',
        'h' => 'string',
        'ms' => 'string',
        'idapp' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
