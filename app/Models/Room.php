<?php

namespace App\Models;

use Symfony\Component\HttpFoundation\ParameterBag;
use App\model\HotelSpecification;
use App\model\Offer;
use App\model\Hotel;


class Room extends  Hotel{

    /** @var array of HotelSpecification */
    private $establishment_equipments;

    /** @var array of HotelSpecification */
    private $room_equipments;

    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct($offer, $title, $description, $pictures, $establishment_equipments, $room_equipments )
    {
        parent::__construct($offer, $title, $description, $pictures,[]);
        $this->establishment_equipments = $establishment_equipments;
        $this->room_equipments = $room_equipments;

    }


    public static function fromJson(Offer $offer){

        $meta_data = $offer->meta_data;
        $result = (array) json_decode($meta_data, true);

        if(is_array($result)){

            $json = new ParameterBag($result);

            $json_pictures = (array) $json->get('pictures');
            $json_establishment_equipments = (array)$json->get('establishment_equipments');
            $establishment_equipments = [];
            foreach($json_establishment_equipments as $item){

                    if(is_array($item)){
                        $icon  = $item['icon'];
                        $name  = $item['name'];
                    }else{
                        $icon  = $item->icon;
                        $name  = $item->name;
                    }

                    array_push($establishment_equipments,new EstablishmentEquipment($icon, $name));
            }

            $room_equipments = [];

            $json_room_equipments = (array)$json->get('room_equipments');

            foreach($json_room_equipments as $item){

                if(is_array($item)){
                    $icon  = $item['icon'];
                    $name  = $item['name'];
                }else{
                    $icon  = $item->icon;
                    $name  = $item->name;
                }

                array_push($room_equipments,new RoomEquipment($icon, $name));
            }


            return new Room (
                $offer,
                $offer->title,
                $offer->description,
                $json_pictures,
                $establishment_equipments,
                $room_equipments

            );
        }else{
            return null;
        }

    }


    public function getEstablishmentEquipments(){

        if(empty($this->establishment_equipments)){
            return [];
        }else{
            return $this->establishment_equipments;
        }
    }

    public function getRoomEquipments(){

        if(empty($this->room_equipments)){
            return [];
        }else{
            return $this->room_equipments;
        }
    }

    public function getPrice(){

        if(empty($offer)){

            return '';

        }

        return $offer->price;
    }

    public function getOriginalPrice(){

        if(empty($offer)){

            return '';

        }

        return $offer->original_price;
    }

    public function getCurrencyCode(){

        if(empty($offer)){

            return 'XOF';

        }

        return $offer->currency_code;
    }


}
