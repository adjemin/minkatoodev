<?php

namespace App\Models;

use Eloquent as Model;
use App\Models\Customer;
use App\Models\Offer;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Favori",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="customer_id",
 *          description="customer_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="offer_id",
 *          description="offer_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="offer",
 *          description="offer",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Favori extends Model
{
    use SoftDeletes;

    public $table = 'favoris';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'customer_id',
        'offer_id',
        'offer'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'string',
        'offer_id' => 'string',
        'offer' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer','customer_id');
    }

    public function offer()
    {
        return $this->belongsTo('App\Models\Offer','offer_id');
    }

    
}
