<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerNotification extends Model
{
    use SoftDeletes;

    public $table = 'customer_notifications';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'title_en',
        'subtitle',
        'subtitle_en',
        'action',
        'action_by',
        'meta_data',
        'type_notification',
        'is_read',
        'is_received',
        'data',
        'user_id',
        'data_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'title_en' => 'string',
        'subtitle' => 'string',
        'subtitle_en' => 'string',
        'action' => 'string',
        'action_by' => 'string',
        'meta_data' => 'string',
        'type_notification' => 'string',
        'is_read' => 'boolean',
        'is_received' => 'boolean',
        'data' => 'string',
        'user_id' => 'integer',
        'data_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
}

