<?php

namespace App\Models;

use Eloquent as Model;
use App\Models\Customer;
use App\Models\OrderItem;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Order",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="customer_id",
 *          description="customer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="is_waiting",
 *          description="is_waiting",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="current_status",
 *          description="current_status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="payment_method_slug",
 *          description="payment_method_slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="delivery_fees",
 *          description="delivery_fees",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_delivered",
 *          description="is_delivered",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="delivery_date",
 *          description="delivery_date",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="amount",
 *          description="amount",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="currency_code",
 *          description="currency_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Order extends Model
{
    use SoftDeletes;

    public $table = 'orders';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'customer_id',
        'is_waiting',
        'current_status',
        'payment_method_slug',
        'delivery_fees',
        'is_delivered',
        'delivery_date',
        'amount',
        'currency_code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'is_waiting' => 'boolean',
        'current_status' => 'string',
        'payment_method_slug' => 'string',
        'delivery_fees' => 'string',
        'is_delivered' => 'boolean',
        'delivery_date' => 'date',
        'amount' => 'string',
        'currency_code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'is_waiting' => 'required',
        'payment_method_slug' => 'required',
        'is_delivered' => 'required'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer','customer_id');
    }

    public function orderitem()
    {
        return $this->hasMany('App\Models\OrderItem','id');
    }

    // public function getOrderItem(){
    //     return  OrderItem::orWhere(['order_id'=>$this->id])->get();
    // }

    // public function getCustomer(){
    //     return Customer::where('id', $this->customer_id)->get();
    // }

    
}
