<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    //
    protected $guarded = [];
    //
    public function specifications()
    {
        return $this->belongsToMany(SpecificationLocation::class, 'location_specification', 'location_id', 'specification_id');
    }
    //
    public function images()
    {
        return $this->hasMany(Media::class);
    }
}