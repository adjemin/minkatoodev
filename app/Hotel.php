<?php

namespace App;

use App\Models\HotelSpecification;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    //
    protected $guarded = [];
    //
    public function specifications()
    {
        return $this->belongsToMany(SpecificationHotel::class, 'hotel_specification', 'hotel_id', 'specification_id');
    }
    //
    public function images()
    {
        return $this->hasMany(Media::class);
    }
}