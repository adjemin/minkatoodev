<?php


namespace App\Utils\SMS;


interface SmsProvider
{

    public function sendMessage(array  $receivers, string $message);


}
