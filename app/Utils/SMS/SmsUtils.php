<?php


namespace App\Utils\SMS;


class SmsUtils
{

    public static  function  send(array  $phones, string  $message, SmsProvider $provider){
        $provider->sendMessage($phones, $message);
    }

    public static function getDefaultProvider(){
        return new BulkSmsSender();
    }
}
