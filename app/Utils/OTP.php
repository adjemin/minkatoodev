<?php

namespace App\Utils;

use Illuminate\Database\Eloquent\Collection;

class OTP
{
    /**
     * Generate a reference
     * Each item of the collection should have a 'reference' table
     *
     * @param Illuminate\Database\Eloquent\Collection
     * @param string $string
     * @param string $max_digit
     */
    public static function generate_reference(Collection $collection, $string, $max_digit)
      {
        $reference = self::random_reference($string, $max_digit);

        while (1)
        {
            if (self::check_reference($collection, $reference) == false)
            {
                break;
            }
            $reference = self::random_reference($string, $max_digit);
        }

        return $reference;
      }

    /**
     * Generates a fake token
     * @param int $maximum
     */
    public static function fakeToken($maximum)
    {
        return substr(
            str_shuffle(
                str_repeat(
                    $x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                    ceil($maximum/strlen($x))
                )
            ), 1, $maximum);
    }
    /**
     * Generate a random reference
     *
     * @param string $string
     * @param integer $max_digit
     *
     * @return string
     */
    public static function random_reference($string, $max_digit)
    {
        $temp = [];
        for ($i=0; $i <$max_digit ; $i++) {
            array_push($temp, rand(0, 9));
        }
        if (!is_null($string))
        {
            return $string.''.implode($temp);
        }
        return implode($temp);
    }

    /**
     * Check if the reference is used or not
     *
     * @param string $temp
     * @param Illuminate\Database\Eloquent\Collection $collection
     */
    public static function check_reference(Collection $collection, $temp)
    {
        foreach ($collection as $item) {
            if ($item->otp == $temp) {
                return true;
            }
        }
        return false;
    }


    /**
     * Generate an unique secret key user
     *
     * @return string $secret
     */
    public static function generateSecret()
    {
        return  self::fakeToken(8).'.'.self::generate_reference(\App\User::all(), '', 5);
    }
}