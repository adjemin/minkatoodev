<?php

namespace App\Http\Controllers;

use App\Models\HotelSpecification;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Flash;
use Imgur;
class HotelSpecificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotelSpecifications = HotelSpecification::orderByDesc('id')->paginate(20);

        return view("hotel_specifications.index", compact("hotelSpecifications"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hotel_specifications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $icon_url = null;

        $hotelSpecification = new HotelSpecification();

//        dd($request->file('icon_url'));
        if($request->file('icon_url') != null) {
            $image = Imgur::upload($request->file('icon_url'));
            $icon_url = $image->link();

        }

        $hotelSpecification->icon = $icon_url;
        $hotelSpecification->name = $request->name;




        if(!empty($hotelSpecification)) {


            $hotelSpecification->save();
            Flash::success('HotelSpecification enregistré avec succès.');
            return redirect(route('hotelSpecifications.index'));
        } else {
            return view('hotel_specifications.create')->with('errors', 'Veuillez renseigner tous les champs.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HotelSpecification  $hotelSpecification
     * @return \Illuminate\Http\Response
     */
    public function show(HotelSpecification $hotelSpecification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HotelSpecification  $hotelSpecification
     * @return \Illuminate\Http\Response
     */
    public function edit(HotelSpecification $hotelSpecification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HotelSpecification  $hotelSpecification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HotelSpecification $hotelSpecification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HotelSpecification  $hotelSpecification
     * @return \Illuminate\Http\Response
     */
    public function destroy(HotelSpecification $hotelSpecification)
    {
        //
    }
}
