<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CustomerRequest;
use Illuminate\Http\Request;
use Auth;

class BackOfficeController extends Controller
{
    /**
     * CustomerBackOfficeController constructor.
     */
    // public function __construct()
    // {
    //     $this->middleware('auth:customer');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
      */
    // public function index()
    // {
    //     // return "ok";
    //     $customer = auth()->customer;
    //     return view('customer.home', compact('customer'));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customerRequest = CustomerRequest::find($id);
        // dd($customerRequest);
        if (empty($customerRequest)) {
            //Flash::error('Customer not found');
            return redirect(route('customer.requests'))->with('error', 'Demande introuvable');
        }

        return view('customer.my_requests.show')->with('customerRequest', $customerRequest);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function myRequests()
    {

        $customerLogin = Auth::guard('customer')->user();
        // $customer = Customer::find($id)->first();
        $customer = Customer::find($customerLogin->id);
        if (!is_null($customer) || !empty($customer)) {
            $customerRequests = CustomerRequest::where('customer_id', '=', $customer->id)->orderBy('id')->get();
            // dd($customerRequests[0]->service_slug);
            return view('customer.my_request.index', compact('customerRequests'));
        }
    }

    public function profile($id)
    {
        $customer = Customer::find($id);
        $customerRequests = CustomerRequest::where('customer_id', '=', $customer->id)->get();
        return view('customer.profile.index', compact('customer', 'customerRequests'));
    }
    public function editProfile($id){
        $customer = Customer::find($id);
        $customerRequests = CustomerRequest::where('customer_id', '=', $customer->id)->get();
        return view('customer.profile.edit', compact('customer', 'customerRequests'));
    }

    public function updateProfile(Request $request, $id){

        if ($request->photo_url != null) {
            // return "ok";
            $photo_url = $request->file('photo_url')->store('images', ['disk' => 'public']);
            $photo_url = $photo_url;
        }

        $customer = Customer::find($id);
        $customer->last_name = $request->last_name;
        $customer->first_name = $request->first_name;
        $customer->email = $request->email;
        $customer->phone_number =$request->phone_number;
        $customer->photo_url = $request->file('photo_url')->store('images', ['disk' => 'public']); // "images/nom_image.extension"
        // dd($photo_url = explode('/', $photo_url)); // sépare "images", "/" et "nom_image.extension"
        // $photo_url = $photo_url[1]; // on récupère que "nom_image.extension"
        // $photo_url = photo_url;
        // dd($customer);
        $customer->save();
        // $customerRequests = CustomerRequest::where('customer_id', '=', $customer->id)->get();
        return redirect()->route("customer.profile", ['id'=>$customer->id]);
    }

}
