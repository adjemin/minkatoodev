<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Offer;


class HomeController extends Controller
{

    protected $redirectTo = '/customer/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('customer.auth:customer');
    }

    /**
     * Show the Customer dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        // return view('customer.home'); // TODO
        $hotels = Offer::where('type', '=', 'hotel')->get();
        $restaurants = Offer::where('type', '=', 'restaurant')->get();
        $excursion = Offer::where('type', 'excursion')->get();
        return view('front.index', compact('hotels', 'restaurants', 'excursion'));
    }

}
