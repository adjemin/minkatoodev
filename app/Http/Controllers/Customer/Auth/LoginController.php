<?php

namespace App\Http\Controllers\Customer\Auth;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Otp;
use App\Utils\SmsCompaigns\MTNSMSApi;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use OTPHP\TOTP;
use Symfony\Component\VarDumper\Caster\CutStub;
use RealRashid\SweetAlert\Facades\Alert;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:customer', ['except' => 'logout']);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('customer');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('customer.auth.login');
    }

    public function login(Request $request)
    {
        $creds = ['phone_number' => $request->phone_number, 'password' => "NULL"];

        if($isLogged = Auth::guard('customer')->attempt($creds))
        {
            // Alert::success('Bienvenu(e) sur E-mairie !', 'Vous êtes connectée.');
            //    return 'Success !!!';
            return redirect()->intended(route('customer.dashboard'));
        }else{
            // echo "Failled";
        Alert::error('Oops!', 'Il y a quelque chose qui cloche.');

        return redirect()->back()->withInput($request->only('email', 'remember'));
        }

    }

    public function loginWithOtp(Request $request)
    {
        $this->validate($request, [
            'phone_number' => 'required|max:8',
            // 'password' => 'required',
        ]);
        // dd($request->dial_code);
        if (is_null($request->dial_code) || empty($request->dial_code)) {
            $request->dial_code = 225;
        }
        // dd($request->dial_code);


        $l = Customer::where('phone_number', '=', $request->phone_number)->first(); // customer


        // dd($l);
        // $c = $request->password; // customer password
        if (!is_null($l) || !empty($l)) {
            $this->getOTPByPhone($request);
            return view('customer.auth.verification_code')->with('l', $l);
        } else {
            // return redirect('customer/auth/register');
            // toastr()->info("Numéro de téléphone inconnu, veuillez vous inscrire");
            return redirect()->back();
        }
    }

    public function loginNew(Request $request)
    {
        $newCustomer = Customer::find($request->l);
        // $c = $request->c;
        return view('customer.auth.quicklogin', compact('newCustomer'));
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */


    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect()->route('customer.dashboard');
    }

    public function getOTPByPhone(Request $request)
    {
        $dial_code  = $request->dial_code;
        $phone_number  = $request->phone_number;

        $lang  = 'FR';

        if (!$request->has('dial_code')) {
            return response('Dial Code not found');
        }

        if (!$request->has('phone_number')) {
            return response('Phone number not found');
        }

        // if (!$request->has('lang')) {
        //     return response('lang not found');
        // }

        $totp = TOTP::create(
            null,   // Let the secret be defined by the class
            10,     // The period (10 seconds)
            'sha1', // The digest algorithm
            6       // The output will generate 6 digits
        );

        $otp_code = $totp->now();

        //SEND SMS
        $client_phone = $dial_code . "" . $phone_number;
        // if ($lang == 'FR') {
        $message  = "Veuillez entrer ce code de vérification sur Minkatoo: " . $otp_code;
        // } else {
        //     $message  = "You must enter this code in E-Mairie: " . $otp_code;
        // }
        $otpModel = Otp::where(['code' => $otp_code])->first();
        if (empty($otpModel)) {
            $otpModel = new Otp();
            $otpModel->code = $otp_code;
            $otpModel->dial_code = $dial_code;
            $otpModel->phone_number = $phone_number;
            $deadline = Carbon::now()->addMinutes(60)->toDateTimeString();
            $otpModel->deadline = $deadline;
            $otpModel->is_used = false;
            $otpModel->save();
        }


        $otpModel->dial_code = $dial_code;
        $otpModel->phone_number = $phone_number;
        $deadline = Carbon::now()->addMinutes(60)->toDateTimeString();
        $otpModel->deadline = $deadline;
        $otpModel->is_used = false;
        $otpModel->save();

        // dd($otpModel);
        $this->sendSMS($client_phone, $message);

        // if ($result) {
        //     Flash::success('OTP sent.');
        //     return response($otpModel->toArray(), 'OTP sent');
        // } else {
        //     Flash::error('OTP not sent.');
        //     return response('Error found');
        // }
    }

    public function verifyOTPByPhone(Request $request)
    {
        dd($request->all());
        $customer = Otp::where('otps.code', '=', $request->verification_code)
            ->leftJoin('customers', 'customers.phone_number', '=', 'otps.phone_number')
            ->select('customers.*')
            ->firstOrFail();

        $dial_code  = $customer->dial_code;
        $phone_number  = $customer->phone_number;
        $code  = $request->verification_code;
        // dd($dial_code, $phone_number, $code);


        if (is_null($dial_code)) {
            return response('Dial Code is required');
        }

        if (is_null($phone_number)) {
            return response('Phone number is required');
        }

        if (is_null($code)) {
            return response('OTP is required');
        }

        $otpModel = Otp::where(['code' => $code, 'dial_code' => $dial_code, 'phone_number' => $phone_number])->first();
        // dd($otpModel);

        if (empty($otpModel)) {
            return response('OTP not found', 401);
        }

        $currentDate  = Carbon::now();
        $deadline = Carbon::parse($otpModel->deadline);
        if ($currentDate->greaterThan($deadline)) {
            return response('the delay is passed', 401);
        }

        $otpModel->is_used = true;
        $otpModel->save();

        // return $this->sendResponse($otpModel->toArray(), 'OTP verified');

        $a = Hash::make("lorem ipsum dolor sit amet random random");
        $b = Hash::make("lorem ipsum dolor sit amet random random");
        // $c plain password
        $c = $request->password;
        $d = Hash::make("lorem ipsum dolor sit amet random random");
        $e = Hash::make("lorem ipsum dolor sit amet random random");
        $l = $customer->id;
        // redirect with customer
        // TODO secure
        return redirect()->route('customer.loginNew', compact('a', 'b', 'c', 'd', 'e', 'l'));
    }

    public function sendSMS($client_phone, $message)
    {
        $sender_id = 'ADJEMIN';
        $token = "YlSf8vDE8LcYGs1oLqxqRkGDRSyuzpiJGGR";
        $msa = new MTNSMSApi($sender_id, $token);
        if (strpos($client_phone, '+') !== false) {
            $client_phone = str_replace("+", "", $client_phone);
        }

        /**
         * Send a new Campaign
         *
         * @var array $recipients {Ex: ["225xxxxxxxx", "225xxxxxxxx"]}
         * @var string $message
         */
        $recipients = [$client_phone];

        $result = $msa->newCampaign($recipients, $message);

        $result = (array)json_decode($result, true);

        $smsCount = array_key_exists('smsCount', $result) ? $result['smsCount'] : 0;


        if ($smsCount >= 1) {
            return true;
        } else {
            return false;
        }
    }
}
