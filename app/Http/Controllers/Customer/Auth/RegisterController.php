<?php

namespace App\Http\Controllers\Customer\Auth;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Otp;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Utils\SmsCompaigns\MTNSMSApi;
use Carbon\Carbon;
use Session;
use Cookie;
use Illuminate\Http\Request;
use Illuminate\Contracts\Session\Session as SessionSession;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;
use OTPHP\TOTP;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new admins as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('customer.guest:customer');
    }

    // /**
    //  * Get a validator for an incoming registration request.
    //  *
    //  * @param array $data
    //  * @return \Illuminate\Contracts\Validation\Validator
    //  */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => ['required', 'string', 'max:255'],
    //         'email' => ['required', 'string', 'email', 'max:255', 'unique:customers'],
    //         'password' => ['required', 'string', 'min:8', 'confirmed'],
    //     ]);
    // }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Customer
     */
    protected function create(Request $request)
    {
        // return response()->json($request->all());

        // return 'ok';
        if (is_null($request->dial_code) || empty($request->dial_code)) {
            $request->dial_code = 225;
        }

        $dial_code = $request->dial_code;
        $phone_number = trim($request->phone_number);
        $input["phone"] = $dial_code . "" . $phone_number;
        // dd($dial_code);
        // $input["phone"] = "225".$request->phone_number;
        $this->validate($request, [
            'last_name' => 'required',
            'first_name' => 'required',
            'email' => 'required',
            'phone_number' => 'required|size:10|unique:customers',
            // 'password' => 'required|min:8'
        ]);

        // return $request->all();


        $cus = Customer::where('phone_number', '=', $phone_number)->get();
        if ($cus->count() <= 0) {
            if ($request->phone_number != null) {
                $customer = new Customer();
                $customer->dial_code = $dial_code;
                $customer->last_name = strtoupper($request->last_name);
                $customer->first_name = $request->first_name;
                $customer->phone = $input["phone"];
                $customer->phone_number = $phone_number;
                $customer->email = $request->email;
                $customer->name = strtoupper($request->last_name) ." ". $request->first_name;
                $customer->password = Hash::make("NULL");
                $customer->save();

                $a = Hash::make("lorem ipsum dolor sit amet random random");
                $b = Hash::make("lorem ipsum dolor sit amet random random");
                // $c plain password
                // $c = $request->password;
                $d = Hash::make("lorem ipsum dolor sit amet random random");
                $e = Hash::make("lorem ipsum dolor sit amet random random");
                $l = $customer->id;


                return response()->json(["customer"=> $customer, "status"=> 200]);
                // redirect with customer
                // TODO secure
                // return redirect()->route('customer.loginNew', compact('a', 'b', 'c', 'd', 'e', 'l'));
                // $this->getOTPByPhone($request);
                // return view('customer.auth.verification_code')->with('l', $l);
            }
        } else {
            return back()->withInput();
        }
    }

    public function getOTPByPhone(Request $request)
    {
        $dial_code  = $request->dial_code;
        $phone_number  = $request->phone_number;

        $lang  = 'FR';

        if (!$request->has('dial_code')) {
            return response('Dial Code not found');
        }

        if (!$request->has('phone_number')) {
            return response('Phone number not found');
        }

        // if (!$request->has('lang')) {
        //     return response('lang not found');
        // }

        $totp = TOTP::create(
            null,   // Let the secret be defined by the class
            10,     // The period (10 seconds)
            'sha1', // The digest algorithm
            6       // The output will generate 6 digits
        );

        $otp_code = $totp->now();

        //SEND SMS
        $client_phone = $dial_code . "" . $phone_number;
        // if ($lang == 'FR') {
        $message  = "Veuillez entrer ce code de vérification sur Minkatoo: " . $otp_code;
        // } else {
        //     $message  = "You must enter this code in E-Mairie: " . $otp_code;
        // }

        $otpModel = Otp::where(['code' => $otp_code])->first();
        if (empty($otpModel)) {
            $otpModel = new Otp();
            $otpModel->code = $otp_code;
            $otpModel->dial_code = $dial_code;
            $otpModel->phone_number = $phone_number;
            $deadline = Carbon::now()->addMinutes(60)->toDateTimeString();
            $otpModel->deadline = $deadline;
            $otpModel->is_used = false;

            $otpModel->save();
        }

        $otpModel->dial_code = $dial_code;
        $otpModel->phone_number = $phone_number;
        $deadline = Carbon::now()->addMinutes(60)->toDateTimeString();
        $otpModel->deadline = $deadline;
        $otpModel->is_used = false;
        $otpModel->save();

        // dd($otpModel);
        $this->sendSMS($client_phone, $message);

        // if ($result) {
        //     return response($otpModel->toArray(), 'OTP sent');
        // } else {
        //     return response('Error found');
        // }
    }

    public function verifyOTPByPhone(Request $request)
    {
        $this->validate($request, [
            'verification_code' => 'required|max:6',
        ]);
        // dd($request->all());
        $customer = Otp::where('otps.code', '=', $request->verification_code)
            ->leftJoin('customers', 'customers.phone_number', '=', 'otps.phone_number')
            ->select('customers.*')
            ->firstOrFail();

        $dial_code  = $customer->dial_code;
        $phone_number  = $customer->phone_number;
        $code  = $request->verification_code;
        // dd($dial_code, $phone_number, $code);


        if (is_null($dial_code)) {
            return response('Dial Code is required');
        }

        if (is_null($phone_number)) {
            return response('Phone number is required');
        }

        if (is_null($code)) {
            return response('OTP is required');
        }

        $otpModel = Otp::where(['code' => $code, 'dial_code' => $dial_code, 'phone_number' => $phone_number])->first();
        // dd($otpModel);

        if (empty($otpModel)) {
            return response('OTP not found', 401);
        }

        $currentDate  = Carbon::now();
        $deadline = Carbon::parse($otpModel->deadline);
        if ($currentDate->greaterThan($deadline)) {
            return response('the delay is passed', 401);
        }

        $otpModel->is_used = true;
        $otpModel->save();

        // return $this->sendResponse($otpModel->toArray(), 'OTP verified');

        $a = Hash::make("lorem ipsum dolor sit amet random random");
        $b = Hash::make("lorem ipsum dolor sit amet random random");
        // $c plain password
        $c = $request->password;
        $d = Hash::make("lorem ipsum dolor sit amet random random");
        $e = Hash::make("lorem ipsum dolor sit amet random random");
        $l = $customer->id;
        // redirect with customer
        // TODO secure
        return redirect()->route('customer.loginNew', compact('a', 'b', 'c', 'd', 'e', 'l'));
    }

    public function sendSMS($client_phone, $message)
    {

        $sender_id = 'ADJEMIN';
        $token = "YlSf8vDE8LcYGs1oLqxqRkGDRSyuzpiJGGR";
        $msa = new MTNSMSApi($sender_id, $token);
        if (strpos($client_phone, '+') !== false) {
            $client_phone = str_replace("+", "", $client_phone);
        }


        /**
         * Send a new Campaign
         *
         * @var array $recipients {Ex: ["225xxxxxxxx", "225xxxxxxxx"]}
         * @var string $message
         */
        $recipients = [$client_phone];

        $result = $msa->newCampaign($recipients, $message);

        $result = (array)json_decode($result, true);

        $smsCount = array_key_exists('smsCount', $result) ? $result['smsCount'] : 0;


        if ($smsCount >= 1) {

            return true;
        } else {
            return false;
        }
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('customer.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('customer');
    }

}
