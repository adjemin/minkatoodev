<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOfferRequest;
use App\Http\Requests\UpdateOfferRequest;
// use App\Models\Hotel;
use App\Hotel;
use App\Models\HotelSpecification;
use App\Repositories\OfferRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Offer;
use App\Models\Customer;
use App\Models\FoodSpeialties;
use App\Models\ExcursionActivity;
use App\Models\Musee;
use Flash;
use Auth;
use Imgur;
// use Yish\Imgur\Upload;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Response;
use Symfony\Component\HttpFoundation\ParameterBag;
//
use GuzzleHttp\Exception\RequestException;
use PhpParser\Node\Stmt\Foreach_;

class OfferController extends AppBaseController
{


    /** @var  OfferRepository */
    private $offerRepository;

    public function __construct(OfferRepository $offerRepo)
    {
        // $this->middleware('customer.auth:customer');
        $this->offerRepository = $offerRepo;
    }

    /**
     * Display a listing of the Offer.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {

        // $offers = $this->offerRepository->orderByDe()->get();
        $offers = Offer::orderBy("id", "desc")->get();

        // return $offers;
        return view('offers.index')->with('offers', $offers);

        //$offers = Offer::orderBy('created_at', 'desc')->get();
        // if(auth()->user()){
        //     return view('offers.index')->with('offers', $offers);
        // } else {
        //     return redirect(route("login"));
        // }
    }

    /**
     * Show the form for creating a new Offer.
     *
     * @return Response
     */
    public function create(Request $request)
    {

        $offer = null;
        $hotel = Offer::where('type', 'hotel')->get();

        $residence = Offer::where('type', 'residence')->get();

        $restaurant = Offer::where('type', 'restaurant')->get();

        $musee = Offer::where('type', 'musee')->get();

        $voiture = Offer::where('type', 'car')->get();

        $excursion = Offer::where('type', 'excursion')->get();

        $customer = Customer::where('customer_type', '!=', 'client')->get();

        if(auth()->user()){
            return view('offers.create')
            ->with('offer', $offer)
            ->with('hotel', $hotel)
            ->with('restaurant', $restaurant)
            ->with('residence', $residence)
            ->with('musee', $musee)
            ->with('customer', $customer)
            ->with('voiture', $voiture)
            ->with('excursion', $excursion);
        } else {
            return redirect(route("login"));
        }
    }

    /**
     * Store a newly created Offer in storage.
     *
     * @param CreateOfferRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
        // TODO validate !!!!

        // ! Required !
        $food1 = FoodSpeialties::where('id', 1)->first();
        $food2 = FoodSpeialties::where('id', 2)->first();
        $food3 = FoodSpeialties::where('id', 3)->first();
        $food4 = FoodSpeialties::where('id', 4)->first();

        $foods = FoodSpeialties::all();

        $excur1 = ExcursionActivity::where('id', 1)->first();
        $excur2 = ExcursionActivity::where('id', 2)->first();
        $excur3 = ExcursionActivity::where('id', 3)->first();
        $excur4 = ExcursionActivity::where('id', 4)->first();

        $excursions = ExcursionActivity::all();

        // Restaurants // Excursion // Residence
        $specification1 = $request->specification1;
        $specification2 = $request->specification2;
        // Residence

        $room1 = $request->room1;
        $room2 = $request->room2;
        $room3 = $request->room3;

        // rooms
        $hotel1 = $request->hotel1;
        $hotel2 = $request->hotel2;
        $hotel3 = $request->hotel3;

        // global variables
        $slide_url ?? null;
        $parent_source_id = $request->parent_source_id ?? null;

        $meta = [];

        if ($request["type"] == "aucun") {
            Flash::error('Type d\'offre obligatoire.');
            return redirect()->back();
        }

        // Enregistrement des images
        $photos = $request->file('pictures');
        $pictures = [];

        $location_gps = $request->location_lat . ',' . $request->location_lng;

        // $hotelSpecification = new HotelSpecification(
        //     '',
        //     $request->specification_hotel,
        // );

        if($photos != null){
            for($i = 0; $i < sizeof($photos); $i++){
                ${'image'.($i)} = Imgur::upload($photos[$i]) ?? null;
                ${'picture'.($i)} = ${'image'.($i)}->link() ?? null;
                array_push($pictures, ${'picture'.($i)});
            }
        }   


        // if($photos != null){
        //     for($i = 0; $i < sizeof($photos); $i++){
        //         // ${'image'.($i+1)} = Imgur::upload($photos[$i]) ?? null;
        //         // ${'picture'.($i+1)} = ${'image'.($i+1)}->link() ?? null;
        //         ${'image'.($i)} = $photos[$i]->store('images', ['disk' => 'public']);
        //         ${'picture'.($i)} = ${'image'.($i)} ?? null;
        //         array_push($pictures, ${'picture'.($i)});
        //     }
        // }

        //     }
        // }
        // $image1 = Imgur::upload($photos[0]) ?? null;
        // $picture1 = $image1->link() ?? null;
        // $image2 = Imgur::upload($photos[1]) ?? null;
        // $picture2 = $image2->link() ?? null;
        // $image3 = Imgur::upload($photos[2]) ?? null;
        // $picture3 = $image3->link() ?? null;

        if ($request->slide_url != null) {
            $photo = $request->file('slide_url');
            $image = Imgur::upload($photo);
            $slide_url = $image->link();
        }
        // if (request()->has('slide_url')) {
        //     $offer->update([
        //         'slide_url' => request()->file('slide_url')->store('images', 'public'),
        //     ]);
        // }



        // if ($request->slide_url != null) {

        //     $slide_url = $request->file('slide_url')->store('images', ['disk' => 'public']);
        //     $slide_url = $slide_url;
        // }
        // if ($request->slide_url != null) {
        //     // return "ok";
        //     $slide_url = $request->file('slide_url')->store('images', ['disk' => 'public']);
        //     $slide_url = $slide_url;
        // }
            // *** Non Sauvegarde du type aucun

        // *** Sauvegarde du type hotel
        if($input["type"] == 'hotel'){

            // Enregistrement meta // hotel

            $specifications = [];
            foreach ($request->Hotelspecification as $key) {
                $spec = Hotelspecification::where([
                    "name" => $key
                ])->first();
                
                // var_dump($spec->icon);
                 $icon = $spec->icon;
                 
                array_push(
                    $specifications,
                    ["icon" => $icon, "name" => $spec->name]
                );
            }
            
            die();

            $meta = [
                "specifications" => $specifications,
                "pictures" => $pictures

            ];
        }
            //
            // *** Sauvegarde du type restaurant

        if ($input["type"] == "restaurant" || $input["type"] == "menu") {
            $food_spe=[];
            
            $food_specialties = $request->food_specialties;
            
            if ($food_specialties !=null && count($food_specialties) > 0 ) {
                foreach ($food_specialties as $food_specialty) {
                $food_specialty = FoodSpeialties::where('slug', $food_specialty)->first();
                    // dd($food_specialty);
                
                    if($food_specialty != null){
                        array_push($food_spe,[
                            "image"=>$food_specialty->image,
                            "name"=>$food_specialty->name,
                            "slug"=>$food_specialty->slug,
                            "id"=>$food_specialty->id
                        ]);
                    }  
                }
                // return $food_spe;
            }
            
            // $restaurant = new $this->food_restaurant;
            $offer = Offer::where("type", $input["type"])->get();
            $specification1 = $request->specification1 ?? '';
            $specification2 = $request->specification2 ?? '';
            // Enregistrement meta
            $meta = [
                "specialties" => $food_spe,
                "pictures" => $pictures
                // [
                //     $picture1,
                //     $picture2,
                //     $picture3
                // ]
            ];
            // dd($specification1);
        }




        // if($input["type"]== "excursion'){

        // }
            //
            // *** Sauvegarde du type voiture
        if ($input["type"] == 'car') {
            if ($request->model == null || $request->brand == null || $request->year == null || $request->body_type == null || $request->color == null
                || $request->doors == null || $request->motors == null || $request->gearbox == null) {
                Flash::error('Les informations sur la voiture doivent être renseignée');
                return redirect()->back();
            }

            $meta = [
                "model" => $request->model,
                // "model_label" => "Modèle",
                "brand" => $request->brand,
                // "brand_label" => "Marque",
                "year" => $request->year,
                // "year_label" => "Année",
                "body_type" => $request->body_type,
                // "body_type_label" => "Type de carroserie",
                "color" => $request->color,
                // "color_label" => "Couleur",
                "doors" => $request->doors,
                // "doors_label" => "Nombre de portières",
                "motors" => $request->motors,
                // "motors_label" => "Moteur",
                "gearbox" => $request->gearbox,
                // "gearbox_label" => "Boîte de vitesse",
                // "pictures" => $pictures,
            ];
        }
            //
            // *** Sauvegarde du type musée

        if ($input["type"] == 'musee') {

            $meta = [
                "specifications" => [
                    [
                        "icon" => "",
                        "label_name" => "Visiter le Site Web",
                        "label_value" => $specification1
                    ],
                    [
                        "icon" => "https://image.adjemin.com/eoeooe.png",
                        "label_name" => "Telephone",
                        "label_value" => $specification2
                    ]
                ],
                "pictures" => $pictures,
            ];
        }

            //
            // *** Sauvegarde du type excusion

        if ($input["type"] == 'excursion') {
            $offer = Offer::where("type", $input["type"])->get();
            
             $excursion_activities = $request->excursion_activities;
             
             
            //     ${'image'.($i)} = Imgur::upload($photos[$i]) ?? null;
            //     ${'picture'.($i)} = ${'image'.($i)}->link() ?? null;
            //     array_push($pictures, ${'picture'.($i)});
            $excursion_items = [];
             
             
    // if($excursion_activities != null){
    //     for ($i=0; $i < sizeof($excursion_activities) ; $i++) { 
    // // var_dump($excursion_activities[$i]);
    //         ${'excursion_activity'.($i) }= 
    //         ExcursionActivity::where('slug', $excursion_activities[$i])->get(['id','image','name','slug']) ?? null;
       
    //         array_push($exursion_items, ${'excursion_activity'.($i) });
    //     }
        
    //     return $exursion_items;
    // }
    $foods = [];
    
    if ($excursion_activities !=null && count($excursion_activities) > 0 ) {
        foreach ($excursion_activities as $excursion_activity) {
        $excursion_activity = ExcursionActivity::where('slug', $excursion_activity)->first();

            if ($excursion_activity != null) {
                array_push($excursion_items, [
                    "image" => $excursion_activity->image,
                    "name" => $excursion_activity->name,
                    "slug" => $excursion_activity->slug,
                    "id" => $excursion_activity->id
                ]);
            }  
}
        // return $excursion_items;
    }
    
            //   $excursion_activities;
            
            // $excursion_activities = ExcursionActivity::find($request);
            
            // return $offer;
            

            $meta = [
                "specifications" => [
                    [
                        "icon" => "",
                        "label_name" => "Visiter le Site Web",
                        "label_value" => $specification1
                    ],
                    [
                        "icon" => "",
                        "label_name" => "Telephone",
                        "label_value" => $specification2
                    ]
                ],
                
                "specialties" => $excursion_items,
 
                // "excursion_activities" => $request->excursion_activities,
                "pictures" => $pictures
            ];

            // return $meta;

        }
            //
            // *** Sauvegarde du type residence

        if ($input["type"] == 'residence') {

            $meta = [
                "room_equipments" => [
                    [
                        "icon" => "",
                        "name" => $room1
                    ],
                    [
                        "icon" => "",
                        "name" => $room2
                    ],
                    [
                        "icon" => "",
                        "name" => $room3
                    ]
                ],
                "pictures" => $pictures,
            ];
        }
            //
            // *** Sauvegarde du type chambre

        if ($input["type"] == 'chambre') {

            $meta = [
                "establishment_equipments" => [
                    [
                        "icon" => "",
                        "name" => $hotel1
                    ],
                    [
                        "icon" => "",
                        "name" => $hotel2
                    ],
                    [
                        "icon" => "",
                        "name" => $hotel3
                    ]
                ],
                "room_equipments" => [
                    [
                        "icon" => "",
                        "name" => $room1
                    ],
                    [
                        "icon" => "",
                        "name" => $room2
                    ],
                    [
                        "icon" => "",
                        "name" => $room3
                    ]
                ],
                "pictures" => $pictures,
            ];
            // dd($meta);
            // dd($request);
        }

        // *** Enfin on enregistre l'offre
        $offer = Offer::create([
                'parent_source_id' => $parent_source_id,
                'title' => $request->title,
                // 'slug' =>str_slug($request->title),
                'meta_data' => json_encode($meta, JSON_UNESCAPED_SLASHES),
                // 'meta_data' => $meta,
                'type' => $request->type,
                // 'owner_id' => $request->owner_id,
                'owner_id' => $input["owner_id"],
                'publisher_id' => $request->publisher_id,
                // 'publisher_name' => Auth::user()->name,
                // 'has_children' => $request->has_children,
                'description' => $request->description,
                'location_name' => $request->location_name,
                'picture1' => sizeof($pictures) >= 1 ? $pictures[0] : null,
                'picture2' => sizeof($pictures) >= 2 ? $pictures[1] : null,
                'picture3' => sizeof($pictures) >= 3 ? $pictures[2] : null,
                'picture4' => sizeof($pictures) == 4 ? $pictures[3] : null,
                'room' => $request->room1 . ',' . $request->room2 . ',' . $request->room3,
                'etablissement' => $request->hotel1 . ',' . $request->hotel2 . ',' . $request->hotel3,
                // 'specification' => $request->specification1 . ' / ' . $request->specification2,
                'location_lat' => $request->location_lat,
                'location_lng' => $request->location_lng,
                'location_gps' => $location_gps,
                'original_price' => $request->original_price,
                'price' => $request->price,
                'currency_code' => $request->currency_code,
                'has_price' => $request->has_price,
                'rating' => $request->rating,
                'slide_title' => $request->slide_title,
                'is_escort' => $request->is_escort,
                'escort_fees' => $request->escort_fees,
                'slide_url' => $slide_url,
                'phone_number' =>$request->phone_number,
                'website' =>$request->website
            ]);
            // dd($offer);

            Flash::success('Offre sauvegardée avec success !');

            return redirect()->route('offers.index');

        if(auth()->user()){
            return redirect(route('offers.index'));
        } else {
            return redirect(route("login"));
        }
    }


    /**
     * Display the specified Offer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $offer = $this->offerRepository->find($id);

        // return $offer;

        $hotel = Offer::where('type', 'hotel')->get();

        $restaurant = Offer::where('type', 'restaurant')->get();

        $residence = Offer::where('type', 'residence')->get();

        $musee = Offer::where('type', 'musee')->get();

        $voiture = Offer::where('type', 'car')->get();

        $excursion = Offer::where('type', 'excursion')->get();

        $customer = Customer::where('customer_type', '!=', 'client')->get();

        if (empty($offer)) {
            Flash::error('Offer not found');

            return redirect(route('offers.index'));
        }

        if (auth()->user()) {
            return view('offers.show')
            ->with('offer', $offer)
            ->with('hotel', $hotel)
            ->with('restaurant', $restaurant)
            ->with('musee', $musee)
            ->with('customer', $customer)
            ->with('residence', $residence)
            ->with('voiture', $voiture)
            ->with('excursion', $excursion);
        } else {
            return redirect(route("login"));
        }
    }

    /**
     * Show the form for editing the specified Offer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $offer = $this->offerRepository->find($id);

        $hotel = Offer::where('type', 'hotel')->get();

        $residence = Offer::where('type', 'residence')->get();

        $restaurant = Offer::where('type', 'restaurant')->get();

        $musee = Offer::where('type', 'musee')->get();

        $voiture = Offer::where('type', 'car')->get();

        $excursion = Offer::where('type', 'excursion')->get();

        $customer = Customer::where('customer_type', '!=', 'client')->get();

        if (empty($offer)) {
            Flash::error('Offer not found');

            return redirect(route('offers.index'));
        }

        if(auth()->user()){
            return view('offers.edit')
            ->with('offer', $offer)
            ->with('hotel', $hotel)
            ->with('restaurant', $restaurant)
            ->with('musee', $musee)
            ->with('customer', $customer)
            ->xith('residence', $residence)
            ->with('voiture', $voiture)
            ->with('excursion', $excursion);
        } else {
            return redirect(route("login"));
        }
    }

    /**
     * Update the specified Offer in storage.
     *
     * @param int $id
     * @param UpdateOfferRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOfferRequest $request)
    {
        $offer = $this->offerRepository->find($id);

        if (empty($offer)) {
            Flash::error('Offer not found');

            return redirect(route('offers.index'));
        }

        $slide_url = null;

        if ($request->slide_url != null) {
            $photo = $request->file('slide_url');
            $image = Imgur::upload($photo);
            $slide_url = $image->link();
        }
        // dd($slide_url);


        $photos = $request->file('pictures');
        $pictures = [];

        if($photos != null){
            for($i = 0; $i < sizeof($photos); $i++){
                ${'image'.($i)} = Imgur::upload($photos[$i]) ?? null;
                ${'picture'.($i)} = ${'image'.($i)}->link() ?? null;
                array_push($pictures, ${'picture'.($i)});
            }
        }

        $meta = [];

        if($request->type = "hotel"){
            $meta = [
                "specifications" => [
                [
                    "icon" => "",
                    "name" => $request->hotel1],
                    ["icon" => "",
                    "name" => $request->hotel2],
                    ["icon" => "",
                    "name" => $request->hotel3]
                ],

            "pictures" => $pictures
            ];
        }

        $offer->parent_source_id = $request->parent_source_id;
        $offer->title = $request->title;
        $offer->meta_data = json_encode($meta, JSON_UNESCAPED_SLASHES);
        $offer->type = $request->type;
        $offer->owner_id = $request->owner_id;
        $offer->publisher_id = $request->publisher_id;
        $offer->publisher_name = Auth::user()->name;
        $offer->has_children = $request->has_children;
        $offer->description = $request->description;
        $offer->location_name = $request->location_name;
        $offer->picture1 = sizeof($pictures) >= 1 ? $pictures[0] : null;
        $offer->picture2 = sizeof($pictures) >= 2 ? $pictures[1] : null;
        $offer->picture3 = sizeof($pictures) >= 3 ? $pictures[2] : null;
        $offer->picture4 = sizeof($pictures) == 4 ? $pictures[3] : null;
        $offer->room = $request->room1 . ',' . $request->room2 . ',' . $request->room3;
        $offer->etablissement = $request->hotel1 . ',' . $request->hotel2 . ',' . $request->hotel3;
        $offer->specification = $request->specification1 . ' / ' . $request->specification2;
        $offer->location_lat = $request->location_lat;
        $offer->location_lng = $request->location_lng;
        $offer->location_gps = $request->location_gps;
        $offer->original_price = $request->original_price;
        $offer->price = $request->price;
        $offer->currency_code = $request->currency_code;
        $offer->has_price = $request->has_price;
        $offer->rating = $request->rating;
        $offer->slide_title = $request->slide_title;
        $offer->is_escort = $request->is_escort;
        $offer->escort_fees = $request->escort_fees;

        $offer->slide_url = $slide_url;
        $offer->save();
        $offer = $this->offerRepository->update($request->all(), $id);

        Flash::success('Offer updated successfully.');
        if(auth()->user()){
            return redirect(route('offers.index'));
        } else {
            return redirect(route("login"));
        }
    }

    /**
     * Remove the specified Offer from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $offer = $this->offerRepository->find($id);
        // dd($offer->id);
        if (empty($offer)) {
            Flash::error('Offer not found');

            return redirect(route('offers.index'));
        }

        $this->offerRepository->delete($offer->id);


        Flash::success('Offer deleted successfully.');
        if(auth()->user()){
            return redirect(route('offers.index'));
        } else {
            return redirect(route("login"));
        }
    }

    public function search(Request $request)
    {


        $offer = $this->offerRepository->find($id);
        // dd($offer->id);
        if (empty($offer)) {
            Flash::error('Offer not found');

            return redirect(route('offers.index'));
        }

        $this->offerRepository->delete($offer->id);


        Flash::success('Offer deleted successfully.');
        if(auth()->user()){
            return redirect(route('offers.index'));
        } else {
            return redirect(route("login"));
        }
    }

    public function rate(Request $request, Offer $offer){

        $customer = $request->user();
        // Is user image owner ?
        if($this->OfferRepository->isOwner ($customer, $offer)) {
            return response()->json(['status' => 'no']);
        }
        // Rating
        $rate = $this->OfferRepository->rateImage ($customer, $offer, $request->value);
        $this->imageRepository->setImageRate ($offer);
        return [
            'status' => 'ok',
            'id' => $offer->id,
            'value' => $offer->rate,
            'count' => $offer->customer->count(),
            'rate' => $rate
        ];
    }
}
