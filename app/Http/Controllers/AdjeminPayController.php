<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\InvoicePayment;
use Illuminate\Http\Request;

class AdjeminPayController extends Controller
{
    //
    public function changeStatus($id){
        // dd($id);

        $invoicePayment = InvoicePayment::find($id);
        $invoicePayment->status = 'paid';
        $invoicePayment->save();

        return response($invoicePayment)->json();

        $invoice = Invoice::find($invoicePayment->invoice_id);

        $invoice->status= 'paid';
        $invoice->is_paid_by_customer= true;
        $invoice->save();

        $order = Order::find($invoice->order_id);
        $order->status = 'confirmed';
        $order->save();

        return redirect()->route('customerRequest', ['id'=>$invoicePayment->creator_id]);
    }

}
