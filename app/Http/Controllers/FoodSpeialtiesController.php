<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFoodSpeialtiesRequest;
use App\Http\Requests\UpdateFoodSpeialtiesRequest;
use App\Repositories\FoodSpeialtiesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class FoodSpeialtiesController extends AppBaseController
{
    /** @var  FoodSpeialtiesRepository */
    private $foodSpeialtiesRepository;

    public function __construct(FoodSpeialtiesRepository $foodSpeialtiesRepo)
    {
        $this->foodSpeialtiesRepository = $foodSpeialtiesRepo;
    }

    /**
     * Display a listing of the FoodSpeialties.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $foodSpeialties = $this->foodSpeialtiesRepository->all();

        return view('food_speialties.index')
            ->with('foodSpeialties', $foodSpeialties);
    }

    /**
     * Show the form for creating a new FoodSpeialties.
     *
     * @return Response
     */
    public function create()
    {
        return view('food_speialties.create');
    }

    /**
     * Store a newly created FoodSpeialties in storage.
     *
     * @param CreateFoodSpeialtiesRequest $request
     *
     * @return Response
     */
    public function store(CreateFoodSpeialtiesRequest $request)
    {
        $input = $request->all();

        $foodSpeialties = $this->foodSpeialtiesRepository->create($input);

        Flash::success('Food Speialties saved successfully.');

        return redirect(route('foodSpeialties.index'));
    }

    /**
     * Display the specified FoodSpeialties.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $foodSpeialties = $this->foodSpeialtiesRepository->find($id);

        if (empty($foodSpeialties)) {
            Flash::error('Food Speialties not found');

            return redirect(route('foodSpeialties.index'));
        }

        return view('food_speialties.show')->with('foodSpeialties', $foodSpeialties);
    }

    /**
     * Show the form for editing the specified FoodSpeialties.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $foodSpeialties = $this->foodSpeialtiesRepository->find($id);

        if (empty($foodSpeialties)) {
            Flash::error('Food Speialties not found');

            return redirect(route('foodSpeialties.index'));
        }

        return view('food_speialties.edit')->with('foodSpeialties', $foodSpeialties);
    }

    /**
     * Update the specified FoodSpeialties in storage.
     *
     * @param int $id
     * @param UpdateFoodSpeialtiesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFoodSpeialtiesRequest $request)
    {
        $foodSpeialties = $this->foodSpeialtiesRepository->find($id);

        if (empty($foodSpeialties)) {
            Flash::error('Food Speialties not found');

            return redirect(route('foodSpeialties.index'));
        }

        $foodSpeialties = $this->foodSpeialtiesRepository->update($request->all(), $id);

        Flash::success('Food Speialties updated successfully.');

        return redirect(route('foodSpeialties.index'));
    }

    /**
     * Remove the specified FoodSpeialties from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $foodSpeialties = $this->foodSpeialtiesRepository->find($id);

        if (empty($foodSpeialties)) {
            Flash::error('Food Speialties not found');

            return redirect(route('foodSpeialties.index'));
        }

        $this->foodSpeialtiesRepository->delete($id);

        Flash::success('Food Speialties deleted successfully.');

        return redirect(route('foodSpeialties.index'));
    }
}
