<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateExcursionActivityRequest;
use App\Http\Requests\UpdateExcursionActivityRequest;
use App\Repositories\ExcursionActivityRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\ExcursionActivity;
use App\Http\Controllers\Controller;
use App\Models\Offer;

class ExcursionActivityController extends AppBaseController
{
    /** @var  ExcursionActivityRepository */
    private $excursionActivityRepository;

    public function __construct(ExcursionActivityRepository $excursionActivityRepo)
    {
        $this->excursionActivityRepository = $excursionActivityRepo;
    }

    /**
     * Display a listing of the ExcursionActivity.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $ExcursionActivity = $this->excursionActivityRepository->all();

        return view('excursion_activities.index')
            ->with('excursionActivities', $ExcursionActivity);
        //

    }

    public function indexFront(Request $request){

        $excursions = Offer::where('type', 'excursion')->take(4)->get();
        // dd($hotels[0]->slide_url);
        return view("front.excursion.index", compact("excursions"));
    }

    public function excursionBySpecification(Request $request, $speciality)
    {

        // dd( $speciality);
        $excursions = Offer::where('type', '=', 'excursion')->get();
        $query = collect($excursions)->filter(function($excursion) use ($speciality) {
            $specialties = json_decode($excursion->meta_data)->specialties;
            // dd($specialities);
            
            foreach ($specialties as $item){
                // dd($item->slug);
                if($item != null){
                    if($item->slug == $speciality){
                        return $excursion;
                    } 
                }
                
            }
        });

        // return response()->json(['data']);
        // return response()->json(['data']);

        switch ($speciality) {
            case "excursions_et_visites":
                return view("front.excursion.excursion_speciality.excursion_visit", compact("query"));
                break;
            case "visites_guidées":
                return view("front.excursion.excursion_speciality.guided_tour", compact("query"));
                break;
            case "sites_historiques":
                return view("front.excursion.excursion_speciality.touristic_site", compact("query"));
            break;
            case "randonnees":
                return view("front.excursion.excursion_speciality.hiking", compact("query"));
                break;
        }
    }


    /**
     * Show the form for creating a new ExcursionActivity.
     *
     * @return Response
     */
    public function create()
    {
        return view('excursion_activities.create');
    }

    /**
     * Store a newly created ExcursionActivity in storage.
     *
     * @param CreateExcursionActivityRequest $request
     *
     * @return Response
     */
    public function store(CreateExcursionActivityRequest $request)
    {
        $input = $request->all();

        $excursionActivity = $this->excursionActivityRepository->create($input);

        Flash::success('Excursion Activity saved successfully.');

        return redirect(route('excursionActivities.index'));
    }

    /**
     * Display the specified ExcursionActivity.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $excursionActivity = $this->excursionActivityRepository->find($id);

        if (empty($excursionActivity)) {
            Flash::error('Excursion Activity not found');

            return redirect(route('excursionActivities.index'));
        }

        return view('excursion_activities.show')->with('excursionActivity', $excursionActivity);


    }

    public function showFront($id){

        $excursion = Offer::find($id);
        return view("front.excursion.show", compact("excursion"));
    }

    public function requestFormex($id)
    {
        $excursion = Offer::find($id);

        return view("front.excursion.booking_excur", compact('excursion'));
    }

    /**
     * Show the form for editing the specified ExcursionActivity.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $excursionActivity = $this->excursionActivityRepository->find($id);

        if (empty($excursionActivity)) {
            Flash::error('Excursion Activity not found');

            return redirect(route('excursionActivities.index'));
        }

        return view('excursion_activities.edit')->with('excursionActivity', $excursionActivity);
    }

    /**
     * Update the specified ExcursionActivity in storage.
     *
     * @param int $id
     * @param UpdateExcursionActivityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateExcursionActivityRequest $request)
    {
        $excursionActivity = $this->excursionActivityRepository->find($id);

        if (empty($excursionActivity)) {
            Flash::error('Excursion Activity not found');

            return redirect(route('excursionActivities.index'));
        }

        $excursionActivity = $this->excursionActivityRepository->update($request->all(), $id);

        Flash::success('Excursion Activity updated successfully.');

        return redirect(route('excursionActivities.index'));
    }

    /**
     * Remove the specified ExcursionActivity from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $excursionActivity = $this->excursionActivityRepository->find($id);

        if (empty($excursionActivity)) {
            Flash::error('Excursion Activity not found');

            return redirect(route('excursionActivities.index'));
        }

        $this->excursionActivityRepository->delete($id);

        Flash::success('Excursion Activity deleted successfully.');

        return redirect(route('excursionActivities.index'));
    }
}
