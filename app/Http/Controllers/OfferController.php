<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOfferRequest;
use App\Http\Requests\UpdateOfferRequest;
use App\Models\Hotel;
use App\Models\HotelSpecification;
use App\Repositories\OfferRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Offer;
use App\Models\Customer;
use App\Models\FoodSpeialties;
use App\Models\ExcursionActivity;
use Flash;
use Auth;
use Imgur;
use Response;
use Symfony\Component\HttpFoundation\ParameterBag;

class OfferController extends AppBaseController
{
    /** @var  OfferRepository */
    private $offerRepository;

    public function __construct(OfferRepository $offerRepo)
    {
        $this->offerRepository = $offerRepo;
    }

    /**
     * Display a listing of the Offer.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $offers = $this->offerRepository->all();
        // $offers = Offer::orderBy('created_at', 'desc')->get();
        // dd($offers);

        return view('offers.index')->with('offers', $offers);
    }

    /**
     * Show the form for creating a new Offer.
     *
     * @return Response
     */
    public function create()
    {
        $hotel = Offer::where('type', 'hotel')->get();

        $restaurant = Offer::where('type', 'restaurant')->get();

        $residence = Offer::where('type', 'residence')->get();

        $musee = Offer::where('type', 'musee')->get();

        $voiture = Offer::where('type', 'car')->get();

        $excursion = Offer::where('type', 'excursion')->get();

        $customer = Customer::where('customer_type', '!=', 'client')->get();

        return view('offers.create')
            ->with('hotel', $hotel)
            ->with('restaurant', $restaurant)
            ->with('residence', $residence)
            ->with('musee', $musee)
            ->with('customer', $customer)
            ->with('voiture', $voiture)
            ->with('excursion', $excursion);
    }

    /**
     * Store a newly created Offer in storage.
     *
     * @param CreateOfferRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();


//        $food1 = FoodSpeialties::where('id', 1)->first();
//        $food2 = FoodSpeialties::where('id', 2)->first();
//        $food3 = FoodSpeialties::where('id', 3)->first();
//        $food4 = FoodSpeialties::where('id', 4)->first();

//        $foods = FoodSpeialties::all();

        /*$excur1 = ExcursionActivity::where('id', 1)->first();
        $excur2 = ExcursionActivity::where('id', 2)->first();
        $excur3 = ExcursionActivity::where('id', 3)->first();
        $excur4 = ExcursionActivity::where('id', 4)->first();

        $excursions = ExcursionActivity::all();*/

        /*if ($request->type == 'aucun') {
            Flash::error('Type d\'offre obligatoire.');
            return redirect()->back();
        }

        if ($request->type == 'hotel') {
            if ($request->hotel1 == null) {
                $request->hotel1 = '';
            }
            if ($request->hotel2 == null) {
                $request->hotel2 = '';
            }
            if ($request->hotel3 == null) {
                $request->hotel3 = '';
            }
            if ($request->picture1 == null || $request->picture2 == null || $request->picture3 == null) {
                Flash::error('picture obligatoire');
                return redirect()->back();
            }
            $photo1 = $request->file('picture1');
            $image1 = Imgur::upload($photo1);
            $picture1 = $image1->link();
            $photo2 = $request->file('picture2');
            $image2 = Imgur::upload($photo2);
            $picture2 = $image2->link();
            $photo3 = $request->file('picture3');
            $image3 = Imgur::upload($photo3);
            $picture3 = $image3->link();
            $meta = [
                "specifications" => [
                    ["icon" => "",
                        "name" => $request->hotel1],
                    ["icon" => "",
                        "name" => $request->hotel2],
                    ["icon" => "",
                        "name" => $request->hotel3]
                ],

                "pictures" => [
                    $picture1,
                    $picture2,
                    $picture3
                ]
            ];
        }

        if ($request->type == 'restaurant' || $request->type == 'menu') {
            if ($request->specification1 == null) {
                $request->specification1 = '';
            }
            if ($request->specification2 == null) {
                $request->specification2 = '';
            }
            if ($request->picture1 == null || $request->picture2 == null || $request->picture3 == null) {
                Flash::error('picture obligatoire');
                return redirect()->back();
            }

            $photo1 = $request->file('picture1');
            $image1 = Imgur::upload($photo1);
            $picture1 = $image1->link();
            $photo2 = $request->file('picture2');
            $image2 = Imgur::upload($photo2);
            $picture2 = $image2->link();
            $photo3 = $request->file('picture3');
            $image3 = Imgur::upload($photo3);
            $picture3 = $image3->link();

            $meta = [
                "specifications" => [
                    [
                        "icon" => "",
                        "label_name" => "Visiter le Site Web",
                        "label_value" => $request->specification1
                    ],
                    [
                        "icon" => "",
                        "label_name" => "Telephone",
                        "label_value" => $request->specification2
                    ]
                ],
                "specialties" => [

                    [
                        "image" => $food1->image,
                        "name" => $food1->name,
                        "slug" => $food1->slug,
                        "id" => $food1->id
                    ],
                    [
                        "image" => $food2->image,
                        "name" => $food2->name,
                        "slug" => $food2->slug,
                        "id" => $food2->id
                    ],
                    [
                        "image" => $food3->image,
                        "name" => $food3->name,
                        "slug" => $food3->slug,
                        "id" => $food3->id
                    ],
                    [
                        "image" => $food4->image,
                        "name" => $food4->name,
                        "slug" => $food4->slug,
                        "id" => $food4->id
                    ]


                ],
                "pictures" => [
                    $picture1,
                    $picture2,
                    $picture3
                ]
            ];
        }

        if ($request->type == 'musee') {
            if ($request->specification1 == null) {
                $request->specification1 = '';
            }
            if ($request->specification2 == null) {
                $request->specification2 = '';
            }
            if ($request->picture1 == null || $request->picture2 == null || $request->picture3 == null) {
                Flash::error('picture obligatoire');
                return redirect()->back();
            }

            $photo1 = $request->file('picture1');
            $image1 = Imgur::upload($photo1);
            $picture1 = $image1->link();
            $photo2 = $request->file('picture2');
            $image2 = Imgur::upload($photo2);
            $picture2 = $image2->link();
            $photo3 = $request->file('picture3');
            $image3 = Imgur::upload($photo3);
            $picture3 = $image3->link();

            $meta = [
                "specifications" => [
                    [
                        "icon" => "",
                        "label_name" => "Visiter le Site Web",
                        "label_value" => $request->specification1
                    ],
                    [
                        "icon" => "https://image.adjemin.com/eoeooe.png",
                        "label_name" => "Telephone",
                        "label_value" => $request->specification2
                    ]
                ],
                "pictures" => [
                    $picture1,
                    $picture2,
                    $picture3
                ]
            ];
        }

        if ($request->type == 'car') {
            if ($request->model == null || $request->brand == null || $request->year == null || $request->body_type == null || $request->color == null
                || $request->doors == null || $request->motors == null || $request->gearbox == null) {
                Flash::error('les informations sur la voiture doivent être renseigné');
                return redirect()->back();
            }
            if ($request->picture1 == null || $request->picture2 == null || $request->picture3 == null) {
                Flash::error('picture obligatoire');
                return redirect()->back();
            }

            $photo1 = $request->file('picture1');
            $image1 = Imgur::upload($photo1);
            $picture1 = $image1->link();
            $photo2 = $request->file('picture2');
            $image2 = Imgur::upload($photo2);
            $picture2 = $image2->link();
            $photo3 = $request->file('picture3');
            $image3 = Imgur::upload($photo3);
            $picture3 = $image3->link();

            $meta = [
                "model" => $request->model,
                "model_label" => "Modèle",
                "brand" => $request->brand,
                "brand_label" => "Marque",
                "year" => $request->year,
                "year_label" => "Année",
                "body_type" => $request->body_type,
                "body_type_label" => "Type de carroserie",
                "color" => $request->color,
                "color_label" => "Couleur",
                "doors" => $request->doors,
                "doors_label" => "Nombre de portières",
                "motors" => $request->motors,
                "motors_label" => "Moteur",
                "gearbox" => $request->gearbox,
                "gearbox_label" => "Boîte de vitesse",
                "pictures" => [
                    $picture1,
                    $picture2,
                    $picture3

                ]
            ];
        }

        if ($request->type == 'excursion') {
            if ($request->specification1 == null) {
                $request->specification1 = '';
            }
            if ($request->specification2 == null) {
                $request->specification2 = '';
            }
            if ($request->picture1 == null || $request->picture2 == null || $request->picture3 == null) {
                Flash::error('picture obligatoire');
                return redirect()->back();
            }

            $photo1 = $request->file('picture1');
            $image1 = Imgur::upload($photo1);
            $picture1 = $image1->link();
            $photo2 = $request->file('picture2');
            $image2 = Imgur::upload($photo2);
            $picture2 = $image2->link();
            $photo3 = $request->file('picture3');
            $image3 = Imgur::upload($photo3);
            $picture3 = $image3->link();

            $meta = [
                "specifications" => [
                    [
                        "icon" => "",
                        "label_name" => "Visiter le Site Web",
                        "label_value" => $request->specification1
                    ],
                    [
                        "icon" => "",
                        "label_name" => "Telephone",
                        "label_value" => $request->specification2
                    ]
                ],
                "specialties" => [
                    [
                        "image" => $excur1->image,
                        "name" => $excur1->name,
                        "slug" => $excur1->slug,
                        "id" => $excur1->id
                    ],
                    [
                        "image" => $excur2->image,
                        "name" => $excur2->name,
                        "slug" => $excur2->slug,
                        "id" => $excur2->id
                    ],
                    [
                        "image" => $excur3->image,
                        "name" => $excur3->name,
                        "slug" => $excur3->slug,
                        "id" => $excur3->id
                    ],
                    [
                        "image" => $excur4->image,
                        "name" => $excur4->name,
                        "slug" => $excur4->slug,
                        "id" => $excur4->id
                    ]
                ],
                "pictures" => [
                    $picture1,
                    $picture2,
                    $picture3
                ]
            ];
        }

        if ($request->type == 'residence') {
            if ($request->room1 == null) {
                $request->room1 = '';
            }
            if ($request->room2 == null) {
                $request->room2 = '';
            }
            if ($request->room3 == null) {
                $request->room3 = '';
            }
            if ($request->picture1 == null || $request->picture2 == null || $request->picture3 == null) {
                Flash::error('picture obligatoire');
                return redirect()->back();
            }

            $photo1 = $request->file('picture1');
            $image1 = Imgur::upload($photo1);
            $picture1 = $image1->link();
            $photo2 = $request->file('picture2');
            $image2 = Imgur::upload($photo2);
            $picture2 = $image2->link();
            $photo3 = $request->file('picture3');
            $image3 = Imgur::upload($photo3);
            $picture3 = $image3->link();

            $meta = [
                "room_equipments" => [
                    [
                        "icon" => "",
                        "name" => $request->room1
                    ],
                    [
                        "icon" => "",
                        "name" => $request->room2
                    ],
                    [
                        "icon" => "",
                        "name" => $request->room3
                    ]
                ],
                "pictures" => [
                    $picture1,
                    $picture2,
                    $picture3
                ]
            ];
        }

        if ($request->type == 'chambre') {
            if ($request->hotel1 == null) {
                $request->hotel1 = '';
            }
            if ($request->hotel2 == null) {
                $request->hotel2 = '';
            }
            if ($request->hotel3 == null) {
                $request->hotel3 = '';
            }
            if ($request->room1 == null) {
                $request->room1 = '';
            }
            if ($request->room2 == null) {
                $request->room2 = '';
            }
            if ($request->room3 == null) {
                $request->room3 = '';
            }

            if ($request->picture1 == null || $request->picture2 == null || $request->picture3 == null) {
                Flash::error('picture obligatoire');
                return redirect()->back();
            }

            $photo1 = $request->file('picture1');
            $image1 = Imgur::upload($photo1);
            $picture1 = $image1->link();
            $photo2 = $request->file('picture2');
            $image2 = Imgur::upload($photo2);
            $picture2 = $image2->link();
            $photo3 = $request->file('picture3');
            $image3 = Imgur::upload($photo3);
            $picture3 = $image3->link();

            $meta = [
                "establishment_equipments" => [
                    [
                        "icon" => "",
                        "name" => $request->hotel1
                    ],
                    [
                        "icon" => "",
                        "name" => $request->hotel2
                    ],
                    [
                        "icon" => "",
                        "name" => $request->hotel3
                    ]
                ],
                "room_equipments" => [
                    [
                        "icon" => "",
                        "name" => $request->room1
                    ],
                    [
                        "icon" => "",
                        "name" => $request->room2
                    ],
                    [
                        "icon" => "",
                        "name" => $request->room3
                    ]
                ],
                "pictures" => [
                    $picture1,
                    $picture2,
                    $picture3
                ]
            ];
        }

        $gps = $request->location_lat . ',' . $request->location_lng;*/

        // $rooms = [
        //     'equipement de la chambre'=> $request->room1.','.$request->room2.','.$request->room3
        // ];
        // $etalissements = [
        //     'equipement etablissement'=> $request->hotel1.','.$request->hotel2.','.$request->hotel3
        // ];
        // $specifications =[
        //     'site web' => $request->specification1,
        //     'Telephone' => $request->specification2
        // ];


        $hotelSpecification = new HotelSpecification(
            '',
            $request->specification_hotel,
        );

        $hotel = new Hotel(
            $request->offer,
            $request->title,
            $request->description,
            $request->pictures,
            $hotelSpecification,
            $request->location_name,
            $request->location_lat,
            $request->location_lng,
            $request->location_gps
        );



        /*$offer = Offer::Create([
            'parent_source_id' => $request->parent_source_id,
            'title' => $request->title,
            'meta_data' => json_encode($meta, JSON_UNESCAPED_SLASHES),
            'type' => $request->type,
            'owner_id' => $request->owner_id,
            'publisher_id' => $request->publisher_id,
            'publisher_name' => Auth::user()->name,
            'has_children' => $request->has_children,
            'description' => $request->description,
            'location_name' => $request->location_name,
            'picture1' => $picture1,
            'picture2' => $picture2,
            'picture3' => $picture3,
            'room' => $request->room1 . ',' . $request->room2 . ',' . $request->room3,
            'etalissement' => $request->hotel1 . ',' . $request->hotel2 . ',' . $request->hotel3,
            'specification' => $request->specification1 . ' / ' . $request->specification2,
            'location_lat' => $request->location_lat,
            'location_lng' => $request->location_lng,
            'location_gps' => $gps,
            'original_price' => $request->original_price,
            'price' => $request->price,
            'currency_code' => $request->currency_code,
            'has_price' => $request->has_price,
            'rating' => $request->rating,
            'slide_title' => $request->slide_title,
            'is_escort' => $request->is_escort,
            'escort_fees' => $request->escort_fees
        ]);
        if ($request->parent_source_id != 0) {
            $parent = Offer::where('id', $request->parent_source_id)->first();
            $offer->location_lat = $parent->location_lat;
            $offer->location_lng = $parent->location_lng;
            $offer->location_name = $parent->location_name;
            $offer->location_gps = $parent->location_gps;
            $offer->save();
        }

        if ($request->slide_url != null) {
            $photo = $request->file('slide_url');
            $image = Imgur::upload($photo);
            $offer->slide_url = $image->link();
            $offer->save();
        }

        dd($offer);*/


        //$offer = $this->offerRepository->create($input);

        Flash::success('Offer saved successfully.');

        return redirect(route('offers.index'));
    }

    /**
     * Display the specified Offer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(Offer $offers)
    {
        $offer = $this->offerRepository->find($id);

        return $offer;

        $hotel = Offer::where('type', 'hotel')->get();

        $restaurant = Offer::where('type', 'restaurant')->get();

        $musee = Offer::where('type', 'musee')->get();

        $voiture = Offer::where('type', 'car')->get();

        $excursion = Offer::where('type', 'excursion')->get();

        $customer = Customer::where('customer_type', '!=', 'client')->get();

        if (empty($offer)) {
            Flash::error('Offer not found');

            return redirect(route('offers.index'));
        }

        return view('offers.show')
            ->with('offer', $offer)
            ->with('hotel', $hotel)
            ->with('restaurant', $restaurant)
            ->with('musee', $musee)
            ->with('customer', $customer)
            ->with('voiture', $voiture)
            ->with('excursion', $excursion);
    }

    /**
     * Show the form for editing the specified Offer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $offer = $this->offerRepository->find($id);

        $hotel = Offer::where('type', 'hotel')->get();

        $restaurant = Offer::where('type', 'restaurant')->get();

        $musee = Offer::where('type', 'musee')->get();

        $voiture = Offer::where('type', 'car')->get();

        $excursion = Offer::where('type', 'excursion')->get();

        $customer = Customer::where('customer_type', '!=', 'client')->get();

        if (empty($offer)) {
            Flash::error('Offer not found');

            return redirect(route('offers.index'));
        }

        return view('offers.edit')
            ->with('offer', $offer)
            ->with('hotel', $hotel)
            ->with('restaurant', $restaurant)
            ->with('musee', $musee)
            ->with('customer', $customer)
            ->with('voiture', $voiture)
            ->with('excursion', $excursion);
    }

    /**
     * Update the specified Offer in storage.
     *
     * @param int $id
     * @param UpdateOfferRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOfferRequest $request)
    {
        $offer = $this->offerRepository->find($id);

        if (empty($offer)) {
            Flash::error('Offer not found');

            return redirect(route('offers.index'));
        }

        $offer = $this->offerRepository->update($request->all(), $id);

        Flash::success('Offer updated successfully.');

        return redirect(route('offers.index'));
    }

    /**
     * Remove the specified Offer from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $offer = $this->offerRepository->find($id);

        if (empty($offer)) {
            Flash::error('Offer not found');

            return redirect(route('offers.index'));
        }

        $this->offerRepository->delete($id);

        Flash::success('Offer deleted successfully.');

        return redirect(route('offers.index'));
    }
}
