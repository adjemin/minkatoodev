<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerAPIRequest;
use App\Http\Requests\API\UpdateCustomerAPIRequest;
use App\Models\Customer;
use App\Models\CustomerSession;
use App\Repositories\CustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Utils\OTP;
use Response;

/**
 * Class CustomerController
 * @package App\Http\Controllers\API
 */

class CustomerAPIController extends AppBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepository = $customerRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/customers",
     *      summary="Get a listing of the Customers.",
     *      tags={"Customer"},
     *      description="Get all Customers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Customer")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $customers = $this->customerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customers->toArray(), 'Customers retrieved successfully');
    }


    public function findByPhone(Request $request){

        /** @var string $phone_number*/
        $phone_number = $request->phone_number;

        /** @var string $dial_code*/
        $dial_code = $request->dial_code;

        $customer = Customer::where(['dial_code' => $dial_code, 'phone_number' => $phone_number])->first();

        if(empty($customer)){

            return $this->sendError('Customer is not found', 404);

        }

        return $this->sendResponse($customer->toArray(), 'Customer saved successfully');
    }

    /**
     * @param CreateCustomerAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/customers",
     *      summary="Store a newly created Customer in storage",
     *      tags={"Customer"},
     *      description="Store Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Customer that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Customer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Customer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
        $input = $request->all();

        /** @var string $email */
            $email = $request->email;
            if ($email == null) {

                return $this->sendError('Address email required', 400);
            }

        /** @var string $phone_number*/
        $phone_number = $request->phone_number;

        /** @var string $dial_code*/
        $dial_code = $request->dial_code;

        /** @var string $phone*/
        $phone = $dial_code.''.$phone_number;

        $customer = Customer::where('phone',$phone)->first();

        /** @var int $otp*/
        //$otp = OTP::generate_reference(Customer::all(),'',6);

        if($customer == null)
        {
            /** @var string $first_name */
            $first_name = ucfirst(strtolower($request->first_name));

            /** @var string $last_name */
            $last_name = ucfirst(strtolower($request->last_name));

            /** @var string $name */
            $name = $first_name.' '.$last_name;

            /** @var string $country_code */
            //$country_code = strtoupper($request->country_code);
            $country_code = 'CI';

            /** @var string $gender */
            $gender = $request->gender;

            /** @var  $session */
            $session = array($request->session);

            // Create customer
            $customer = Customer::create([
                'dial_code' => $dial_code,
                'phone_number' => $phone_number,
                'phone' => $phone,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'name' => $name,
                'email' => $email,
                'country_code' => $country_code,
                'gender' => $gender,
            ]);

            $session["customer_id"] = $customer->id;

            $customerSession = CustomerSession::create($session);

            return $this->sendResponse($customer->toArray(), 'Customer saved successfully');

        }

        
        return $this->sendError('Customer exist',400);
        //$customer = $this->customerRepository->create($input);

        
    }

    /**
     * Login Customer
     *
     * @param Request $request
     * @return Reponse
     * @SWG\Post(
     *      path="/customers/login",
     *      summary="Login Customer in storage",
     *      tags={"Customer"},
     *      description="Login Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="dial_code",
     *          in="path",
     *          description="Customer that should be logged",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          name="phone_number",
     *          in="path",
     *          description="Customer that should be logged",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Customer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function login(Request $request)
    {
        /** @var string $dial_code */
        $dial_code = $request->dial_code;

        /** @var string $phone_number */
        $phone_number = $request->phone_number;

        /** @var int $otp */
        //$otp = $request->otp;

        if($phone_number != null)
        {
            /** @var string $phone */
        $phone = $dial_code.''.$phone_number;

        // get customer
        $customer = Customer::where('phone',$phone)->first();

            if(!empty($customer)){
                
                $session["customer_id"] = $customer->id;
                $customerSession = CustomerSession::create($session);
                
                return $this->sendResponse($customer->toArray(),'Customer find successfully');
                    
            }else{
                
                return $this->sendError('Customer not found', 404);
            
            }
            
        }

        return $this->sendError('Please give your phone_number',400);
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/customers/{id}",
     *      summary="Display the specified Customer",
     *      tags={"Customer"},
     *      description="Get Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Customer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Customer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Customer $customer */
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }

        return $this->sendResponse($customer->toArray(), 'Customer retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCustomerAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/customers/{id}",
     *      summary="Update the specified Customer in storage",
     *      tags={"Customer"},
     *      description="Update Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Customer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Customer that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Customer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Customer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCustomerAPIRequest $request)
    {
        $input = $request->all();

        /** @var Customer $customer */
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }

        $customer = $this->customerRepository->update($input, $id);

        return $this->sendResponse($customer->toArray(), 'Customer updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/customers/{id}",
     *      summary="Remove the specified Customer from storage",
     *      tags={"Customer"},
     *      description="Delete Customer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Customer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Customer $customer */
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            return $this->sendError('Customer not found');
        }

        $customer->delete();

        return $this->sendSuccess('Customer deleted successfully');
    }
}
