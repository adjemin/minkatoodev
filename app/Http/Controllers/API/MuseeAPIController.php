<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMuseeAPIRequest;
use App\Http\Requests\API\UpdateMuseeAPIRequest;
use App\Models\Musee;
use App\Repositories\MuseeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MuseeController
 * @package App\Http\Controllers\API
 */

class MuseeAPIController extends AppBaseController
{
    /** @var  MuseeRepository */
    private $museeRepository;

    public function __construct(MuseeRepository $museeRepo)
    {
        $this->museeRepository = $museeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/musees",
     *      summary="Get a listing of the Musees.",
     *      tags={"Musee"},
     *      description="Get all Musees",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Musee")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $musees = $this->museeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($musees->toArray(), 'Musees retrieved successfully');
    }

    /**
     * @param CreateMuseeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/musees",
     *      summary="Store a newly created Musee in storage",
     *      tags={"Musee"},
     *      description="Store Musee",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Musee that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Musee")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Musee"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMuseeAPIRequest $request)
    {
        $input = $request->all();

        $musee = $this->museeRepository->create($input);

        return $this->sendResponse($musee->toArray(), 'Musee saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/musees/{id}",
     *      summary="Display the specified Musee",
     *      tags={"Musee"},
     *      description="Get Musee",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Musee",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Musee"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Musee $musee */
        $musee = $this->museeRepository->find($id);

        if (empty($musee)) {
            return $this->sendError('Musee not found');
        }

        return $this->sendResponse($musee->toArray(), 'Musee retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMuseeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/musees/{id}",
     *      summary="Update the specified Musee in storage",
     *      tags={"Musee"},
     *      description="Update Musee",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Musee",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Musee that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Musee")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Musee"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMuseeAPIRequest $request)
    {
        $input = $request->all();

        /** @var Musee $musee */
        $musee = $this->museeRepository->find($id);

        if (empty($musee)) {
            return $this->sendError('Musee not found');
        }

        $musee = $this->museeRepository->update($input, $id);

        return $this->sendResponse($musee->toArray(), 'Musee updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/musees/{id}",
     *      summary="Remove the specified Musee from storage",
     *      tags={"Musee"},
     *      description="Delete Musee",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Musee",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Musee $musee */
        $musee = $this->museeRepository->find($id);

        if (empty($musee)) {
            return $this->sendError('Musee not found');
        }

        $musee->delete();

        return $this->sendSuccess('Musee deleted successfully');
    }
}
