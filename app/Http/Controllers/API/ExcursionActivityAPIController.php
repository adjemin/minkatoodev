<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateExcursionActivityAPIRequest;
use App\Http\Requests\API\UpdateExcursionActivityAPIRequest;
use App\Models\ExcursionActivity;
use App\Repositories\ExcursionActivityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ExcursionActivityController
 * @package App\Http\Controllers\API
 */

class ExcursionActivityAPIController extends AppBaseController
{
    /** @var  ExcursionActivityRepository */
    private $excursionActivityRepository;

    public function __construct(ExcursionActivityRepository $excursionActivityRepo)
    {
        $this->excursionActivityRepository = $excursionActivityRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/excursionActivities",
     *      summary="Get a listing of the ExcursionActivities.",
     *      tags={"ExcursionActivity"},
     *      description="Get all ExcursionActivities",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ExcursionActivity")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $excursionActivities = $this->excursionActivityRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($excursionActivities->toArray(), 'Excursion Activities retrieved successfully');
    }

    /**
     * @param CreateExcursionActivityAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/excursionActivities",
     *      summary="Store a newly created ExcursionActivity in storage",
     *      tags={"ExcursionActivity"},
     *      description="Store ExcursionActivity",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ExcursionActivity that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ExcursionActivity")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ExcursionActivity"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateExcursionActivityAPIRequest $request)
    {
        $input = $request->all();

        $excursionActivity = $this->excursionActivityRepository->create($input);

        return $this->sendResponse($excursionActivity->toArray(), 'Excursion Activity saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/excursionActivities/{id}",
     *      summary="Display the specified ExcursionActivity",
     *      tags={"ExcursionActivity"},
     *      description="Get ExcursionActivity",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ExcursionActivity",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ExcursionActivity"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ExcursionActivity $excursionActivity */
        $excursionActivity = $this->excursionActivityRepository->find($id);

        if (empty($excursionActivity)) {
            return $this->sendError('Excursion Activity not found');
        }

        return $this->sendResponse($excursionActivity->toArray(), 'Excursion Activity retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateExcursionActivityAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/excursionActivities/{id}",
     *      summary="Update the specified ExcursionActivity in storage",
     *      tags={"ExcursionActivity"},
     *      description="Update ExcursionActivity",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ExcursionActivity",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ExcursionActivity that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ExcursionActivity")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ExcursionActivity"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateExcursionActivityAPIRequest $request)
    {
        $input = $request->all();

        /** @var ExcursionActivity $excursionActivity */
        $excursionActivity = $this->excursionActivityRepository->find($id);

        if (empty($excursionActivity)) {
            return $this->sendError('Excursion Activity not found');
        }

        $excursionActivity = $this->excursionActivityRepository->update($input, $id);

        return $this->sendResponse($excursionActivity->toArray(), 'ExcursionActivity updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/excursionActivities/{id}",
     *      summary="Remove the specified ExcursionActivity from storage",
     *      tags={"ExcursionActivity"},
     *      description="Delete ExcursionActivity",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ExcursionActivity",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ExcursionActivity $excursionActivity */
        $excursionActivity = $this->excursionActivityRepository->find($id);

        if (empty($excursionActivity)) {
            return $this->sendError('Excursion Activity not found');
        }

        $excursionActivity->delete();

        return $this->sendSuccess('Excursion Activity deleted successfully');
    }
}
