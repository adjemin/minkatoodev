<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOwnerSourceAPIRequest;
use App\Http\Requests\API\UpdateOwnerSourceAPIRequest;
use App\Models\OwnerSource;
use App\Repositories\OwnerSourceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class OwnerSourceController
 * @package App\Http\Controllers\API
 */

class OwnerSourceAPIController extends AppBaseController
{
    /** @var  OwnerSourceRepository */
    private $ownerSourceRepository;

    public function __construct(OwnerSourceRepository $ownerSourceRepo)
    {
        $this->ownerSourceRepository = $ownerSourceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ownerSources",
     *      summary="Get a listing of the OwnerSources.",
     *      tags={"OwnerSource"},
     *      description="Get all OwnerSources",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/OwnerSource")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $ownerSources = $this->ownerSourceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($ownerSources->toArray(), 'Owner Sources retrieved successfully');
    }

    /**
     * @param CreateOwnerSourceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ownerSources",
     *      summary="Store a newly created OwnerSource in storage",
     *      tags={"OwnerSource"},
     *      description="Store OwnerSource",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OwnerSource that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OwnerSource")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OwnerSource"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateOwnerSourceAPIRequest $request)
    {
        $input = $request->all();

        $ownerSource = $this->ownerSourceRepository->create($input);

        return $this->sendResponse($ownerSource->toArray(), 'Owner Source saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ownerSources/{id}",
     *      summary="Display the specified OwnerSource",
     *      tags={"OwnerSource"},
     *      description="Get OwnerSource",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OwnerSource",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OwnerSource"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var OwnerSource $ownerSource */
        $ownerSource = $this->ownerSourceRepository->find($id);

        if (empty($ownerSource)) {
            return $this->sendError('Owner Source not found');
        }

        return $this->sendResponse($ownerSource->toArray(), 'Owner Source retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOwnerSourceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ownerSources/{id}",
     *      summary="Update the specified OwnerSource in storage",
     *      tags={"OwnerSource"},
     *      description="Update OwnerSource",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OwnerSource",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OwnerSource that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OwnerSource")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OwnerSource"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOwnerSourceAPIRequest $request)
    {
        $input = $request->all();

        /** @var OwnerSource $ownerSource */
        $ownerSource = $this->ownerSourceRepository->find($id);

        if (empty($ownerSource)) {
            return $this->sendError('Owner Source not found');
        }

        $ownerSource = $this->ownerSourceRepository->update($input, $id);

        return $this->sendResponse($ownerSource->toArray(), 'OwnerSource updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ownerSources/{id}",
     *      summary="Remove the specified OwnerSource from storage",
     *      tags={"OwnerSource"},
     *      description="Delete OwnerSource",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of OwnerSource",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var OwnerSource $ownerSource */
        $ownerSource = $this->ownerSourceRepository->find($id);

        if (empty($ownerSource)) {
            return $this->sendError('Owner Source not found');
        }

        $ownerSource->delete();

        return $this->sendSuccess('Owner Source deleted successfully');
    }
}
