<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFavoriAPIRequest;
use App\Http\Requests\API\UpdateFavoriAPIRequest;
use App\Models\Favori;
use App\Models\Offer;
use App\Repositories\FavoriRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class FavoriController
 * @package App\Http\Controllers\API
 */

class FavoriAPIController extends AppBaseController
{
    /** @var  FavoriRepository */
    private $favoriRepository;

    public function __construct(FavoriRepository $favoriRepo)
    {
        $this->favoriRepository = $favoriRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/favoris",
     *      summary="Get a listing of the Favoris.",
     *      tags={"Favori"},
     *      description="Get all Favoris",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Favori")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $favoris = $this->favoriRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($favoris->toArray(), 'Favoris retrieved successfully');
    }

    /**
     * @param CreateFavoriAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/favoris",
     *      summary="Store a newly created Favori in storage",
     *      tags={"Favori"},
     *      description="Store Favori",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Favori that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Favori")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Favori"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
        $offer = Offer::where('id',$request->offer_id)->first();
        $favori = Favori::create([
            'customer_id' => $request->customer_id,
            'offer_id' => $request->offer_id,
            'offer' => json_encode($offer)
        ]);

        return $this->sendResponse($favori->toArray(), 'Favori saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/favoris/{id}",
     *      summary="Display the specified Favori",
     *      tags={"Favori"},
     *      description="Get Favori",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Favori",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Favori"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Favori $favori */
        $favori = $this->favoriRepository->find($id);

        if (empty($favori)) {
            return $this->sendError('Favori not found');
        }

        return $this->sendResponse($favori->toArray(), 'Favori retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateFavoriAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/favoris/{id}",
     *      summary="Update the specified Favori in storage",
     *      tags={"Favori"},
     *      description="Update Favori",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Favori",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Favori that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Favori")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Favori"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateFavoriAPIRequest $request)
    {
        $input = $request->all();

        /** @var Favori $favori */
        $favori = $this->favoriRepository->find($id);

        if (empty($favori)) {
            return $this->sendError('Favori not found');
        }

        $favori = $this->favoriRepository->update($input, $id);

        return $this->sendResponse($favori->toArray(), 'Favori updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/favoris/{id}",
     *      summary="Remove the specified Favori from storage",
     *      tags={"Favori"},
     *      description="Delete Favori",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Favori",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Favori $favori */
        $favori = $this->favoriRepository->find($id);

        if (empty($favori)) {
            return $this->sendError('Favori not found');
        }

        $favori->delete();

        return $this->sendSuccess('Favori deleted successfully');
    }
}
