<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMetaDataServiceAPIRequest;
use App\Http\Requests\API\UpdateMetaDataServiceAPIRequest;
use App\Models\MetaDataService;
use App\Repositories\MetaDataServiceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MetaDataServiceController
 * @package App\Http\Controllers\API
 */

class MetaDataServiceAPIController extends AppBaseController
{
    /** @var  MetaDataServiceRepository */
    private $metaDataServiceRepository;

    public function __construct(MetaDataServiceRepository $metaDataServiceRepo)
    {
        $this->metaDataServiceRepository = $metaDataServiceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/metaDataServices",
     *      summary="Get a listing of the MetaDataServices.",
     *      tags={"MetaDataService"},
     *      description="Get all MetaDataServices",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/MetaDataService")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $metaDataServices = $this->metaDataServiceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($metaDataServices->toArray(), 'Meta Data Services retrieved successfully');
    }

    /**
     * @param CreateMetaDataServiceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/metaDataServices",
     *      summary="Store a newly created MetaDataService in storage",
     *      tags={"MetaDataService"},
     *      description="Store MetaDataService",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MetaDataService that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MetaDataService")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MetaDataService"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMetaDataServiceAPIRequest $request)
    {
        $input = $request->all();

        $metaDataService = $this->metaDataServiceRepository->create($input);

        return $this->sendResponse($metaDataService->toArray(), 'Meta Data Service saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/metaDataServices/{id}",
     *      summary="Display the specified MetaDataService",
     *      tags={"MetaDataService"},
     *      description="Get MetaDataService",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MetaDataService",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MetaDataService"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var MetaDataService $metaDataService */
        $metaDataService = $this->metaDataServiceRepository->find($id);

        if (empty($metaDataService)) {
            return $this->sendError('Meta Data Service not found');
        }

        return $this->sendResponse($metaDataService->toArray(), 'Meta Data Service retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMetaDataServiceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/metaDataServices/{id}",
     *      summary="Update the specified MetaDataService in storage",
     *      tags={"MetaDataService"},
     *      description="Update MetaDataService",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MetaDataService",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="MetaDataService that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/MetaDataService")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/MetaDataService"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMetaDataServiceAPIRequest $request)
    {
        $input = $request->all();

        /** @var MetaDataService $metaDataService */
        $metaDataService = $this->metaDataServiceRepository->find($id);

        if (empty($metaDataService)) {
            return $this->sendError('Meta Data Service not found');
        }

        $metaDataService = $this->metaDataServiceRepository->update($input, $id);

        return $this->sendResponse($metaDataService->toArray(), 'MetaDataService updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/metaDataServices/{id}",
     *      summary="Remove the specified MetaDataService from storage",
     *      tags={"MetaDataService"},
     *      description="Delete MetaDataService",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of MetaDataService",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var MetaDataService $metaDataService */
        $metaDataService = $this->metaDataServiceRepository->find($id);

        if (empty($metaDataService)) {
            return $this->sendError('Meta Data Service not found');
        }

        $metaDataService->delete();

        return $this->sendSuccess('Meta Data Service deleted successfully');
    }
}
