<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFoodSpeialtiesAPIRequest;
use App\Http\Requests\API\UpdateFoodSpeialtiesAPIRequest;
use App\Models\FoodSpeialties;
use App\Repositories\FoodSpeialtiesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class FoodSpeialtiesController
 * @package App\Http\Controllers\API
 */

class FoodSpeialtiesAPIController extends AppBaseController
{
    /** @var  FoodSpeialtiesRepository */
    private $foodSpeialtiesRepository;

    public function __construct(FoodSpeialtiesRepository $foodSpeialtiesRepo)
    {
        $this->foodSpeialtiesRepository = $foodSpeialtiesRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/foodSpeialties",
     *      summary="Get a listing of the FoodSpeialties.",
     *      tags={"FoodSpeialties"},
     *      description="Get all FoodSpeialties",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/FoodSpeialties")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $foodSpeialties = $this->foodSpeialtiesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($foodSpeialties->toArray(), 'Food Speialties retrieved successfully');
    }

    /**
     * @param CreateFoodSpeialtiesAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/foodSpeialties",
     *      summary="Store a newly created FoodSpeialties in storage",
     *      tags={"FoodSpeialties"},
     *      description="Store FoodSpeialties",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="FoodSpeialties that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/FoodSpeialties")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FoodSpeialties"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateFoodSpeialtiesAPIRequest $request)
    {
        $input = $request->all();

        $foodSpeialties = $this->foodSpeialtiesRepository->create($input);

        return $this->sendResponse($foodSpeialties->toArray(), 'Food Speialties saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/foodSpeialties/{id}",
     *      summary="Display the specified FoodSpeialties",
     *      tags={"FoodSpeialties"},
     *      description="Get FoodSpeialties",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FoodSpeialties",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FoodSpeialties"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var FoodSpeialties $foodSpeialties */
        $foodSpeialties = $this->foodSpeialtiesRepository->find($id);

        if (empty($foodSpeialties)) {
            return $this->sendError('Food Speialties not found');
        }

        return $this->sendResponse($foodSpeialties->toArray(), 'Food Speialties retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateFoodSpeialtiesAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/foodSpeialties/{id}",
     *      summary="Update the specified FoodSpeialties in storage",
     *      tags={"FoodSpeialties"},
     *      description="Update FoodSpeialties",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FoodSpeialties",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="FoodSpeialties that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/FoodSpeialties")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/FoodSpeialties"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateFoodSpeialtiesAPIRequest $request)
    {
        $input = $request->all();

        /** @var FoodSpeialties $foodSpeialties */
        $foodSpeialties = $this->foodSpeialtiesRepository->find($id);

        if (empty($foodSpeialties)) {
            return $this->sendError('Food Speialties not found');
        }

        $foodSpeialties = $this->foodSpeialtiesRepository->update($input, $id);

        return $this->sendResponse($foodSpeialties->toArray(), 'FoodSpeialties updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/foodSpeialties/{id}",
     *      summary="Remove the specified FoodSpeialties from storage",
     *      tags={"FoodSpeialties"},
     *      description="Delete FoodSpeialties",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of FoodSpeialties",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var FoodSpeialties $foodSpeialties */
        $foodSpeialties = $this->foodSpeialtiesRepository->find($id);

        if (empty($foodSpeialties)) {
            return $this->sendError('Food Speialties not found');
        }

        $foodSpeialties->delete();

        return $this->sendSuccess('Food Speialties deleted successfully');
    }
}
