<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOfferAPIRequest;
use App\Http\Requests\API\UpdateOfferAPIRequest;
use App\Models\Offer;
use App\Repositories\OfferRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class OfferController
 * @package App\Http\Controllers\API
 */

class OfferAPIController extends AppBaseController
{
    /** @var  OfferRepository */
    private $offerRepository;

    public function __construct(OfferRepository $offerRepo)
    {
        $this->offerRepository = $offerRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/offers",
     *      summary="Get a listing of the Offers.",
     *      tags={"Offer"},
     *      description="Get all Offers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Offer")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $offers = $this->offerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($offers->toArray(), 'Offers retrieved successfully');
    }

    /**
     * @param CreateOfferAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/offers",
     *      summary="Store a newly created Offer in storage",
     *      tags={"Offer"},
     *      description="Store Offer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Offer that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Offer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Offer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateOfferAPIRequest $request)
    {
        $input = $request->all();

        $offer = $this->offerRepository->create($input);

        return $this->sendResponse($offer->toArray(), 'Offer saved successfully');
    }


    /**
     * @param int $price, string $currency
     * @return Response
     *
     * @SWG\Get(
     *      path="/offers_by_price/{price}/{currency}",
     *      summary="Get a listing of the offer by price.",
     *      tags={"Offer"},
     *      description="Get all Offer by price",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="price",
     *          in="path",
     *          description="Offer that should be get by price",
     *          required=true,
     *          type="integer"
     *      ),
     *      @SWG\Parameter(
     *          name="currency",
     *          in="path",
     *          description="Offer that should be get by price",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Offer")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function price($price, $currency)
    {
        if($price == null || $currency == null)
        {
            return $this->sendError('Enter a number', 401);
        }
        $offer = Offer::where('has_price',1)->where('price','<=',$price)->where('currency_code',$currency)->get();

        if(count($offer) == 0)
        {
            return $this->sendError('Offer not found', 404);
        }

        return $this->sendResponse($offer->toArray(), 'Offer find successfully');

    }


    /**
     * @param string $details
     * @return Response
     *
     * @SWG\Get(
     *      path="/offers_by_details/{details}",
     *      summary="Get a listing of the offer detail.",
     *      tags={"Offer"},
     *      description="Get all Offer by detail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="detail",
     *          in="path",
     *          description="Offer that should be get by detail",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Offer")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function details($details)
    {
        $offer = Offer::where("meta_data","LIKE","%".$details."%")->get();
            if (count($offer) == 0) {
                return $this->sendError('Reseach not found', 404);
            }
        return $this->sendResponse($offer->toArray(), 'Object find successfully');
            
    }

    /**
     * @param string $search
     * @return Response
     *
     * @SWG\Get(
     *      path="/search_offers/{search}",
     *      summary="Search a listing of the offer.",
     *      tags={"Offer"},
     *      description="Get Offers",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="search",
     *          in="path",
     *          description="Offer that should be search",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Offer")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function search($search)
    {
        // '%'.$search.'%'
        $research = Offer::where('type','LIKE','%'.$search.'%')->
        orWhere('title','LIKE','%'.$search.'%')->
        orWhere('location_name','LIKE','%'.$search.'%')->get();
        if(count($research) == 0)
        {
            return $this->sendError('Nothing for this research', 404);
        }

        foreach ($research as $e) {
            $son = Offer::where('parent_source_id',$e->id)->get();
            if(is_null($son))
            {
                return $this->sendResponse($research->toArray(),'Search results');
            }
            $data = [
                'parent' => $research,
                'son' => $son
            ];
                return $this->sendResponse($data,'Search results');
        }
    }

        /**
     * @param string $search
     * 
     * @return Response
     *
     * @SWG\Get(
     *      path="/search_offers_by_geofence",
     *      summary="Search a listing of the offer.",
     *      tags={"Offer"},
     *      description="Get Offers",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="lat",
     *          in="path",
     *          description="latitude",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          name="long",
     *          in="path",
     *          description="longitude",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Offer")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function searchByGeofence(Request $request)
    {
        if(!$request->has('location_latitude')){
            return $this->sendError('Latitude not found');
        }

        if(!$request->has('location_longitude')){
            return $this->sendError('Longitude not found');
        }

        $offer_type = "restaurant";
        $offer_type1 = "hotel";
        $offer_type2 = "residence";
        $offer_type3 = "car";
        $offer_type4 = "musee";

        $latitude = $request->location_latitude;
        $longitude = $request->location_longitude;

        $inner_radius = 0;

        $outer_radius = 500;

        $query = Offer::geofence($latitude, $longitude, $inner_radius, $outer_radius);

        $all = $query->
        where(['type' => $offer_type])
        ->orWhere(['type' => $offer_type1])
        ->orWhere(['type' => $offer_type2])
        ->orWhere(['type' => $offer_type3])
        ->orWhere(['type' => $offer_type4])
        ->get();
        
        return $this->sendResponse($all,'Search results');
        
    
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/offers/{id}",
     *      summary="Display the specified Offer",
     *      tags={"Offer"},
     *      description="Get Offer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Offer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Offer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Offer $offer */
        $offer = $this->offerRepository->find($id);

        if (empty($offer)) {
            return $this->sendError('Offer not found');
        }

        return $this->sendResponse($offer->toArray(), 'Offer retrieved successfully');
    }


    /**
     * 
     * @return Response
     *
     * @SWG\Get(
     *      path="/excursion",
     *      summary="Display Top Excursion",
     *      tags={"Offer"},
     *      description="Get Excursion",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Offer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function excursion()
    {
        $excursion = Offer::where('type','Excursion')->orderBy('created_at', 'desc')->take(5)->get();
        if(count($excursion) == 0)
        {
            return $this->sendError('Excursion not found',404);
        }

    return $this->sendResponse($excursion->toArray(),'five last excursion');
    }

    /**
     * @param int $id
     * @param UpdateOfferAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/offers/{id}",
     *      summary="Update the specified Offer in storage",
     *      tags={"Offer"},
     *      description="Update Offer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Offer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Offer that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Offer")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Offer"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOfferAPIRequest $request)
    {
        $input = $request->all();

        /** @var Offer $offer */
        $offer = $this->offerRepository->find($id);

        if (empty($offer)) {
            return $this->sendError('Offer not found');
        }

        $offer = $this->offerRepository->update($input, $id);

        return $this->sendResponse($offer->toArray(), 'Offer updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/offers/{id}",
     *      summary="Remove the specified Offer from storage",
     *      tags={"Offer"},
     *      description="Delete Offer",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Offer",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Offer $offer */
        $offer = $this->offerRepository->find($id);

        if (empty($offer)) {
            return $this->sendError('Offer not found');
        }

        $offer->delete();

        return $this->sendSuccess('Offer deleted successfully');
    }
}
