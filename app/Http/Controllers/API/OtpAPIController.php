<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOtpAPIRequest;
use App\Http\Requests\API\UpdateOtpAPIRequest;
use App\Models\Otp;
use App\Repositories\OtpRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class OtpController
 * @package App\Http\Controllers\API
 */

class OtpAPIController extends AppBaseController
{
    /** @var  OtpRepository */
    private $otpRepository;

    public function __construct(OtpRepository $otpRepo)
    {
        $this->otpRepository = $otpRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/otps",
     *      summary="Get a listing of the Otps.",
     *      tags={"Otp"},
     *      description="Get all Otps",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Otp")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $otps = $this->otpRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($otps->toArray(), 'Otps retrieved successfully');
    }

    /**
     * @param CreateOtpAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/otps",
     *      summary="Store a newly created Otp in storage",
     *      tags={"Otp"},
     *      description="Store Otp",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Otp that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Otp")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Otp"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateOtpAPIRequest $request)
    {
        $input = $request->all();

        $otp = $this->otpRepository->create($input);

        return $this->sendResponse($otp->toArray(), 'Otp saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/otps/{id}",
     *      summary="Display the specified Otp",
     *      tags={"Otp"},
     *      description="Get Otp",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Otp",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Otp"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Otp $otp */
        $otp = $this->otpRepository->find($id);

        if (empty($otp)) {
            return $this->sendError('Otp not found');
        }

        return $this->sendResponse($otp->toArray(), 'Otp retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOtpAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/otps/{id}",
     *      summary="Update the specified Otp in storage",
     *      tags={"Otp"},
     *      description="Update Otp",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Otp",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Otp that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Otp")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Otp"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOtpAPIRequest $request)
    {
        $input = $request->all();

        /** @var Otp $otp */
        $otp = $this->otpRepository->find($id);

        if (empty($otp)) {
            return $this->sendError('Otp not found');
        }

        $otp = $this->otpRepository->update($input, $id);

        return $this->sendResponse($otp->toArray(), 'Otp updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/otps/{id}",
     *      summary="Remove the specified Otp from storage",
     *      tags={"Otp"},
     *      description="Delete Otp",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Otp",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Otp $otp */
        $otp = $this->otpRepository->find($id);

        if (empty($otp)) {
            return $this->sendError('Otp not found');
        }

        $otp->delete();

        return $this->sendSuccess('Otp deleted successfully');
    }
}
