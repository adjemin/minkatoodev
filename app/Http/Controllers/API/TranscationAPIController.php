<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTranscationAPIRequest;
use App\Http\Requests\API\UpdateTranscationAPIRequest;
use App\Models\Transcation;
use App\Repositories\TranscationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TranscationController
 * @package App\Http\Controllers\API
 */

class TranscationAPIController extends AppBaseController
{
    /** @var  TranscationRepository */
    private $transcationRepository;

    public function __construct(TranscationRepository $transcationRepo)
    {
        $this->transcationRepository = $transcationRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/transcations",
     *      summary="Get a listing of the Transcations.",
     *      tags={"Transcation"},
     *      description="Get all Transcations",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Transcation")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $transcations = $this->transcationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($transcations->toArray(), 'Transcations retrieved successfully');
    }

    /**
     * @param CreateTranscationAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/transcations",
     *      summary="Store a newly created Transcation in storage",
     *      tags={"Transcation"},
     *      description="Store Transcation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Transcation that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Transcation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Transcation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTranscationAPIRequest $request)
    {
        $input = $request->all();

        $transcation = $this->transcationRepository->create($input);

        return $this->sendResponse($transcation->toArray(), 'Transcation saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/transcations/{id}",
     *      summary="Display the specified Transcation",
     *      tags={"Transcation"},
     *      description="Get Transcation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Transcation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Transcation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Transcation $transcation */
        $transcation = $this->transcationRepository->find($id);

        if (empty($transcation)) {
            return $this->sendError('Transcation not found');
        }

        return $this->sendResponse($transcation->toArray(), 'Transcation retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTranscationAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/transcations/{id}",
     *      summary="Update the specified Transcation in storage",
     *      tags={"Transcation"},
     *      description="Update Transcation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Transcation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Transcation that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Transcation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Transcation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTranscationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Transcation $transcation */
        $transcation = $this->transcationRepository->find($id);

        if (empty($transcation)) {
            return $this->sendError('Transcation not found');
        }

        $transcation = $this->transcationRepository->update($input, $id);

        return $this->sendResponse($transcation->toArray(), 'Transcation updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/transcations/{id}",
     *      summary="Remove the specified Transcation from storage",
     *      tags={"Transcation"},
     *      description="Delete Transcation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Transcation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Transcation $transcation */
        $transcation = $this->transcationRepository->find($id);

        if (empty($transcation)) {
            return $this->sendError('Transcation not found');
        }

        $transcation->delete();

        return $this->sendSuccess('Transcation deleted successfully');
    }
}
