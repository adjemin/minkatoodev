<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerRequestAPIRequest;
use App\Http\Requests\API\UpdateCustomerRequestAPIRequest;
use App\Models\CustomerRequest;
use App\Repositories\CustomerRequestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CustomerRequestController
 * @package App\Http\Controllers\API
 */

class CustomerRequestAPIController extends AppBaseController
{
    /** @var  CustomerRequestRepository */
    private $customerRequestRepository;

    public function __construct(CustomerRequestRepository $customerRequestRepo)
    {
        $this->customerRequestRepository = $customerRequestRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/customerRequests",
     *      summary="Get a listing of the CustomerRequests.",
     *      tags={"CustomerRequest"},
     *      description="Get all CustomerRequests",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CustomerRequest")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $customerRequests = $this->customerRequestRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customerRequests->toArray(), 'Customer Requests retrieved successfully');
    }

    /**
     * @param CreateCustomerRequestAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/customerRequests",
     *      summary="Store a newly created CustomerRequest in storage",
     *      tags={"CustomerRequest"},
     *      description="Store CustomerRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomerRequest that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomerRequest")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerRequest"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCustomerRequestAPIRequest $request)
    {
        $input = $request->all();

        $customerRequest = $this->customerRequestRepository->create($input);

        return $this->sendResponse($customerRequest->toArray(), 'Customer Request saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/customerRequests/{id}",
     *      summary="Display the specified CustomerRequest",
     *      tags={"CustomerRequest"},
     *      description="Get CustomerRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerRequest",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerRequest"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CustomerRequest $customerRequest */
        $customerRequest = $this->customerRequestRepository->find($id);

        if (empty($customerRequest)) {
            return $this->sendError('Customer Request not found');
        }

        return $this->sendResponse($customerRequest->toArray(), 'Customer Request retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCustomerRequestAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/customerRequests/{id}",
     *      summary="Update the specified CustomerRequest in storage",
     *      tags={"CustomerRequest"},
     *      description="Update CustomerRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerRequest",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomerRequest that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomerRequest")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerRequest"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCustomerRequestAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomerRequest $customerRequest */
        $customerRequest = $this->customerRequestRepository->find($id);

        if (empty($customerRequest)) {
            return $this->sendError('Customer Request not found');
        }

        $customerRequest = $this->customerRequestRepository->update($input, $id);

        return $this->sendResponse($customerRequest->toArray(), 'CustomerRequest updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/customerRequests/{id}",
     *      summary="Remove the specified CustomerRequest from storage",
     *      tags={"CustomerRequest"},
     *      description="Delete CustomerRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerRequest",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CustomerRequest $customerRequest */
        $customerRequest = $this->customerRequestRepository->find($id);

        if (empty($customerRequest)) {
            return $this->sendError('Customer Request not found');
        }

        $customerRequest->delete();

        return $this->sendSuccess('Customer Request deleted successfully');
    }
}
