<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrderAPIRequest;
use App\Http\Requests\API\UpdateOrderAPIRequest;
use App\Models\Order;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\InvoicePayment;
use App\Models\Offer;
use App\Models\OrderItem;
use Illuminate\Support\Collection;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class OrderController
 * @package App\Http\Controllers\API
 */

class OrderAPIController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepo)
    {
        $this->orderRepository = $orderRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/orders",
     *      summary="Get a listing of the Orders.",
     *      tags={"Order"},
     *      description="Get all Orders",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Order")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $orders = $this->orderRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }

    /**
     * @param CreateOrderAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/orders",
     *      summary="Store a newly created Order in storage",
     *      tags={"Order"},
     *      description="Store Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Order that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Order")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Order"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(Request $request)
    {
        try{

        /** @var ParameterBag $jsonFields */
            $jsonFields = $request->json();
            //

            $customer = Customer::where('id', $jsonFields->getInt('customer_id'))->first();
            //die($customer);
            $items = (array) $jsonFields->get('items');

            $currencyCode = 'XOF';

            $order = Order::create([
            'customer_id' => $customer->id,
            'current_status' => 'waiting',
            'currency_code' => $currencyCode
        ]);


            foreach ($items as $item){

                $item = (array)$item;

                /** @var Offer $offer */
                $offer= Offer::where('id', $item['offer_id'])->first();

                
                //$currencyCode = $offer->currency_code;
                $totalAmount = ((int)$offer->price) * ((int)$item['quantity']);

                $subtotal = $totalAmount;

                //die($subtotal);

                    $orderitem = OrderItem::create([
            'order_id' => $order->id,
            'meta_data_id' => $offer->id,
            'meta_data' => json_encode($offer),
            'quantity' => $item['quantity'],
            'unit_price' => $offer->price,
            'currency_code' => $currencyCode,
            ]);
            }

            $tax = $subtotal * Invoice::TAXES;

            $total = $subtotal + $tax;

            $order->amount = $total;
            $order->save();

            if($offer->type != 'Voiture')
            {
            $order->payment_method_slug = 'cp';
            $order->save();
            }

            $invoice = Invoice::create([
            'order_id' => $order->id,
            'customer_id' => $jsonFields->getInt('customer_id'),
            'reference' => Invoice::generateID('BOOKING', $order->id, $jsonFields->getInt('customer_id')),
            'link' => '#',
            'subtotal' => $subtotal,
            'tax' => $tax,
            'total' => $total,
            'status' => 'unpaid',
            'currency_code' => $currencyCode

            ]);
            if($offer->is_ecort == 1)
            {
            $invoice->fees_delivery == $offer->escort_fees;
            $invoice->save();
            }

            //TODO Notify user
            $transactionId = self::generateTransId($customer->id, $invoice->id, $order->id);

            $transaction = InvoicePayment::create([
                'invoice_id' =>  $invoice->id,
                'payment_method'=> $order->payment_method_slug,
                'payment_reference' => $transactionId,
                'amount' => $invoice->total,
                'creator_id' => $customer->id,
                'creator' => 'customer',
                'creator_name' => $customer->name

            ]);


            return $this->sendResponse([
                'order' => $order,
                'transaction' => [
                    'payment_method'=> $transaction->payment_method,
                    'transaction_id' => $transactionId,
                    'transaction_designation' => 'BOOKING',
                    'amount' => $transaction->amount,
                    'currency_code' => $transaction->currency_code,
                    'status' => $transaction->status
                ]
            ], 'Order saved successfully');

        }catch (\Exception $exception){
            return $this->sendError($exception->getMessage());
        }

    }


    /**
     * generate transId
     * @return int
     */
    public static function generateTransId($customerId, $invoiceId, $orderId)
    {
        $timestamp = time();
        $parts = explode(' ', microtime());
        $id = ($timestamp + $parts[0] - strtotime('today 00:00')) * 10;
        $id = sprintf('%06d', $id) . mt_rand(100, 9999);
        $transId = $id."-".$customerId."-".$invoiceId."-".$orderId;

        $transactions = InvoicePayment::where('payment_reference', $transId)->get();
        if( count($transactions) == 0){

            return  $transId;
        }else{
            $autoIncrement = count($transactions) + 1;
            $transId = $transId.'-'.$autoIncrement;
            return  $transId;
        }


    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/orders/{id}",
     *      summary="Display the specified Order",
     *      tags={"Order"},
     *      description="Get Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Order",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Order"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        return $this->sendResponse($order->toArray(), 'Order retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOrderAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/orders/{id}",
     *      summary="Update the specified Order in storage",
     *      tags={"Order"},
     *      description="Update Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Order",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Order that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Order")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Order"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOrderAPIRequest $request)
    {
        $input = $request->all();

        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        $order = $this->orderRepository->update($input, $id);

        return $this->sendResponse($order->toArray(), 'Order updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/orders/{id}",
     *      summary="Remove the specified Order from storage",
     *      tags={"Order"},
     *      description="Delete Order",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Order",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Order $order */
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        $order->delete();

        return $this->sendSuccess('Order deleted successfully');
    }
}
