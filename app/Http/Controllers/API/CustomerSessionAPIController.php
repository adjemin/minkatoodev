<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCustomerSessionAPIRequest;
use App\Http\Requests\API\UpdateCustomerSessionAPIRequest;
use App\Models\CustomerSession;
use App\Repositories\CustomerSessionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CustomerSessionController
 * @package App\Http\Controllers\API
 */

class CustomerSessionAPIController extends AppBaseController
{
    /** @var  CustomerSessionRepository */
    private $customerSessionRepository;

    public function __construct(CustomerSessionRepository $customerSessionRepo)
    {
        $this->customerSessionRepository = $customerSessionRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/customerSessions",
     *      summary="Get a listing of the CustomerSessions.",
     *      tags={"CustomerSession"},
     *      description="Get all CustomerSessions",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CustomerSession")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $customerSessions = $this->customerSessionRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($customerSessions->toArray(), 'Customer Sessions retrieved successfully');
    }

    /**
     * @param CreateCustomerSessionAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/customerSessions",
     *      summary="Store a newly created CustomerSession in storage",
     *      tags={"CustomerSession"},
     *      description="Store CustomerSession",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomerSession that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomerSession")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerSession"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCustomerSessionAPIRequest $request)
    {
        $input = $request->all();

        $customerSession = $this->customerSessionRepository->create($input);

        return $this->sendResponse($customerSession->toArray(), 'Customer Session saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/customerSessions/{id}",
     *      summary="Display the specified CustomerSession",
     *      tags={"CustomerSession"},
     *      description="Get CustomerSession",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerSession",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerSession"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CustomerSession $customerSession */
        $customerSession = $this->customerSessionRepository->find($id);

        if (empty($customerSession)) {
            return $this->sendError('Customer Session not found');
        }

        return $this->sendResponse($customerSession->toArray(), 'Customer Session retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCustomerSessionAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/customerSessions/{id}",
     *      summary="Update the specified CustomerSession in storage",
     *      tags={"CustomerSession"},
     *      description="Update CustomerSession",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerSession",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomerSession that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomerSession")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomerSession"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCustomerSessionAPIRequest $request)
    {
        $input = $request->all();

        /** @var CustomerSession $customerSession */
        $customerSession = $this->customerSessionRepository->find($id);

        if (empty($customerSession)) {
            return $this->sendError('Customer Session not found');
        }

        $customerSession = $this->customerSessionRepository->update($input, $id);

        return $this->sendResponse($customerSession->toArray(), 'CustomerSession updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/customerSessions/{id}",
     *      summary="Remove the specified CustomerSession from storage",
     *      tags={"CustomerSession"},
     *      description="Delete CustomerSession",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomerSession",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CustomerSession $customerSession */
        $customerSession = $this->customerSessionRepository->find($id);

        if (empty($customerSession)) {
            return $this->sendError('Customer Session not found');
        }

        $customerSession->delete();

        return $this->sendSuccess('Customer Session deleted successfully');
    }
}
