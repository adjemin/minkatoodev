<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\Http\Controllers\Controller;
use App\Models\Offer;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return 'ok';
        $hotels = Offer::where('type', '=', 'hotel')->take(8)->get();

        $residence = Offer::where('type', '=', 'residence')->take(8)->get();
        // dd($hotels[0]->slide_url);
        return view("front.hotel.index", compact("hotels", "residence"));
    }


    public function indexAll(){

        $hotels = Offer::where('type', '=', 'hotel')->get();
        return view("front.hotel.hotelAll", compact("hotels"));
    }


    public function indexAlls()
    {
        $residence = Offer::where('type', '=', 'residence')->get();
        return view("front.hotel.residence.residence_all", compact("residence"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return "ok";
        $hotel= Offer::find($id);

        // $chambres = Offer::where('parent_source_id', '=', $hotel->id )->get();


        $chambres = Offer::where('type','=','chambre')
                            ->where('parent_source_id', '=', $hotel->id)
                            ->get();

        return view("front.hotel.show", compact('hotel', 'chambres'));
    }


    public function showes($id)
    {
        $residence = Offer::find($id);

        return view("front.hotel.residence.residence_show", compact('residence'));
    }

    public function ShowBedroom($id)
    {
        $chambre = Offer::find($id);
        return view("front.hotel.bedrooms_show", compact('chambre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $hotel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        //
    }


    public function requestFormulaireBedroom($id)
    {
        $chambre = Offer::find($id);

        return view("front.hotel.booking_bedrom", compact('chambre'));
    }


    //Residence
    // public function requestFormulaireBedroom($id)
    // {
    //     $chambre = Offer::find($id);

    //     return view("front.hotel.booking_bedrom", compact('chambre'));
    // }

}
