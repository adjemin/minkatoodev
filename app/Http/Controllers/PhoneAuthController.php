<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PhoneAuthController extends Controller
{
    public function getOtpView(){
        return view('firebase');
        // return view('firebase_copy');
    }

    public function getRegisterView(){
        return view('firebase_register');
        // return view('firebase_copy');
    }
}
