<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Offer;
use App\Models\Customer;
use App\Models\InvoicePayment;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offer = count(Offer::get());
        $customer = count(Customer::get());

        $payment = count(InvoicePayment::where('status','SUCCESSFUL')->get());

        $total = InvoicePayment::where('status','SUCCESSFUL')->sum('payment_gateway_amount');

        return view('home',compact('offer', 'customer','payment','total'));
    }

    public function otpVerification()
    {
        return view('firbase');
    }
}
