<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Hash;
use Flash;
use App\Models\User;
use Response;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->userRepository->all();
        if(auth()->user()){
            return view('users.index')
            ->with('users', $users);
        } else {
            return redirect(route("login"));
        }
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

        return redirect(route("login"));

    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

        $input = $request->all();

        if($request->password == null)
        {
            Flash::error('Mot de passe obligatoire');
            return redirect()->back();
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        if( $user->save())
        {
            Flash::success('User saved successfully.');
        return redirect(route('users.index'));
        }
        else
        {
            Flash::error("User don\'t saved.");
            return redirect()->back();        }
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

        $user = $this->userRepository->find($id);
           // recupere toutes les saisies
            $input = $request->all();
          // criptage du password s'il a ete saisi.
            if(!empty($input['password']))
            {
            $input['password'] = Hash::make($input['password']);
            $user->update($input);
            }
            else{
            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();
            }
            //Mise a jour des entrés saisies
            Flash::success('User updated successfully.');
            return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        } 

        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Flash::success('User deleted successfully.');

        return redirect(route('users.index'));
    }
}