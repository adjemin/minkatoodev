<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInvoicePaymentRequest;
use App\Http\Requests\UpdateInvoicePaymentRequest;
use App\Repositories\InvoicePaymentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class InvoicePaymentController extends AppBaseController
{
    /** @var  InvoicePaymentRepository */
    private $invoicePaymentRepository;

    public function __construct(InvoicePaymentRepository $invoicePaymentRepo)
    {
        $this->invoicePaymentRepository = $invoicePaymentRepo;
    }

    /**
     * Display a listing of the InvoicePayment.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $invoicePayments = $this->invoicePaymentRepository->all();

        return view('invoice_payments.index')
            ->with('invoicePayments', $invoicePayments);
    }

    /**
     * Show the form for creating a new InvoicePayment.
     *
     * @return Response
     */
    public function create()
    {
        return view('invoice_payments.create');
    }

    /**
     * Store a newly created InvoicePayment in storage.
     *
     * @param CreateInvoicePaymentRequest $request
     *
     * @return Response
     */
    public function store(CreateInvoicePaymentRequest $request)
    {
        $input = $request->all();

        $invoicePayment = $this->invoicePaymentRepository->create($input);

        Flash::success('Invoice Payment saved successfully.');

        return redirect(route('invoicePayments.index'));
    }

    /**
     * Display the specified InvoicePayment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $invoicePayment = $this->invoicePaymentRepository->find($id);

        if (empty($invoicePayment)) {
            Flash::error('Invoice Payment not found');

            return redirect(route('invoicePayments.index'));
        }

        return view('invoice_payments.show')->with('invoicePayment', $invoicePayment);
    }

    /**
     * Show the form for editing the specified InvoicePayment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $invoicePayment = $this->invoicePaymentRepository->find($id);

        if (empty($invoicePayment)) {
            Flash::error('Invoice Payment not found');

            return redirect(route('invoicePayments.index'));
        }

        return view('invoice_payments.edit')->with('invoicePayment', $invoicePayment);
    }

    /**
     * Update the specified InvoicePayment in storage.
     *
     * @param int $id
     * @param UpdateInvoicePaymentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInvoicePaymentRequest $request)
    {
        $invoicePayment = $this->invoicePaymentRepository->find($id);

        if (empty($invoicePayment)) {
            Flash::error('Invoice Payment not found');

            return redirect(route('invoicePayments.index'));
        }

        $invoicePayment = $this->invoicePaymentRepository->update($request->all(), $id);

        Flash::success('Invoice Payment updated successfully.');

        return redirect(route('invoicePayments.index'));
    }

    /**
     * Remove the specified InvoicePayment from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $invoicePayment = $this->invoicePaymentRepository->find($id);

        if (empty($invoicePayment)) {
            Flash::error('Invoice Payment not found');

            return redirect(route('invoicePayments.index'));
        }

        $this->invoicePaymentRepository->delete($id);

        Flash::success('Invoice Payment deleted successfully.');

        return redirect(route('invoicePayments.index'));
    }
}
