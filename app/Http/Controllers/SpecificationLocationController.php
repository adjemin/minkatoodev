<?php

namespace App\Http\Controllers;

use App\SpecificationLocation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SpecificationLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SpecificationLocation  $specificationLocation
     * @return \Illuminate\Http\Response
     */
    public function show(SpecificationLocation $specificationLocation)
    {
        //
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SpecificationLocation  $specificationLocation
     * @return \Illuminate\Http\Response
     */
    public function edit(SpecificationLocation $specificationLocation)
    {
        //
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SpecificationLocation  $specificationLocation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SpecificationLocation $specificationLocation)
    {
        //
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SpecificationLocation  $specificationLocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpecificationLocation $specificationLocation)
    {
        //
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

    }
}