<?php

namespace App\Http\Controllers;

use App\Restaurant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Offer;

class RestaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurants = Offer::where('type', '=', 'restaurant')->take(4)->get();

        return view("front.restaurant.index", compact("restaurants"));
    }

    public function indexAll()
    {
        $restaurants = Offer::where('type', '=', 'restaurant')->get();
        return view("front.restaurant.restaurant_all", compact("restaurants"));
    }


    public function restaurantBySpecification(Request $request, $speciality)
    {

        // dd( $speciality);
        $restaurants = Offer::where('type', '=', 'restaurant')->get();
        
        $query = collect($restaurants)->filter(
            function($restaurant) use ($speciality) {

            $specialties = json_decode($restaurant->meta_data)->specialties;
            
            // dd($specialities);
            foreach ($specialties as $item){
                if($item->slug == $speciality){
                    return $restaurant;
                }
            }
        });

        // return response()->json(['data'=>$query]);
        // return response()->json(['data']);
        switch ($speciality) {
            case "cuisine_asiatique":
                return view("front.restaurant.restaurant_spec.asian_cook", compact("query"));
                break;
            case "cusisine_africaine":
                return view("front.restaurant.restaurant_spec.cook_local", compact("query"));
                break;
            case "cuisine-orientale":
                return view("front.restaurant.restaurant_spec.refined_cook", compact("query"));
                break;
            case "cuisine-francaise":
                return view("front.restaurant.restaurant_spec.european_cook", compact("query"));
                break;
        }
        // return view("front.restaurant.restaurant_spec.asian_cook", compact("query"));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // if(auth()->user()){
        // } else {
        //     return redirect(route("login"));
        // }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // if(auth()->user()){
        // } else {
        //     return redirect(route("login"));
        // }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $restaurant = Offer::find($id);
        //
        return view("front.restaurant.show", compact("restaurant"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function edit(Restaurant $restaurant)
    {
        // if(auth()->user()){
        // } else {
        //     return redirect(route("login"));
        // }

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Restaurant $restaurant)
    {
        // if(auth()->user()){
        // } else {
        //     return redirect(route("login"));
        // }

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Restaurant $restaurant)
    {
        // if(auth()->user()){
        // } else {
        //     return redirect(route("login"));
        // }

        //
    }
}
