<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFavoriRequest;
use App\Http\Requests\UpdateFavoriRequest;
use App\Repositories\FavoriRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class FavoriController extends AppBaseController
{
    /** @var  FavoriRepository */
    private $favoriRepository;

    public function __construct(FavoriRepository $favoriRepo)
    {
        $this->favoriRepository = $favoriRepo;
    }

    /**
     * Display a listing of the Favori.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $favoris = $this->favoriRepository->all();

        return view('favoris.index')
            ->with('favoris', $favoris);
    }

    /**
     * Show the form for creating a new Favori.
     *
     * @return Response
     */
    public function create()
    {
        return view('favoris.create');
    }

    /**
     * Store a newly created Favori in storage.
     *
     * @param CreateFavoriRequest $request
     *
     * @return Response
     */
    public function store(CreateFavoriRequest $request)
    {
        $input = $request->all();

        $favori = $this->favoriRepository->create($input);

        Flash::success('Favori saved successfully.');

        return redirect(route('favoris.index'));
    }

    /**
     * Display the specified Favori.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $favori = $this->favoriRepository->find($id);

        if (empty($favori)) {
            Flash::error('Favori not found');

            return redirect(route('favoris.index'));
        }

        return view('favoris.show')->with('favori', $favori);
    }

    /**
     * Show the form for editing the specified Favori.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $favori = $this->favoriRepository->find($id);

        if (empty($favori)) {
            Flash::error('Favori not found');

            return redirect(route('favoris.index'));
        }

        return view('favoris.edit')->with('favori', $favori);
    }

    /**
     * Update the specified Favori in storage.
     *
     * @param int $id
     * @param UpdateFavoriRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFavoriRequest $request)
    {
        $favori = $this->favoriRepository->find($id);

        if (empty($favori)) {
            Flash::error('Favori not found');

            return redirect(route('favoris.index'));
        }

        $favori = $this->favoriRepository->update($request->all(), $id);

        Flash::success('Favori updated successfully.');

        return redirect(route('favoris.index'));
    }

    /**
     * Remove the specified Favori from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $favori = $this->favoriRepository->find($id);

        if (empty($favori)) {
            Flash::error('Favori not found');

            return redirect(route('favoris.index'));
        }

        $this->favoriRepository->delete($id);

        Flash::success('Favori deleted successfully.');

        return redirect(route('favoris.index'));
    }
}
