<?php

namespace App\Http\Controllers;

use App\Models\Musee;
use App\Http\Requests\CreateMuseeRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Offer;
use Flash;
use Response;

class MuseeController extends AppBaseController
{

    /**
     * Display a listing of the Musee.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $musees = $this->museeRepository->all();

        // return view('musees.index')
        //     ->with('musees', $musees);
        $musee = Offer::where('type', '=', 'musee')->get();
        // dd('musee');
        // $locations = Location::all();
        return view("front.musee.index", compact("musee"));
    }

    /**
     * Show the form for creating a new Musee.
     *
     * @return Response
     */
    public function create()
    {
        // return view('musees.create');
    }

    /**
     * Store a newly created Musee in storage.
     *
     * @param CreateMuseeRequest $request
     *
     * @return Response
     */
    public function store(CreateMuseeRequest $request)
    {
        // $input = $request->all();

        // $musee = $this->museeRepository->create($input);

        // Flash::success('Musee saved successfully.');

        // return redirect(route('musees.index'));
    }

    /**
     * Display the specified Musee.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        // $musee = $this->museeRepository->find($id);

        // if (empty($musee)) {
        //     Flash::error('Musee not found');

        //     return redirect(route('musees.index'));
        // }

        // return view('musees.show')->with('musee', $musee);
        $musee = Offer::find($id);
        // dd($musee);
        return view("front.musee.show", compact('musee'));
    }

    /**
     * Show the form for editing the specified Musee.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // $musee = $this->museeRepository->find($id);

        // if (empty($musee)) {
        //     Flash::error('Musee not found');

        //     return redirect(route('musees.index'));
        // }

        // return view('musees.edit')->with('musee', $musee);
    }

    /**
     * Update the specified Musee in storage.
     *
     * @param int $id
     * @param UpdateMuseeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMuseeRequest $request)
    {
        // $musee = $this->museeRepository->find($id);

        // if (empty($musee)) {
        //     Flash::error('Musee not found');

        //     return redirect(route('musees.index'));
        // }

        // $musee = $this->museeRepository->update($request->all(), $id);

        // Flash::success('Musee updated successfully.');

        // return redirect(route('musees.index'));
    }

    /**
     * Remove the specified Musee from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        // $musee = $this->museeRepository->find($id);

        // if (empty($musee)) {
        //     Flash::error('Musee not found');

        //     return redirect(route('musees.index'));
        // }

        // $this->museeRepository->delete($id);

        // Flash::success('Musee deleted successfully.');

        // return redirect(route('musees.index'));
    }
}
