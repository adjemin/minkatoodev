<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOwnerSourceRequest;
use App\Http\Requests\UpdateOwnerSourceRequest;
use App\Repositories\OwnerSourceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OwnerSourceController extends AppBaseController
{
    /** @var  OwnerSourceRepository */
    private $ownerSourceRepository;

    public function __construct(OwnerSourceRepository $ownerSourceRepo)
    {
        $this->ownerSourceRepository = $ownerSourceRepo;
    }

    /**
     * Display a listing of the OwnerSource.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

        $ownerSources = $this->ownerSourceRepository->all();

        return view('owner_sources.index')
            ->with('ownerSources', $ownerSources);
    }

    /**
     * Show the form for creating a new OwnerSource.
     *
     * @return Response
     */
    public function create()
    {
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

        return view('owner_sources.create');
    }

    /**
     * Store a newly created OwnerSource in storage.
     *
     * @param CreateOwnerSourceRequest $request
     *
     * @return Response
     */
    public function store(CreateOwnerSourceRequest $request)
    {
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

        $input = $request->all();

        $ownerSource = $this->ownerSourceRepository->create($input);

        Flash::success('Owner Source saved successfully.');

        return redirect(route('ownerSources.index'));
    }

    /**
     * Display the specified OwnerSource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

        $ownerSource = $this->ownerSourceRepository->find($id);

        if (empty($ownerSource)) {
            Flash::error('Owner Source not found');

            return redirect(route('ownerSources.index'));
        }

        return view('owner_sources.show')->with('ownerSource', $ownerSource);
    }

    /**
     * Show the form for editing the specified OwnerSource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

        $ownerSource = $this->ownerSourceRepository->find($id);

        if (empty($ownerSource)) {
            Flash::error('Owner Source not found');

            return redirect(route('ownerSources.index'));
        }

        return view('owner_sources.edit')->with('ownerSource', $ownerSource);
    }

    /**
     * Update the specified OwnerSource in storage.
     *
     * @param int $id
     * @param UpdateOwnerSourceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOwnerSourceRequest $request)
    {
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

        $ownerSource = $this->ownerSourceRepository->find($id);

        if (empty($ownerSource)) {
            Flash::error('Owner Source not found');

            return redirect(route('ownerSources.index'));
        }

        $ownerSource = $this->ownerSourceRepository->update($request->all(), $id);

        Flash::success('Owner Source updated successfully.');

        return redirect(route('ownerSources.index'));
    }

    /**
     * Remove the specified OwnerSource from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        if(auth()->user()){
        } else {
            return redirect(route("login"));
        }

        $ownerSource = $this->ownerSourceRepository->find($id);

        if (empty($ownerSource)) {
            Flash::error('Owner Source not found');

            return redirect(route('ownerSources.index'));
        }

        $this->ownerSourceRepository->delete($id);

        Flash::success('Owner Source deleted successfully.');

        return redirect(route('ownerSources.index'));
    }
}