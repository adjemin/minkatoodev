<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOtpRequest;
use App\Http\Requests\UpdateOtpRequest;
use App\Repositories\OtpRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OtpController extends AppBaseController
{
    /** @var  OtpRepository */
    private $otpRepository;

    public function __construct(OtpRepository $otpRepo)
    {
        $this->otpRepository = $otpRepo;
    }

    /**
     * Display a listing of the Otp.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $otps = $this->otpRepository->all();

        return view('otps.index')
            ->with('otps', $otps);
    }

    /**
     * Show the form for creating a new Otp.
     *
     * @return Response
     */
    public function create()
    {
        return view('otps.create');
    }

    /**
     * Store a newly created Otp in storage.
     *
     * @param CreateOtpRequest $request
     *
     * @return Response
     */
    public function store(CreateOtpRequest $request)
    {
        $input = $request->all();

        $otp = $this->otpRepository->create($input);

        Flash::success('Otp saved successfully.');

        return redirect(route('otps.index'));
    }

    /**
     * Display the specified Otp.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $otp = $this->otpRepository->find($id);

        if (empty($otp)) {
            Flash::error('Otp not found');

            return redirect(route('otps.index'));
        }

        return view('otps.show')->with('otp', $otp);
    }

    /**
     * Show the form for editing the specified Otp.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $otp = $this->otpRepository->find($id);

        if (empty($otp)) {
            Flash::error('Otp not found');

            return redirect(route('otps.index'));
        }

        return view('otps.edit')->with('otp', $otp);
    }

    /**
     * Update the specified Otp in storage.
     *
     * @param int $id
     * @param UpdateOtpRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOtpRequest $request)
    {
        $otp = $this->otpRepository->find($id);

        if (empty($otp)) {
            Flash::error('Otp not found');

            return redirect(route('otps.index'));
        }

        $otp = $this->otpRepository->update($request->all(), $id);

        Flash::success('Otp updated successfully.');

        return redirect(route('otps.index'));
    }

    /**
     * Remove the specified Otp from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $otp = $this->otpRepository->find($id);

        if (empty($otp)) {
            Flash::error('Otp not found');

            return redirect(route('otps.index'));
        }

        $this->otpRepository->delete($id);

        Flash::success('Otp deleted successfully.');

        return redirect(route('otps.index'));
    }
}
