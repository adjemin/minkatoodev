<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMetaDataServiceRequest;
use App\Http\Requests\UpdateMetaDataServiceRequest;
use App\Repositories\MetaDataServiceRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class MetaDataServiceController extends AppBaseController
{
    /** @var  MetaDataServiceRepository */
    private $metaDataServiceRepository;

    public function __construct(MetaDataServiceRepository $metaDataServiceRepo)
    {
        $this->metaDataServiceRepository = $metaDataServiceRepo;
    }

    /**
     * Display a listing of the MetaDataService.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $metaDataServices = $this->metaDataServiceRepository->all();

        return view('meta_data_services.index')
            ->with('metaDataServices', $metaDataServices);
    }

    /**
     * Show the form for creating a new MetaDataService.
     *
     * @return Response
     */
    public function create()
    {
        return view('meta_data_services.create');
    }

    /**
     * Store a newly created MetaDataService in storage.
     *
     * @param CreateMetaDataServiceRequest $request
     *
     * @return Response
     */
    public function store(CreateMetaDataServiceRequest $request)
    {
        $input = $request->all();

        $metaDataService = $this->metaDataServiceRepository->create($input);

        Flash::success('Meta Data Service saved successfully.');

        return redirect(route('metaDataServices.index'));
    }

    /**
     * Display the specified MetaDataService.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $metaDataService = $this->metaDataServiceRepository->find($id);

        if (empty($metaDataService)) {
            Flash::error('Meta Data Service not found');

            return redirect(route('metaDataServices.index'));
        }

        return view('meta_data_services.show')->with('metaDataService', $metaDataService);
    }

    /**
     * Show the form for editing the specified MetaDataService.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $metaDataService = $this->metaDataServiceRepository->find($id);

        if (empty($metaDataService)) {
            Flash::error('Meta Data Service not found');

            return redirect(route('metaDataServices.index'));
        }

        return view('meta_data_services.edit')->with('metaDataService', $metaDataService);
    }

    /**
     * Update the specified MetaDataService in storage.
     *
     * @param int $id
     * @param UpdateMetaDataServiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMetaDataServiceRequest $request)
    {
        $metaDataService = $this->metaDataServiceRepository->find($id);

        if (empty($metaDataService)) {
            Flash::error('Meta Data Service not found');

            return redirect(route('metaDataServices.index'));
        }

        $metaDataService = $this->metaDataServiceRepository->update($request->all(), $id);

        Flash::success('Meta Data Service updated successfully.');

        return redirect(route('metaDataServices.index'));
    }

    /**
     * Remove the specified MetaDataService from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $metaDataService = $this->metaDataServiceRepository->find($id);

        if (empty($metaDataService)) {
            Flash::error('Meta Data Service not found');

            return redirect(route('metaDataServices.index'));
        }

        $this->metaDataServiceRepository->delete($id);

        Flash::success('Meta Data Service deleted successfully.');

        return redirect(route('metaDataServices.index'));
    }
}
