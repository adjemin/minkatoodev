<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Repositories\CustomerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Order;
use App\Utils\OTP;
use Carbon\Carbon;
use Flash;
use Illuminate\Support\Facades\Auth;
use Response;
use Imgur;
class CustomerController extends AppBaseController
{

    /** @var  CustomerRepository */
    private $customerRepository;


    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepository = $customerRepo;
    }

    /**
     * Display a listing of the Customer.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $customers = $this->customerRepository->all();

        return view('customers.index')
            ->with('customers', $customers);
    }

    /**
     * Show the form for creating a new Customer.
     *
     * @return Response
     */
    public function create()
    {
        $genders = ['HOMME' => 'HOMME', 'FEMME' => 'FEMME'];
        $type = ['client ' => 'client', 'proprietaire' => 'proprietaire'];
        return view('customers.create')
        ->with('genders',$genders)
        ->with('type', $type);
    }

    /**
     * Store a newly created Customer in storage.
     *
     * @param CreateCustomerRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerRequest $request)
    {
        // $input = $request->all();
        // $customer = $this->customerRepository->create($input);
        // Flash::success('Customer saved successfully.');
        // return redirect(route('customers.index'));
        $input = $request->all();

        $input["phone_number"] = trim($request->phone);
        $input["phone"] = $request->dial_code."".trim($request->phone);

        $input["first_name"] = ucfirst(strtolower($request->first_name));

        $input["last_name"] = ucfirst(strtolower($request->last_name));

        $input["name"] = ucfirst(strtolower($request->first_name)).' '.ucfirst(strtolower($request->last_name));

        $items = $this->customerRepository->all(["phone" => $input["phone"]]);

        $input["otp"] = OTP::generate_reference(Customer::all(),'',6);

        if(count($items) == 0){
            $people = $this->customerRepository->create($input);

            if($request->photo_url != null)
            {
                $photo=$request->file('photo_url');
                $image = Imgur::upload($photo);
                $people->photo = $image->link();
                $people->save();
            }
            if($request->customer_type == null)
            {
                $people->customer_type = 'client';
                $people->save();
            }

            Flash::success('Client enregistré avec succès!');
            return redirect(route('customers.index'));

        }else{
            Flash::error('Téléphone déjà utilisé');
            return redirect(route('customers.create'));
        }

    }

    /**
     * Display the specified Customer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        return view('customers.show')->with('customer', $customer);
    }

    /**
     * Show the form for editing the specified Customer.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customer = $this->customerRepository->find($id);
        $genders = ['HOMME' => 'HOMME', 'FEMME' => 'FEMME'];
        $type = ['client ' => 'client', 'proprietaire' => 'proprietaire'];

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        return view('customers.edit')
        ->with('genders', $genders)
        ->with('customer', $customer)
        ->with('type', $type);
    }

    public function profil($id){
        $customer = $this->customerRepository->find($id);

        return view("front.customer.customerProfil", compact('customer'));
    }

    public function editProfil($id){
        $customer = $this->customerRepository->find($id);

        return view("front.customer.editProfil", compact('customer'));
    }

    public function updateProfile(Request $request, $id){
        // $customer = Customer::find($id);


         $customer = Customer::find($id);


         if ($request->photo_url != null) {
            // return "ok";
            $photo_url = $request->file('photo_url')->store('images', ['disk' => 'public']);
             $customer->photo_url = $photo_url;
            // return $photo_url;
        }


        //  return $request->all();
         $customer->last_name = $request->last_name;
         $customer->first_name = $request->first_name;
         $customer->email= $request->email;
         $customer->phone= $request->phone;

        // return $customer;

        $customer->save();
        // return back();
        return redirect()->route('customerProfile',[$customer->id] );

    }


    public function allCustomerOrders($id){
        $customer = Customer::find($id);

        $orders = Order::where('customer_id', $customer->id)->latest()->get();
        return view('front.customer.customer_request.customer_request', compact('orders', $customer));
    }


    /**
     * Update the specified Customer in storage.
     *
     * @param int $id
     * @param UpdateCustomerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerRequest $request)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        $customer = $this->customerRepository->update($request->all(), $id);

        Flash::success('Customer updated successfully.');

        return redirect(route('customers.index'));
    }

    /**
     * Remove the specified Customer from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        $this->customerRepository->delete($id);

        Flash::success('Customer deleted successfully.');

        return redirect(route('customers.index'));
    }


    public function checkIfUserExist(Request $request){
        $customer = Customer::where('phone', $request->number)->first();
        // return response()->json(["customer"=>$customer, "phone"=> $request->number]);
        if(!empty($customer)){
            return response()->json(["user_data"=>$customer, "status"=> 200]);
        }else{
            return response()->json(["user_data"=> null, "status"=> 404]);
        }
    }

    public function logUser(Request $request){

        // return response()->json($request->all());
        $check = Auth::guard('customer')->loginUsingId($request->id);

        // return response()->json([$check]);

        return response()->json(["status"=> 200]);
    }

}
