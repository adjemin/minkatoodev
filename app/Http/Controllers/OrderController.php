<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Customer;
use App\Repositories\OrderRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Order;
use App\Models\CustomerNotification;
use App\Models\Invoice;
use App\Models\InvoicePayment;
use App\Models\Offer;
use App\Models\OrderItem;
use App\Utils\CustomerNotificationUtils;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Response;
use PDF;

class OrderController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;

    public function __construct(OrderRepository $orderRepo)
    {
        $this->orderRepository = $orderRepo;
    }

    /**
     * Display a listing of the Order.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
//        $orders = $this->orderRepository->all();
        $orders = Order::orderByDesc('id')->paginate(20);

        return view('orders.index')
            ->with('orders', $orders);
    }

    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create()
    {
        return view('orders.create');
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderRequest $request)
    {
        $input = $request->all();

        $order = $this->orderRepository->create($input);

        Flash::success('Order saved successfully.');

        return redirect(route('orders.index'));
    }

    /**
     * Display the specified Order.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        return view('orders.show')->with('order', $order);
    }

    /**
     * Show the form for editing the specified Order.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        return view('orders.edit')->with('order', $order);
    }

    /**
     * Update the specified Order in storage.
     *
     * @param int $id
     * @param UpdateOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderRequest $request)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $order = $this->orderRepository->update($request->all(), $id);

        Flash::success('Order updated successfully.');

        return redirect(route('orders.index'));
    }

    /**
     * Remove the specified Order from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     *
     */
    public function destroy($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $this->orderRepository->delete($id);

        Flash::success('Order deleted successfully.');

        return redirect(route('orders.index'));
    }

    public function makeOrderReady($id)
    {

        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Commande introuvable');

            //return redirect(route('orders.index'))->with('error', 'Commande introuvable');
            return redirect(route('orders.index'));
        }
        $order->current_status = "managed";
        $order->is_waiting = 1;
        $order->is_delivered = 0;
        $order->save();



        /** @var CustomerNotifications */
        $customer = $order->getCustomer();
//        dd($customer->name);

        $customerNotification = new CustomerNotification();
        $customerNotification->title  = $customer->name.", votre commande est traitée.";
        $customerNotification->subtitle  = "Commande #".$order->id." est en cours de traitemnetnt par notre équipe.";
        $customerNotification->action  = "";
        $customerNotification->action_by  = "";
        $customerNotification->meta_data  = $order->toJson();
        $customerNotification->type_notification  = CustomerNotificationUtils::ORDER_MANAGER;
        $customerNotification->is_read  = false;
        $customerNotification->is_received  = false;
        $customerNotification->data  = $order->toJson();
        $customerNotification->user_id  = $customer->id;
        $customerNotification->data_id  = $order->id;
        $customerNotification->save();

        CustomerNotificationUtils::notify($customerNotification);

        Flash::success('La commande est en cours de traitement.');
        return redirect(route('orders.show', [$order->id]));

    }

    public function makeOrderReadyForDelivery($id)
    {

        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Commande introuvable');

//            return redirect(route('orders.index'))->with('error', 'Commande introuvable');
            return redirect(route('orders.index'));
        }
        $order->is_waiting = 0;
        $order->current_status = "ready-for-delivery";
        $order->is_delivered = 1;
        $order->delivery_date = now();
        $order->save();

        /** @var CustomerNotifications */
        $customer = $order->getCustomer();
//        dd($customer->name);

        $customerNotification = new CustomerNotification();
        $customerNotification->title  = $customer->name.", votre commande est prête pour la livraison.";
        $customerNotification->subtitle  = "Commande #".$order->id." a été traitée par notre équipe.";
        $customerNotification->action  = "";
        $customerNotification->action_by  = "";
        $customerNotification->meta_data  = $order->toJson();
        $customerNotification->type_notification  = CustomerNotificationUtils::ORDER_MANAGER;
        $customerNotification->is_read  = false;
        $customerNotification->is_received  = false;
        $customerNotification->data  = $order->toJson();
        $customerNotification->user_id  = $customer->id;
        $customerNotification->data_id  = $order->id;
        $customerNotification->save();

        CustomerNotificationUtils::notify($customerNotification);

        Flash::success('La commande est en cours de livraison.');
        return redirect(route('orders.show', [$order->id]));
    }

    public function makeOrderReadyCancelled($id)
    {

        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Commande introuvable');

//            return redirect(route('orders.index'))->with('error', 'Commande introuvable');
            return redirect(route('orders.index'));
        }

        $order->is_waiting = 0;
        $order->current_status = "cancel";
        $order->is_delivered = 0;
        $order->delivery_date = null;
        $order->save();



        /** @var CustomerNotifications */
        $customer = $order->getCustomer();
//        dd($customer->name);

        $customerNotification = new CustomerNotification();
        $customerNotification->title  = $customer->name.", votre commande a été annulée.";
        $customerNotification->subtitle  = "Commande #".$order->id." a été annulée par notre équipe.";
        $customerNotification->action  = "";
        $customerNotification->action_by  = "";
        $customerNotification->meta_data  = $order->toJson();
        $customerNotification->type_notification  = CustomerNotificationUtils::ORDER_MANAGER;
        $customerNotification->is_read  = false;
        $customerNotification->is_received  = false;
        $customerNotification->data  = $order->toJson();
        $customerNotification->user_id  = $customer->id;
        $customerNotification->data_id  = $order->id;
        $customerNotification->save();

        CustomerNotificationUtils::notify($customerNotification);
        Flash::success('La commande a été annulée.');
        return redirect(route('orders.show', [$order->id]));

    }

    public function invoicePDF($id) {
        $order = $this->orderRepository->find($id);

        $pdf = PDF::loadView('orders.invoicepdf', compact('order', $order));

        return $pdf->stream('invoice.pdf');
    }

    public function storeAjax(Request $request){
        // return 'ok';
        // $jsonFields = $request->json();

        $customer = Auth::guard('customer')->user();
        // return $customer->id;

        $form_data = $request->all();

        // return $form_data;

        $delivery_data = $form_data['delivery_data']['delivery_date'];
        // return $delivery_data;

        // *** Creating order
        // $customer = Customer::where('id', $customer->id)->first();

        $order = new Order();
        $order->customer_id = $customer->id;
        $order->is_waiting = true;
        $order->current_status = 'waiting';
        $currencyCode='XOF';
        $order->payment_method_slug = 'online-payment';
        // $order->delivery_date = Carbon::parse($deliveries);
        $order->delivery_date = Carbon::parse($delivery_data);
        // return $order->delivery_date;
        $order->currency_code = $currencyCode;
        $order->save();

        // return $order;
        $offer = Offer::where('id', $form_data['resume']['bedroom_id'])->first();
        // return $offer;

        $order = Order::where('id', $order->id)->first();
        // return $order;

        $orderItems = new OrderItem();

        $orderItems->order_id = $order->id;
        $orderItems->meta_data_id = $offer->id;
        $orderItems->meta_data = json_encode($offer);
        $orderItems->unit_price = $offer->price;
        $orderItems->quantity_unit = $offer->price;
        $orderItems->currency_code = $currencyCode;
        $orderItems->quantity = $form_data['resume']['date'];
        $orderItems->total_amount = $orderItems->quantity_unit * $orderItems->quantity;
        $orderItems->start_date =  $form_data['delivery_data']['delivery_date'];
        $orderItems->end_date =   $form_data['delivery_data']['delivery_date_bed'];
        $orderItems->save();

        // $orderItems->quantity = $form_data['services']['0']['offer']['0']['quantite'];
        // $orderItems->total_amount = $offer->price * $form_data['services']['0']['offer']['0']['quantite'];
        // return $orderItems->total_amount =  Carbon::parse($form_data['resume']['delivery_date']);
        // $orderItems->unit_price =  $form_data['resume']['bedroom_price'] ;
        // return "bien effectué";

        $fees_delivery = $order->delivery_fees;
        $tax = 0;
        $subtotal = 0;
        $total = 0;

        $orderItems = OrderItem::where(["order_id" => $order->id])->get();

        if($orderItems != null && count($orderItems) > 0){

            for($j = 0; $j < count($orderItems); $j++){
                $subtotal += (int)$orderItems[$j]->total_amount;
            }

            $tax = $subtotal * 0.18;
            $total = $subtotal + $tax + $fees_delivery;
        }

        $reference = Invoice::generateID('BOOKING', $order->id, $customer->id);
        // Invoice
        $invoice = new Invoice();
        $invoice->order_id = $order->id;
        $invoice->customer_id = $customer->id;
        $invoice->reference = $reference;
        $invoice->link = "#";
        $invoice->fees_delivery = $fees_delivery;
        $invoice->status = "unpaid";
        $invoice->is_paid_by_customer = false;
        $invoice->currency_code = $currencyCode;
        $invoice->tax = $tax;
        $invoice->subtotal = $subtotal;
        $invoice->total = $total;
        $invoice->save();


        if ($order->payment_method_slug == "online-payment") {
            $customer = Auth::guard('customer')->user();
            //TODO Notify user
            $transactionId = self::generateTransId($customer->id, $invoice->id, $order->id);

            $transaction = InvoicePayment::create([
                'invoice_id' =>  $invoice->id,
                'payment_method'=> $order->payment_method_slug,
                'payment_reference' => $transactionId,
                'amount' => $invoice->total,
                'creator_id' => $customer->id,
                'creator' => 'customer',
                'creator_name' => $customer->name
            ]);

            // return 'tout va bien';


            /* return $this->sendResponse([
                'order' => $order,
                'transaction' => [
                    'payment_method'=> $transaction->payment_method,
                    'transaction_id' => $transactionId,
                    'transaction_designation' => 'BOOKING',
                    'amount' => $transaction->amount,
                    'currency_code' => $transaction->currency_code,
                    'status' => $transaction->status
                ]
            ], 'Order saved successfully');
            $data = [
            "customer" => $customer,
            "order" => $order,
            "oderItems" => $orderItems,
            "invoice" => $invoice,
            "transaction" => $transaction
            ]; */

            $payment_method = $transaction->payment_method;
            $transaction_designation = $offer->title;
            $amount = $transaction->amount;
            $currency_code = $transaction->currency_code;
            $status = $transaction->status;
            $customer = $customer->id;
            $notifications = "Commande #" .$order->id;
            // dd($transaction_designation);

            // $returnHTML = view("cinetpay.index", ['payment_method' => $payment_method, 'transactionId' => $transactionId, 'transaction_designation' => $transaction_designation, 'amount' => $amount, 'status' => $status, 'customer' => $customer, 'notifications' => $notifications])->render();
            $data = [
                'payment_method' => $payment_method,
                'transactionId' => $transactionId,
                'transaction_designation' => $transaction_designation,
                'amount' => $amount,
                'status' => $status,
                'customer' => $customer,
                'notifications' => $notifications,
                'order'=> $order
            ];
            // return response()->json( array('success' => true, 'html'=>$returnHTML) );
            // return response()->json(
            //     [
            //         'code' => 11,
            //         // TODO crypter transaction->id
            //         'data' => $transaction->id,
            //     ]
            // );

            return response()->json([
                'code' => 11,
                'message' => "Bien éffectué",
                // 'data' => $request->all(),
                "data"=> $transaction->id
            ]);

            // return redirect()->route('cinetpay.index',compact('payment_method', 'transactionId', 'transaction_designation', 'amount', 'status', 'customer', 'notifications'));

        }

        // return view('cinetpay.index',  compact('payment_method', 'transactionId', 'transaction_designation', 'amount', 'status', 'customer', 'notifications'));
    }




    public static function generateTransId($customerId, $invoiceId, $orderId)
    {
        $timestamp = time();
        $parts = explode(' ', microtime());
        $id = ($timestamp + $parts[0] - strtotime('today 00:00')) * 10;
        $id = sprintf('%06d', $id) . mt_rand(100, 9999);
        $transId = $id."-".$customerId."-".$invoiceId."-".$orderId;
        $transactions = InvoicePayment::where('payment_reference', $transId)->get();
        if( count($transactions) == 0){
            return  $transId;
        }else{
            $autoIncrement = count($transactions) + 1;
            $transId = $transId.'-'.$autoIncrement;
            return  $transId;
        }
    }



}
