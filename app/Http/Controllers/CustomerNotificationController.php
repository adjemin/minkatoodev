<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerNotificationsRequest;
use App\Http\Requests\UpdateCustomerNotificationsRequest;
use App\Repositories\CustomerNotificationsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CustomerNotificationsController extends AppBaseController
{
    /** @var  CustomerNotificationsRepository */
    private $customerNotificationsRepository;

    public function __construct(CustomerNotificationsRepository $customerNotificationsRepo)
    {
        $this->customerNotificationsRepository = $customerNotificationsRepo;
    }

    /**
     * Display a listing of the CustomerNotifications.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $customerNotifications = $this->customerNotificationsRepository->all();

        return view('customer_notifications.index')
            ->with('customerNotifications', $customerNotifications);
    }

    /**
     * Show the form for creating a new CustomerNotifications.
     *
     * @return Response
     */
    public function create()
    {
        return view('customer_notifications.create');
    }

    /**
     * Store a newly created CustomerNotifications in storage.
     *
     * @param CreateCustomerNotificationsRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerNotificationsRequest $request)
    {
        $input = $request->all();

        $customerNotifications = $this->customerNotificationsRepository->create($input);

        Flash::success('Customer Notifications saved successfully.');

        return redirect(route('customerNotifications.index'));
    }

    /**
     * Display the specified CustomerNotifications.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customerNotifications = $this->customerNotificationsRepository->find($id);

        if (empty($customerNotifications)) {
            Flash::error('Customer Notifications not found');

            return redirect(route('customerNotifications.index'));
        }

        return view('customer_notifications.show')->with('customerNotifications', $customerNotifications);
    }

    /**
     * Show the form for editing the specified CustomerNotifications.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customerNotifications = $this->customerNotificationsRepository->find($id);

        if (empty($customerNotifications)) {
            Flash::error('Customer Notifications not found');

            return redirect(route('customerNotifications.index'));
        }

        return view('customer_notifications.edit')->with('customerNotifications', $customerNotifications);
    }

    /**
     * Update the specified CustomerNotifications in storage.
     *
     * @param int $id
     * @param UpdateCustomerNotificationsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerNotificationsRequest $request)
    {
        $customerNotifications = $this->customerNotificationsRepository->find($id);

        if (empty($customerNotifications)) {
            Flash::error('Customer Notifications not found');

            return redirect(route('customerNotifications.index'));
        }

        $customerNotifications = $this->customerNotificationsRepository->update($request->all(), $id);

        Flash::success('Customer Notifications updated successfully.');

        return redirect(route('customerNotifications.index'));
    }

    /**
     * Remove the specified CustomerNotifications from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customerNotifications = $this->customerNotificationsRepository->find($id);

        if (empty($customerNotifications)) {
            Flash::error('Customer Notifications not found');

            return redirect(route('customerNotifications.index'));
        }

        $this->customerNotificationsRepository->delete($id);

        Flash::success('Customer Notifications deleted successfully.');

        return redirect(route('customerNotifications.index'));
    }
}
