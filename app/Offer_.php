<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    //

    protected $guarded = [];

    //
    public function specifications()
    {
        return $this->hasMany(SpecificationHotel::class);
    }
}