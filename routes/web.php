<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Customer;
use App\Models\InvoicePayment;
use App\Models\Offer;

Route::get('/', function () {
    $hotels = Offer::where('type', '=', 'hotel')->take(4)->get();
    $restaurants = Offer::where('type', '=', 'restaurant')->take(4)->get();


    $excursion = Offer::where('type', '=', 'excursion')->take(4)->get();
    return view('front.index', compact('hotels', 'restaurants','excursion'));
    });

Route::get('firebase_otp_verification', [HomeController::class, 'otpVerification']);

// Route::view("/", "front.index")->name('front.end.home');
// Route::get("/", "HomeController@index")->name('front.index');

Route::get('test', function(){
    return view('test');
});

// Make order ready
Route::get('/order-make-ready/{id}', 'OrderController@makeOrderReady')->name('order-make-ready');
Route::get('/order-make-ready-for-delivery/{id}', 'OrderController@makeOrderReadyForDelivery')->name('order-make-ready-for-delivery');
Route::get('/order-make-ready-for-cancelled/{id}', 'OrderController@makeOrderReadyCancelled')->name('order-make-ready-for-cancelled');
Route::get('/invoicepdf/{id}', 'OrderController@invoicePDF')->name('invoicepdf');

// Route excursion and excursionBooking
Route::get("/excursion", "ExcursionActivityController@indexFront")->name('excursion');
Route::get("/excursion-detail/{id}", "ExcursionActivityController@showFront")->name('excursion-detail');
Route::get("/formulaire-de-Reservation/{id}", "ExcursionActivityController@requestFormex")->name('request-formulaire')->middleware('customer.auth');



Route::get('checkIfUserExist','CustomerController@checkIfUserExist')->name('checkIfUserExist');
Route::get('loginUsingId','CustomerController@logUser')->name('loginUsingId');

//Customer
Route::get('/customerProfil/{id}', 'CustomerController@profil')->name('customerProfile');
Route::get('/editProfil/{id}', 'CustomerController@editProfil')->name('editProfil');
Route::put('/updateProfile/{id}', 'CustomerController@updateProfile')->name('updateProfile');

Route::get('/customerRequest/{id}', 'CustomerController@allCustomerOrders')->name('customerRequest');

// Route musee
Route::get("/musees", "MuseeController@index")->name('musee');
Route::get("/musee-detail/{id}", "MuseeController@show")->name('musee-detail');

// Route restaurant
Route::get("/restaurant-detail/{id}", "RestaurantController@show")->name('restaurant-detail');
Route::get("/view/restaurants", "RestaurantController@index");
Route::get("/view/Allrestaurants", "RestaurantController@indexAll")->name('All-restaurant');

// Route hotel, residence, HotelBooking and ResidenceBooking
Route::get("/view/hotels", "HotelController@index");
Route::get("/hotel-detail/{id}", "HotelController@show")->name('hotel-detail');
Route::get("/residence-detail/{id}", "HotelController@showes")->name('residence-detail');
// Route::get("/formulaire_Reservation/{id}", "HotelController@requestFormulaire")->name('request_formular')->middleware('customer.auth');


Route::get("/view/Allhotel", "HotelController@indexAll")->name('All-Hotel');
Route::get("/view/AllResidences", "HotelController@indexAlls")->name('All-Residences');

// Route location and locationBooking
Route::get("/location", "LocationController@index");
Route::get("/location-detail/{id}", "LocationController@show")->name('location-detail');
// Route::get("/formulaire_de_Reservation/{id}", "LocationController@requestForm")->name('request_form');
Route::get("/formulaire_de_Reservation/{id}", "LocationController@requestForm")->name('request_form')->middleware('customer.auth');

// Bedroom
Route::get("/bedroom-detail/{id}", "HotelController@ShowBedroom")->name('bedroom-detail');

// Request Formulaire
Route::get("/formulaireReservation/{id}", "HotelController@requestFormulaireBedroom")->name('request_formulaires')->middleware('customer.auth');

Route::get("/formulaireReservationResidence/{id}", "HotelController@requestFormulaireBedroom")->name('request_formular')->middleware('customer.auth');
// ->middleware('customer.auth');

Route::get('/storeAdjeminPay', "AdjeminPayController@storeAdjeminPay")->name('storeAdjeminPay');


// Route::view("/hotel-detail", "front.hotel_detail");
// Route::view("/restaurant-detail", "front.restaurant_detail");
// Route::view("/view/restaurants", "front.restaurants");
// Route::view("/view/hotels", "front.hotels");
// Route::view("/location", "front.location");

Route::view("/_", "_");    // Test view

Auth::routes(['verify' => true]);

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->middleware('verified');
//
Route::resource('hotels', 'HotelController');
Route::resource('locations', 'LocationController');
Route::resource('restaurants', 'RestaurantController');

Route::name ('offer.')->middleware ('ajax')->group (function () {
    Route::prefix('offer')->group(function () {
    });
    Route::name ('rating')->put ('rating/{offer}', 'OfferController@rate');
});


Route::get('/restaurant_by_specification/{speciality}', 'RestaurantController@restaurantBySpecification');

Route::get('/excursion_by_specification/{speciality}', 'ExcursionActivityController@excursionBySpecification');


Route::resource('customers', 'CustomerController');

Route::resource('invoices', 'InvoiceController');

Route::resource('invoicePayments', 'InvoicePaymentController');

Route::resource('metaDataServices', 'MetaDataServiceController');

Route::resource('offers', 'OfferController');

Route::resource('orderItems', 'OrderItemController');

Route::resource('ownerSources', 'OwnerSourceController');

Route::resource('receipts', 'ReceiptController');

Route::resource('users', 'UserController');

Route::resource('orders', 'OrderController');

Route::resource('customerSessions', 'CustomerSessionController');

Route::resource('favoris', 'FavoriController');

Route::resource('foodSpeialties', 'FoodSpeialtiesController');

Route::resource('excursionActivities', 'ExcursionActivityController');

// Customer Routes
Route::post('/orders/storeAjax','OrderController@insertOrderData');

Route::resource('customerDevices', 'CustomerDeviceController');
Route::resource('hotelSpecifications', 'HotelSpecificationController');

Route::resource('otps', 'OtpController');


// Route::post('/orders/storeAjax', "OrderController@storeAjax")->name('orders.storeAjax');
Route::post('/orders/storeAjax', "OrderController@storeAjax")->name('orders.storeAjax');

Route::get('adjeminPay/{id}', function($id){
    // dump($id);
    $transaction = InvoicePayment::find($id);
    $payment_method = $transaction->payment_method;
    $transactionId = $transaction->payment_reference;
    $transaction_designation = $transaction->payment_gateway_designation;
    $amount = $transaction->amount;
    $status = $transaction->status;
    $customer = $transaction->creator_name;
    $customer_id = $transaction->creator_id;
    $notification = $transaction->invoice_id;
    $currency = $transaction->currency_code;
    // dd($transaction);

    if($transaction !== null){
        // return 'ok';
        return view('adjeminpay.index', compact('payment_method', 'transactionId', 'transaction_designation', 'amount','currency', 'status', 'customer', 'notification','customer_id', 'transaction'));
        // return view("cinetpay.index");
    }

});

Route::get('notify/{id}', 'AdjeminPayController@changeStatus');


// Route::get('cinetPay/{id}', function($id){
//     dd($id);

//     $transaction = InvoicePayment::find($id);
//     $payment_method = $transaction->payment_method;
//     $transactionId = $transaction->payment_reference;
//     $transaction_designation = $transaction->payment_gateway_designation;
//     $amount = $transaction->amount;
//     $status = $transaction->status;
//     $customer = $transaction->creator_name;
//     $customer_id = $transaction->creator_id;
//     $notification = $transaction->invoice_id;

//     if($transaction !== null){
//         // return 'ok';
//         // return view("cinetpay.index");
//         return view('cinetpay.index', compact('payment_method', 'transactionId', 'transaction_designation', 'amount', 'status', 'customer', 'notification','customer_id'));
//     }
// });


Route::resource('transcations', 'TranscationController');
