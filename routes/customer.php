<?php

Route::group(['namespace' => 'Customer'], function() {

    Route::get('/', 'HomeController@index')->name('customer.dashboard');
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('customer.login');

    // Login
    Route::get('loginNew', 'Auth\LoginController@loginNew')->name('customer.loginNew');
    Route::post('/login_verification_code', 'Auth\LoginController@loginWithOtp')->name('customer.loginWithOtp');
    Route::post('login', 'Auth\LoginController@login')->name('customer.login.submit');
    Route::get('logout', 'Auth\LoginController@logout')->name('customer.logout');

    // Register
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('customer.register');
    Route::get('/verification_code', 'Auth\RegisterController@showVerificationCodeForm')->name('customer.verification_code');
    Route::post('register', 'Auth\RegisterController@create')->name('customer.register.submit');

    Route::get('login', [App\Http\Controllers\PhoneAuthController::class, 'getOtpView'])->name('customer.login');

    Route::get('register', [App\Http\Controllers\PhoneAuthController::class, 'getRegisterView'])->name('customer.register');

    // Passwords
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('customer.password.email');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('customer.password.request');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('customer.password.reset');

    // Verify
    Route::post('email/resend', 'Auth\VerificationController@resend')->name('customer.verification.resend');
    Route::get('email/verify', 'Auth\VerificationController@show')->name('customer.verification.notice');
    Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('customer.verification.verify');

    Route::get('/my_requests', 'BackOfficeController@myRequests')->name('customer.requests');
    Route::get('/request/{id}', 'BackOfficeController@show')->name('customer.request.show');

    Route::get('/my_profile/{id}', 'BackOfficeController@profile')->name('customer.profile');
    Route::get('/edit_my_profile/{id}', 'BackOfficeController@editProfile')->name('customer.profile.edit');
    Route::post('/update_my_profile/{id}', 'BackOfficeController@updateProfile')->name('customer.profile.update');

    Route::post('get_otp_by_phone', 'Auth\RegisterController@getOTPByPhone')->name('get_otp_by_phone')/* ->middleware('customer') */;
    Route::post('verify_otp_by_phone', 'Auth\RegisterController@verifyOTPByPhone')->name('verify_otp_by_phone')/* ->middleware('customer') */;


});
