<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API RoutesP
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('customers', 'CustomerAPIController');
Route::post('customers/search/phoneNumber', 'CustomerAPIController@findByPhone');

Route::post('customers/login', 'CustomerAPIController@login');

Route::resource('invoices', 'InvoiceAPIController');

Route::resource('invoice_payments', 'InvoicePaymentAPIController');

Route::post('invoice_payments/notify', 'InvoicePaymentAPIController@notify')->name('payments.notify');
Route::post('invoice_payments/cancel', 'InvoicePaymentAPIController@cancel');

Route::resource('meta_data_services', 'MetaDataServiceAPIController');

Route::resource('offers', 'OfferAPIController');

Route::get('search_offers/{search}', 'OfferAPIController@search');
Route::get('search_offers_by_geofence', 'OfferAPIController@searchByGeofence');

Route::get('offers_by_details/{details}', 'OfferAPIController@details');

Route::get('excursion_activities','ExcursionActivityAPIController@index');

Route::get('food_specialties','FoodSpeialtiesAPIController@index');

Route::get('offers_by_price/{price}/{currency}', 'OfferAPIController@price');

Route::get('excursion','OfferAPIController@excursion');

Route::resource('order_items', 'OrderItemAPIController');

Route::resource('owner_sources', 'OwnerSourceAPIController');

Route::resource('receipts', 'ReceiptAPIController');

Route::resource('users', 'UserAPIController');

Route::resource('orders', 'OrderAPIController');

Route::resource('customer_sessions', 'CustomerSessionAPIController');

Route::resource('favoris', 'FavoriAPIController');

Route::resource('musees', 'MuseeAPIController');

Route::resource('otps', 'OtpAPIController');

Route::resource('customer_requests', 'CustomerRequestAPIController');


Route::resource('transcations', 'TranscationAPIController');
