<?php
// return [

//     /**
//      * Set Imgur application client_id and secret key.
//      */

//     'client_id' => env('IMGUR_CLIENT_ID'),

//     'client_secret' => env('IMGUR_CLIENT_SECRET', null),
// ];


// trying to solve curl error 3
$endpoint = env('IMGUR_ENDPOINT', 'https://api.imgur.com/3/upload');
$id = env('IMGUR_CLIENT_ID');
$secret = env('IMGUR_CLIENT_SECRET', null);

return [

    /**
     * Set Imgur application client_id and secret key, endpoint.
     */

    'client_id' => $id,

    'client_secret' => $secret,

    'endpoint' => $endpoint,
];